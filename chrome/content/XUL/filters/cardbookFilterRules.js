if ("undefined" == typeof(cardbookFilterRules)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
	var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");
	var { cardbookXULCardEmails } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULCardEmails.js");

    var loader = Services.scriptloader;
    loader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js", this);
    loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);
    
	var cardbookFilterRules = {
		
        _isOutgoingMail: function(aMsgHdr) {
            if (!aMsgHdr) {
                return false;
            }
            let author = aMsgHdr.mime2DecodedAuthor;
            if (author) {
                let accounts = MailServices.accounts;
                for (let identity of accounts.allIdentities) {
                    if (author.includes(identity.email)) {
                        return true;
                    }
                }
            }
            return false;
        },

        _isLocalSearch: function(aSearchScope) {
			switch (aSearchScope) {
				case Components.interfaces.nsMsgSearchScope.offlineMail:
				case Components.interfaces.nsMsgSearchScope.offlineMailFilter:
				case Components.interfaces.nsMsgSearchScope.onlineMailFilter:
				case Components.interfaces.nsMsgSearchScope.localNews:
				case Components.interfaces.nsMsgSearchScope.newsFilter:
					return true;
				default:
					return false;
				}
		},

		_addEmails: async function(aMsgHdrs, aActionValue, aField) {
            let emails = [];
            for (let msgHdr of aMsgHdrs) {
                let addresses = MailServices.headerParser.parseEncodedHeaderW(msgHdr[aField]);
                for (let address of addresses) {
                    emails.push({name: address.name, email: address.email});
                }
            }
            await notifyTools.notifyBackground({query: "cardbook.filters.addCards", actionValue: aActionValue, emails: emails});
		},

		_removeEmails: async function(aMsgHdrs, aActionValue, aField) {
            let emails = [];
            for (let msgHdr of aMsgHdrs) {
                let addresses = MailServices.headerParser.parseEncodedHeaderW(msgHdr[aField]);
                for (let address of addresses) {
                    emails.push({name: address.name, email: address.email});
                }
            }
            await notifyTools.notifyBackground({query: "cardbook.filters.deleteCards", actionValue: aActionValue, emails: emails});
		},

		_searchEmails: function(aSearchValue, aEmail) {
			if (aSearchValue && aSearchValue != "allAddressBooks") {
				return cardbookXULCardEmails.isEmailInPrefIdRegistered(aSearchValue, aEmail);
			} else {
				return cardbookXULCardEmails.isEmailRegistered(aEmail);
			}
		},

		_matchEmails: function(aMsgHdrEmails, aSearchValue, aSearchOp) {
			let matches = false;
			let addresses = MailServices.headerParser.parseEncodedHeaderW(aMsgHdrEmails);
			let i = 0;
			for (let address of addresses) {
				switch (aSearchOp) {
					case Components.interfaces.nsMsgSearchOp.IsInAB:
					case Components.interfaces.nsMsgSearchOp.IsntInAB:
						if (i === 0) {
							if (cardbookFilterRules._searchEmails(aSearchValue, address.email)) {
								matches = true;
							} else {
								matches = false;
							}
						} else {
							if (cardbookFilterRules._searchEmails(aSearchValue, address.email)) {
								matches = (matches && true);
							} else {
								matches = (matches && false);
							}
						}
						break;
					default:
						Components.utils.reportError("invalid search operator : " + aSearchOp);
				}
				i++
			}
			if (aSearchOp == Components.interfaces.nsMsgSearchOp.IsntInAB) {
				return !matches;
			} else {
				return matches;
			}
		},

		load: async function () {
            let filtersInitialized = await notifyTools.notifyBackground({query: "cardbook.filters.filtersInitialized"});
			if (filtersInitialized == true) {
				return;
			}
			var searchFrom = {
				id: "cardbook#searchFrom",
				name: cardbookXULUtils.localizeMessage("cardbook.searchFrom.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					return cardbookFilterRules._matchEmails(aMsgHdr.author, aSearchValue, aSearchOp);
				}
			};
			MailServices.filters.addCustomTerm(searchFrom);

			var searchTo = {
				id: "cardbook#searchTo",
				name: cardbookXULUtils.localizeMessage("cardbook.searchTo.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					return cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp);
				}
			};
			MailServices.filters.addCustomTerm(searchTo);

			var searchCc = {
				id: "cardbook#searchCc",
				name: cardbookXULUtils.localizeMessage("cardbook.searchCc.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					return cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp);
				}
			};
			MailServices.filters.addCustomTerm(searchCc);

			var searchBcc = {
				id: "cardbook#searchBcc",
				name: cardbookXULUtils.localizeMessage("cardbook.searchBcc.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					return cardbookFilterRules._matchEmails(aMsgHdr.bccList, aSearchValue, aSearchOp);
				}
			};
			MailServices.filters.addCustomTerm(searchBcc);

			var searchToOrCc = {
				id: "cardbook#searchToOrCc",
				name: cardbookXULUtils.localizeMessage("cardbook.searchToOrCc.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				// true && false => false
				// true || false => true
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					if (aSearchOp == Components.interfaces.nsMsgSearchOp.IsntInAB) {
						return (cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) &&
								cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp));
					} else {
						return (cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) ||
								cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp));
					}
				}
			};
			MailServices.filters.addCustomTerm(searchToOrCc);

			var searchAll = {
				id: "cardbook#searchAll",
				name: cardbookXULUtils.localizeMessage("cardbook.searchAll.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				// true && false => false
				// true || false => true
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					if (aSearchOp == Components.interfaces.nsMsgSearchOp.IsntInAB) {
						return (cardbookFilterRules._matchEmails(aMsgHdr.author, aSearchValue, aSearchOp) &&
								cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) &&
								cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp) &&
								cardbookFilterRules._matchEmails(aMsgHdr.bccList, aSearchValue, aSearchOp));
					} else {
						return (cardbookFilterRules._matchEmails(aMsgHdr.author, aSearchValue, aSearchOp) ||
								cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) ||
								cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp) ||
								cardbookFilterRules._matchEmails(aMsgHdr.bccList, aSearchValue, aSearchOp));
					}
				}
			};
			MailServices.filters.addCustomTerm(searchAll);

			var searchCorrespondents = {
				id: "cardbook#searchCorrespondents",
				name: cardbookXULUtils.localizeMessage("cardbook.searchCorrespondents.name"),
				getEnabled: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				needsBody: false,
				getAvailable: function (scope, op) {
					return cardbookFilterRules._isLocalSearch(scope);
				},
				getAvailableOperators: function (scope) {
					if (!cardbookFilterRules._isLocalSearch(scope)) {
						return [];
					}
					return [Components.interfaces.nsMsgSearchOp.IsInAB, Components.interfaces.nsMsgSearchOp.IsntInAB];
				},
				// true && false => false
				// true || false => true
				match: function (aMsgHdr, aSearchValue, aSearchOp) {
					if (aSearchOp == Components.interfaces.nsMsgSearchOp.IsntInAB) {
						if (cardbookFilterRules._isOutgoingMail(aMsgHdr)) {
							return (cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) &&
									cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp) &&
									cardbookFilterRules._matchEmails(aMsgHdr.bccList, aSearchValue, aSearchOp));
						} else {
							return cardbookFilterRules._matchEmails(aMsgHdr.author, aSearchValue, aSearchOp);
						}
					} else {
						if (cardbookFilterRules._isOutgoingMail(aMsgHdr)) {
							return (cardbookFilterRules._matchEmails(aMsgHdr.recipients, aSearchValue, aSearchOp) ||
									cardbookFilterRules._matchEmails(aMsgHdr.ccList, aSearchValue, aSearchOp) ||
									cardbookFilterRules._matchEmails(aMsgHdr.bccList, aSearchValue, aSearchOp));
						} else {
							return cardbookFilterRules._matchEmails(aMsgHdr.author, aSearchValue, aSearchOp);
						}
					}
				}
			};
			MailServices.filters.addCustomTerm(searchCorrespondents);

			var addFrom = {
				id: "cardbook#addFrom",
				name: cardbookXULUtils.localizeMessage("cardbook.addFrom.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "author");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(addFrom);

			var addTo = {
				id: "cardbook#addTo",
				name: cardbookXULUtils.localizeMessage("cardbook.addTo.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "recipients");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(addTo);

			var addCc = {
				id: "cardbook#addCc",
				name: cardbookXULUtils.localizeMessage("cardbook.addCc.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "ccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(addCc);

			var addBcc = {
				id: "cardbook#addBcc",
				name: cardbookXULUtils.localizeMessage("cardbook.addBcc.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "bccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(addBcc);

			var addAll = {
				id: "cardbook#addAll",
				name: cardbookXULUtils.localizeMessage("cardbook.addAll.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "author");
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "recipients");
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "ccList");
					await cardbookFilterRules._addEmails(aMsgHdrs, aActionValue, "bccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(addAll);

			var removeFrom = {
				id: "cardbook#removeFrom",
				name: cardbookXULUtils.localizeMessage("cardbook.removeFrom.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "author");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(removeFrom);

			var removeTo = {
				id: "cardbook#removeTo",
				name: cardbookXULUtils.localizeMessage("cardbook.removeTo.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "recipients");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(removeTo);

			var removeCc = {
				id: "cardbook#removeCc",
				name: cardbookXULUtils.localizeMessage("cardbook.removeCc.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "ccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(removeCc);

			var removeBcc = {
				id: "cardbook#removeBcc",
				name: cardbookXULUtils.localizeMessage("cardbook.removeBcc.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "bccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(removeBcc);

			var removeAll = {
				id: "cardbook#removeAll",
				name: cardbookXULUtils.localizeMessage("cardbook.removeAll.name"),
				isValidForType: function(type, scope) {return true;},
				validateActionValue: function(value, folder, type) { return null;},
				applyAction: async function (aMsgHdrs, aActionValue, aListener, aType, aMsgWindow) {
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "author");
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "recipients");
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "ccList");
					await cardbookFilterRules._removeEmails(aMsgHdrs, aActionValue, "bccList");
				},
				allowDuplicates: false,
				needsBody: false,
				isAsync: true
			};
			MailServices.filters.addCustomAction(removeAll);

			// insertion of a filter for marking emails from CardBook contacts as not junk
			let cardbookFilterName = cardbookXULUtils.localizeMessage("cardbook.filterForNotJunk");
			let cardbookFilterDescOld = "#automatically inserted by CardBook for junk";
			let cardbookFilterDesc = "#automatically inserted by CardBook for junk (v2)";
			let gFilterListMsgWindow = Components.classes["@mozilla.org/messenger/msgwindow;1"].createInstance(Components.interfaces.nsIMsgWindow);
			for (let server of MailServices.accounts.allServers) {
				if (server.canHaveFilters) {
					let folder = server.rootFolder;
					let gCurrentFilterList = folder.getEditableFilterList(gFilterListMsgWindow);
					let found = false;
					let filterCount = gCurrentFilterList.filterCount;
					for (let i = 0; i < filterCount; i++) {
						let filterInserted = gCurrentFilterList.getFilterAt(i);
						if (filterInserted.filterDesc == cardbookFilterDescOld) {
							gCurrentFilterList.removeFilterAt(i);
						} else if (filterInserted.filterDesc == cardbookFilterDesc) {
							if (filterInserted.filterName.includes("??") ) {
								gCurrentFilterList.removeFilterAt(i);
							} else {
								found = true;
								break;
							}
						}
					}

					if (!found) {
						let filter = gCurrentFilterList.createFilter(cardbookFilterName);

						let term = filter.createTerm();
						term.attrib = Components.interfaces.nsMsgSearchAttrib.Custom;
						term.op = Components.interfaces.nsMsgSearchOp.IsInAB;
						term.booleanAnd = true;
						let termValue = term.value;
						termValue.attrib = term.attrib;
						termValue.str = "allAddressBooks";
						term.value = termValue;
						term.customId = "cardbook#searchFrom";
						filter.appendTerm(term);

						let filterAction = filter.createAction();
						filterAction.type = Components.interfaces.nsMsgFilterAction.JunkScore;
						filterAction.junkScore = 0;
						filter.appendAction(filterAction);

						filter.enabled = true;
						filter.filterDesc = cardbookFilterDesc;
						filter.filterType = Components.interfaces.nsMsgFilterType.PostPlugin | Components.interfaces.nsMsgFilterType.Manual;
						
						gCurrentFilterList.insertFilterAt(0, filter);
					}
				}
			}
            await notifyTools.notifyBackground({query: "cardbook.filters.filtersInitialized", value: true});
		}
	};
};
