var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");

var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookComposeMsgObserver.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookSetAutocompletion.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookComposeMsg = {
	origFunctions: {},

	openCBWindow: async function() {
		await notifyTools.notifyBackground({query: "cardbook.openCBWindow"});
	},

	GenericSendMessage: async function() {
		if (gMsgCompose.compFields.deliveryFormat == Components.interfaces.nsIMsgCompSendFormat.Auto ||
			gMsgCompose.compFields.deliveryFormat == Components.interfaces.nsIMsgCompSendFormat.Unset) {
			let fields = GetComposeDetails();
			let allHtml = true;
			let allPlain = true;
			for (let field of ["to", "cc", "bcc"]) {
				if (fields[field] && (allHtml || allPlain)) {
					let addresses = MailServices.headerParser.parseEncodedHeaderW(fields[field]);
					for (let address of addresses) {
						if (allHtml || allPlain) {
							let card = await notifyTools.notifyBackground({query: "cardbook.getCardFromEmail", email: address.email});
							let format = await notifyTools.notifyBackground({query: "cardbook.getMailFormatFromCard", card: card});
							if (format == 2) {
								allPlain = false;
							} else if (format == 1) {
								allHtml = false;
							} else {
								allPlain = false;
								allHtml = false;
							}
						}
					}
				}
			}
			if (allPlain) {
				gMsgCompose.compFields.deliveryFormat = Components.interfaces.nsIMsgCompSendFormat.PlainText;
			}
			if (allHtml) {
				gMsgCompose.compFields.deliveryFormat = Components.interfaces.nsIMsgCompSendFormat.HTML;
			}
		}
	},

	newInCardBook: async function() {
		await notifyTools.notifyBackground({query: "cardbook.createContact"});
	},

	setAB: async function() {
		document.getElementById("tasksMenuAddressBook").removeAttribute("key");
		if (document.getElementById("key_addressbook")) {
			document.getElementById("key_addressbook").setAttribute("key", "");
		}
		var exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
		var myPopup = document.getElementById("menu_NewPopup");
		if (exclusive) {
			document.getElementById('tasksMenuAddressBook').setAttribute('hidden', 'true');
			// this menu has no id, so we have to do manually
			myPopup.lastChild.remove();
		} else {
			document.getElementById('tasksMenuAddressBook').removeAttribute('hidden');
		}

		var myMenuItem = document.createXULElement("menuitem");
		myMenuItem.setAttribute("id", "newCardBookCardFromMsgMenu");
		myMenuItem.addEventListener("command", async function(aEvent) {
				await cardbookComposeMsg.newInCardBook();
				aEvent.stopPropagation();
			}, false);
		myMenuItem.setAttribute("label", cardbookXULUtils.localizeMessage("newCardBookCardMenuLabel"));
		myMenuItem.setAttribute("accesskey", cardbookXULUtils.localizeMessage("newCardBookCardMenuAccesskey"));
		myPopup.appendChild(myMenuItem);
	},

	unloadMsg: function () {
		cardBookComposeMsgObserver.unregister();

		// functions
		GenericSendMessage = cardbookComposeMsg.origFunctions.GenericSendMessage;
		setContactsSidebarVisibility = cardbookComposeMsg.origFunctions.setContactsSidebarVisibility;
		focusContactsSidebarSearchInput = cardbookComposeMsg.origFunctions.focusContactsSidebarSearchInput;

		updateSendCommands = cardbookRecipients.origFunctions.updateSendCommands;
		onRecipientsChanged = cardbookRecipients.origFunctions.onRecipientsChanged;
	},

	loadMsg: async function () {
		cardBookComposeMsgObserver.register();
		await cardbookComposeMsg.setAB();
		setTimeout(function() {
			cardbookSetAutocompletion.setMsgCompletion();
			}, 50);
		setTimeout(function() {
			cardbookSetAutocompletion.loadCssRules();
			}, 500);
	}

};

// GenericSendMessage
(function() {
	// Keep a reference to the original function.
	cardbookComposeMsg.origFunctions.GenericSendMessage = GenericSendMessage;

	// Override a function.
	GenericSendMessage = async function() {

		let fields = GetComposeDetails();
		if (fields) {
			await cardbookComposeMsg.GenericSendMessage();
		}
		var rv = cardbookComposeMsg.origFunctions.GenericSendMessage.apply(null, arguments);
		return rv;
	};
})();
