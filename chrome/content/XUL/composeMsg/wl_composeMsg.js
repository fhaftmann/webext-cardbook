// Import any needed modules.
var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/composeMsg/cardbookComposeMsg.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookSetAutocompletion.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookAutocompletionEngine.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/composeMsg/cardbookRecipients.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/contactsSidebar/ovl_cardbookContactsSidebarMain.js", window, "UTF-8");

function onLoad(wasAlreadyOpen) {
	// autocompletion, buttons and menus
	WL.injectCSS("chrome://cardbook/content/skin/cardbookSetAutocompletion.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookComposeMsg.css");

	WL.injectElements(`
	<!-- horrible hack to have the CardBookKey defined -->
	<!-- <keyset id="viewZoomKeys"> -->
	<key id="CardBookKey" key="__MSG_cardbookMenuItemAccesskey__" modifiers="accel, shift" oncommand="cardbookComposeMsg.openCBWindow();" insertafter="key_fullZoomReduce"/>

	<menupopup id="taskPopup">
		<menuitem id="cardbookMenuItem"
			label="__MSG_cardbookMenuItemLabel__" accesskey="__MSG_cardbookMenuItemAccesskey__"
			key="CardBookKey"
			tooltiptext="__MSG_cardbookMenuItemTooltip__"
			oncommand="cardbookComposeMsg.openCBWindow();"
			insertafter="tasksMenuAddressBook"/>
	</menupopup>
	`);

	window.cardbookComposeMsg.loadMsg();
};

function onUnload(wasAlreadyOpen) {
	window.cardbookComposeMsg.unloadMsg();
};