var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { MimeParser } = ChromeUtils.importESModule("resource:///modules/mimeParser.sys.mjs");

var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookRecipients = {
	origFunctions: {},

	// comes from updateSendLock
	mailListNameExists: async function () {
		gSendLocked = true;
		if (!gMsgCompose) {
			return;
		}
		const addressRows = [ "toAddrContainer", "ccAddrContainer", "bccAddrContainer", "newsgroupsAddrContainer" ];
		
		for (let parentID of addressRows) {
			// loop through all pills to update all the pill.classlist
			// if (!gSendLocked) {
			// 	break;
			// }
			let parent = document.getElementById(parentID);
			if (!parent) {
				continue;
			}
			for (let address of parent.querySelectorAll(".address-pill")) {
				let listNames = MimeParser.parseHeaderField(address.fullAddress, MimeParser.HEADER_ADDRESS);
				let doesListExist = await notifyTools.notifyBackground({query: "cardbook.doesListExist", name: listNames[0].name});
				let isMailingList = listNames.length > 0 && doesListExist;
				let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
				if (!exclusive && !isMailingList) {
					isMailingList = listNames.length > 0 && MailServices.ab.mailListNameExists(listNames[0].name);
				}
				if (isValidAddress(address.emailAddress) || isMailingList || address.emailInput.classList.contains("news-input")) {
					address.classList.remove("invalid-address");
					gSendLocked = false;
					// break;
				}
			}
		}
	},

	isInAddressBook: async function () {
		const addressRows = [ "toAddrContainer", "ccAddrContainer", "bccAddrContainer", "newsgroupsAddrContainer" ];
		for (let parentID of addressRows) {
			let parent = document.getElementById(parentID);
			if (!parent) {
				continue;
			}
			for (let address of parent.querySelectorAll(".address-pill")) {
				let exclusive = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "exclusive"});
				let identity = window.gMsgCompose.identity ? window.gMsgCompose.identity.key : null;
				let isEmailRegistered = await notifyTools.notifyBackground({query: "cardbook.isEmailRegistered", email: address.emailAddress, identity: identity});
				if (isEmailRegistered) {
					address.removeAttribute("tooltiptext");
					address.pillIndicator.hidden = true;
				} else if (exclusive) {
					address.setAttribute(
						"tooltiptext",
						await document.l10n.formatValue("pill-tooltip-not-in-address-book", {
							email: address.fullAddress,
						})
						);
					address.pillIndicator.hidden = false;
				}
			}
		}
	}
};

// updateSendCommands
(function() {
	// Keep a reference to the original function.
	cardbookRecipients.origFunctions.updateSendCommands = updateSendCommands;
	
	// Override a function.
	updateSendCommands = async function() {
		// Execute original function.
		var aHaveController = arguments[0];
		var rv = cardbookRecipients.origFunctions.updateSendCommands.apply(null, arguments);
		
		// Execute some action afterwards.
		await cardbookRecipients.mailListNameExists();

		if (aHaveController) {
			goUpdateCommand("cmd_sendButton");
			goUpdateCommand("cmd_sendNow");
			goUpdateCommand("cmd_sendLater");
			goUpdateCommand("cmd_sendWithCheck");
		} else {
			goSetCommandEnabled(
				"cmd_sendButton",
				defaultController.isCommandEnabled("cmd_sendButton")
				);
			goSetCommandEnabled(
				"cmd_sendNow",
				defaultController.isCommandEnabled("cmd_sendNow")
				);
			goSetCommandEnabled(
				"cmd_sendLater",
				defaultController.isCommandEnabled("cmd_sendLater")
				);
			goSetCommandEnabled(
				"cmd_sendWithCheck",
				defaultController.isCommandEnabled("cmd_sendWithCheck")
				);
		}

		let changed = false;
		const currentStates = {};
		const changedStates = {};
		for (const state of ["cmd_sendNow", "cmd_sendLater"]) {
			currentStates[state] = defaultController.isCommandEnabled(state);
			if (!gLastKnownComposeStates.hasOwnProperty(state) || gLastKnownComposeStates[state] != currentStates[state]) {
				gLastKnownComposeStates[state] = currentStates[state];
				changedStates[state] = currentStates[state];
				changed = true;
			}
		}
		if (changed) {
			window.dispatchEvent(new CustomEvent("compose-state-changed", { detail: changedStates }));
		}		
	};
})();

// onRecipientsChanged
(function() {
	// Keep a reference to the original function.
	cardbookRecipients.origFunctions.onRecipientsChanged = onRecipientsChanged;
	
	// Override a function.
	onRecipientsChanged = function() {
		// Execute original function.
		var rv = cardbookRecipients.origFunctions.onRecipientsChanged.apply(null, arguments);
		// Execute some action afterwards.
		// the function customElements.get("mail-address-pill").prototype.updatePillStatus is async
		// with no way to update it
		if ("undefined" == typeof(setTimeout)) {
			var { setTimeout } = ChromeUtils.importESModule("resource://gre/modules/Timer.sys.mjs");
		}
		setTimeout(function() {
			cardbookRecipients.isInAddressBook();
			}, 500);
	};
})();
