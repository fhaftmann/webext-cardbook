var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookObserverRepository.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookSetAutocompletion.js", window, "UTF-8");

var cardBookCalendarObserver = {
	register: function() {
		cardBookObserverRepository.registerAll(this);
	},
	
	unregister: function() {
		cardBookObserverRepository.unregisterAll(this);
	},
	
	observe: function(aSubject, aTopic, aData) {
		switch (aTopic) {
            // test to do category
			case "cardbook.addressbookCreated":
			case "cardbook.addressbookDeleted":
			case "cardbook.addressbookModified":
			case "cardbook.pref.preferencesChanged":
				cardbookSetAutocompletion.loadCssRules();
				break;
		}
	}
};
