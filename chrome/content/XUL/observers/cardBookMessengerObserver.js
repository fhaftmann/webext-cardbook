var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { MailE10SUtils } = ChromeUtils.importESModule("resource:///modules/MailE10SUtils.sys.mjs");

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookObserverRepository.js", this);

var cardBookMessengerObserver = {
	
	register: function() {
		cardBookObserverRepository.registerAll(this);
	},
	
	unregister: function() {
		cardBookObserverRepository.unregisterAll(this);
	},
	
	observe: async function(aSubject, aTopic, aData) {
		switch (aTopic) {
            case "subdialog-loaded":
                if (!aSubject.location.href.startsWith("chrome://global/content/print.html?")) {
                    return;
                }
        
                await new Promise(resolve =>
                    aSubject.document.addEventListener("print-settings", resolve, { once: true })
                );
            
                if (aSubject.PrintEventHandler.activeCurrentURI != "chrome://cardbook/content/XUL/print/wdw_cardbookPrint.html") {
                    return;
                }
                Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/print/cardbookPrint.js", aSubject);
                Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/print/wdw_cardbookPrint.js", aSubject);		
                break;
            case "cardbook.printCards":
                let printBrowser = document.getElementById("cardbookPrintContent");
                if (aData.length == 0) {
                    return
                } else {
                    printBrowser.setAttribute("cards", aData);
                }
                if (printBrowser.currentURI.spec == "about:blank") {
                    // The template page hasn't been loaded yet. Do that now.
                    await new Promise(resolve => {
                        // Store a strong reference to this progress listener.
                        printBrowser.progressListener = {
                            QueryInterface: ChromeUtils.generateQI([
                                "nsIWebProgressListener",
                                "nsISupportsWeakReference",
                            ]),
    
                            /** nsIWebProgressListener */
                            onStateChange(webProgress, request, stateFlags, status) {
                                if (stateFlags & Components.interfaces.nsIWebProgressListener.STATE_STOP && printBrowser.currentURI.spec != "about:blank") {
                                    printBrowser.webProgress.removeProgressListener(this);
                                    delete printBrowser.progressListener;
                                    resolve();
                                }
                            },
                        };
    
                        printBrowser.webProgress.addProgressListener(printBrowser.progressListener, Components.interfaces.nsIWebProgress.NOTIFY_STATE_ALL);
                        MailE10SUtils.loadURI(printBrowser, "chrome://cardbook/content/XUL/print/wdw_cardbookPrint.html");
                    });
                }
                PrintUtils.startPrintWindow(printBrowser.browsingContext, {});
                break;
        }
	}
};
