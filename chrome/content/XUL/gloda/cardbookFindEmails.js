if ("undefined" == typeof(cardbookFindEmails)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
	var { GlodaIndexer } = ChromeUtils.importESModule("resource:///modules/gloda/GlodaIndexer.sys.mjs");
	var { GlodaMsgSearcher } = ChromeUtils.importESModule("resource:///modules/gloda/GlodaMsgSearcher.sys.mjs");

	var loader = Services.scriptloader;
    loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

	var cardbookFindEmails = {

		findEmailsFromEmail: function(emailAddressNode) {
			let email = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			cardbookFindEmails.findEmails(null, [email]);
		},

		findAllEmailsFromContact: async function(emailAddressNode) {
			let email = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			let isEmailRegistered = await notifyTools.notifyBackground({query: "cardbook.isEmailRegistered", email: email, identity: cardbookAboutMessage.getIdentityKey()});
	
			if (isEmailRegistered) {
				let card = await notifyTools.notifyBackground({query: "cardbook.getCardFromEmail", email: email});
				cardbookFindEmails.findEmails([card], null);
			} else {
				cardbookFindEmails.findEmails(null, [email]);
			}	
		},

		findEmails: function (aListOfSelectedCard, aListOfSelectedEmails) {
			let listOfEmail = [];
			if (aListOfSelectedCard) {
				for (var i = 0; i < aListOfSelectedCard.length; i++) {
					if (!aListOfSelectedCard[i].isAList) {
						for (var j = 0; j < aListOfSelectedCard[i].email.length; j++) {
							listOfEmail.push(aListOfSelectedCard[i].email[j][0][0].toLowerCase());
						}
					} else {
						listOfEmail.push(aListOfSelectedCard[i].fn.replace('"', '\"'));
					}
				}
			} else if (aListOfSelectedEmails) {
				listOfEmail = JSON.parse(JSON.stringify(aListOfSelectedEmails));
			}
			
			let tabmail;
			let mail3PaneWindow = Services.wm.getMostRecentWindow("mail:3pane");
			if (mail3PaneWindow) {
				tabmail = mail3PaneWindow.document.getElementById("tabmail");
				mail3PaneWindow.focus();
			}
			// gloda is not defined when used from an independant window
			tabmail.openTab("glodaFacet", {searcher: new GlodaMsgSearcher(null, '"' + listOfEmail.join('" "') + '"', false)});
		}
	};
};
