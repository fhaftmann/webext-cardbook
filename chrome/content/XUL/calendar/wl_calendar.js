// Import any needed modules.
var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookCalendarObserver.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookSetAutocompletion.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/autocomplete/cardbookAutocompletionEngine.js", this);

function onUnload(wasAlreadyOpen) {
    cardBookCalendarObserver.unregister();
};

function onLoad(wasAlreadyOpen) {
	WL.injectCSS("chrome://cardbook/content/skin/cardbookSetAutocompletion.css");
    cardBookCalendarObserver.register();
    cardbookSetAutocompletion.loadCssRules();

    window.setInterval(function() {
        cardbookSetAutocompletion.setLightningCompletion();
        }, 500);
};
