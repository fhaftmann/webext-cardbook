var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");
var { VCardUtils } = ChromeUtils.importESModule("resource:///modules/VCardUtils.sys.mjs");

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js", this);
Services.scriptloader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var EXPORTED_SYMBOLS = ["cardbookMigrate"];
var cardbookMigrate = {

	allLists : {},
	defaultEmailFormat: "X-MOZILLA-HTML",
	defaultYear: "1604",

	translateStandardCards: async function (aDirPrefIdTarget, aABCard, aVersion, aDateFormat) {
		try {
			let vCard = aABCard.getProperty("_vCard", "");
			let card = await notifyTools.notifyBackground({query: "cardbook.getCardbookCardParser", data: vCard});
			if (!vCard) {
				card.dirPrefId = aDirPrefIdTarget;
				card.version = aVersion;
				var myMap = [ ["FirstName", "firstname"], ["LastName", "lastname"], ["DisplayName", "fn"], ["NickName", "nickname"], ["JobTitle", "title"], ["Notes", "note"] ];
				for (let i = 0; i < myMap.length; i++) {
					var myMapData = aABCard.getProperty(myMap[i][0],"");
					card[myMap[i][1]] = myMapData;
				}
								
				var myDep = aABCard.getProperty("Department","");
				var myOrg = aABCard.getProperty("Company","");
				if (myOrg ) {
					card.org.push(myOrg);
				}
				if (myDep ) {
					card.org.push(myDep);
				}
				
				var listMap = [ ["PrimaryEmail", ["TYPE=PREF" , "TYPE=HOME"] , "email"], ["SecondEmail", ["TYPE=HOME"], "email"], ["WorkPhone", ["TYPE=WORK"], "tel"], ["HomePhone", ["TYPE=HOME"], "tel"],
								  ["FaxNumber", ["TYPE=FAX"], "tel"], ["PagerNumber", ["TYPE=PAGER"], "tel"], ["CellularNumber", ["TYPE=CELL"], "tel"], ["WebPage1", ["TYPE=WORK"], "url"],
								  ["WebPage2", ["TYPE=HOME"], "url"] ];
				for (var i = 0; i < listMap.length; i++) {
					var myMapData = aABCard.getProperty(listMap[i][0],"");
					var myPreferDisplayName = aABCard.getProperty("PreferDisplayName","1");
					if (myMapData != "") {
						card[listMap[i][2]].push([[myMapData], listMap[i][1], "", []]);
						if (listMap[i][2] == "email" && myPreferDisplayName == "0") {
							await notifyTools.notifyBackground({ query: "cardbook.addPrefDispName", email: myMapData});
						}
					}
				}
		  
				var myAdrMap = [ [ [ ["HomeAddress", "HomeAddress2"], "HomeCity", "HomeState", "HomeZipCode", "HomeCountry"], ["TYPE=HOME"] ],
								 [ [ ["WorkAddress", "WorkAddress2"], "WorkCity", "WorkState", "WorkZipCode", "WorkCountry"], ["TYPE=WORK"] ] ];
				for (var i = 0; i < myAdrMap.length; i++) {
					var lString = "";
					var myAdr = ["", ""];
					for (var j = 0; j < myAdrMap[i][0][0].length; j++) {
						var myProp = aABCard.getProperty(myAdrMap[i][0][0][j],"");
						if (myProp != "") {
							if (lString != "") {
								lString = lString + "\n" + myProp;
							} else {
								lString = myProp;
							}
						}
					}
					myAdr.push(lString);
					for (var j = 1; j < myAdrMap[i][0].length; j++) {
						myAdr.push(aABCard.getProperty(myAdrMap[i][0][j],""));
					}
					if (cardbookMigrate.notNull(myAdr, "") != "") {
						card.adr.push([myAdr, myAdrMap[i][1], "", []]);
					}
				}
				
				var day = aABCard.getProperty("BirthDay", "");
				var month = aABCard.getProperty("BirthMonth", "");
				var year = aABCard.getProperty("BirthYear", "");
				if (day != "" || month != "" || year != "" ) {
					card.bday = cardbookMigrate.getFinalDateString(day, month, year, aDateFormat)
				}
	
				var photoURI = aABCard.getProperty("PhotoURI", "");
				var photoType = aABCard.getProperty("PhotoType", "");
				if (photoType == "file" || photoType == "web") {
					try {
						let [ base64, extension ] = await cardbookXULUtils.getImageFromURI(card.fn, "import standard AB", photoURI);
						if (base64) {
							card.photo.value = base64;
							card.photo.extension = extension || cardbookXULUtils.getFileExtension(photoURI);
						}
					} catch (e) {}
				}
				cardbookMigrate.getNotNullFn(card, aABCard);
				
				var PreferMailFormat = aABCard.getProperty("PreferMailFormat", "");
				if (PreferMailFormat == "1") {
					card.others.push(cardbookMigrate.defaultEmailFormat + ":FALSE");
				} else if (PreferMailFormat == "2") {
					card.others.push(cardbookMigrate.defaultEmailFormat + ":TRUE");
				}
	
				var email = aABCard.getProperty("PrimaryEmail", "").toLowerCase();
				var emailValue = parseInt(aABCard.getProperty("PopularityIndex", "0"));
				if (email != "" && emailValue != "0" && emailValue != " ") {
					await notifyTools.notifyBackground({query: "cardbook.addMailPop", email: email, value: emailValue});
				}
			} else {
				card.dirPrefId = aDirPrefIdTarget;
				
				let photoName = aABCard.getProperty("PhotoName", "");
				let file = Services.dirsvc.get("ProfD", Components.interfaces.nsIFile);
				file.append("Photos");
				file.append(photoName);
				if (file.exists()) {
					let photoURI = Services.io.newFileURI(file).spec;
					try {
						let [ base64, extension ] = await cardbookXULUtils.getImageFromURI(card.fn, "import standard AB", photoURI);
						if (base64 && extension != "text/xml") {
							card.photo.value = base64;
							card.photo.extension = extension || cardbookXULUtils.getFileExtension(photoURI);
						}
					} catch (e) {}
				}
				
				let preferDisplayName = aABCard.getProperty("PreferDisplayName", "");
				for (let emailLine of card.email) {
					let email = emailLine[0][0];
					if (preferDisplayName == "1") {
						await notifyTools.notifyBackground({ query: "cardbook.removePrefDispName", email: email});
					} else {
						await notifyTools.notifyBackground({ query: "cardbook.addPrefDispName", email: email});
					}
				}

				let email = aABCard.getProperty("PrimaryEmail", "").toLowerCase();
				let emailValue = parseInt(aABCard.getProperty("PopularityIndex", "0"));
				if (email != "" && emailValue != "0" && emailValue != " ") {
					await notifyTools.notifyBackground({query: "cardbook.addMailPop", email: email, value: emailValue});
				}
			}
			await notifyTools.notifyBackground({query: "cardbook.importCard", card: card, dirPrefId: aDirPrefIdTarget});
		}
		catch (e) {
			await notifyTools.notifyBackground({query: "cardbook.importCardsError", dirPrefId: aDirPrefIdTarget, field: "cardbookMigrate.translateStandardCards", message: e});
		}
	},

	getSolvedListNumber: function (aDirPrefIdTarget) {
		let result = 0;
		for (let i in cardbookMigrate.allLists[aDirPrefIdTarget]) {
			if (cardbookMigrate.allLists[aDirPrefIdTarget][i].solved) {
				result++;
			}
		}
		return result;
	},

	mayTheListBeResolved: function (aDirPrefIdTarget, aABList) {
        for (let card of aABList.childCards) {
            let ABCard = card.QueryInterface(Components.interfaces.nsIAbCard);
            let email = ABCard.primaryEmail;
            let fn = ABCard.getProperty("DisplayName","");
            if ((fn == email) && cardbookMigrate.allLists[aDirPrefIdTarget][fn]) {
                if (!cardbookMigrate.allLists[aDirPrefIdTarget][fn].solved) {
                    return false;
                }
            }
        }
        return true
	},

	translateStandardLists: async function (aDirPrefIdTarget, aVersion, aKindPref, aMemberPref) {
		try {
			let beforeNumber = cardbookMigrate.getSolvedListNumber(aDirPrefIdTarget);
			let afterNumber = 0;
			let condition = true;
			// loop until all lists may be solved
			while (condition) {
				for (let listName in cardbookMigrate.allLists[aDirPrefIdTarget]) {
					if (!cardbookMigrate.allLists[aDirPrefIdTarget][listName].solved && cardbookMigrate.mayTheListBeResolved(aDirPrefIdTarget, cardbookMigrate.allLists[aDirPrefIdTarget][listName].list)) {
						let list = cardbookMigrate.allLists[aDirPrefIdTarget][listName].list;
						let card = await notifyTools.notifyBackground({query: "cardbook.getCardbookCardParser"});
						card.dirPrefId = aDirPrefIdTarget;
						card.version = aVersion;
						let maps = [ ["dirName", "fn"], ["listNickName", "nickname"], ["description", "note"] ];
						for (let map of maps) {
							card[map[1]] = list[map[0]];
						}
						var targetMembers = [];
						for (let card of list.childCards) {
							let ABCard = card.QueryInterface(Components.interfaces.nsIAbCard);
							let email = ABCard.primaryEmail;
							let name = ABCard.getProperty("DisplayName", "");
							try {
								// within a standard list all members are simple cards… weird…
								if ((name == email) && cardbookMigrate.allLists[aDirPrefIdTarget][name] && cardbookMigrate.allLists[aDirPrefIdTarget][name].solved) {
									targetMembers.push("urn:uuid:" + cardbookMigrate.allLists[aDirPrefIdTarget][name].uid);
								}
							}
							catch (e) {}
						}

						cardbookMigrate.addMemberstoCard(card, targetMembers, "group", aKindPref, aMemberPref);
						
						await notifyTools.notifyBackground({query: "cardbook.importCard", card: card, dirPrefId: aDirPrefIdTarget});

						cardbookMigrate.allLists[aDirPrefIdTarget][listName].solved = true;
						cardbookMigrate.allLists[aDirPrefIdTarget][listName].uid = card.uid;
					}
				}
				afterNumber = cardbookMigrate.getSolvedListNumber(aDirPrefIdTarget);
				condition = (beforeNumber != afterNumber);
				beforeNumber = afterNumber;
			}
		}
		catch (e) {
			await notifyTools.notifyBackground({query: "cardbook.importCardsError", dirPrefId: aDirPrefIdTarget, field: "cardbookMigrate.translateStandardLists", message: e});
		}
	},

	getNotNullFn: function (aCard, aABCard) {
        if (aCard.fn != "") {
            return;
        }
        if (aCard.org != "") {
            aCard.fn = aCard.org;
            return;
        }
        if (aCard.lastname != "") {
            aCard.fn = aCard.lastname;
            return;
        }
        if (aCard.firstname != "") {
            aCard.fn = aCard.firstname;
            return;
        }
        let email = aABCard.getProperty("PrimaryEmail", "");
        if (email != "") {
            let tmpArray = email.split("@");
            aCard.fn = tmpArray[0].replace(/\./g, " ");
            return;
        }
	},

	notNull: function (vArray1, vArray2) {
		let vString1 = vArray1.join("");
		if (vString1) {
			return vArray1;
		} else {
			return vArray2;
		}
	},

    getFinalDateString: function (aDay, aMonth, aYear, aDateFormat) {
		aMonth = cardbookMigrate.lPad(aMonth);
		aDay = cardbookMigrate.lPad(aDay);
		if (aDateFormat == "YYYY-MM-DD") {
			if (aYear == "") {
				aYear = cardbookMigrate.defaultYear;
			}
			return `${aYear}-${aMonth}-${aDay}`;
		} else if (aDateFormat == "3.0") {
			if (aYear == "") {
				aYear = cardbookMigrate.defaultYear;
			}
			return `${aYear}${aMonth}${aDay}`;
		} else {
			if (aYear == "") {
				aYear = "--";
			}
			return `${aYear}${aMonth}${aDay}`;
		}
	},

	lPad: function (aValue) {
		return (`0${aValue}`).slice(-2);
	},

	addMemberstoCard: function(aCard, aMemberLines, aKindValue, aKindPref, aMemberPref) {
		if (aCard.version == "4.0") {
			aCard.member = JSON.parse(JSON.stringify(aMemberLines));
			if (aKindValue) {
				aCard.kind = aKindValue;
			} else {
				aCard.kind = "group";
			}
		} else if (aCard.version == "3.0") {
			for (let i = 0; i < aCard.others.length; i++) {
				localDelim1 = aCard.others[i].indexOf(":",0);
				if (localDelim1 >= 0) {
					let header = aCard.others[i].substr(0,localDelim1).toUpperCase();
					if (header == aKindPref || header == aMemberPref) {
						aCard.others.splice(i, 1);
						i--;
						continue;
					}
				}
			}
			for (let member of aMemberLines) {
				if (i === 0) {
					if (aKindValue) {
						aCard.others.push(`${aKindPref}:${aKindValue}`);
					} else {
						aCard.others.push(`${aKindPref}:group`);
					}
				}
				aCard.others.push(`${aMemberPref}:${member}`);
			}
		}
	},

    importCards: async function (aDirPrefIdSource, aDirPrefIdTarget, aVersion, aDateFormat, aKindPref, aMemberPref) {
		for (let addrbook of MailServices.ab.directories) {
			if (addrbook.UID == aDirPrefIdSource) {
				cardbookMigrate.allLists[aDirPrefIdTarget] = {};
				await notifyTools.notifyBackground({query: "cardbook.setCardbookServerCardSyncTotal", dirPrefId: aDirPrefIdTarget, count: addrbook.childCards.length});
				for (let ABCard of addrbook.childCards) {	
					if (ABCard.isMailList && ABCard.isMailList === true) {
						let myABList = MailServices.ab.getDirectory(ABCard.mailListURI);
						cardbookMigrate.allLists[aDirPrefIdTarget][myABList.dirName] = {};
						cardbookMigrate.allLists[aDirPrefIdTarget][myABList.dirName].solved = false;
						cardbookMigrate.allLists[aDirPrefIdTarget][myABList.dirName].list = myABList;
					} else {
						await cardbookMigrate.translateStandardCards(aDirPrefIdTarget, ABCard, aVersion, aDateFormat);
					}
				}
				await cardbookMigrate.translateStandardLists(aDirPrefIdTarget, aVersion, aKindPref, aMemberPref);
				break;
			}
		}
	},
};

