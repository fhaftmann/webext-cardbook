var { cal } = ChromeUtils.importESModule("resource:///modules/CalAttendee.sys.mjs");

var loader = Services.scriptloader;
loader.loadSubScript("chrome://calendar/content/calendar-item-editing.js", this);
// for migration
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookEncryptor.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBCard.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBCat.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBUndo.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBImage.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBMailPop.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBPrefDispName.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBSearch.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/indexedDBOld/cardbookIDBDuplicate.js", this);

loader.loadSubScript("chrome://cardbook/content/XUL/gloda/cardbookFindEmails.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/activityManager/cardbookActivityManager.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/attachments/cardbookAttachmentUtils.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULPasswordManager.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULCardEmails.js", this);
// test loader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULEnigmail.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/migrate/cardbookMigrate.js", this);
loader.loadSubScript("chrome://cardbook/content/XUL/calendar/cardbookCalendarUtils.js", this);

notifyTools.addListener(async (message) => {
	switch (message.query) {
		case "cardbook.getImageFromURI":
			return cardbookXULUtils.getImageFromURI(message.URI);
		case "cardbook.identityChanged":
			Services.obs.notifyObservers(null, message, "identityChanged", message.windowId);
			break;
		case "cardbook.getWrongBirthdayCalendars": {
			let results = { exist: [], writable: [], ok: [] };
			let cals = cal.manager.getCalendars();
			for (let calendar of cals) {
				if (!message.calendars.includes(calendar.id)) {
					results.exist.push(calendar.id);
				} else if (calendar.getProperty("disabled") || calendar.readOnly) {
					results.writable.push(calendar.name);
				} else {
					results.ok.push(calendar.id);
				}
			}
			return results;
		}
		case "cardbook.addBirthdaysToCalendar": {
			let birthdaysSyncResult = {};

			let calendarsNameList = message.prefs.calendarsNameList.split(",");
			let allCalendars = [];
			let cals = cal.manager.getCalendars();
			for (let calendar of cals) {
				if (calendarsNameList.includes(calendar.id)) {
					if (!(calendar.getProperty("disabled") || calendar.readOnly)) {
						allCalendars.push([ calendar.name, calendar.id ]);
					}
				}
			}
			cardbookXULUtils.sortMultipleArrayByString(allCalendars,0,1);

			let date_of_today = new Date();
			let defaultTimezoneId = cal.dtz.defaultTimezone.tzid;

			for (let [ calendarName, calendarId ] of allCalendars) {
				birthdaysSyncResult[calendarId] = {name: calendarName, created: 0, updated: 0, existing: 0, failed: 0};

				let allCBEvents = [];				
				let calendar = cals.filter(x => x.id == calendarId)[0];
				let filter = 0;
				filter |= calendar.ITEM_FILTER_TYPE_EVENT | calendar.ITEM_FILTER_CLASS_OCCURRENCES;
				let startDate = cal.createDateTime();
				let endDate = cal.dtz.jsDateToDateTime(new Date(new Date().setFullYear(new Date().getFullYear() + 1)));
				let iterator = cal.iterate.streamValues(
					calendar.getItems(filter, 0, startDate, endDate)
				);
				for await (let items of iterator) {
					for (let item of items) {
						let title = item.title;
						let cbuid = item.getProperty("X-CARDBOOK-EVENTID");
						let vevent = item.icalComponent.toString();
						let valarms = [];
						for (let alarm of item.getAlarms()) {
							valarms.push(alarm.icalComponent.toString());
						}
						allCBEvents.push([title, cbuid, vevent, valarms, item]);
					}
				}

				for (let birthday of message.birthdayList) {
					let trueEvents = allCBEvents.filter(x => x[1] == birthday[8]);
					let allEvents = allCBEvents.filter(x => x[0] == birthday[1] && x[1] != birthday[8]);
					let events = trueEvents.concat(allEvents);

					let ldaysUntilNextBirthday = birthday[0];
					let lBirthdayTitle = birthday[1];
					let lBirthdayDate = new Date();
					lBirthdayDate.setDate(date_of_today.getDate()+parseInt(ldaysUntilNextBirthday));
					// generate Date as Ical compatible text string
					let lYear = lBirthdayDate.getUTCFullYear();
					let lMonth = lBirthdayDate.getMonth() + 1;
					lMonth = cardbookXULUtils.lPad(lMonth);
					var lDay = lBirthdayDate.getDate();
					lDay = cardbookXULUtils.lPad(lDay);
					let lBirthdayDateString = `${lYear}${lMonth}${lDay}`;
					let lBirthdayDateNext = new Date(lBirthdayDate.getTime() + (24 * 60 * 60 * 1000));
					lYear = lBirthdayDateNext.getFullYear();
					lMonth = lBirthdayDateNext.getMonth() + 1;
					lMonth = cardbookXULUtils.lPad(lMonth);
					lDay = lBirthdayDateNext.getDate();
					lDay = cardbookXULUtils.lPad(lDay);
					let lBirthdayDateNextString = `${lYear}${lMonth}${lDay}`;
					let lBirthdayId = birthday[9];
					let icalString = "BEGIN:VCALENDAR\n";
					icalString += "BEGIN:VEVENT\n";
					let calendarEntryCategories = message.prefs.calendarEntryCategories;
					if (calendarEntryCategories !== "") {
						icalString += `CATEGORIES:${calendarEntryCategories}\n`;
					}
					icalString += "TRANSP:TRANSPARENT\n";
					if (message.prefs.repeatingEvent) {
						icalString += "RRULE:FREQ=YEARLY\n";
					}
					let dtstart;
					let dtend;
					if (message.prefs.eventEntryWholeDay) {
						dtstart = "DTSTART;VALUE=DATE:";
						dtend = "DTEND;VALUE=DATE:";
						icalString += `${dtstart}${lBirthdayDateString}\n`;
						icalString += `${dtend}${lBirthdayDateNextString}\n`;
					} else {
						dtstart = `DTSTART;TZID=${defaultTimezoneId}:`;
						dtend = `DTEND;TZID=${defaultTimezoneId}:`;
						let lBirthdayTimeString = "000000";
						let eventEntryTime = message.prefs.eventEntryTime;
						let EmptyParamRegExp1 = new RegExp("(.*)([^0-9])(.*)", "ig");
						if (eventEntryTime.replace(EmptyParamRegExp1, "$1")!=eventEntryTime) {
							let eventEntryTimeHour = eventEntryTime.replace(EmptyParamRegExp1, "$1");
							let eventEntryTimeMin = eventEntryTime.replace(EmptyParamRegExp1, "$3");
							if ( eventEntryTimeHour < 10 && eventEntryTimeHour.length == 1 ) {
								eventEntryTimeHour = `0${eventEntryTimeHour}`;
							}
							if ( eventEntryTimeMin < 10 && eventEntryTimeMin.length == 1 ) {
								eventEntryTimeMin = `0${eventEntryTimeMin}`;
							}
							lBirthdayTimeString = eventEntryTimeHour.toString() + eventEntryTimeMin.toString() + "00";
						}
						icalString += `${dtstart}${lBirthdayDateString}T${lBirthdayTimeString}\n`;
						icalString += `${dtend}${lBirthdayDateString}T${lBirthdayTimeString}\n`;
					}
					// set Alarms
					let calendarEntryAlarm = message.prefs.calendarEntryAlarm;
					let alarms = [];
					for (let alarm of calendarEntryAlarm.split(",")) {
						// default before alarm before event
						let sign = "-";
						alarm = alarm.replace(/\-/g, "").replace(/ /g, "");
						if (alarm.includes("+")) {
							sign = "";
							alarm = alarm.replace(/\+/g, "").replace(/ /g, "");
						}
						let alarmString = `${sign}PT${parseInt(alarm)}H`;
						if (parseInt(alarm) % 168 == 0) {
							let weeks = parseInt(alarm)/168;
							alarmString = `${sign}P${weeks}W`;
						} else if (parseInt(alarm) % 24 == 0) {
							let days = parseInt(alarm)/24;
							alarmString = `${sign}P${days}D`;
						}
						if (!isNaN(parseInt(alarm))) {
							let alarm = `BEGIN:VALARM\nACTION:DISPLAY\nTRIGGER:${alarmString}\nEND:VALARM\n`;
							alarms.push(alarm);
							icalString += alarm;
						}
					}
					// finalize icalString
					icalString += cardbookXULUtils.splitLine(`X-CARDBOOK-EVENTID:${birthday[8]}\n`);
					icalString += "END:VEVENT\n";
					icalString += "END:VCALENDAR\n";

					// existing
					if (events.length) {
						let origItemLine = events[0];
						let origIcalString = origItemLine[2];
						let origAlarms = origItemLine[3];
						let origItem = origItemLine[4];
						if (cardbookCalendarUtils.hasTheEventChanged(icalString, origIcalString, alarms, origAlarms, lBirthdayTitle, origItem.title)) {
							let item = await cardbookCalendarUtils.updateBirthdayEvent(calendarId, origItem, lBirthdayId, lBirthdayTitle, icalString);
							if (item) {
								await notifyTools.notifyBackground({query: "cardbook.formatStringForOutput", string: "syncListUpdatedEntry", values: [calendarName, lBirthdayTitle]});
								birthdaysSyncResult[calendarId].updated++;
							} else {
								await notifyTools.notifyBackground({query: "cardbook.formatStringForOutput", string: "syncListErrorEntry", values: [calendarName, lBirthdayTitle], error: "Error"});
								birthdaysSyncResult[calendarId].failed++;
							}
						} else {
							await notifyTools.notifyBackground({query: "cardbook.formatStringForOutput", string: "syncListExistingEntry", values: [calendarName, lBirthdayTitle]});
							birthdaysSyncResult[calendarId].existing++;
						}
						// delete others 
						let restEvents = events.slice(1);
						for (let event of restEvents) {
							let origItem = event[4];
							await cardbookCalendarUtils.deleteBirthdayEvent(calendarId, origItem);
						}
					// new
					} else {
						let item = await cardbookCalendarUtils.addBirthdayEvent(calendarId, lBirthdayId, lBirthdayTitle, icalString)
						if (item) {
							await notifyTools.notifyBackground({query: "cardbook.formatStringForOutput", string: "syncListCreatedEntry", values: [calendarName, lBirthdayTitle]});
							birthdaysSyncResult[calendarId].created++;
						} else {
							await notifyTools.notifyBackground({query: "cardbook.formatStringForOutput", string: "syncListErrorEntry", values: [calendarName, lBirthdayTitle], error: "Error"});
							birthdaysSyncResult[calendarId].failed++;
						}
					}
					if (message.sendResults) {
						await notifyTools.notifyBackground({query: "cardbook.syncBirthdays.sendResults", results: birthdaysSyncResult});
					}
				}
			}
			break;
		}
		case "cardbook.getEvents":
			function formatEventDateTime(aDatetime) {
				return cal.dtz.formatter.formatDateTime(aDatetime.getInTimezone(cal.dtz.defaultTimezone));
			};
			function getEventEndDate(x) {
				let eventEndDate = x.endDate.clone();
				if (x.startDate.isDate) {
					eventEndDate.day = eventEndDate.day - 1;
				}
				return eventEndDate;
			};
			let events = []
			let calendars = cal.manager.getCalendars();
			for (let calendar of calendars) {
				if (!calendar.getProperty("disabled")) {
					let filter = 0;
					filter |= calendar.ITEM_FILTER_TYPE_EVENT;
					let iterator = cal.iterate.streamValues(
						calendar.getItems(filter, 0, null, null)
					);
					
					let allItems = [];
					for await (let items of iterator) {
						allItems = allItems.concat(items);
					}		

                    for (let item of allItems) {
                        let found = false;
                        let attendees = cal.email.createRecipientList(item.getAttendees({})).split(", ");
                        for (let attendee of attendees) {
                            if (!found) {
                                for (let email of message.emails) {
                                    if (!found && attendee.toLowerCase().indexOf(email.toLowerCase()) >= 0) {
                                        found = true;
                                        events.push(item);
                                    }
                                }
                            }
                        }
                    }
				}
			}
			cal.unifinder.sortItems(events, message.column, message.order);
			let dataFromEvents = events.map(x => [ (x.title ? x.title.replace(/\n/g, " ") : ""),
																		formatEventDateTime(x.startDate),
																		formatEventDateTime(getEventEndDate(x)),
																		x.getCategories({}).join(", "),
																		x.getProperty("LOCATION"),
																		x.calendar.name,
																		x.hashId.replace("##" + x.calendar.id, ""),
																		x.calendar.id ]);
			return dataFromEvents;
		case "cardbook.editEvent":
			// code taken from modifyEventWithDialog
			let calendar = cal.manager.getCalendarById(message.calendarId);
			let eventFound = await calendar.getItem(message.eventId);

			let dlg = cal.item.findWindow(eventFound);
			if (dlg) {
				dlg.focus();
				return;
			}

			var editListener = {
				onTransactionComplete: async function(item, oldItem) {
					await notifyTools.notifyBackground({query: "cardbook.displayEvents"});
				}
			};

			var onModifyItem = function(eventFound, calendar, originalItem, listener, extresponse=null) {
				cardbookCalendarUtils.doTransaction("modify", eventFound, calendar, originalItem, editListener, extresponse);
			};

			let item = eventFound;
			let response;
			[item, , response] = promptOccurrenceModification(item, true, "edit");
			
			if (item && (response || response === undefined)) {
				cardbookCalendarUtils.modifyLightningEvent(item, item.calendar, "modify", onModifyItem, null, null, null);
			}
			break;
		case "cardbook.createEvent":
			var createListener = {
				onTransactionComplete: async function(item, oldItem) {
					await notifyTools.notifyBackground({query: "cardbook.displayEvents"});
				}
			};

			var onNewEvent = function(item, calendar, originalItem, listener) {
				if (item.id) {
					// If the item already has an id, then this is the result of
					// saving the item without closing, and then saving again.
					cardbookCalendarUtils.doTransaction("modify", item, calendar, originalItem, createListener);
				} else {
					// Otherwise, this is an addition
					cardbookCalendarUtils.doTransaction("add", item, calendar, null, createListener);
				}
			};
		
			let attendees = [];
			for (let contact of message.contacts) {
				for (let email of contact[0]) {
					attendees.push(["mailto:" + email, contact[1]]);
				}
			}
			cardbookCalendarUtils.createLightningEvent(attendees, onNewEvent);
			break;
		case "cardbook.createToDo":
			cardbookCalendarUtils.createLightningTodo(message.title, message.description);
			break;
		case "cardbook.getAllCalendars":
			let tmpArray = [];
			let cals = cal.manager.getCalendars();
			for (let calendar of cals) {
				tmpArray.push([calendar.name, calendar.id]);
			}
			cardbookXULUtils.sortMultipleArrayByString(tmpArray,0,1);
			return tmpArray;
		case "cardbook.findEmails":
			if (message.card.length) {
				cardbookFindEmails.findEmails(message.card, null);
			} else {
				cardbookFindEmails.findEmails(null, message.email);
			}
			break;
		case "cardbook.searchForThKeyEdit":
			// test
			// if (message.card.length) {
			// 	return cardbookXULEnigmail.searchForThKeyEdit(message.card, null);
			// } else {
			// 	return cardbookXULEnigmail.searchForThKeyEdit(null, message.email);
			// }
			return "";
			break;
		case "cardbook.getCardsData":
			await cardbookIDBCard.openDBForTransfert();
			return await cardbookIDBCard.getDataForTransfert();
		case "cardbook.getCatsData":
			await cardbookIDBCat.openDBForTransfert();
			return await cardbookIDBCat.getDataForTransfert();
		case "cardbook.getDuplicatesData":
			await cardbookIDBDuplicate.openDBForTransfert();
			return await cardbookIDBDuplicate.getDataForTransfert();
		case "cardbook.getImagesData":
			await cardbookIDBImage.openDBForTransfert();
			return await cardbookIDBImage.getDataForTransfert();
		case "cardbook.getMailPopsData":
			await cardbookIDBMailPop.openDBForTransfert();
			return await cardbookIDBMailPop.getDataForTransfert();
		case "cardbook.getPrefDispNamesData":
			await cardbookIDBPrefDispName.openDBForTransfert();
			return await cardbookIDBPrefDispName.getDataForTransfert();
		case "cardbook.getSearchesData":
			await cardbookIDBSearch.openDBForTransfert();
			return await cardbookIDBSearch.getDataForTransfert();
		case "cardbook.getUndosData":
			await cardbookIDBUndo.openDBForTransfert();
			return await cardbookIDBUndo.getDataForTransfert();
		case "cardbook.initCryptoActivity":
			cardbookActivityManager.initCryptoActivity(message.mode);
			break;
		case "cardbook.fetchCryptoCount":
			cardbookActivityManager.fetchCryptoCount(message.count);
			break;
		case "cardbook.fetchCryptoActivity":
			cardbookActivityManager.fetchCryptoActivity(message.mode);
			break;
		case "cardbook.finishCryptoActivity":
			cardbookActivityManager.finishCryptoActivity();
			break;
		case "cardbook.finishCryptoActivityOK":
			cardbookActivityManager.finishCryptoActivityOK(message.mode);
			break;
		case "cardbook.initSyncActivity":
			cardbookActivityManager.initSyncActivity(message.dirPrefId, message.name);
			break;
		case "cardbook.fetchSyncCount":
			cardbookActivityManager.fetchSyncCount(message.count);
			break;
		case "cardbook.fetchSyncActivity":
			cardbookActivityManager.fetchSyncActivity(message.dirPrefId, message.done, message.total);
			break;
		case "cardbook.finishSyncActivity":
			cardbookActivityManager.finishSyncActivity(message.dirPrefId);
			break;
		case "cardbook.finishSyncActivityOK":
			cardbookActivityManager.finishSyncActivityOK(message.dirPrefId, message.name);
			break;
		case "cardbook.addActivity":
			cardbookActivityManager.addActivity(message.code, message.array, message.icon);
			break;
		case "cardbook.addActivityFromUndo":
			cardbookActivityManager.addActivityFromUndo(message.actionCode, message.actionName);
			break;
		case "cardbook.notifyObserver":
			Services.obs.notifyObservers(null, message.value, message.params);
			break;
		case "cardbook.callDirPicker": {
			let callDirPicker = new Promise( async function(resolve, reject) {
				let windowTitle = cardbookXULUtils.localizeMessage(message.title);
				let nsIFilePicker = Components.interfaces.nsIFilePicker;
				let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
				let window = Services.wm.getMostRecentWindow(null);
				fp.init(cardbookXULUtils.filePickerInit(window), windowTitle, nsIFilePicker.modeGetFolder);
				fp.open(async rv => {
					if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
						resolve({file: fp.file.path});
					} else {
						resolve();
					}
				});
			});
			return callDirPicker;
		}
		case "cardbook.callFilePicker": {
			let callFilePicker = new Promise( async function(resolve, reject) {
				let windowTitle = cardbookXULUtils.localizeMessage(message.title);
				let nsIFilePicker = Components.interfaces.nsIFilePicker;
				let fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
				let window = Services.wm.getMostRecentWindow(null);
				if (message.mode === "SAVE") {
					fp.init(cardbookXULUtils.filePickerInit(window), windowTitle, nsIFilePicker.modeSave);
				} else if (message.mode === "OPEN") {
					fp.init(cardbookXULUtils.filePickerInit(window), windowTitle, nsIFilePicker.modeOpen);
				} else if (message.mode === "DIR") {
					fp.init(cardbookXULUtils.filePickerInit(window), windowTitle, nsIFilePicker.modeGetFolder);
				}
				if (message.type === "VCF") {
					fp.appendFilter("VCF File","*.vcf");
				}
				fp.appendFilters(fp.filterAll);
				if (message.defaultFileName) {
					fp.defaultString = message.defaultFileName;
				}
				if (message.defaultDir) {
					let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
					file.initWithPath(message.defaultDir.replace("file://", ""));
					fp.displayDirectory = file.parent;
				}
				fp.open(async rv => {
					if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {
						if (message.result == "path") {
							resolve({file: fp.file.path, filename: fp.file.leafName});
						}
					}
					resolve();
				});
			});
			return callFilePicker;
		}
		case "cardbook.getFilesFromDir": {
			try {
				let dir = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
				dir.initWithPath(message.url);
				let files = cardbookXULUtils.getFilesFromDir(dir.path);
				return files;
			} catch(e) { return "" }
		}
		case "cardbook.readContentFromFile": {
			try {
				let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
				file.initWithPath(message.url);
				let content = await cardbookXULUtils.readContentFromFile(file.path);
				return content;
			} catch(e) { return "" }
		}
		case "cardbook.getLeafName": {
			let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(message.url);
			return file.leafName;
		}
		case "cardbook.deleteFile": {
			let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(message.url);
			file.remove(true);
			break;
		}
		case "cardbook.writeContentToFile": {
			let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(message.url);
			return await cardbookXULUtils.writeContentToFile(file.path, message.data, message.utf8);
		}
		case "cardbook.importCards": {
			await cardbookMigrate.importCards(message.sourceId, message.targetId, message.version, message.dateFormat, message.kindCustom, message.memberCustom);
			break;
		}
		case "cardbook.getSystemPref":
			if (message.type == "boolean") {
				return Services.prefs.getBoolPref(message.value);
			} else if (message.type == "string") {
				return Services.prefs.getStringPref(message.value);
			} else if (message.type == "int") {
				return Services.prefs.getIntPref(message.value);
			}
			return "";
		case "cardbook.getStandardNameForEmail": {
			for (let ab of MailServices.ab.directories) {
				try {
					let card = ab.cardForEmailAddress(message.email);
					if (card) {
						if (card.getProperty("PreferDisplayName", "1") == "1") {
							return card.displayName;
						}
					}
				} catch (ex) {}
			}
			return null
		}
		case "cardbook.getDensity":
			return Services.prefs.getIntPref("mail.uidensity", 1);
		case "cardbook.getCollectedStandardAB":
			// Thunderburd bug
			// return Services.prefs.getStringPref("ldap_2.servers.history.uid");
			var ABBundle = Services.strings.createBundle("chrome://messenger/locale/addressbook/addressBook.properties");
			return ABBundle.GetStringFromName("ldap_2.servers.history.description");
		case "cardbook.getLDAPStandardAB": {
			let localAB = [];
			let prefs = Services.prefs.getChildList("ldap_2.servers");
			prefs = prefs.filter(x => x.endsWith("filename"));
			for (let pref of prefs) {
				if (Services.prefs.getStringPref(pref, "").includes("ldap")) {
					let uid = pref.replace(/filename$/, "uid");
					localAB.push(Services.prefs.getStringPref(uid));
				}
			}
			return localAB; }
		case "cardbook.getRemoteStandardAB":
			let remoteAB = [];
			let prefs = Services.prefs.getChildList("ldap_2.servers");
			prefs = prefs.filter(x => x.endsWith("carddav.url"));
			for (let pref of prefs) {
				let user = pref.replace(/carddav.url$/, "carddav.username");
				let name = pref.replace(/carddav.url$/, "description");
				let uid = pref.replace(/carddav.url$/, "uid");
				remoteAB.push({"user": Services.prefs.getStringPref(user),
								"url": Services.prefs.getStringPref(pref), 
								"name": Services.prefs.getStringPref(name),
								"uid": Services.prefs.getStringPref(uid)});
			}
			return remoteAB;
		case "cardbook.localizeLogin":
			cardbookXULPasswordManager.localizeLogin(message.loginName);
			break;
		case "cardbook.getStoredPwd":
			return cardbookXULPasswordManager.getStoredPwd(message.loginName);
		case "cardbook.storeKey":
			return await cardbookXULPasswordManager.storeKey(message.loginName, message.key);
		case "cardbook.getPassword":
			return cardbookXULPasswordManager.getPassword(message.user, message.url);
		case "cardbook.getDomainPassword":
			return cardbookXULPasswordManager.getDomainPassword(message.domain);
		case "cardbook.removePassword":
			cardbookXULPasswordManager.removePassword(message.user, message.url);
			break;
		case "cardbook.rememberPassword":
			await cardbookXULPasswordManager.rememberPassword(message.user, message.url, message.pwd, message.save);
			break;
		case "cardbook.getTimezoneIds":
			let timezoneIds = [];
			for (let tzid of cal.timezoneService.timezoneIds) {
				timezoneIds.push([cal.timezoneService.getTimezone(tzid).displayName, tzid]);
			}
			return timezoneIds;
		case "cardbook.openExternalURL": {
			let url = message.link.replace(/\s*/g, "");
			cardbookXULUtils.openExternalURL(url);
			break;
		}
		case "cardbook.migratePrefsFromXul":
			if (!Services.prefs.getBoolPref("extensions.cardbook.optionsMigrated", false)) {
				console.log("Starting migration");
				let result = await notifyTools.notifyBackground({query: "cardbook.pref.migrateClear"});
				if (result == "KO") {
					return
				}
				let oldPrefix = "extensions.cardbook."
				let allPrefs = Services.prefs.getChildList(oldPrefix);
				for (let pref of allPrefs) {
					let startReg = new RegExp("^" + oldPrefix);
					let newPref = pref.replace(startReg, "");
					let type = Services.prefs.getPrefType(pref);
					let value = "";
					let result = "KO";
					switch (type) {
						case Services.prefs.PREF_STRING:
							value = Services.prefs.getStringPref(pref);
							result = await notifyTools.notifyBackground({query: "cardbook.pref.migrateString", key: newPref, value: value})
							break;
						case Services.prefs.PREF_INT:
							value = Services.prefs.getIntPref(pref);
							result = await notifyTools.notifyBackground({query: "cardbook.pref.migrateString", key: newPref, value: value})
							break;
						case Services.prefs.PREF_BOOL:
							value = Services.prefs.getBoolPref(pref);
							result = await notifyTools.notifyBackground({query: "cardbook.pref.migrateString", key: newPref, value: value})
							break;
						default:
							result = "OK";
					}
					if (result == "KO") {
						console.log("FAILED migration for : prefName : " + newPref + " : prefValue : " + value);
					} else {
						console.log("OK migration for : prefName : " + newPref + " : prefValue : " + value);
					}
				}
				console.log("migration OK");
				Services.prefs.setBoolPref("extensions.cardbook.optionsMigrated", true);
			} else {
				console.log("no migration");
			}
			break;			
		case "cardbook.setThunderbirdPrefs":
			// force to have nice svg
			Services.prefs.setBoolPref("svg.context-properties.content.enabled", true);
			break;
		case "cardbook.getDefaultRegion":
			return Services.prefs.getStringPref("browser.search.region");
		case "cardbook.addAttachments":
			cardbookAttachmentUtils.loadAttachment(message.attachment, message.dirPrefId);
			break;
		case "cardbook.finishImport": {
			let win = Services.wm.getMostRecentWindow("mail:3pane", true);
			Services.prompt.alert(win, message.title, message.message);
			break;
		}
		case "cardbook.addCardToEmails": {
			cardbookXULCardEmails.addCardToEmails(message.card);
			break;
		}
		case "cardbook.removeCardFromEmails": {
			cardbookXULCardEmails.removeCardFromEmails(message.card);
			break;
		}
		case "cardbook.addCardToAccounts": {
			cardbookXULCardEmails.addCardToAccounts(message.account);
			break;
		}
		case "cardbook.setUncat": {
			cardbookXULCardEmails.setUncat(message.uncat);
			break;
		}
	}
});
notifyTools.notifyBackground({query: "cardbook.CardBookXULNotifyListenerReady"});
