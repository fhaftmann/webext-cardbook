var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js", this);

var EXPORTED_SYMBOLS = ["cardbookXULPasswordManager"];
var cardbookXULPasswordManager = {

    logins: {},
	oauthPrefix: "chrome://cardbook/oauth",
    oauthPrefixGOOGLE2: "chrome://cardbook/oauth/people",
	oauthPrefixGOOGLE3: "chrome://cardbook/oauth/directory",

	getRootUrl: function (aUrl) {
		try {
			var urlArray1 = aUrl.split("://");
			var urlArray2 = urlArray1[1].split("/");
			if (urlArray1[0] != "http" && urlArray1[0] != "https") {
				return "";
			}
			return urlArray1[0] + "://" + urlArray2[0];
		}
		catch (e) {
			return "";
		}
	},

	getDomainPassword: function (aDomain) {
		let password = "";
		let foundLogins = Services.logins.findLogins("smtp://smtp." + aDomain, "", "");
		if (foundLogins.length > 0) {
			password = foundLogins[0].password;
		}
		return password;
	},

	getPassword: function (aUsername, aUrl) {
		let url = cardbookXULPasswordManager.getRootUrl(aUrl);
		if (cardbookXULPasswordManager.logins[aUsername] && cardbookXULPasswordManager.logins[aUsername][url]) {
			return cardbookXULPasswordManager.logins[aUsername][url];
		} else {
			if (aUrl == cardbookXULPasswordManager.oauthPrefixGOOGLE2 ||
				aUrl == cardbookXULPasswordManager.oauthPrefixGOOGLE3) {
				var logins = Services.logins.findLogins(aUrl, "User Refresh Token", null);
			} else {
				var logins = Services.logins.findLogins(cardbookXULPasswordManager.getRootUrl(aUrl), "User login", null);
			}
			for (var i = 0; i < logins.length; i++) {
				if (logins[i].username == aUsername) {
					return logins[i].password;
				}
			}
		}
		return "";
	},

	addPassword: async function (aUsername, aUrl, aPassword) {
		var nsLoginInfo = new Components.Constructor("@mozilla.org/login-manager/loginInfo;1", Components.interfaces.nsILoginInfo, "init");
		if (aUrl.startsWith(cardbookXULPasswordManager.oauthPrefix)) {
			// google case
			var login_info = new nsLoginInfo(aUrl, "User Refresh Token", null, aUsername, aPassword, "", "");
		} else {
			var login_info = new nsLoginInfo(cardbookXULPasswordManager.getRootUrl(aUrl), "User login", null, aUsername, aPassword, "", "");
		}
		if (Services.logins.addLoginAsync) {
			await Services.logins.addLoginAsync(login_info);
		} else {
			await Services.logins.addLogin(login_info);
		}
		return true;
	},

	removePassword: function (aUsername, aUrl) {
		if (aUrl.startsWith(cardbookXULPasswordManager.oauthPrefix)) {
			// google case
			var logins = Services.logins.findLogins(aUrl, "User Refresh Token", null);
		} else {
			var logins = Services.logins.findLogins(cardbookXULPasswordManager.getRootUrl(aUrl), "User login", null);
		}
		for (var i = 0; i < logins.length; i++) {
			if (logins[i].username == aUsername) {
				Services.logins.removeLogin(logins[i]);
				return true;
			}
		}
		return false;
	},

	rememberPassword: async function (aUsername, aUrl, aPassword, aSave) {
		if (aUsername && aPassword) {
			if (aSave) {
				cardbookXULPasswordManager.removePassword(aUsername, aUrl);
				await cardbookXULPasswordManager.addPassword(aUsername, aUrl, aPassword);
			} else {
				cardbookXULPasswordManager.logins[aUsername] = {};
				cardbookXULPasswordManager.logins[aUsername][cardbookXULPasswordManager.getRootUrl(aUrl)] = aPassword;
			}
		}
	},

	localizeLogin: function (loginName) {
		var correctLogins = Services.logins.findLogins(loginName, null, cardbookXULUtils.localizeMessage("keyDescription"));
		if (correctLogins.length > 0) {
			return;
		}
		var uncorrectLogins = Services.logins.findLogins(loginName, null, "");
		if (uncorrectLogins.length == 0) {
			return;
		} else {
			var baseLogin = uncorrectLogins[0];
		}
		var newLogin = Components.classes["@mozilla.org/login-manager/loginInfo;1"].createInstance(Components.interfaces.nsILoginInfo);
		newLogin.init(baseLogin.hostname, null, cardbookXULUtils.localizeMessage("keyDescription"), "", baseLogin.password, "", "");
		Services.logins.modifyLogin(baseLogin, newLogin);
	},

	getStoredPwd: function (loginName) {
		try {
			var foundLogins = Services.logins.findLogins(loginName, null, "");
			if (foundLogins.length > 0) {
				return foundLogins[0].password;
			}
		}
		catch(e) {}
		return null;
	},

	storeKey: async function (loginName, key) {
		var foundLogins = Services.logins.findLogins(loginName, null, "");
		for (let login of foundLogins) {
			Services.logins.removeLogin(login);
		}
		var newLogin = Components.classes["@mozilla.org/login-manager/loginInfo;1"].createInstance(Components.interfaces.nsILoginInfo);
		newLogin.init(loginName, null, cardbookXULUtils.localizeMessage("keyDescription"), "", key, "", "");
		if (Services.logins.addLoginAsync) {
			await Services.logins.addLoginAsync(newLogin);
		} else {
			await Services.logins.addLogin(newLogin);
		}
	},
};
