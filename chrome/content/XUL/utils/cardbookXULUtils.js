var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { MailServices } = ChromeUtils.importESModule("resource:///modules/MailServices.sys.mjs");
var { ExtensionParent } = ChromeUtils.importESModule("resource://gre/modules/ExtensionParent.sys.mjs");

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

if ("undefined" == typeof(cardbookXULUtils)) {
    var EXPORTED_SYMBOLS = ["cardbookXULUtils"];
	var cardbookXULUtils = {

		filePickerInit: function(aWindow) {
			if (!aWindow) return null;
			if (Services.vc.compare(Services.appinfo.version, "125") >= 0) {
				return aWindow.browsingContext;
			}
			return aWindow;
		},
	
		localizeMessage: function(aMessage, aParams) {
			let extension = ExtensionParent.GlobalManager.getExtension("cardbook@vigneau.philippe");
			return extension.localeData.localizeMessage(aMessage, aParams);
		},

		arrayUnique: function (array) {
			let a = array.concat();
			for (var i=0; i<a.length; ++i) {
				for (var j=i+1; j<a.length; ++j) {
					if (a[i] == a[j])
						a.splice(j--, 1);
				}
			}
			return a;
		},
	
		cleanArray: function (vArray, trim=true) {
			let newArray = [];
			for(let i = 0; i<vArray.length; i++){
				if (vArray[i] && vArray[i] != ""){
					if (trim) {
						newArray.push(vArray[i].trim());
					} else {
						newArray.push(vArray[i]);
					}
				}
			}
			return newArray;
		},
		
		normalizeString: function (aString) {
			return aString.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
		},
	
		makeSearchString: function (aString) {
			return cardbookXULUtils.normalizeString(aString.replace(/([\\\/\:\*\?\"\'\-\<\>\| ]+)/g, "").toLowerCase());
		},
	
		getTextColorFromBackgroundColor: function (aHexBackgroundColor) {
			function hexToRgb(aColor) {
				var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(aColor);
				return result ? {r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16)} : null;
			}
	
			let rgbColor = hexToRgb(aHexBackgroundColor);
			// http://www.w3.org/TR/AERT#color-contrast
			let o = Math.round(((parseInt(rgbColor.r) * 299) + (parseInt(rgbColor.g) * 587) + (parseInt(rgbColor.b) * 114)) / 1000);
			let fore = (o > 125) ? "black" : "white";
			return fore;
		},
	
		getABIconType: function (aType) {
			switch(aType) {
				case "DIRECTORY":
					return "directory";
					break;
				case "FILE":
					return "file";
					break;
				case "LOCALDB":
					return "localdb";
					break;
				case "APPLE":
				case "CARDDAV":
				case "GOOGLE2":
				case "GOOGLE3":
				case "OFFICE365":
				case "YAHOO":
					return "remote";
					break;
				case "SEARCH":
					return "search";
					break;
				case "ALL":
					return [ "directory", "file", "localdb", "remote", "search" ];
					break;
			};
			return aType.toLowerCase();
		},

		lPad: function (aValue) {
			return ("0" + aValue).slice(-2);
		},

		splitLine: function (vString) {
			let lLineLength = 75;
			let lResult = "";
			while (vString.length) {
				if (lResult == "") {
					lResult = vString.substr(0, lLineLength);
					vString = vString.substr(lLineLength);
				} else {
					lResult = lResult + "\r\n " + vString.substr(0, lLineLength - 1);
					vString = vString.substr(lLineLength - 1);
				}
			}
			return lResult;
		},
	
		getFilePath: function(aPath1, aPath2) {
			let file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
			file.initWithPath(aPath1);
			file.append(aPath2);
			return file.path;
		},

		readContentFromFile: async function (aFilePath) {
			try {
				let win = Services.wm.getMostRecentWindow("mail:3pane", true);
				let result = await win.IOUtils.readUTF8(aFilePath);
				return result;
			} catch(e) {
				console.log(e);
				return "";
			};
		},
	
		writeContentToFile: async function (aFileName, aContent, aConvertion) {
            try {
                let win = Services.wm.getMostRecentWindow("mail:3pane", true);
                if (aConvertion == "UTF8") {
                    await win.IOUtils.writeUTF8(aFileName, aContent);
                } else {
                    let len = aContent.length;
                    let bytes = new Uint8Array(len);
                    for (let i = 0; i < len; i++) {
                        bytes[i] = aContent.charCodeAt(i);
                    }
                    await win.IOUtils.write(aFileName, new Uint8Array(bytes.buffer));
                }
				return "OK";
            }
            catch (e) {
                console.log("cardbookXULUtils.writeContentToFile error : filename : " + aFileName + ", error : " + e, "Error");
				return "KO";
            }
        },

		getFilesFromDir: function (aDirName) {
			let listOfFileName = [];
			try {
				let directory = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsIFile);
				directory.initWithPath(aDirName);
				let files = directory.directoryEntries;
				while (files.hasMoreElements()) {
					let file = files.getNext().QueryInterface(Components.interfaces.nsIFile);
					if (file.isFile()) {
						if (cardbookXULUtils.getFileNameExtension(file.leafName) == "vcf") {
							listOfFileName.push(file.path);
						}
					}
				}
			} catch(e) {}
			return listOfFileName;
		},

		sumElements: function (aObject) {
            let sum = 0;
            for (var i in aObject) {
                sum = sum + aObject[i];
            }
            return sum;
        },

		getAccountId: function(aAccount) {
			let result = aAccount.split("::");
			if (result) {
				return result[0];
			} else {
				return aAccount;
			}
		},
	
		getNodeName: function(aAccountId) {
			let tmpArray = aAccountId.split("::");
			return tmpArray[tmpArray.length - 1];
		},
	
		getName: function (aCard, aShowNameAs) {
			if (aCard.isAList || aShowNameAs == "DSP") {
				return aCard.fn;
			}
			if (aCard.lastname != "" && aCard.firstname != "") {
				let result = "";
				if (aShowNameAs == "LF") {
					result = aCard.lastname + " " + aCard.firstname;
				} else if (aShowNameAs == "FL") {
					result = aCard.firstname + " " + aCard.lastname;
				} else if (aShowNameAs == "LFCOMMA") {
					result = aCard.lastname + ", " + aCard.firstname;
				}
				return result.trim();
			} else {
				return aCard.fn;
			}
		},

		sortMultipleArrayByString: function (aArray, aIndex, aInvert) {
			function compare(a, b) { return a[aIndex].localeCompare(b[aIndex])*aInvert; };
			return aArray.sort(compare);
		},
	
		getMimeEmailsFromCards: function (aListOfCards, aOnlyEmail = false) {
			let result = [];
			for (let card of aListOfCards) {
				for (let email of card.emails) {
					if (aOnlyEmail) {
						result.push(email);
					} else {
						result.push(MailServices.headerParser.makeMimeAddress(card.fn, email));
					}
				}
			}
			return result;
		},
	
		verifyABRestrictions: function (aDirPrefId, aSearchAB, aABExclRestrictions, aABInclRestrictions) {
            if (aABExclRestrictions[aDirPrefId]) {
                return false;
            }
            if (((aABInclRestrictions.length == 0) && ((aSearchAB == aDirPrefId) || (aSearchAB === "allAddressBooks"))) ||
                ((aABInclRestrictions.length > 0) && ((aSearchAB == aDirPrefId) || ((aSearchAB === "allAddressBooks") && aABInclRestrictions[aDirPrefId])))) {
                return true;
            } else {
                return false;
            }
        },

        verifyCatRestrictions: function (aDirPrefId, aCategory, aSearchInput, aABExclRestrictions, aCatExclRestrictions, aCatInclRestrictions) {
            if (aABExclRestrictions[aDirPrefId]) {
                return false;
            }
            if (aCatExclRestrictions[aDirPrefId] && aCatExclRestrictions[aDirPrefId][aCategory]) {
                return false;
            }
            if (((!(aCatInclRestrictions[aDirPrefId])) && (aCategory.replace(/[\s+\-+\.+\,+\;+]/g, "").toUpperCase().indexOf(aSearchInput) >= 0 || aSearchInput == "")) ||
                    ((aCatInclRestrictions[aDirPrefId]) && (aCatInclRestrictions[aDirPrefId][aCategory]))) {
                return true;
            } else {
                return false;
            }
        },

        addToCardBookMenuSubMenu: async function(aDocument, aMenuName, aIdentityKey, aCallback, aProperties) {
            try {
				let ABInclRestrictions = {};
				let ABExclRestrictions = {};
				let catInclRestrictions = {};
				let catExclRestrictions = {};

				async function _loadRestrictions(aIdentityKey) {
					var result = [];
					result = await notifyTools.notifyBackground({query: "cardbook.pref.getAllRestrictions"});
					ABInclRestrictions = {};
					ABExclRestrictions = {};
					catInclRestrictions = {};
					catExclRestrictions = {};
					if (!aIdentityKey) {
						ABInclRestrictions["length"] = 0;
						return;
					}
					for (var i = 0; i < result.length; i++) {
						var resultArray = result[i];
						if ((resultArray[0] == "true") && (resultArray[3] != "") && ((resultArray[2] == aIdentityKey) || (resultArray[2] == "allMailAccounts"))) {
							if (resultArray[1] == "include") {
								ABInclRestrictions[resultArray[3]] = 1;
								if (resultArray[4]) {
									if (!(catInclRestrictions[resultArray[3]])) {
										catInclRestrictions[resultArray[3]] = {};
									}
									catInclRestrictions[resultArray[3]][resultArray[4]] = 1;
								}
							} else {
								if (resultArray[4]) {
									if (!(catExclRestrictions[resultArray[3]])) {
										catExclRestrictions[resultArray[3]] = {};
									}
									catExclRestrictions[resultArray[3]][resultArray[4]] = 1;
								} else {
									ABExclRestrictions[resultArray[3]] = 1;
								}
							}
						}
					}
					ABInclRestrictions["length"] = cardbookXULUtils.sumElements(ABInclRestrictions);
				};

				await _loadRestrictions(aIdentityKey);

				var popup = aDocument.getElementById(aMenuName);
				while (popup.hasChildNodes()) {
					popup.lastChild.remove();
				}
				let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
				for (let account of accounts) {
					if (account[2] && !account[4] && (account[3] != "SEARCH")) {
						var myDirPrefId = account[1];
						if (cardbookXULUtils.verifyABRestrictions(myDirPrefId, "allAddressBooks", ABExclRestrictions, ABInclRestrictions)) {
							var menuItem = aDocument.createXULElement("menuitem");
							menuItem.setAttribute("id", account[1]);
							for (let prop in aProperties) {
								menuItem.setAttribute(prop, aProperties[prop]);
							}
							menuItem.addEventListener("command", function(aEvent) {
									// first case add an attachment
									if (this.hasAttribute("emailAttachment")) {
										aCallback(this.id);
									} else {
										let headerField = aEvent.currentTarget.parentNode.parentNode.parentNode.headerField;
										aCallback(this.id, headerField.emailAddress, headerField.displayName);
									}
									aEvent.stopPropagation();
								}, false);
							menuItem.setAttribute("label", account[0]);
							popup.appendChild(menuItem);
						}
					}
				}
			}
			catch (e) {
				let errorTitle = "addToCardBookMenuSubMenu";
				Services.prompt.alert(null, errorTitle, e);
			}
		},

		openExternalURL: function (aUrl) {
			let uri = Services.io.newURI(aUrl, null, null);
			let externalProtocolService = Components.classes["@mozilla.org/uriloader/external-protocol-service;1"].getService(Components.interfaces.nsIExternalProtocolService);
			externalProtocolService.loadURI(uri, null);
		},
	
		addCardToIMPPMenuSubMenu: async function(aDocument, aCard, aMenuName) {
			try {
				if (!aDocument.getElementById(aMenuName)) {
					return;
				}
				let popup = aDocument.getElementById(aMenuName);
				let menu = aDocument.getElementById(aMenuName.replace("MenuPopup", ""));
				while (popup.hasChildNodes()) {
					popup.lastChild.remove();
				}
				
				menu.disabled = true;
				if (aCard) {
					let telProtocolLine = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "tels.0"});
					let preferIMPPPref = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "preferIMPPPref"});
					let rowNumber = 0;
					if (telProtocolLine) {
						let telProtocolLineArray = telProtocolLine.split(":");
						let telLabel = telProtocolLineArray[1];
						let telProtocol = telProtocolLineArray[2];
						let tels = await notifyTools.notifyBackground({query: "cardbook.getPrefAddressFromCard", card: aCard, type: "tel", pref: preferIMPPPref});
						for (let i = 0; i < tels.length; i++) {
							let menuItem = aDocument.createXULElement("menuitem");
							let myRegexp = new RegExp("^" + telProtocol + ":");
							let myAddress = tels[i].replace(myRegexp, "");
							menuItem.setAttribute("id", rowNumber);
							menuItem.addEventListener("command", async function(aEvent) {
                                    await notifyTools.notifyBackground({query: "cardbook.connectTel", tel: this.value});
									aEvent.stopPropagation();
								}, false);
							menuItem.setAttribute("label", telLabel + ": " + myAddress);
							menuItem.setAttribute("value", myAddress);
							popup.appendChild(menuItem);
							rowNumber++;
							menu.disabled = false;
						}
					}
                    let IMPPs = await notifyTools.notifyBackground({query: "cardbook.getPrefAddressFromCard", card: aCard, type: "impp", pref: preferIMPPPref});
					for (let i = 0; i < IMPPs.length; i++) {
						let serviceProtocol = await notifyTools.notifyBackground({query: "cardbook.getIMPPProtocol", value: [IMPPs[i]]});
						let serviceLine = await notifyTools.notifyBackground({query: "cardbook.getIMPPLineForProtocol", protocol: serviceProtocol});
						if (serviceLine[0]) {
							let menuItem = aDocument.createXULElement("menuitem");
							let myRegexp = new RegExp("^" + serviceLine[2] + ":");
							let myAddress = IMPPs[i].replace(myRegexp, "");
							menuItem.setAttribute("id", rowNumber);
							menuItem.addEventListener("command", async function(aEvent) {
									cardbookXULUtils.openExternalURL(this.value);
									aEvent.stopPropagation();
								}, false);
							menuItem.setAttribute("label", serviceLine[1] + ": " + myAddress);
							menuItem.setAttribute("value", serviceLine[2] + ":" + myAddress);
							popup.appendChild(menuItem);
							rowNumber++;
							menu.disabled = false;
						}
					}
				}
				if (!popup.hasChildNodes()) {
					menu.disabled=true;
				}
			}
			catch (e) {
				var errorTitle = "addCardToIMPPMenuSubMenu";
				Services.prompt.alert(null, errorTitle, e);
			}
		},

		getFileExtension: function (aFile) {
			let fileArray = aFile.split("/");
			let fileArray1 = fileArray[fileArray.length-1].split("\\");
			return cardbookXULUtils.getFileNameExtension(fileArray1[fileArray1.length-1]);
		},
	
		getFileNameExtension: function (aFileName) {
			let fileArray = aFileName.split(".");
			let myExtension = "";
			if (fileArray.length != 1) {
				myExtension = fileArray[fileArray.length - 1];
			}
			return myExtension;
		},
		
		getImageFromURI: function (aImageURI) {
			return new Promise((resolve, reject) => {
				let xhr = new XMLHttpRequest();
				xhr.responseType = "arraybuffer";
				xhr.onload = function () {
					if (xhr.status === 200) {
						let uInt8Array = new Uint8Array(this.response);
						let i = uInt8Array.length;
						let binaryString = new Array(i);
						while (i--) {
							binaryString[i] = String.fromCharCode(uInt8Array[i]);
						}
						let data = binaryString.join("");
						let base64 = btoa(data);
						resolve(base64);
					} else {
						reject();
					}
				};
				xhr.onerror = function () {
					reject();
				};
				xhr.ontimeout = function () {
					reject();
				};
				xhr.open("GET", aImageURI, true);
				xhr.send();
			});
		},
	}
};
