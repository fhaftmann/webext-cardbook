var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBCard = {
	cardbookDatabaseVersion: "7",
	cardbookDatabaseName: "CardBook",
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBCard.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});

			var request = indexedDB.open(cardbookIDBCard.cardbookDatabaseName, cardbookIDBCard.cardbookCardDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (e.oldVersion < 1) {
					let store = db.createObjectStore("cards", {keyPath: "cbid", autoIncrement: false});
					store.createIndex("cacheuriIndex", "cacheuri", { unique: false });
				}
				if (e.oldVersion < 7) {
					let store = request.transaction.objectStore("cards");
					store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBCard.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCard.db.transaction(["cards"], "readonly");
			var store = transaction.objectStore("cards");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var card of result) {
						card = await cardbookIDBCard.checkCard(card);
						finalResult.push(card);
					}
				}
				cardbookIDBCard.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBCard.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},

	// check if the card is in a wrong encryption state
	// then decrypt the card if possible
	checkCard: async function(aCard) {
		try {
			var stateMismatched = cardbookIDBCard.encryptionEnabled != 'encrypted' in aCard;
			var versionMismatched = aCard.encryptionVersion && aCard.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aCard) {
					aCard = await cardbookEncryptor.decryptCard(aCard);
				}
			} else {
				if ('encrypted' in aCard) {
					aCard = await cardbookEncryptor.decryptCard(aCard);
				}
			}
			return aCard;
		}
		catch(e) {
			throw new Error("failed to decrypt the card : " + e);
		}
	},
};
