var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var cardbookIDBDuplicate = {
	cardbookDuplicateDatabaseVersion: "9",
	cardbookDuplicateDatabaseName: "cardbookDuplicate",
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			var request = indexedDB.open(cardbookIDBDuplicate.cardbookDuplicateDatabaseName, cardbookIDBDuplicate.cardbookDuplicateDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
                if (e.oldVersion < 9) {
                    if (db.objectStoreNames.contains("duplicate")) {
                        db.deleteObjectStore("duplicate");
                    }
                    let store = db.createObjectStore("duplicate", {keyPath: "duplicateId", autoIncrement: false});
                }
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBDuplicate.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBDuplicate.db.transaction(["duplicate"], "readonly");
			var store = transaction.objectStore("duplicate");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var duplicate of result) {
						finalResult.push(duplicate);
					}
				}
				cardbookIDBDuplicate.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBDuplicate.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},
};
