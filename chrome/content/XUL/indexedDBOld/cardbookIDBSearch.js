var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var cardbookIDBSearch = {
	cardbookSearchDatabaseVersion: "8",
	cardbookSearchDatabaseName: "cardbookSearch",
	doUpgrade: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			var request = indexedDB.open(cardbookIDBSearch.cardbookSearchDatabaseName, cardbookIDBSearch.cardbookSearchDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (e.oldVersion < 1) {
					if (db.objectStoreNames.contains("search")) {
						db.deleteObjectStore("search");
					}
					let store = db.createObjectStore("search", {keyPath: "dirPrefId", autoIncrement: false});
				}
				if (e.oldVersion < 8) {
					cardbookIDBSearch.doUpgrade = true;
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBSearch.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["search"], "readonly");
			var store = transaction.objectStore("search");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var search of result) {
						finalResult.push(search);
					}
				}
				cardbookIDBSearch.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBSearch.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},
};
