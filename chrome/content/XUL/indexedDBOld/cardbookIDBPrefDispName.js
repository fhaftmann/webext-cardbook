var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

var loader = Services.scriptloader;
loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookIDBPrefDispName = {
	cardbookPrefDispNameDatabaseVersion: "1",
	cardbookPrefDispNameDatabaseName: "CardBookPrefDispName",
	encryptionEnabled: false,
	db: {},

	openDBForTransfert: function() {
        let openDB = new Promise( async function(resolve, reject) {
			cardbookIDBPrefDispName.encryptionEnabled = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "localDataEncryption"});

			var request = indexedDB.open(cardbookIDBPrefDispName.cardbookPrefDispNameDatabaseName, cardbookIDBPrefDispName.cardbookPrefDispNameDatabaseVersion);
			// when version changes
			// for the moment delete all and recreate one new empty
			request.onupgradeneeded = function(e) {
				var db = e.target.result;
				e.target.transaction.onerror = console.log;
				if (e.oldVersion < 1) {
					if (db.objectStoreNames.contains("prefDispName")) {
						db.deleteObjectStore("prefDispName");
					}
					let store = db.createObjectStore("prefDispName", {keyPath: "prefDispNameId", autoIncrement: true});
				}
			};
			// when success, call the observer for starting the load cache and maybe the sync
			request.onsuccess = function(e) {
				cardbookIDBPrefDispName.db = e.target.result;
				resolve();
			};
			// when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				console.log(e);
				reject();
			};
		});
		return openDB;
	},

	getDataForTransfert: function() {
        let getDataForTransfert = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBPrefDispName.db.transaction(["prefDispName"], "readonly");
			var store = transaction.objectStore("prefDispName");
			var cursorRequest = store.getAll();
			var finalResult = [];
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var prefDispName of result) {
						prefDispName = await cardbookIDBPrefDispName.checkPrefDispName(prefDispName);
						finalResult.push(prefDispName);
					}
				}
				cardbookIDBPrefDispName.db.close();
				resolve(finalResult);
			};
			cursorRequest.onerror = (e) => {
				console.log(e);
				cardbookIDBPrefDispName.db.close();
				reject(finalResult);
			};
		});
		return getDataForTransfert;
	},

	// check if the prefer display name is in a wrong encryption state
	// then decrypt the prefer display name if possible
	checkPrefDispName: async function(aPrefDispName) {
		try {
			var stateMismatched = cardbookIDBPrefDispName.encryptionEnabled != 'encrypted' in aPrefDispName;
			var versionMismatched = aPrefDispName.encryptionVersion && aPrefDispName.encryptionVersion != cardbookEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aPrefDispName) {
					aPrefDispName = await cardbookEncryptor.decryptPrefDispName(aPrefDispName);
				}
			} else {
				if ('encrypted' in aPrefDispName) {
					aPrefDispName = await cardbookEncryptor.decryptPrefDispName(aPrefDispName);
				}
			}
			return aPrefDispName;
		}
		catch(e) {
			throw new Error("failed to decrypt the prefer display name : " + e);
		}
	},
};
