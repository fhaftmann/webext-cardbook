var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

Services.scriptloader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

var cardbookPrint = {
	options: {},

	getTypes: async function (aDirPrefId, aType, aInputTypes, aPgType, aPgName, aCardValue) {
		var myInputTypes = await notifyTools.notifyBackground({query: "cardbook.getOnlyTypesFromTypes", types: aInputTypes});
		var myDisplayedTypes = [];
		if (aPgType.length != 0 && aPgName != "") {
			let label = "";
			for (let j = 0; j < aPgType.length; j++) {
				let tmpArray = aPgType[j].split(":");
				if (tmpArray[0] == "X-ABLABEL") {
					label = tmpArray[1];
					break;
				}
			}
			if (label) {
				let types = await notifyTools.notifyBackground({query: "cardbook.whichLabelTypeShouldBeChecked", type: aType, dirPrefId: aDirPrefId, types: [ label ]});
				myDisplayedTypes.push(types);
			} else {
				let types = await notifyTools.notifyBackground({query: "cardbook.whichLabelTypeShouldBeChecked", type: aType, dirPrefId: aDirPrefId, types: myInputTypes});
				myDisplayedTypes.push(types);
			}
		} else {
			let types = await notifyTools.notifyBackground({query: "cardbook.whichLabelTypeShouldBeChecked", type: aType, dirPrefId: aDirPrefId, types: myInputTypes});
			myDisplayedTypes.push(types);
		}
		if (aType == "impp") {
			let serviceCode = await notifyTools.notifyBackground({query: "cardbook.getIMPPCode", types: aInputTypes});
			let serviceProtocol = await notifyTools.notifyBackground({query: "cardbook.getIMPPProtocol", value: aCardValue});
			if (serviceCode != "") {
				let serviceLine = await notifyTools.notifyBackground({query: "cardbook.getIMPPLineForCode", protocol: serviceCode});
				if (serviceLine[0]) {
					myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
				} else {
					myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
				}
			} else if (serviceProtocol != "") {
				let serviceLine = await notifyTools.notifyBackground({query: "cardbook.getIMPPLineForProtocol", protocol: serviceProtocol});
				if (serviceLine[0]) {
					myDisplayedTypes = myDisplayedTypes.concat(serviceLine[1]);
				} else {
					myDisplayedTypes = myDisplayedTypes.concat(serviceCode);
				}
			}
		}
		return myDisplayedTypes;
	},

	attachHeaderNode: function (document, child, headerNode, headerKey) {
		if (child.querySelector(".keyheader") || child.querySelector(".key") || child.querySelector(".value")) {
			if (headerNode && cardbookPrint.options.headers && cardbookPrint.options.fieldNames) {
				let templateHeader = document.getElementById("list-item-header");
				let itemHeaderNode = templateHeader.content.cloneNode(true);
				let prefix = cardbookXULUtils.localizeMessage(headerKey);
				itemHeaderNode.querySelector(".keyheader").textContent = prefix;
				headerNode.appendChild(itemHeaderNode);
			}
		} else {
			if (headerNode) {
				headerNode.remove();
			}
			child.remove();
		}
	},

	setupLabelPrefPropertyRow: function (document, itemNode, propValue, propType, propPref, propKey) {
		let templateProperty = document.getElementById("list-item-labelprefproperties");
		let itemPropertyNode = templateProperty.content.cloneNode(true);
		if (propValue) {
			if (propPref) {
				// test to change 
				// itemPropertyNode.querySelector(".pref").classlist.add("pref");
				itemPropertyNode.querySelector(".pref").textContent = "★";
			}
			if (propType) {
				itemPropertyNode.querySelector(".type").textContent = propType;
			}
			if (!cardbookPrint.options.fieldNames) {
				let key = itemPropertyNode.querySelector(".key");
				key.remove();
			} else {
				itemPropertyNode.querySelector(".key").textContent = propKey;
			}
			itemPropertyNode.querySelector(".value").textContent = propValue;
			itemNode.appendChild(itemPropertyNode);
		}
	},

	setupPrefPropertyRow: function (document, itemNode, propValue, propType, propPref) {
		let templateProperty = document.getElementById("list-item-prefproperties");
		let itemPropertyNode = templateProperty.content.cloneNode(true);
		if (propValue) {
			if (propPref) {
				// test to change 
				// itemPropertyNode.querySelector(".pref").classList.add("pref");
				itemPropertyNode.querySelector(".pref").textContent = "★";
			}
			if (propType) {
				itemPropertyNode.querySelector(".type").textContent = propType;
			}
			itemPropertyNode.querySelector(".value").textContent = propValue;
			itemNode.appendChild(itemPropertyNode);
		}
	},

	setupPropertyRow: function (document, itemNode, propValue, propLabel) {
		let templateProperty = document.getElementById("list-item-properties");
		let itemPropertyNode = templateProperty.content.cloneNode(true);
		if (propValue) {
			if (propLabel && cardbookPrint.options.fieldNames) {
				itemPropertyNode.querySelector(".key").textContent = propLabel;
			} else {
				let key = itemPropertyNode.querySelector(".key");
				key.remove();
			}
			itemPropertyNode.querySelector(".value").textContent = propValue;
			itemNode.appendChild(itemPropertyNode);
		}
	},

	buildHTML: async function (document, cards, options) {
		for (let option in options) {
			cardbookPrint.options[option] = options[option];
		}

		let listContainer = document.getElementById("list-container");
		while (listContainer.lastChild) {
			listContainer.lastChild.remove();
		}

		let template = document.getElementById("list-item-template");
		let customFields = await notifyTools.notifyBackground({query: "cardbook.getAllCustomFields"});
		let allColumns = await notifyTools.notifyBackground({query: "cardbook.getAllColumns"});
		let dateFields = await notifyTools.notifyBackground({query: "cardbook.getDateFields"});
		let multilineFields = await notifyTools.notifyBackground({query: "cardbook.getMultilineFields"});
		let dateDisplayedFormat = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "dateDisplayedFormat"});
		let orgStructure = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "orgStructure"});
		
		let listOfSelectedCard = cards.split(",");
		let dirPrefIds = Array.from(listOfSelectedCard, card => card.dirPrefId);
		dirPrefIds = cardbookXULUtils.arrayUnique(dirPrefIds);
		cardbookPrint.result = '';

		for (let cbid of listOfSelectedCard) {
			let itemNode = template.content.firstElementChild.cloneNode(true);
			let card = await notifyTools.notifyBackground({query: "cardbook.getCard", cbid: cbid});

			// display name
			if (cardbookPrint.options.fn) {
				let titleNode = itemNode.querySelector(".titlerow");
				cardbookPrint.setupPropertyRow(document, titleNode, card.fn, "");
				cardbookPrint.attachHeaderNode(document, titleNode);
			}

			// categories
			if (cardbookPrint.options.categories) {
				let categoriesNode = itemNode.querySelector(".categoriesrow");
				for (let category of card.categories) {
					let categoryLabel = "";
					if (!cardbookPrint.options.headers) {
						categoryLabel = cardbookXULUtils.localizeMessage("categoriesLabel");
					}
					cardbookPrint.setupPropertyRow(document, categoriesNode, category, categoryLabel);
				}
				let categoriesHeaderNode = itemNode.querySelector(".categoriesheaderrow");
				cardbookPrint.attachHeaderNode(document, categoriesNode, categoriesHeaderNode, "categoriesHeader");
			}

			// personal fields
			if (cardbookPrint.options.personal) {
				let personalNode = itemNode.querySelector(".personalrow");
				for (let field of allColumns["personal"]) {
					let value = card[field];
					if (dateFields.includes(field)) {
						value = await notifyTools.notifyBackground({query: "cardbook.getFormattedDateForDateString", dateString: card[field], target: dateDisplayedFormat});
					}
					let label = cardbookXULUtils.localizeMessage(`${field}Label`)
					cardbookPrint.setupPropertyRow(document, personalNode, value, label);
				}
				if (cardbookPrint.options.custom) {
					for (let customField of customFields["personal"]) {
						let customValue1 = await notifyTools.notifyBackground({query: "cardbook.getCardValueByField", card: card, column: customField[0], includePref: false});
						let customValue = customValue1[0];
						let customLabel = customField[1];
						cardbookPrint.setupPropertyRow(document, personalNode, customValue, customLabel);
					}
				}
				let personalHeaderNode = itemNode.querySelector(".personalheaderrow");
				cardbookPrint.attachHeaderNode(document, personalNode, personalHeaderNode, "personalGroupboxLabel");
			}

			if (cardbookPrint.options.org) {
				// org fields
				let orgNode = itemNode.querySelector(".orgrow");
				if (orgStructure.length) {
					for (let l = 0; l < card.org.length; l++) {
						let label = orgStructure[l];
						let value = card.org[l];
						cardbookPrint.setupPropertyRow(document, orgNode, value, label);
					}
					cardbookPrint.setupPropertyRow(document, orgNode, card.title, cardbookXULUtils.localizeMessage("titleLabel"));
					cardbookPrint.setupPropertyRow(document, orgNode, card.role, cardbookXULUtils.localizeMessage("roleLabel"));
				} else {
					let value = card.org[0];
					let label = cardbookXULUtils.localizeMessage(`${field}Label`)
					cardbookPrint.setupPropertyRow(document, orgNode, value, label);
				}
				if (cardbookPrint.options.custom) {
					for (let customField of customFields["org"]) {
						let customValue1 = await notifyTools.notifyBackground({query: "cardbook.getCardValueByField", card: card, column: customField[0], includePref: false});
						let customValue = customValue1[0];
						let customLabel = customField[1];
						cardbookPrint.setupPropertyRow(document, orgNode, customValue, customLabel);
					}
				}
				let orgHeaderNode = itemNode.querySelector(".orgheaderrow");
				cardbookPrint.attachHeaderNode(document, orgNode, orgHeaderNode, "orgGroupboxLabel");
			}

			// multine line fields
			if (!card.isAList) {
				for (let field of multilineFields) {
					if (cardbookPrint.options[field] == true) {
						let fieldNode = itemNode.querySelector(`.${field}row`);
						for (let line of card[field]) {
							let value = line[0];
							let pref = await notifyTools.notifyBackground({query: "cardbook.getPrefBooleanFromTypes", types: line[1]});
							let types = await cardbookPrint.getTypes(card.dirPrefId, field, line[1], line[3], line[2], line[0][0]);
							let type = types.join(" ");
							if (field == "adr") {
									value = await notifyTools.notifyBackground({query: "cardbook.formatAddress", address: line[0]});
							}
							if (!cardbookPrint.options.headers) {
								let initField = field[0].toUpperCase() + field.slice(1);
								let label = cardbookXULUtils.localizeMessage(`typesCategory${initField}Label`);
								cardbookPrint.setupLabelPrefPropertyRow(document, fieldNode, value, type, pref, label);
							} else {
								cardbookPrint.setupPrefPropertyRow(document, fieldNode, value, type, pref);
							}
						}
						let fieldHeaderNode = itemNode.querySelector(`.${field}headerrow`);
						cardbookPrint.attachHeaderNode(document, fieldNode, fieldHeaderNode, `${field}GroupboxLabel`);
					}
				}
			}

			// events
			if (!card.isAList) {
				if (cardbookPrint.options.event) {
					let eventNode = itemNode.querySelector(".eventrow");
					let events = await notifyTools.notifyBackground({query: "cardbook.getEventsFromCard", cardNote: card.note.split("\n"), cardOthers: card.others});
					for (let event of events.result) {
						let pref = event[2];
						let type = await notifyTools.notifyBackground({query: "cardbook.getFormattedDateForDateString", dateString: event[0], target: dateDisplayedFormat});
						let value = event[1];
						if (!cardbookPrint.options.headers) {
							let label = cardbookXULUtils.localizeMessage("eventLabel");
							cardbookPrint.setupLabelPrefPropertyRow(document, eventNode, value, type, pref, label);
						} else {
							cardbookPrint.setupPrefPropertyRow(document, eventNode, value, type, pref);
						}
					}
					let eventHeaderNode = itemNode.querySelector(".eventheaderrow");
					cardbookPrint.attachHeaderNode(document, eventNode, eventHeaderNode, "eventGroupboxLabel");
				}
			}

			// list
			if (card.isAList) {
				let listNode = itemNode.querySelector(".listrow");
				let conversionList = await notifyTools.notifyBackground({query: "cardbook.getListConversion", email: `${card.fn} <${card.fn}>`});
				for (let email of conversionList.emailResult) {
					cardbookPrint.setupPropertyRow(document, listNode, email, "");
				}
				let listHeaderNode = itemNode.querySelector(".eventheaderrow");
				cardbookPrint.attachHeaderNode(document, listNode, listHeaderNode, "addedCardsGroupboxLabel");
			}
		
			// note
			if (cardbookPrint.options.note) {
				let noteNode = itemNode.querySelector(".noterow");
				if (!cardbookPrint.options.headers) {
					let label = cardbookXULUtils.localizeMessage("noteLabel");
					cardbookPrint.setupPropertyRow(document, noteNode, card.note, label);
				} else {
					cardbookPrint.setupPropertyRow(document, noteNode, card.note);
				}
				let noteHeaderNode = itemNode.querySelector(".noteheaderrow");
				cardbookPrint.attachHeaderNode(document, noteNode, noteHeaderNode, "noteGroupboxLabel");
			}

			listContainer.appendChild(itemNode);
		}
	}
};
