// Import any needed modules.
var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
var { TagUtils } = ChromeUtils.importESModule("resource:///modules/TagUtils.sys.mjs");
var { QuickFilterManager } = ChromeUtils.importESModule("resource:///modules/QuickFilterManager.sys.mjs");

Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/observers/cardBookAbout3PanesObserver.js", window, "UTF-8");
Services.scriptloader.loadSubScript("chrome://cardbook/content/XUL/About3Panes/cardbookAbout3Panes.js", window, "UTF-8");

// called on window load or on add-on activation while window is already open
function onLoad(wasAlreadyOpen) {
	WL.injectCSS("chrome://cardbook/content/skin/cardbookAddressBooks.css");
	WL.injectCSS("chrome://cardbook/content/skin/cardbookQFB.css");

	WL.injectElements(`

	<div id="quickFilterBarContainer">
		<html:button id="qfb-cardbook"
			is="toggle-button"
			class="button collapsible-button icon-button check-button"
			title="__MSG_cardbookQFBButtonTooltip__"
			insertafter="qfb-starred">
			<html:span>__MSG_cardbookQFBButtonLabel__</html:span>
		</html:button>
	</div>

	<div id="quickFilterBarSecondFilters">
		<div id="quickFilterBarCardBookContainer" insertafter="quickFilterBarTagsContainer" hidden="true">
			<menulist id="qfb-cardbook-boolean-mode"
						tooltiptext="__MSG_quickFilterBar.booleanMode.tooltip__"
						persist="value" value="OR">
				<menupopup id="qfb-cardbook-boolean-mode-popup">
					<menuitem id="qfb-cardbook-boolean-mode-or" value="OR"
								label="__MSG_quickFilterBar.booleanModeAny.label__"
								tooltiptext="__MSG_quickFilterBar.booleanModeAny.tooltip__"/>
					<menuitem id="qfb-cardbook-boolean-mode-and" value="AND"
								label="__MSG_quickFilterBar.booleanModeAll.label__"
								tooltiptext="__MSG_quickFilterBar.booleanModeAll.tooltip__"/>
				</menupopup>
			</menulist>
		</div>
	</div>

	`);

	window.cardbookAbout3Panes.load();
};

function onUnload(wasAlreadyOpen) {
	window.cardbookAbout3Panes.unload();
};
