if ("undefined" == typeof(cardbookFindEvents)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

	var loader = Services.scriptloader;
    loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

	var cardbookFindEvents = {

		findEventsFromEmail: function(emailAddressNode) {
			let email = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			let displayname = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			cardbookFindEvents.findEvents(null, [email], displayname);
		},

		findAllEventsFromContact: async function(emailAddressNode) {
			let email = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			let displayname = cardbookAboutMessage.getEmailFromEmailAddressNode(emailAddressNode);
			let isEmailRegistered = await notifyTools.notifyBackground({query: "cardbook.isEmailRegistered", email: email, identity: cardbookAboutMessage.getIdentityKey()});
	
			if (isEmailRegistered) {
				let card = await notifyTools.notifyBackground({query: "cardbook.getCardFromEmail", email: email});
				cardbookFindEvents.findEvents(card, null, card.fn);
			} else {
				cardbookFindEvents.findEvents(null, [email], displayname);
			}	
		},

		findEvents: async function (aCard, aListOfSelectedEmails, aDisplayName) {
			var listOfEmail = [];
			if (aCard) {
				if (!aCard.isAList) {
					for (var j = 0; j < aCard.email.length; j++) {
						listOfEmail.push(aCard.email[j][0][0].toLowerCase());
					}
				} else {
					listOfEmail.push(aCard.fn.replace('"', '\"'));
				}
			} else if (aListOfSelectedEmails) {
				listOfEmail = JSON.parse(JSON.stringify(aListOfSelectedEmails));
			}
			let url = "chrome/content/HTML/contactEvents/wdw_cardbookContactEvents.html";
			let params = new URLSearchParams();
			params.set("displayName", aDisplayName);
			params.set("listOfEmail", listOfEmail.join(","));
			let win = await notifyTools.notifyBackground({query: "cardbook.openWindow",
													url: `${url}?${params.toString()}`,
													type: "popup"});
		}
	};
};
