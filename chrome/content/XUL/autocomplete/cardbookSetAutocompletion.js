if ("undefined" == typeof(cardbookSetAutocompletion)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;
	var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");
	var { cardbookXULCSS } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULCSS.js");

    var loader = Services.scriptloader;
    loader.loadSubScript("chrome://cardbook/content/scripts/notifyTools.js", this);

    var cardbookSetAutocompletion = {
		
		createCssMsgAccountRules60: function (aStyleSheet, aStyle, aColor, aColorProperty) {
			let textRuleString = ".autocomplete-richlistitem[type=\"" + aStyle + "\"]{\
				" + aColorProperty + ": " + aColor + ";\
				}";
			let ruleIndex = aStyleSheet.insertRule(textRuleString, aStyleSheet.cssRules.length);
		},

		createCssMsgAccountSelectedRules60: function (aStyleSheet, aStyle, aColor, aColorProperty) {
			let textRuleString = ".autocomplete-richlistitem[type=\"" + aStyle + "\"][selected=\"true\"]{\
				" + aColorProperty + ": " + aColor + ";\
				}";
			let ruleIndex = aStyleSheet.insertRule(textRuleString, aStyleSheet.cssRules.length);
		},

		loadCssRules: async function () {
			try {
                let autocompleteWithColor = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "autocompleteWithColor"});
                let useColor = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "useColor"});
				let styleSheet = "chrome://cardbook/content/skin/cardbookSetAutocompletion.css";
				let styleSheetRuleName = "cardbookSetAutocompletion";
				for (let styleSheet of window.InspectorUtils.getAllStyleSheets(window.document, false)) {
					for (let rule of styleSheet.cssRules) {
						// difficult to find as the sheet as no href 
						if (rule.cssText.includes(styleSheetRuleName)) {
							cardbookXULCSS.deleteCssAllRules(styleSheet);
							cardbookXULCSS.createMarkerRule(styleSheet, styleSheetRuleName);
                            let accounts = await notifyTools.notifyBackground({query: "cardbook.getAccounts"});
							for (let account of accounts) {
								if (account[2] && account[3] != "SEARCH") {
									let dirPrefId = account[1];
                                    let color = await notifyTools.notifyBackground({query: "cardbook.pref.getColor", dirPrefId: dirPrefId});
									let oppositeColor = cardbookXULCSS.getTextColorFromBackgroundColor(color);
									let style = cardbookXULUtils.getABIconType(account[3]) + "_color_" + dirPrefId + "-abook";
									if (useColor == "text" && autocompleteWithColor) {
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, color, "color");
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, oppositeColor, "background-color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "HighlightText", "color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "Highlight", "background-color");
									} else if (useColor == "background" && autocompleteWithColor) {
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, color, "background-color");
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, oppositeColor, "color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "Highlight", "background-color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "HighlightText", "color");
									}
								}
							}
                            let nodeColors = await notifyTools.notifyBackground({query: "cardbook.getCardbookNodeColors"});
							for (let category in nodeColors) {
								let color = nodeColors[category];
								let oppositeColor = cardbookXULCSS.getTextColorFromBackgroundColor(color);
								for (let type of cardbookXULUtils.getABIconType("ALL")) {
									let style =  type + "_color_category_" + cardbookXULCSS.formatCategoryForCss(category) + "-abook";
									if (useColor == "text" && autocompleteWithColor) {
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, color, "color");
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, oppositeColor, "background-color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "HighlightText", "color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "Highlight", "background-color");
									} else if (useColor == "background" && autocompleteWithColor) {
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, color, "background-color");
										cardbookSetAutocompletion.createCssMsgAccountRules60(styleSheet, style, oppositeColor, "color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "Highlight", "background-color");
										cardbookSetAutocompletion.createCssMsgAccountSelectedRules60(styleSheet, style, "HighlightText", "color");
									}
								}
							}
							return;
						}
					}
				}
            } catch(e) {
                console.log("cardbookSetAutocompletion.loadCssRules error : " + e);
            }
        },

		setCompletion: async function(aWindow, aIsLightning) {
			try {
                let autocompletion = await notifyTools.notifyBackground({query: "cardbook.pref.getPref", pref: "autocompletion"});
				let nodes = aWindow.querySelectorAll("input");
				for (let node of nodes) {
					if (node.getAttribute("autocompletesearch")) {
						if (aIsLightning) {
							node.addEventListener("change", async function() {
									await cardbookSetAutocompletion.setLightningCompletion();
								}, false);
						}
						let attrArray = node.getAttribute("autocompletesearch").split(" ");
						if (autocompletion) {
							let index1 = attrArray.indexOf("addrbook");
							if (index1 > -1) {
								attrArray.splice(index1, 1);
							}
							let index2 = attrArray.indexOf("ldap");
							if (index2 > -1) {
								attrArray.splice(index2, 1);
							}
							if (attrArray.indexOf("addrbook-cardbook") == -1) {
								attrArray.push("addrbook-cardbook");
							}
							let resultArray = cardbookXULUtils.cleanArray(attrArray);
                            let result = resultArray.join(" ");
                            let autocompletesearch = node.getAttribute("autocompletesearch");
                            if (result != autocompletesearch) {
							    node.setAttribute("autocompletesearch", result);
                            }
						} else {
							let index1 = attrArray.indexOf("addrbook-cardbook");
							if (index1 > -1) {
								attrArray.splice(index1, 1);
							}
							if (attrArray.indexOf("addrbook") == -1) {
								attrArray.push("addrbook");
							}
							if (attrArray.indexOf("ldap") == -1) {
								attrArray.push("ldap");
							}
							let resultArray = cardbookXULUtils.cleanArray(attrArray);
                            let result = resultArray.join(" ");
                            let autocompletesearch = node.getAttribute("autocompletesearch");
                            if (result != autocompletesearch) {
							    node.setAttribute("autocompletesearch", result);
                            }
						}
					}
				}
			} catch(e) {
				console.log("cardbookSetAutocompletion.setCompletion error : " + e, "Error");
			};
		},

		setLightningCompletion: async function() {
			await cardbookSetAutocompletion.setCompletion(document.getElementById("calendar-event-dialog-attendees-v2"), true);
		},

		setMsgCompletion: async function() {
			await cardbookSetAutocompletion.setCompletion(document.getElementById("msgcomposeWindow"), false);
		}

	};
};
