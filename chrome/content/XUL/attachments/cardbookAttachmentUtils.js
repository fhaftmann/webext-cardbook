if ("undefined" == typeof(cardbookAttachmentUtils)) {
	var Services = globalThis.Services || ChromeUtils.import("resource://gre/modules/Services.jsm").Services;

	var cardbookAttachmentUtils = {
		loadAttachment: async function(aAttachment, aDirPrefId) {
			let myFileArray = aAttachment.name.split(".");
			let myExtension =  myFileArray[myFileArray.length-1];
			if (myExtension.toLowerCase() == "vcf") {
                let attachmentId = aAttachment.partID || aAttachment.partName;
				let content = await notifyTools.notifyBackground({query: "cardbook.attachments.getAttachmentContent", attachmentId: attachmentId});
				await notifyTools.notifyBackground({query: "cardbook.attachments.importCardsFromFile", dirPrefId: aDirPrefId, content: content, filename: aAttachment.name});
			}
		},

		importFileIntoCardBook: async function(aDirPrefId) {
			var attachmentList = document.getElementById('attachmentList');
			var selectedAttachments = attachmentList.selectedItems;
			if (selectedAttachments.length == 0) {
				for (var i = 0; i < currentAttachments.length; i++) {
					await cardbookAttachmentUtils.loadAttachment(currentAttachments[i], aDirPrefId);
				}
			} else {
				for (var i = 0; i < selectedAttachments.length; i++) {
					await cardbookAttachmentUtils.loadAttachment(selectedAttachments[i].attachment, aDirPrefId);
				}
			}
		},
	};
};
