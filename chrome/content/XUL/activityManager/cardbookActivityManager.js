var { cardbookXULUtils } = ChromeUtils.import("chrome://cardbook/content/XUL/utils/cardbookXULUtils.js");

if ("undefined" == typeof(cardbookActivityManager)) {
	var cardbookActivityManager = {

		mainContext: "CardBook",
		cryptoCount: 0,
		cryptoDone: 0,
		cryptoActivity: {},
		syncCount: 0,
		syncDone: 0,
		syncActivity: {},
		
		addActivityFromUndo: function(aActionCode, aEventName) {
			let iconClass = "";
			switch(aActionCode) {
				case "cardsConverted":
				case "cardsMerged":
				case "cardsDuplicated":
				case "displayNameGenerated":
				case "cardModified":
				case "categoryRenamed":
				case "categoryConvertedToList":
				case "listConvertedToCategory":
				case "nodeRenamed":
				case "listCreatedFromNode":
				case "cardsFormatted":
					iconClass = "copyMail";
					break;
				case "outgoingEmailCollected":
				case "emailCollectedByFilter":
				case "emailDeletedByFilter":
				case "cardsImportedFromFile":
				case "cardsImportedFromDir":
				case "cardCreated":
				case "categoryCreated":
				case "categorySelected":
					iconClass = "addItem";
					break;
				case "cardsExportedToZip":
				case "cardsImagesExported":
				case "cardsPasted":
				case "cardsDragged":
				case "linePasted":
					iconClass = "moveMail";
					break;
				case "undoActionDone":
				case "redoActionDone":
					iconClass = "undo";
					break;
				case "categoryUnselected":
				case "categoryDeleted":
				case "cardsDeleted":
				case "nodeDeleted":
					iconClass = "deleteMail";
					break;
			}
			cardbookActivityManager.addEvent(aEventName, iconClass);
		},

		addEvent: function(aEventName, aIcon) {
			let gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			let event = Components.classes["@mozilla.org/activity-event;1"].createInstance(Components.interfaces.nsIActivityEvent);
			event.init(aEventName, null, "", null, Date.now());
			event.iconClass = aIcon;
			gActivityManager.addActivity(event);
			return event;
		},

		initProcess: function(aProcessName, aContextDisplayText, aContextType, aIcon) {
			let gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			let process = Components.classes["@mozilla.org/activity-process;1"].createInstance(Components.interfaces.nsIActivityProcess);
			process.init(aProcessName, "CardBook");
			process.iconClass = aIcon;
			process.groupingStyle = Components.interfaces.nsIActivity.GROUPING_STYLE_BYCONTEXT;
			process.contextDisplayText = aContextDisplayText;
			process.contextType = aContextType;
			gActivityManager.addActivity(process);
			return process;
		},

		finishProcess: function(aProcess) {
			let gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
			if (gActivityManager.containsActivity(aProcess.id)) {
				aProcess.state = Components.interfaces.nsIActivityProcess.STATE_COMPLETED;
				gActivityManager.removeActivity(aProcess.id);
			}
		},

		initSyncActivity: function(aDirPrefId, aDirPrefName) {
			var processName = cardbookXULUtils.localizeMessage("synchroRunning", [aDirPrefName]);
			var process = cardbookActivityManager.initProcess(processName, aDirPrefName, aDirPrefId, "syncMail");
			if (!cardbookActivityManager.syncActivity[aDirPrefId]) {
				cardbookActivityManager.syncActivity[aDirPrefId] = {};
			}
			cardbookActivityManager.syncActivity[aDirPrefId].syncProcess = process;
		},

		fetchSyncCount: async function(aCountTotal) {
			cardbookActivityManager.syncCount = cardbookActivityManager.syncCount + aCountTotal;
			await notifyTools.notifyBackground({query: "cardbook.conf.addProgressBar", type: "sync", total: cardbookActivityManager.syncCount, done: cardbookActivityManager.syncDone});
		},

		fetchSyncActivity: function(aDirPrefId, aCountDone, aCountTotal) {
			if (cardbookActivityManager.syncActivity[aDirPrefId] && cardbookActivityManager.syncActivity[aDirPrefId].syncProcess) {
				let processMessage = cardbookXULUtils.localizeMessage("synchroProcessed", [aCountDone, aCountTotal]);
				cardbookActivityManager.syncActivity[aDirPrefId].syncProcess.setProgress(processMessage, aCountDone, aCountTotal);
			}
		},

		finishSyncActivity: function(aDirPrefId) {
			if (cardbookActivityManager.syncActivity[aDirPrefId] && cardbookActivityManager.syncActivity[aDirPrefId].syncProcess) {
				cardbookActivityManager.finishProcess(cardbookActivityManager.syncActivity[aDirPrefId].syncProcess);
			}
		},

		finishSyncActivityOK: function(aDirPrefId, aDirPrefName) {
			cardbookActivityManager.finishSyncActivity(aDirPrefId);
			if (cardbookActivityManager.syncActivity[aDirPrefId].syncEvent) {
				let gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
				if (gActivityManager.containsActivity(cardbookActivityManager.syncActivity[aDirPrefId].syncEvent.id)) {
					gActivityManager.removeActivity(cardbookActivityManager.syncActivity[aDirPrefId].syncEvent.id);
				}
			}
			let eventName = cardbookXULUtils.localizeMessage("synchroFinished", [aDirPrefName]);
			let event = cardbookActivityManager.addEvent(eventName, "syncMail");
			cardbookActivityManager.syncActivity[aDirPrefId].syncEvent = event;
		},

		initCryptoActivity: function(aMode) {
			cardbookActivityManager.cryptoDone = 0;
			cardbookActivityManager.cryptoCount = 0;
			let processName = cardbookXULUtils.localizeMessage(`${aMode}Started`);
			let processTitle = cardbookXULUtils.localizeMessage("cardbookTitle");
			let process = cardbookActivityManager.initProcess(processName, processTitle, cardbookActivityManager.mainContext, "syncMail");
			if (!cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext]) {
				cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext] = {};
			}
			cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncProcess = process;
		},

		fetchCryptoCount: async function(aCountTotal) {
			cardbookActivityManager.cryptoCount = cardbookActivityManager.cryptoCount + aCountTotal;
			await notifyTools.notifyBackground({query: "cardbook.conf.addProgressBar", type: "crypto", total: cardbookActivityManager.cryptoCount, done: cardbookActivityManager.cryptoDone});
		},

		fetchCryptoActivity: async function(aMode) {
			cardbookActivityManager.cryptoDone++;
			if (cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext] && cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncProcess) {
				if (cardbookActivityManager.cryptoDone % 100 == 0) {
					let processMessage = cardbookXULUtils.localizeMessage(`${aMode}Processed`, [cardbookActivityManager.cryptoDone, cardbookActivityManager.cryptoCount]);
					cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncProcess.setProgress(processMessage, cardbookActivityManager.cryptoDone, cardbookActivityManager.cryptoCount);
					await notifyTools.notifyBackground({query: "cardbook.conf.addProgressBar", type: "crypto", total: cardbookActivityManager.cryptoCount, done: cardbookActivityManager.cryptoDone});
				}
			}
			if (cardbookActivityManager.cryptoDone == cardbookActivityManager.cryptoCount) {
				await notifyTools.notifyBackground({query: "cardbook.conf.addProgressBar", type: "crypto", total: cardbookActivityManager.cryptoCount, done: cardbookActivityManager.cryptoDone});
				cardbookActivityManager.finishCryptoActivityOK(aMode);
			}
		},

		finishCryptoActivity: function() {
			if (cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext] && cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncProcess) {
				cardbookActivityManager.finishProcess(cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncProcess);
			}
		},

		finishCryptoActivityOK: function(aMode) {
			cardbookActivityManager.finishCryptoActivity();
			if (cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncEvent) {
				let gActivityManager = Components.classes["@mozilla.org/activity-manager;1"].getService(Components.interfaces.nsIActivityManager);
				if (gActivityManager.containsActivity(cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncEvent.id)) {
					gActivityManager.removeActivity(cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncEvent.id);
				}
			}
			let eventName = cardbookXULUtils.localizeMessage(`${aMode}Finished`);
			let event = cardbookActivityManager.addEvent(eventName, "syncMail");
			cardbookActivityManager.cryptoActivity[cardbookActivityManager.mainContext].syncEvent = event;
		},

		addActivity: function(aMessageCode, aArray, aIcon) {
			let eventName = cardbookXULUtils.localizeMessage(aMessageCode, aArray);
			cardbookActivityManager.addEvent(eventName, aIcon);
		},
	}
};
