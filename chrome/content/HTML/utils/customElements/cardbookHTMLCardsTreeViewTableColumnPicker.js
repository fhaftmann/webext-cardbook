/**
 * Class used to generate a column picker used for the TreeViewTableHeader in
 * case the visibility of the columns of a table can be changed.
 *
 * Include treeView.ftl for strings.
 */
class CBTreeViewTableColumnPicker extends HTMLTableCellElement {
	/**
	 * The clickable button triggering the picker context menu.
	 * @type {HTMLButtonElement}
	 */
	#button;
	#dropdown;
	#container;

	connectedCallback() {
		if (this.hasConnected) {
			return;
		}
		this.hasConnected = true;

		this.setAttribute("is", "cb-tree-view-table-column-picker");
		this.classList.add("tree-table-cell-container", "cb-tree-view-table-column-picker");

		this.#dropdown = document.createElement("div", {is: "cb-button-dropdown"});
		this.appendChild(this.#dropdown);
		this.#dropdown.classList.add("tree-table-cell-container", "cb-tree-view-table-column-picker");
		this.#container = this.#dropdown.getContainer();
		this.#container.classList.add("tree-table-cell-container", "cb-tree-view-table-column-picker");

		this.#button = this.#dropdown.getButton();
		this.#button.classList.add("button-flat", "button-column-picker");
		const img = document.createElement("img");
		img.src = "";
		img.alt = "";
		this.#button.appendChild(img);

		let treeview = this.closest("tree-view");
		let table = this.closest("table");
		let columns = table.columns;
		columns.sort(function(a, b){ return a.label.localeCompare(b.label) })
		columns.sort(function(a, b){ return a.ordinal-b.ordinal })
		let options = [];
		for (let column of columns) {
			let checked = !column.hidden;
			options.push([`${column.label}`, `${treeview.id}-Picker-${column.id}`, checked]);
		}
		function columnsChanged(event) {
			let value = event.target.id.replace(`${treeview.id}-Picker-`, "");
			table.dispatchEvent(new CustomEvent("columns-changed", {bubbles: true, detail: {target: event.target, value: value}}));
		}

		let restFunctions = []
		function restoreColumns(event) {
			table.dispatchEvent(new CustomEvent("restore-columns", {bubbles: true}));
			event.stopImmediatePropagation();
		}
		let restoreId = `${treeview.id}Picker-RestoreDefault`;
		let restoreLabel = messenger.i18n.getMessage("columnPickerRestoreColumnOrder");
		let restore = { restId: restoreId, restLabel: restoreLabel, restFunction: restoreColumns };
		restFunctions.push(restore);

		if (table.applyColumns) {
			function applyColumns(event) {
				table.dispatchEvent(new CustomEvent("applyTo-columns", {bubbles: true}));
				event.stopImmediatePropagation();
			}
			let applyToId = `${treeview.id}Picker-ApplyTo`;
			let applyToLabel = messenger.i18n.getMessage("columnPickerApplyCurrentViewTo");
			let applyTo = { restId: applyToId, restLabel: applyToLabel, restFunction: applyColumns };
			restFunctions.push(applyTo);
		}

		this.#dropdown.loadOptions(`${treeview.id}Picker`, options, columnsChanged, restFunctions, null);
		this.#dropdown.setClass([ "down", "right" ]);
	}
}
customElements.define("cb-tree-view-table-column-picker", CBTreeViewTableColumnPicker, { extends: "th" });
