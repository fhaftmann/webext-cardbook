class cardbookDirTreeRow extends HTMLLIElement {
    depth;
    nameLabel;
    icon;
    childList;

    constructor() {
        super();
        this.setAttribute("is", "cb-tree-list-row");
        this.append(document.getElementById("cardbookAccountsTemplate").content.cloneNode(true));
        this.nameLabel = this.querySelector(".name");
        this.icon = this.querySelector(".icon");
        this.statusIcon = this.querySelector(".statusIcon");
        this.childList = this.querySelector("ul");
    }

    connectedCallback() {
        let parent = this.parentNode.closest(`li[is="cb-tree-list-row"]`);
        this.depth = parent ? parent.depth + 1 : 1;
        this.childList.style.setProperty("--depth", this.depth);
        this.setAttribute("draggable", "true");
    }

    get id() {
        return this.getAttribute("id");
    }
    
    set id(value) {
        this.setAttribute("id", value);
    }

    get name() {
        return this.nameLabel.textContent;
    }
    
    set name(value) {
        if (this.name != value) {
            this.nameLabel.textContent = value;
        }
    }

    get enabled() {
        return this.getAttribute("enabled");
    }
    
    set enabled(value) {
        this.setAttribute("enabled", value);
    }

    get nodetype() {
        return this.getAttribute("type");
    }
    
    set nodetype(value) {
        this.setAttribute("type", value);
    }

    get root() {
        return this.getAttribute("root");
    }
    
    set root(value) {
        this.setAttribute("root", value);
    }

    get icontype() {
        return this.getAttribute("icontype");
    }
    
    set icontype(value) {
        this.setAttribute("icontype", value);
    }

    get statustype() {
        this.getAttribute("statustype");
    }

    set statustype(value) {
        this.setAttribute("statustype", value);
    }

    set statustypeTitle(value) {
        this.statusIcon.setAttribute("title", value);
    }

    set color(value) {
        this.icon.style.setProperty("--CBicon-color", value);
    }
}
customElements.define("cb-tree-list-row", cardbookDirTreeRow, { extends: "li" });
