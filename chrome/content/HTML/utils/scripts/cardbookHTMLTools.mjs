import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";

import { cardbookIDBCard } from "../../../BG/indexedDB/cardbookIDBCard.mjs";

export var cardbookHTMLTools = {
    // label can't be disabled, only set in grey
    disableNode: function (aNode, aDisabledState) {
        aNode.disabled = aDisabledState;
        if (aDisabledState) {
            aNode.classList.add("disabled");
        } else {
            aNode.classList.remove("disabled");
        }
    },
 
    deleteRows: function (aObjectName) {
        try {
            var aListRows = document.getElementById(aObjectName);
            while (aListRows.hasChildNodes()) {
                aListRows.lastChild.remove();
            }
        } catch (e) {}
    },
 
    deleteTableRows: function (aTableName, aTableHeaderRowName) {
        let table = document.getElementById(aTableName);
        let toDelete = [];
        for (let row of table.rows) {
            if (aTableHeaderRowName) {
                if (row.id != aTableHeaderRowName) {
                    toDelete.push(row);
                }
            } else {
                toDelete.push(row);
            }
        }
        for (let row of toDelete) {
            let oldChild = table.removeChild(row);
        }
    },

    addHTMLElement: function (aElement, aParent, aId, aParameters) {
        let element = document.createElementNS("http://www.w3.org/1999/xhtml", `${aElement}`);
        if (aId) {
            element.setAttribute("id", aId);
        }
        for (let prop in aParameters) {
            element.setAttribute(prop, aParameters[prop]);
        }
        aParent.appendChild(element);
        return element;
    },

    addHTMLDIV: function (aParent, aId, aParameters) {
        let div = this.addHTMLElement("div", aParent, aId, aParameters);
        return div;
    },

    addHTMLBLABEL: function (aParent, aId, aValue, aParameters) {
        let aH2Label = this.addHTMLElement("label", aParent, aId, aParameters);
        let aH2 = this.addHTMLElement("b", aH2Label, "", {})
        aH2.textContent = aValue;
        return aH2Label;
    },

    addHTMLLABEL: function (aParent, aId, aValue, aParameters) {
        let label = this.addHTMLElement("label", aParent, aId, aParameters);
        label.textContent = aValue;
        return label;
    },
    
    addHTMLINPUT: function (aParent, aId, aValue, aParameters) {
        let input = this.addHTMLElement("input", aParent, aId, aParameters);
        input.setAttribute("value", aValue);
        return input;
    },

    addHTMLTEXTAREA: function (aParent, aId, aValue, aParameters) {
        let textarea = this.addHTMLElement("textarea", aParent, aId, aParameters);
        textarea.textContent = aValue;
        return textarea;
    },

    addHTMLBUTTON: function (aParent, aId, aLabel, aParameters) {
        let button = this.addHTMLElement("button", aParent, aId, aParameters);
        button.textContent = aLabel;
        return button;
    },

    addHTMLPROGRESS: function (aParent, aId, aParameters) {
        let progress = this.addHTMLElement("progress", aParent, aId, aParameters);
        progress.setAttribute("max", "100");
        return progress;
    },

    addHTMLTABLE: function (aParent, aId, aParameters) {
        let table = this.addHTMLElement("table", aParent, aId, aParameters);
        return table;
    },

    addHTMLTR: function (aParent, aId, aParameters) {
        let tr = this.addHTMLElement("tr", aParent, aId, aParameters);
        return tr;
    },

    addHTMLTD: function (aParent, aId, aParameters) {
        let td = this.addHTMLElement("td", aParent, aId, aParameters);
        return td;
    },

    addHTMLTHEAD: function (aParent, aId, aParameters) {
        let thead = this.addHTMLElement("thead", aParent, aId, aParameters);
        return thead;
    },

    addHTMLTBODY: function (aParent, aId, aParameters) {
        let tbody = this.addHTMLElement("tbody", aParent, aId, aParameters);
        return tbody;
    },

    addHTMLTH: function (aParent, aId, aParameters) {
        let th = this.addHTMLElement("th", aParent, aId, aParameters);
        return th;
    },

    addHTMLIMAGE: function (aParent, aId, aParameters) {
        let image = this.addHTMLElement("img", aParent, aId, aParameters);
        return image;
    },

    addHTMLSELECT: function (aParent, aId, aValue, aParameters) {
        let aSelect = this.addHTMLElement("select", aParent, aId, aParameters);
        aSelect.value = aValue;
        return aSelect;
    },

    addHTMLOPTION: function (aParent, aId, aValue, aLabel, aParameters) {
        let option = this.addHTMLElement("option", aParent, aId, aParameters);
        option.value = aValue;
        option.textContent = aLabel;
        return option;
    },

    addCUSTOMINPUTDATE: function (aParent, aId, aParameters) {
        let element = document.createElement("div", {is: "cb-input-date"});
        element.id = aId;
        for (let prop in aParameters) {
            element.setAttribute(prop, aParameters[prop]);
        }
        aParent.appendChild(element);
        return element;
    },

    addTreeTable: function (aId, aHeaders, aData, aDataParameters, aRowParameters, aTableParameters, aSortFunction, aDataId, aDragAndDrop, aDataClass) {
        let table = document.getElementById(aId);
        let sortColumn = table.getAttribute("data-sort-column");
        let orderColumn = table.getAttribute("data-sort-order");

        let selectedRows = table.querySelectorAll("tr[rowselected='true']");
        let selectedValues = [];
        if (typeof aDataId !== "undefined" && aDataId) {
            selectedValues = Array.from(selectedRows, row => row.cells[aDataId].textContent);
        } else {
            selectedValues = Array.from(selectedRows, row => Array.from(row.cells, cell => cell.textContent).join());
        }
        this.deleteRows(aId);

        if (aHeaders.length) {
            let thead = this.addHTMLTHEAD(table, `${aId}_thead`);
            let tr = this.addHTMLTR(thead, `${aId}_thead_tr`);
            for (let i = 0; i < aHeaders.length; i++) {
                let th = this.addHTMLTH(tr, `${aId}_thead_th_${i}`);
                th.textContent = messenger.i18n.getMessage(`${aHeaders[i]}Label`);
                th.setAttribute("data-value", aHeaders[i]);
                if (aSortFunction) {
                    th.setAttribute("title", messenger.i18n.getMessage("columnsSortBy", [th.textContent]));
                }
                if (aHeaders[i] == sortColumn) {
                    let sortImg;
                    if (orderColumn == "ascending" ) {
                        sortImg = this.addHTMLIMAGE(th, `${aId}_thead_th_${i}_image`, { "src": "../../skin/small-icons/arrow-down.svg" } );
                    } else {
                        sortImg = this.addHTMLIMAGE(th, `${aId}_thead_th_${i}_image`, { "src": "../../skin/small-icons/arrow-up.svg" } );
                    }
                    if (aSortFunction) {
                        sortImg.addEventListener("click", aSortFunction, false);
                    }
                }
            }
            if (aSortFunction) {
                tr.addEventListener("click", aSortFunction, false);
            }
        }
        if (aData.length) {
            let tbody = this.addHTMLTBODY(table, `${aId}_tbody`);
            for (let i = 0; i < aData.length; i++) {
                let tr = this.addHTMLTR(tbody, `${aId}_thead_tr_${i}`, {"tabindex": "0"});
                let trValue = "";
                if (typeof aDataId !== "undefined") {
                    trValue = aData[i][aDataId];
                } else {
                    trValue = aData[i].join();
                }
                for (let j = 0; j < aData[i].length; j++) {
                    let td = this.addHTMLTD(tr, `${aId}_thead_td_${i}_${j}`);
                    let last = td;
                    if (typeof aData[i][j] === "boolean") {
                        let checkbox = this.addHTMLElement("input", td, `${aId}_thead_td_${i}_${j}_checkbox`, {"type": "checkbox"})
                        checkbox.checked = aData[i][j];
                        last = checkbox;
                    } else {
                        td.textContent = aData[i][j];
                    }
                    if (aDataParameters && aDataParameters[j] && aDataParameters[j].events) {
                        for (let event of aDataParameters[j].events) {
                            last.addEventListener(event[0], event[1], false);
                        }
                    }
                    if (aDataClass && aDataClass[i] && aDataClass[i][j]) {
                        last.classList.add(aDataClass[i][j]);
                    }
                }
                if (selectedValues.includes(trValue)) {
                    tr.setAttribute("rowselected", "true");
                }
                if (aRowParameters && aRowParameters.titles && aRowParameters.titles[i]) {
                    tr.setAttribute("title", aRowParameters.titles[i]);
                }
                if (aRowParameters && aRowParameters.values && aRowParameters.values[i]) {
                    tr.setAttribute("data-value", aRowParameters.values[i]);
                }
            }
        }
        if (aTableParameters && aTableParameters.events) {
            for (let event of aTableParameters.events) {
                table.addEventListener(event[0], event[1], false);
            }
        }
        if (typeof aDragAndDrop !== "undefined") {
            if (typeof aDragAndDrop.dragStart !== "undefined") {
                table.setAttribute("draggable", "true");
                table.addEventListener("dragstart", aDragAndDrop.dragStart, false);
            }
            if (typeof aDragAndDrop.drop !== "undefined") {
                table.addEventListener("dragover", event => event.preventDefault(), false);
                table.addEventListener("drop", aDragAndDrop.drop, false);
            }
        }
        return table;
    },

    loadOptions: function (aSelect, aList, aDefaultValue, aAddEmpty, aLabelChoice) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aAddEmpty) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_empty`, "", "");
        }
        let i = 0;
        for (let [value, label] of aList) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, label);
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            } else if (aLabelChoice === true && label.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadInclExcl: function (aSelect, aDefaultValue) {
        let id = aSelect.id;
        this.deleteRows(id);
        let i = 0;
        for (let value of [ "include", "exclude" ]) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, messenger.i18n.getMessage(`${value}Label`));
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadMailAccounts: async function (aSelect, aDefaultValue, aAddAllMailAccounts) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aAddAllMailAccounts) {
            let value = "allMailAccounts";
            let option = this.addHTMLOPTION(aSelect, `${id}_option_all`, value, messenger.i18n.getMessage("allMailAccounts"));
            if (value == aDefaultValue) {
                option.selected = true;
            }
        }
        let i = 0;
        for (let account of await browser.accounts.list()) {
			if (account.type == "pop3" || account.type == "imap") {
				for (let identity of account.identities) {
                    let value = identity.id;
                    let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, identity.email);
                    if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                        option.selected = true;
                    }
                    i++;
				}
			}
		}
    },

    loadAddressBooks: async function (aSelect, aDefaultValue, aExclusive, aAddAllABs, aIncludeReadOnly, aIncludeSearch, aIncludeDisabled, aInclRestrictionList, aExclRestrictionList) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aAddAllABs) {
            let value = "allAddressBooks";
            let option = this.addHTMLOPTION(aSelect, `${id}_option_all`, value, messenger.i18n.getMessage("allAddressBooks"));
            if (value == aDefaultValue) {
                option.selected = true;
            }
        }
        let addressbooks = [];
        addressbooks = await messenger.runtime.sendMessage({query: "cardbook.getABs", exclusive: aExclusive, includeReadOnly: aIncludeReadOnly,
                                                        includeSearch: aIncludeSearch, includeDisabled: aIncludeDisabled, inclRestrictionList: aInclRestrictionList,
                                                        exclRestrictionList: aExclRestrictionList});
        let i = 0;
        for (let [label, value, type] of addressbooks) {
            // to do AB icon
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, label);
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadCategories: async function (aSelect, aDefaultPrefId, aDefaultValue, aAddAllCats, aAddOnlyCats, aAddNoCats, aAddEmptyCats, aInclRestrictionList, aExclRestrictionList) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aAddEmptyCats) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_all`, "", "");
        }
        if (!(aInclRestrictionList && aInclRestrictionList[aDefaultPrefId])) {
            if (aAddAllCats) {
                let value = "allCategories";
                let option = this.addHTMLOPTION(aSelect, `${id}_option_all`, value, messenger.i18n.getMessage("allCategories"));
                if (value == aDefaultValue) {
                    option.selected = true;
                }
            }
            if (aAddOnlyCats) {
                let value = "onlyCategorieaDefaultPrefIds";
                let option = this.addHTMLOPTION(aSelect, `${id}_option_only`, value, messenger.i18n.getMessage("onlyCategories"));
                if (value == aDefaultValue) {
                    option.selected = true;
                }
            }
            if (aAddNoCats) {
                let value = "noCategory";
                let option = this.addHTMLOPTION(aSelect, `${id}_option_no`, value, messenger.i18n.getMessage("noCategory"));
                if (value == aDefaultValue) {
                    option.selected = true;
                }
            }
        }
        var categories = [];
        categories = await messenger.runtime.sendMessage({query: "cardbook.getCategories", defaultPrefId: aDefaultPrefId,
                                                        inclRestrictionList: aInclRestrictionList,
                                                        exclRestrictionList: aExclRestrictionList});
        let i = 0;
        for (let [label, value] of categories) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, label);
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadContacts: async function (aSelect, aDirPrefId, aDefaultValue, aAddEmpty) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aAddEmpty) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_empty`, "", "");
            if (!aDefaultValue) {
                option.selected = true;
            }
        }

        await cardbookIDBCard.openCardDB({});
        let [ cards, searchId ] = await cardbookIDBCard.getCards(aDirPrefId, "", "", "", 0);

        let contacts = cards.map(card => [card.fn, card.uid, card.isAList]);
        cardbookHTMLUtils.sortMultipleArrayByString(contacts,0,1);
        let i = 0;
        for (let [label, value, type] of contacts) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, label);
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadConvertionFuntions: function (aSelect, aDefaultValue) {
        let id = aSelect.id;
        this.deleteRows(id);
        let option = this.addHTMLOPTION(aSelect, `${id}_option_no`, "", "");
        let i = 0;
        for (let value of [ "uppercase", "lowercase", "capitalization" ]) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, messenger.i18n.getMessage(`${value}Label`));
            if (value.toUpperCase() == aDefaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadVCardVersions: function (aSelect, aDefaultList, aSupportedVersions) {
        let id = aSelect.id;
        this.deleteRows(id);
        if (aDefaultList && aDefaultList.length && aDefaultList.length > 0) {
            var versions = aDefaultList;
        } else {
            var versions = aSupportedVersions;
        }
        if (versions.includes("3.0")) {
            var defaultValue = "3.0";
        } else {
            var defaultValue = "4.0";
        }
        let i = 0;
        for (let value of versions) {
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, value, value);
            if (value.toUpperCase() == defaultValue.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    loadRemotePageTypes: function (aSelect, aDefaultId, aSupportedConnections) {
        let id = aSelect.id;
        this.deleteRows(id);
        let i = 0;
        for (let connection of aSupportedConnections) {
            let id = connection[0].toLowerCase();
            let option = this.addHTMLOPTION(aSelect, `${id}_option_${i}`, connection[0], id[0].toUpperCase() + id.substr(1).toLowerCase(),
                                    {"type": connection[1], "url": connection[2]});
            try {
                let locale = messenger.i18n.getMessage(`remotePageType.${id}.label`);
                if (locale) {
                    option.textContent = locale;
                }
            } catch(e) {}
            if (id == aDefaultId.toLowerCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    addMenuCaselist: function (aParent, aType, aIndex, aValue, aParameters) {
        let caseOperators = [["dig", "ignoreCaseIgnoreDiacriticLabel"], ["ig", "ignoreCaseMatchDiacriticLabel"],
                                ["dg", "matchCaseIgnoreDiacriticLabel"], ["g", "matchCaseMatchDiacriticLabel"]]

        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menulistCase`, "", aParameters);
        let i = 0;
        for (let operator of caseOperators) {
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menulistCase_${i}`, operator[0], messenger.i18n.getMessage(operator[1]));
            if (aValue.toUpperCase() == operator[0].toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    addMenuObjlist: function (aParent, aType, aIndex, aColumns, aValue, aParameters) {
        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menulistObj`, "", aParameters);

        let i = 0;
        for (let column of aColumns) {
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menuitemObj_${i}`, column[0], column[1]);
            if (aValue.toUpperCase() == column[0].toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
    },

    addMenuTermlist: function (aParent, aType, aIndex, aValue, aParameters) {
        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menulistTerm`, "", aParameters);

        let operators = ["Contains", "DoesntContain", "Is", "Isnt", "BeginsWith", "EndsWith", "IsEmpty", "IsntEmpty"]
        let i = 0;
        for (let operator of operators) {
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menulistTerm_${i}`, operator, messenger.i18n.getMessage(`${operator}Operator`));
            if (aValue.toUpperCase() == operator.toUpperCase()) {
                option.selected = true;
            }
            i++;
        }
        return select;
    },

    loadGender: function (menuname, value) {
        let genderLookup = [ [ "F", messenger.i18n.getMessage("types.gender.f") ],
                                    [ "M", messenger.i18n.getMessage("types.gender.m") ],
                                    [ "N", messenger.i18n.getMessage("types.gender.n") ],
                                    [ "O", messenger.i18n.getMessage("types.gender.o") ],
                                    [ "U", messenger.i18n.getMessage("types.gender.u") ] ];
        let menu = document.getElementById(menuname);
		cardbookHTMLTools.loadOptions(menu, genderLookup, value, true, false);
   },

    loadPreferMailFormat: function (select, value) {
        let list = [ [ "0", messenger.i18n.getMessage("Unknown.label") ],
                    [ "1", messenger.i18n.getMessage("PlainText.label") ],
                    [ "2", messenger.i18n.getMessage("HTML.label") ] ];
        cardbookHTMLTools.loadOptions(select, list, value, false, false);
    },

	loadCountries: function (countryList, menu, value) {
		cardbookHTMLTools.loadOptions(menu, countryList, value, true, true);
	},

    addTreeSelect: function (aSelect, aData, aRowParameters) {
        if (aData.length) {
            for (let i = 0; i < aData.length; i++) {
                let option = this.addHTMLOPTION(aSelect, `${aSelect.id}_option_${i}`, { "value": aData[i] } );
                option.textContent = aData[i];
                if (aRowParameters && aRowParameters.values && aRowParameters.values[i]) {
                    option.setAttribute("data-value", aRowParameters.values[i]);
                }
            }
        }
    },

    checkEditButton: function (event) {
        let idArray = event.target.id.split("_");
        let type = idArray[0];
        let index = idArray[1];
        let prevIndex = parseInt(index) - 1;
        let nextIndex = parseInt(index) + +1;
        document.getElementById(`${type}_${index}_removeButton`).disabled = false;
        if (event.target.value == "") {
            document.getElementById(`${type}_${index}_addButton`).disabled = true;
        } else {
            document.getElementById(`${type}_${index}_addButton`).disabled = false;
        }
        document.getElementById(`${type}_${index}_downButton`).disabled = true;
        document.getElementById(`${type}_${index}_upButton`).disabled = true;
        if (document.getElementById(`${type}_${prevIndex}_addButton`)) {
            document.getElementById(`${type}_${prevIndex}_addButton`).disabled = true;
            document.getElementById(`${type}_${prevIndex}_downButton`).disabled = false;
            document.getElementById(`${type}_${index}_upButton`).disabled = false;
        }
        if (document.getElementById(`${type}_${nextIndex}_addButton`)) {
            document.getElementById(`${type}_${index}_addButton`).disabled = true;
            document.getElementById(`${type}_${index}_downButton`).disabled = false;
            document.getElementById(`${type}_${nextIndex}_upButton`).disabled = false;
        }
    },

    addKeyInput: function (aParent, aId, aValue, aParameters) {
        let keyInput = this.addHTMLINPUT(aParent, aId, aValue, aParameters);
        keyInput.addEventListener("input", cardbookHTMLTools.checkEditButton, false);
        return keyInput;
    },

    addKeyTextarea: function (aParent, aId, aValue, aParameters) {
        let keyTextarea = this.addHTMLTEXTAREA(aParent, aId, aValue, aParameters);
        keyTextarea.addEventListener("input", cardbookHTMLTools.checkEditButton, false);
        return keyTextarea;
    },

    addMenuType: function (aParent, aType, aIndex, aValues) {
        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menuType`);
        let i = 0;
        let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menuType_${i}`, "", "");
        option.selected = true;
        i++
        for (let line of aValues) {
            let found = false;
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menuType_${i}`, line[1], line[0]);
            if (line[2] === true) {
                option.selected = true;
            }
            i++;
        }
        return select;
    },

    addMenuIMPPType: async function (aParent, aType, aIndex, aAllIMPPs, aCode, aProtocol) {
        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menuIMPPType`);
        let i = 0;
        for (let line of aAllIMPPs) {
            let found = false;
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menuIMPPType${i}`, line[0], line[1]);
            if (aCode != "") {
                if (line[0].toLowerCase() == aCode.toLowerCase()) {
                    found = true;
                }
            } else if (aProtocol != "") {
                if (line[2].toLowerCase() == aProtocol.toLowerCase()) {
                    found = true;
                }
            }
            if (found === true) {
                option.selected = true;
            }
            i++;
        }
        return select;
    },

    addMenuTzlist: async function (aParent, aType, aIndex, aValue, aParameters) {
        let select = this.addHTMLSELECT(aParent, `${aType}_${aIndex}_menulistTz`);
        let i = 0;
        let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menulistTz_${i}`, "", "");
        option.selected = true;
        i++
        let timezoneIds = [];
        timezoneIds = await messenger.runtime.sendMessage({query: "cardbook.getTimezoneIds"});
        for (let [ label, value ] of timezoneIds) {
            let option = this.addHTMLOPTION(select, `${aType}_${aIndex}_menulistTz_${i}`, value, label);
             if (aValue == value) {
                option.selected = true;
            }
            i++;
        }
        return select;
    },

    addEditButton: function (aParent, aType, aIndex, aButtonType, aButtonName, aFunction) {
        let aEditButton = this.addHTMLBUTTON(aParent, `${aType}_${aIndex}_${aButtonName}Button`, "");
        let aEditButtonImg = this.addHTMLIMAGE(aEditButton, `${aType}_${aIndex}_${aButtonName}ButtonImg`);
        aEditButton.classList.add("small-button");
        if (aButtonType == "add") {
            aEditButtonImg.classList.add("cardbookAdd");
        } else if (aButtonType == "remove") {
            aEditButtonImg.classList.add("cardbookDelete");
        } else if (aButtonType == "up") {
            aEditButtonImg.classList.add("cardbookUp");
        } else if (aButtonType == "down") {
            aEditButtonImg.classList.add("cardbookDown");
        } else if (aButtonType == "validated") {
            aEditButtonImg.classList.add("cardbookValidated");
        } else if (aButtonType == "notValidated") {
            aEditButtonImg.classList.add("cardbookNotValidated");
        } else if (aButtonType == "link") {
            aEditButtonImg.classList.add("cardbookLink");
        }
        aEditButton.setAttribute("title", messenger.i18n.getMessage(`${aButtonType}EntryTooltip`));
        aEditButton.addEventListener("click", aFunction, false);
    },

    addProcessButton: function (aParent, aId, aFunction) {
        let processButton = this.addHTMLIMAGE(aParent, aId);
        processButton.setAttribute("class", "cardbookProcessClass");
        processButton.setAttribute("autoConvertField", "true");
        processButton.addEventListener("click", aFunction, false);
        processButton.setAttribute("title", messenger.i18n.getMessage("dontAutoConvertField"));
    },

    addPrefStar: function (aParent, aType, aIndex, aValue, aReadOnly) {
        let prefButton = this.addHTMLIMAGE(aParent, `${aType}_${aIndex}_PrefImage`, {class: "cardbookPrefStarClass"});
        if (aValue === true) {
            prefButton.setAttribute("haspref", "true");
        } else {
            prefButton.removeAttribute("haspref");
        }
        prefButton.setAttribute("title", messenger.i18n.getMessage("prefLabel"));

        if (aReadOnly === false) {
            function firePrefCheckBox(event) {
                if (this.getAttribute("haspref")) {
                    this.removeAttribute("haspref");
                } else {
                    this.setAttribute("haspref", "true");
                }
            };
            prefButton.addEventListener("click", firePrefCheckBox, false);
        }
        return prefButton;
    },

};
