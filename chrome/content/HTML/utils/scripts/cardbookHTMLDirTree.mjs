import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "../../../BG/utils/cardbookBGUtils.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

export var cardbookHTMLDirTree = {
	COL_NAME: 0,
	COL_ID: 1,
	COL_ENABLED: 2,
	COL_TYPE: 3,
	COL_READONLY: 4,
	COL_ROOT: 5,
    COL_COLOR: 6,
    visibleData: [],

    setSelectedAccount: function (aAccountId) {
        if (document.getElementById("cardbookAccountsTree").querySelector("li[id=\"" + aAccountId + "\"]")) {
            document.getElementById("cardbookAccountsTree").selectedRow = document.getElementById("cardbookAccountsTree").querySelector("li[id=\"" + aAccountId + "\"]");
        }
    },

    setSyncingIcon: async function (aAccountId) {
        if (document.getElementById("cardbookAccountsTree").querySelector("li[id=\"" + aAccountId + "\"]")) {
            let row = document.getElementById("cardbookAccountsTree").querySelector("li[id=\"" + aAccountId + "\"]");
            let [statusType, statusMessage] = await cardbookHTMLUtils.getABStatusType(aAccountId);
            row.statustype = statusType;
            row.statustypeTitle = statusMessage;
        }
    },

    getAccountsData(aAccounts, aCategories, aUncats, aNodes, aNodesColors) {
        let result = [];
		let accountsShown = cardbookBGPreferences.getPref("accountsShown");
		switch(accountsShown) {
			case "enabled":
				aAccounts = aAccounts.filter(child => child[cardbookHTMLDirTree.COL_ENABLED]);
				break;
			case "disabled":
				aAccounts = aAccounts.filter(child => (!child[cardbookHTMLDirTree.COL_ENABLED]));
				break;
			case "local":
				aAccounts = aAccounts.filter(child => child[cardbookHTMLDirTree.COL_TYPE] == "LOCALDB" || child[cardbookHTMLDirTree.COL_TYPE] == "FILE" || child[cardbookHTMLDirTree.COL_TYPE] == "DIRECTORY");
				break;
			case "remote":
				aAccounts = aAccounts.filter(child => cardbookBGUtils.isMyAccountRemote(child[cardbookHTMLDirTree.COL_TYPE]));
				break;
			case "search":
				aAccounts = aAccounts.filter(child => child[cardbookHTMLDirTree.COL_TYPE] == "SEARCH");
				break;
		};
        let useColor = cardbookBGPreferences.getPref("useColor");
        for (let account of aAccounts) {
            let accountId = account[cardbookHTMLDirTree.COL_ID];
            let root = account[cardbookHTMLDirTree.COL_ROOT];
            let color = useColor == "nothing" ? "" : cardbookBGPreferences.getColor(root);
            let row = account.concat(color);
            result.push(row);
            if (cardbookBGPreferences.getNode(accountId) != "org") {
                if (aCategories[accountId]) {
                    for (let category of aCategories[accountId]) {
                        let nodeColor = aNodesColors[category.name] ? aNodesColors[category.name] : "";
                        let color = useColor == "nothing" ? "" : nodeColor;
                        result.push([ category.name, category.id, true, "categories", false, accountId, color ]);
                    }
                }
                if (aUncats[accountId]) {
                    for (let category of aUncats[accountId]) {
                        let nodeColor = aNodesColors[category.name] ? aNodesColors[category.name] : "";
                        let color = useColor == "nothing" ? "" : nodeColor;
                        result.push([ category.name, category.id, true, "categories", false, accountId, color ]);
                    }
                }
            } else {
                if (aNodes[accountId]) {
                    for (let node of aNodes[accountId]) {
                        let nodeColor = aNodesColors[node.name] ? aNodesColors[node.name] : "";
                        let color = useColor == "nothing" ? "" : nodeColor;
                        result.push([ node.name, node.id, true, "org", false, accountId, color ]);
                    }
                }
            }
        }
        cardbookHTMLUtils.sortMultipleArrayByString(result,1,1);
        return result;
    },

    async createRow(node, existingRows) {
        try {
            let row = document.createElement("li", { is: "cb-tree-list-row" });
            row.name = node[cardbookHTMLDirTree.COL_NAME];
            row.id = node[cardbookHTMLDirTree.COL_ID];
            row.enabled = node[cardbookHTMLDirTree.COL_ENABLED];
            row.nodetype = node[cardbookHTMLDirTree.COL_TYPE];
            row.icontype = cardbookBGUtils.getABIconType(node[cardbookHTMLDirTree.COL_TYPE]);
            row.root = node[cardbookHTMLDirTree.COL_ROOT];
            row.color = node[cardbookHTMLDirTree.COL_COLOR];

            if (row.nodetype != "categories" && row.nodetype != "org" && row.enabled === "true") {
                let [statusType, statusMessage] = await cardbookHTMLUtils.getABStatusType(node[cardbookHTMLDirTree.COL_ROOT]);
                row.statustype = statusType;
                row.statustypeTitle = statusMessage;
            }

            if (!cardbookBGPreferences.getExpanded(row.id)) {
                row.classList.add("collapsed");
            }

            if (row.root == row.id) {
                return row;
            } else {
                let rootParent = existingRows.filter(x => x.id == row.root)[0];
                if (row.nodetype == "categories") {
                    rootParent.querySelector("ul").appendChild(row);
                } else if (row.nodetype == "org") {
                    let tempArray = row.id.split("::");
                    if (tempArray.length == 3) {
                        rootParent.querySelector("ul").appendChild(row);
                    } else {
                        tempArray.pop();
                        let parentId = tempArray.join("::");
                        rootParent.querySelector("li[id=\"" + parentId.replace(/\\/g, "\\\\") + "\"]").querySelector("ul").appendChild(row);
                    }
                }
            }
        } catch(e) { console.log(e) }
    },
}
