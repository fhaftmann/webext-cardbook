import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";
import { cardbookHTMLDownloads } from "./cardbookHTMLDownloads.mjs";

export var cardbookHTMLImages = {
	defaultCardImage: "../../skin/contact-generic.svg",

    setMediaContentForCard: function (aDisplayDefault, aCard, aType, aExtension, aValue) {
		document.getElementById(`${aType}URIInputText`).value = aValue;
		document.getElementById(`${aType}ExtensionInputText`).value = aExtension;
		// edition
		if (aDisplayDefault && aType == "photo") {
			aCard[aType].extension = cardbookHTMLUtils.formatExtension(aExtension, aCard.version);
			aCard[aType].value = aValue;
			aCard[aType].URI = "";
		}
	},

	displayImageCard: async function (aCard, aDisplayDefault) {
		let dirname = cardbookBGPreferences.getName(aCard.dirPrefId);
		cardbookHTMLImages.setMediaContentForCard(aDisplayDefault, aCard, "logo", aCard.logo.extension, aCard.logo.value);
		cardbookHTMLImages.setMediaContentForCard(aDisplayDefault, aCard, "sound", aCard.sound.extension, aCard.sound.value);
		if (aCard.photo.attachmentId) {
			document.getElementById("photoAttachmentIdInputText").value = aCard.photo.attachmentId;
		}
		if (aCard.photo.value) {
			let image = { cbid: aCard.dirPrefId + "::" + aCard.uid, dirPrefId: aCard.dirPrefId, extension: aCard.photo.extension, content: aCard.photo.value };
			cardbookHTMLImages.resizeImageCard(aCard, image, aDisplayDefault);
		} else {
			let image = await messenger.runtime.sendMessage({ query: "cardbook.getImage", field: "photo", dirName: dirname, cardId: aCard.cbid, cardName: aCard.fn });
			if (image) {
				cardbookHTMLImages.resizeImageCard(aCard, image, aDisplayDefault);
			} else {
				if (aDisplayDefault) {
					cardbookHTMLImages.resizeImageCard(aCard, null, aDisplayDefault);
				} else {
					document.getElementById("imageBox").classList.add("hidden");
				}
			}
		}
	},

	resizeImageCard: function (aCard, aContent, aDisplayDefault) {
		let myImage = document.getElementById("defaultCardImage");
		let myDummyImage = document.getElementById("imageForSizing");
		let content = "";
		if (aContent && aContent.content) {
			content = "data:image/" + aContent.extension + ";base64," + aContent.content;
			cardbookHTMLImages.setMediaContentForCard(aDisplayDefault, aCard, "photo", aContent.extension, aContent.content);
		} else {
			content = cardbookHTMLImages.defaultCardImage;
			document.getElementById("photoURIInputText").value = "";
			document.getElementById("photoExtensionInputText").value = "";
		}
		myImage.src = "";
		myDummyImage.src = "";
		myDummyImage.src = content;
		myDummyImage.onload = function (e) {
			let widthFound = 170;
			let heightFound = 170;
			if (myDummyImage.width >= myDummyImage.height) {
				heightFound = Math.round(myDummyImage.height * widthFound / myDummyImage.width);
			} else {
				widthFound = Math.round(myDummyImage.width * heightFound / myDummyImage.height);
			}
			myImage.width = widthFound;
			myImage.height = heightFound;
			myImage.src = content;
			document.getElementById("imageBox").classList.remove("hidden");
		}
		myDummyImage.onerror = function (e) {
			document.getElementById("photoURIInputText").value = "";
			document.getElementById("photoExtensionInputText").value = "";
			if (aDisplayDefault) {
				cardbookHTMLImages.resizeImageCard(null, null, aDisplayDefault);
			}
		}
	},

	resizeImage64Card: function (aCard, b64) {
		let result = {};
		let tmpArray = b64.split(",");
		result.content = tmpArray[1];
		let tmpArray2 = tmpArray[0].split(";")
		let tmpArray3 = tmpArray2[0].split("/")
		result.extension = tmpArray3[1];
		cardbookHTMLImages.resizeImageCard(aCard, result, true);
	},

	saveImageCard: async function () {
		const content = atob(document.getElementById("photoURIInputText").value);
		const byteNumbers = new Array(content.length);
		for (let i = 0; i < content.length; i++) {
			byteNumbers[i] = content.charCodeAt(i);
		}
		const byteArray = new Uint8Array(byteNumbers);
		let type = {};
		if (document.getElementById("photoExtensionInputText").value) {
			type = { type: "image/"+document.getElementById("photoExtensionInputText").value.toLowerCase()};
		}
		const file = new Blob([byteArray], type);
		let url = URL.createObjectURL(file);
		let filename = document.getElementById("fnInputText").value + "." + document.getElementById("photoExtensionInputText").value.toLowerCase();

		await cardbookHTMLDownloads.download(url, filename, "saveImage");
	},

	copyImageCard: function () {
		let b64 = document.getElementById("photoURIInputText").value;
		let type = document.getElementById("photoExtensionInputText").value;
		messenger.runtime.sendMessage({query: "cardbook.clipboardSetImage", type: type, b64: b64});
	},

	deleteImageCard: function () {
		cardbookHTMLImages.resizeImageCard(null, null, true);
	},

};
