import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";

import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookBGSynchronizationUtils } from "../../../BG/utils/cardbookBGSynchronizationUtils.mjs";
import { cardbookBGUtils } from "../../../BG/utils/cardbookBGUtils.mjs";


export var cardbookHTMLPermissions = {

    getURLsForPermission: function (aUrl, aType) {
        let urls = [];
        let rootUrl = cardbookBGSynchronizationUtils.getRootUrl(aUrl);
        let removePort = rootUrl.split(":")
        let finalUrl = cardbookBGSynchronizationUtils.getSlashedUrl(removePort[0]+":"+removePort[1]);
        urls.push(`${finalUrl}*`);
        if (aType && aType.startsWith("GOOGLE")) {
            urls.push("https://*.googleusercontent.com/*");
        } else if (aType == "APPLE") {
            urls = [];
            urls.push("https://*.icloud.com/*");
        }
        return urls;
    },

    getAllOrigins: async function () {
        let permissions = await browser.permissions.getAll();
        return permissions.origins ? permissions.origins : [];
    },

    isPermissionRequired: async function (aUrl) {
        let permissions = await browser.permissions.getAll();
        let required = permissions.origins ? !permissions.origins.includes(aUrl) : true;
        return required;
    },

    requestURLPermission: async function (aDirPrefId, aUrl, aType) {
        let url = aDirPrefId ? cardbookBGPreferences.getUrl(aDirPrefId) : aUrl;
        let urls = cardbookHTMLPermissions.getURLsForPermission(url, aType)
        let result = await browser.permissions.request({ "origins": urls });
        return result;
    },

    requestAllURLPermissions: async function (aAccounts) {
        let origins = [];
        for (let dirPrefId of aAccounts) {
            if (!cardbookBGPreferences.getEnabled(dirPrefId)) {
                continue;
            }
            let type = cardbookBGPreferences.getType(dirPrefId);
            if (!cardbookBGUtils.isMyAccountRemote(type)) {
                continue;
            }
            let url = cardbookBGPreferences.getUrl(dirPrefId);
            let urls = cardbookHTMLPermissions.getURLsForPermission(url, type);
            origins = origins.concat(urls);
        }
        origins = cardbookHTMLUtils.arrayUnique(origins);
        let result = await browser.permissions.request({ "origins": origins });
        return result;
    },
}
