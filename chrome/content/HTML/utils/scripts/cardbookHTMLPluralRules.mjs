export var cardbookHTMLPluralRules = {
    escapeSemicolon: function (aValue) {
		if (aValue instanceof String || typeof aValue == "string") {
            return aValue.replace(/\\\\/g, "ESCAPEDBACKSLASH").replace(/\\;/g, "ESCAPEDSEMICOLON");
		} else {
			return aValue;
		}
    },

    escapeSemicolon1: function (aValue) {
		if (aValue instanceof String || typeof aValue == "string") {
            return aValue.replace(/\\\\/g, "ESCAPEDBACKSLASH").replace(/\\;/g, "ESCAPEDSEMICOLON").replace(/;/g, "SEMICOLON");
		} else {
			return aValue;
		}
    },

    unescapeSemicolon: function (aValue) {
		if (aValue instanceof String || typeof aValue == "string") {
            return aValue.replace(/ESCAPEDBACKSLASH/g, "\\\\").replace(/ESCAPEDSEMICOLON/g, "\\;").replace(/SEMICOLON/g, ";");
		} else {
			return aValue;
		}
    },

    getPluralMessage: function (message, count, params) {
        // https://developer.mozilla.org.cach3.com/en/Localization_and_Plurals
        // ar : zero, one, two, few, many, other
        // cs, sk : one, few, other
        // da, de, el, hu, it, en-US, es-ES, nl, pt-PT, sv-SE, vi : one, other
        // fr, pt-BR : one, other
        // hr, ru, uk : one, few, other
        // id, ja, ko, tr, zh-CN : other
        // lt : one, few, other
        // pl : one, few, many
        // ro : one, few, other
        // sl : one, two, few, other
        let rules = { "ar": [ "zero", "one", "two", "few", "many", "other" ],
                        "cs": [ "one", "few", "other" ],
                        "da": [ "one", "other" ],
                        "de": [ "one", "other" ],
                        "el": [ "one", "other" ],
                        "en-US": [ "one", "other" ],
                        "es-ES": [ "one", "other" ],
                        "fr": [ "one", "other" ],
                        "hr": [ "one", "few", "other" ],
                        "hu": [ "one", "other" ],
                        "id": [ "other" ],
                        "it": [ "one", "other" ],
                        "ja": [ "other" ],
                        "ko": [ "other" ],
                        "lt": [ "one", "few", "other" ],
                        "nl": [ "one", "other" ],
                        "pl": [ "one", "few", "many" ],
                        "pt-BR": [ "one", "other" ],
                        "pt-PT": [ "one", "other" ],
                        "ro": [ "one", "few", "other" ],
                        "ru": [ "one", "few", "other" ],
                        "sk": [ "one", "few", "other" ],
                        "sl": [ "one", "two", "few", "other" ],
                        "sv-SE": [ "one", "other" ],
                        "tr": [ "other" ],
                        "uk": [ "one", "few", "other" ],
                        "vi": [ "one", "other" ],
                        "zh-CN": [ "other" ] };
        let lang = navigator.language;
        let pluralizer = new Intl.PluralRules(lang);
        if (!rules[lang]) {
            lang = "en-US";
        }

        let num = count;
        let selector = pluralizer.select(num);
        params = params || [ count ];

        let index = rules[lang].indexOf(selector);
        params = params.map(x => cardbookHTMLPluralRules.escapeSemicolon1(x));
        let messages = browser.i18n.getMessage(message, params);
        messages = cardbookHTMLPluralRules.escapeSemicolon(messages);
        let messageArray = messages.split(";")
        let result = messageArray[index];
        result = cardbookHTMLPluralRules.unescapeSemicolon(result);
        return result;
    }
};