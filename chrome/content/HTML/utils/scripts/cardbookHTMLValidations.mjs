import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookHTMLUtils } from "./cardbookHTMLUtils.mjs";
import { cardbookHTMLNotification } from "./cardbookHTMLNotification.mjs";
import { cardbookHTMLTypes } from "./cardbookHTMLTypes.mjs";

export var cardbookHTMLValidations = {
	validateOffice365: async function (aCard, aAddedCards, aNotification) {
		let ABType = cardbookBGPreferences.getType(aCard.dirPrefId);

		if (ABType != "OFFICE365") {
			return true;
		}

		for (let field of ["suffixname", "prefixname", "role"]) {
			let fieldValue = document.getElementById(`${field}TextBox`).value.trim();
			if (fieldValue) {
				let fieldLabel = document.getElementById(`${field}Label`).value.trim();
				cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Field", [fieldLabel, fieldValue]);
				return false;
			}
		}

		let i = 0;
		while (true) {
			if (document.getElementById(`event_${i}_hbox`)) {
				let eventDate = document.getElementById(`event_${i}_InputDate`).getValue();
				let eventName = document.getElementById(`event_${i}_valueBox`).value.trim();
				if (eventDate || eventName) {
					let fieldLabel = document.getElementById("event_caption").textContent.trim();
					cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Field", [fieldLabel, eventDate + " : " + eventName]);
					return false;
				}
				i++;
			} else {
				break;
			}
		}

		i = 0;
		while (true) {
			if (document.getElementById(`url_${i}_hbox`)) {
				let urlName = document.getElementById(`url_${i}_valueBox`).value.trim();
				if (urlName) {
					let fieldLabel = document.getElementById("url_caption").textContent.trim();
					cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Field", [fieldLabel, urlName]);
					return false;
				}
				i++;
			} else {
				break;
			}
		}

		i = 0;
		let emailQuota = 3;
		let emailCount = 0;
		while (true) {
			if (document.getElementById(`email_${i}_hbox`)) {
				let emailName = document.getElementById(`email_${i}_valueBox`).value.trim();
				if (emailName) {
					emailCount++;
					if (emailCount > emailQuota) {
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Email");
						return false;
					}
				}
				i++;
			} else {
				break;
			}
		}

		i = 0;
		let imppQuota = 1;
		let imppCount = 0;
		while (true) {
			if (document.getElementById(`impp_${i}_hbox`)) {
				let imppName = document.getElementById(`impp_${i}_valueBox`).value.trim();
				if (imppName) {
					imppCount++;
					if (imppCount > imppQuota) {
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Impp");
						return false;
					}
				}
				i++;
			} else {
				break;
			}
		}

		i = 0;
		let adrQuota = 3;
		let adrCount = 0;
		while (true) {
			if (document.getElementById(`adr_${i}_hbox`)) {
				let adrName = document.getElementById(`adr_${i}_valueBox`).value.trim();
				if (adrName) {
					adrCount++;
					if (adrCount > adrQuota) {
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Adr");
						return false;
					}
				}
				i++;
			} else {
				break;
			}
		}

		i = 0;
		let telQuota = 17;
		let telCount = 0;
		while (true) {
			if (document.getElementById(`tel_${i}_hbox`)) {
				let telName = document.getElementById(`tel_${i}_valueBox`).value.trim();
				if (telName) {
					telCount++;
					if (telCount > telQuota) {
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Tel");
						return false;
					}
				}
				i++;
			} else {
				break;
			}
		}

		for (let field of cardbookHTMLUtils.multilineFields) {
			aCard[field] = cardbookHTMLWindowUtils.getAllTypes(field, true);
		}

		let telTypeQuota = { "assistanttype": 1, "workfaxtype": 1, "callbacktype": 1, "carphonetype": 1, "pagertype": 1,
							"telextype": 1, "ttytype": 1, "radiotype": 1, "worktype": 3, "homefaxtype": 1, "otherfaxtype": 1,
							"hometype": 2, "celltype": 1, "othertype": 1 };
		let telTypeCount = { "assistanttype": 0, "workfaxtype": 0, "callbacktype": 0, "carphonetype": 0, "pagertype": 0,
							"telextype": 0, "ttytype": 0, "radiotype": 0, "worktype": 0, "homefaxtype": 0, "otherfaxtype": 0,
							"hometype": 0, "celltype": 0, "othertype": 0 };
		for (let tel of aCard.tel) {
			let type = cardbookHTMLTypes.getCodeType("tel", aCard.dirPrefId, tel[1]).result;
			if (type) {
				telTypeCount[type]++;
			} else {
				let fieldLabel = document.getElementById("tel_caption").textContent.trim();
				cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365EmptyType", [fieldLabel]);
				return false;
			}
		}
		for (let type in telTypeCount) {
			if (telTypeCount[type] > telTypeQuota[type]) {
				let fieldLabel = document.getElementById("tel_caption").textContent.trim();
				let fieldType = messenger.i18n.getMessage(type);
				cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Type", [fieldLabel, fieldType, telTypeQuota[type]]);
				return false;
			}
		}

		let adrTypeQuota = { "worktype": 1, "hometype": 1, "othertype": 1 };
		let adrTypeCount = { "worktype": 0, "hometype": 0, "othertype": 0 };
		for (let adr of aCard.adr) {
			let type = cardbookHTMLTypes.getCodeType("adr", aCard.dirPrefId, adr[1]).result;
			if (type) {
				adrTypeCount[type]++;
			} else {
				let fieldLabel = document.getElementById("adr_caption").textContent.trim();
				cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365EmptyType", [fieldLabel]);
				return false;
			}
		}
		for (let type in adrTypeCount) {
			if (adrTypeCount[type] > adrTypeQuota[type]) {
				let fieldLabel = document.getElementById("adr_caption").textContent.trim();
				let fieldType = messenger.i18n.getMessage(type);
				cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Type", [fieldLabel, fieldType, adrTypeQuota[type]]);
				return false;
			}
		}

		if (aCard.isAList) {
			for (let field of ["firstname", "othername", "lastname", "nickname", "bday", "title", "birthplace", "deathplace", "deathplace", "anniversary"]) {
				let fieldValue = document.getElementById(`${field}TextBox`).value.trim();
				if (fieldValue) {
					let fieldLabel = document.getElementById(`${field}Label`).value.trim();
					cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Field", [fieldLabel, fieldValue]);
					return false;
				}
			}
			let j = 0;
			while (true) {
				if (document.getElementById(`orgTextBox_${j}`)) {
					let fieldValue = document.getElementById(`orgTextBox_${j}`).value.trim();
					if (fieldValue) {
						let fieldLabel = document.getElementById(`orgLabel_${j}`).value.trim();
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Field", [fieldLabel, fieldValue]);
						return false;
					}
					j++;
				} else {
					break;
				}
			}
			for (let addedCardLine of aAddedCards) {
				if (addedCardLine[5] == "EMAIL") {
					let email = addedCardLine[1];
					let card = await messenger.runtime.sendMessage({query: "cardbook.getCardFromEmail", email: email, dirPrefId: aCard.dirPrefId});
					if (!card) {
						cardbookHTMLNotification.setNotification(aNotification, "warning", "invalidOffice365Member", [email]);
						return false;
					}
				}
			}
		}
		return true;
	},

	validateEvents: function (aNotification) {
		var i = 0;
		while (true) {
			if (document.getElementById(`event_${i}_hbox`)) {
				let eventDate = document.getElementById(`event_${i}_InputDate`).getValue();
				let eventName = document.getElementById(`event_${i}_valueBox`).value.trim();
				if (eventDate != "" && eventName != "") {
					i++;
					continue;
				} else if (eventDate == "" && eventName == "") {
					i++;
					continue;
				} else if (eventDate == "") {
					cardbookHTMLNotification.setNotification(aNotification, "warning", "eventDateNull", []);
					return false;
				} else if (eventName == "") {
					cardbookHTMLNotification.setNotification(aNotification, "warning", "eventNameNull", []);
					return false;
				}
			} else {
				break;
			}
		}
		return true;
	},

	validateNodesNumber: function (aNotification) {
		let nodes = document.querySelectorAll("input[type='number']");
		for (let node of nodes) {
			if (!cardbookHTMLValidations.validateNode(aNotification, node)) {
				return false;
			}
		}
		return true;
	},

	validateNode: function (aNotification, aNode) {
		cardbookHTMLNotification.setNotification(aNotification, "OK");
		if (!aNode.checkValidity()) {
			let label = document.querySelector(`label[for='${aNode.id}']`);
			let message = `"${label.textContent}" : ${aNode.validationMessage}`
			cardbookHTMLNotification.setNotification(aNotification, "warning", "", "", message);
			return false;
		}
		return true;
	},

};
