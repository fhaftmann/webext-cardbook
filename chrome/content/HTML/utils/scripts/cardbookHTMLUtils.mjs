import { cardbookHTMLDates } from "./cardbookHTMLDates.mjs";
import { cardbookHTMLFormulas } from "./cardbookHTMLFormulas.mjs";
import { cardbookHTMLTypes } from "./cardbookHTMLTypes.mjs";
import { cardbookBGPreferences } from "../../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "../../../BG/utils/cardbookBGUtils.mjs";
import { cardbookCardParser } from "../../../BG/utils/cardbookCardParser.mjs";

export var cardbookHTMLUtils = {

	allColumns: { "display": ["fn"],
					"personal": ["prefixname", "firstname", "othername", "lastname", "suffixname", "nickname", "gender", "bday",
									"birthplace", "anniversary", "deathdate", "deathplace"],
					"org": ["org", "title", "role"],
					"categories": ["categories"],
					"arrayColumns": [ ["email", ["email"] ],
						["adr", ["postOffice", "extendedAddr", "street", "locality", "region", "postalCode", "country"] ],
						["impp", ["impp"] ],
						["tel", ["tel"] ],
						["url", ["url"] ] ],
					"note": ["note"],
					"calculated": ["age", "ABName"],
					"technical": ["version", "rev", "uid"],
					"media": [ 'photo', 'logo', 'sound' ],
					"technicalForTree": ["cardIcon", "name", "dirPrefId", "cbid", "class1", "etag", "geo", "mailer",
											"prodid", "tz", "sortstring", "kind"] },
	multilineFields: [ "email", "tel", "adr", "impp", "url" ],
	possibleNodes: [ "categories", "org" ],
	newFields: [ "gender", "birthplace", "anniversary", "deathdate", "deathplace" ],
	adrElements: [ "postOffice", "extendedAddr", "street", "locality", "region", "postalCode", "country" ],
	defaultEmailFormat: "X-MOZILLA-HTML",
	prefCSVPrefix: "*:",
	countriesList: ["ad","ae","af","ag","ai","al","am","ao","aq","ar","as","at","au","aw","az","ba","bb","bd","be","bf","bg",
						"bh","bi","bj","bl","bm","bn","bo","bq","br","bs","bt","bv","bw","by","bz","ca","cc","cd","cf","cg","ch",
						"ci","ck","cl","cm","cn","co","cp","cr","cu","cv","cw","cx","cy","cz","de","dg","dj","dk","dm","do","dz",
						"ec","ee","eg","eh","er","es","et","fi","fj","fk","fm","fo","fr","ga","gb","gd","ge","gf","gg","gh","gi",
						"gl","gm","gn","gp","gq","gr","gs","gt","gu","gw","gy","hk","hm","hn","hr","ht","hu","id","ie","il","im",
						"in","io","iq","ir","is","it","je","jm","jo","jp","ke","kg","kh","ki","km","kn","kp","kr","kw","ky","kz",
						"la","lb","lc","li","lk","lr","ls","lt","lu","lv","ly","ma","mc","md","me","mf","mg","mh","mk","ml","mm",
						"mn","mo","mp","mq","mr","ms","mt","mu","mv","mw","mx","my","mz","na","nc","ne","nf","ng","ni","nl","no",
						"np","nr","nu","nz","om","pa","pe","pf","pg","ph","pk","pl","pm","pn","pr","pt","pw","py","qa","qm","qs",
						"qu","qw","qx","qz","re","ro","rs","ru","rw","sa","sb","sc","sd","se","sg","sh","si","sk","sl","sm","sn",
						"so","sr","ss","st","sv","sx","sy","sz","tc","td","tf","tg","th","tj","tk","tl","tm","tn","to","tr","tt",
						"tv","tw","tz","ua","ug","us","uy","uz","va","vc","ve","vg","vi","vn","vu","wf","ws","xa","xb","xc","xd",
						"xe","xg","xh","xj","xk","xl","xm","xp","xq","xr","xs","xt","xu","xv","xw","ye","yt","za","zm","zw"],

	sortArrayByString: function (aArray, aInvert) {
		function compare(a, b) { return a.localeCompare(b)*aInvert; };
		return aArray.sort(compare);
	},

	sortMultipleArrayByString: function (aArray, aIndex, aInvert) {
		function compare(a, b) { return a[aIndex].localeCompare(b[aIndex])*aInvert; };
		return aArray.sort(compare);
	},

	sortMultipleArrayByNumber: function (aArray, aIndex, aInvert) {
		function compare(a, b) { return (a[aIndex] - b[aIndex])*aInvert; };
		return aArray.sort(compare);
	},

	arrayUnique: function (array) {
		let a = array.concat();
		for (var i=0; i<a.length; ++i) {
			for (var j=i+1; j<a.length; ++j) {
				if (a[i] == a[j])
					a.splice(j--, 1);
			}
		}
		return a;
	},

	cleanArray: function (vArray) {
		var newArray = [];
		for(let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				newArray.push(vArray[i].trim());
			}
		}
		return newArray;
	},

	cleanArrayWithoutTrim: function (vArray) {
		var newArray = [];
		for(let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				newArray.push(vArray[i]);
			}
		}
		return newArray;
	},
	
	escapeString: function (vString) {
		return vString.replace(/\\;/g,"@ESCAPEDSEMICOLON@").replace(/\\,/g,"@ESCAPEDCOMMA@");
	},

	escapeStringSemiColonNew: function (vString) {
		return vString.replace(/\\;/g,"@ESCAPEDSEMICOLON@");
	},

	escapeString1: function (vString) {
		return vString.replace(/\\\(/g,"@ESCAPEDLEFTPARENTHESIS@").replace(/\\\)/g,"@ESCAPEDRIGHTPARENTHESIS@").replace(/\\\|/g,"@ESCAPEDPIPE@"); 
	},

	escapeArray2: function (vArray) {
		var result = []
		for (let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				result[i] = vArray[i].replace(/\(/g,"@ESCAPEDLEFTPARENTHESIS@").replace(/\)/g,"@ESCAPEDRIGHTPARENTHESIS@").replace(/\|/g,"@ESCAPEDPIPE@");
			} else {
				result[i] = "";
			}
		}
		return result;
	},

	escapeArray: function (vArray) {
		for (let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				vArray[i] = vArray[i].replace(/\\;/g,"@ESCAPEDSEMICOLON@").replace(/\\,/g,"@ESCAPEDCOMMA@");
			}
		}
		return vArray;
	},

	replaceComma: function (vString) {
		return vString.replace(/,/g,"\n");
	},

	escapeStringBackslash: function (vString) {
		return vString.replace(/\\n/g,"@ESCAPEDRETURN@").replace(/\\/g,"\\\\").replace(/@ESCAPEDRETURN@/g,"\\n");
	},

	escapeStringSemiColon: function (vString) {
		return vString.replace(/;/g,"@ESCAPEDSEMICOLON@");
	},

	unescapeStringBackslash: function (vString) {
		return vString.replace(/\\\\/g,"\\");
	},

	unescapeStringSemiColon: function (vString) {
		return vString.replace(/@ESCAPEDSEMICOLON@/g,"\\;");
	},

	unescapeString: function (vString) {
		return vString.replace(/@ESCAPEDSEMICOLON@/g,";").replace(/\\;/g,";").replace(/@ESCAPEDCOMMA@/g,",").replace(/\\,/g,",");
	},

	unescapeString1: function (vString) {
		return vString.replace(/@ESCAPEDLEFTPARENTHESIS@/g,"(").replace(/@ESCAPEDRIGHTPARENTHESIS@/g,")").replace(/@ESCAPEDPIPE@/g,"|");
	},

	unescapeArray: function (vArray) {
		for (let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				vArray[i] = cardbookHTMLUtils.unescapeString(vArray[i]);
			}
		}
		return vArray;
	},

	unescapeArrayNew: function (vArray) {
		for (let i = 0; i<vArray.length; i++){
			if (vArray[i] && vArray[i] != ""){
				vArray[i] = cardbookHTMLUtils.unescapeStringNew(vArray[i]);
			}
		}
		return vArray;
	},

	unescapeStringNew: function (vString) {
		return vString.replace(/@ESCAPEDSEMICOLON@/g,";").replace(/@ESCAPEDCOMMA@/g,",");
	},

	rtrimArray: function (aArray) {
		for (let i = aArray.length - 1; i >= 0; i--) {
			if (aArray[i] == "") {
				aArray.pop();
			} else {
				break;
			}
		}
		return aArray;
	},

	getRadioNodes: function (aName) {
		let searchString = `input[type='radio'][name='${aName}']`;
		return document.querySelectorAll(searchString);
	},

	getRadioValue: function (aName) {
		for (let node of this.getRadioNodes(aName)) {
			if (node.checked) {
				return node.value;
			}
		}
		return null;
	},

	randomChannel: function(brightness) {
		var r = 255-brightness;
		var n = 0|((Math.random() * r) + brightness);
		var s = n.toString(16);
		return (s.length==1) ? "0"+s : s;
	},

	randomColor: function(brightness) {
		return "#" + this.randomChannel(brightness) + this.randomChannel(brightness) + this.randomChannel(brightness);
	},

	convertField: function(aFunction, aValue) {
		switch(aFunction) {
			case "lowercase":
				return aValue.toLowerCase();
				break;
			case "uppercase":
				return aValue.toUpperCase();
				break;
			case "capitalization":
				return aValue.charAt(0).toUpperCase() + aValue.substr(1).toLowerCase();
				break;
		};
	},

	getTextColorFromBackgroundColor: function (aHexBackgroundColor) {
		function hexToRgb(aColor) {
			var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(aColor);
			return result ? {r: parseInt(result[1], 16), g: parseInt(result[2], 16), b: parseInt(result[3], 16)} : null;
		}

		let rgbColor = hexToRgb(aHexBackgroundColor);
		// http://www.w3.org/TR/AERT#color-contrast
		let o = Math.round(((parseInt(rgbColor.r) * 299) + (parseInt(rgbColor.g) * 587) + (parseInt(rgbColor.b) * 114)) / 1000);
		let fore = (o > 125) ? "black" : "white";
		return fore;
	},

	getTime: function() {
		var objToday = new Date();
		var year = objToday.getFullYear();
		var month = ("0" + (objToday.getMonth() + 1)).slice(-2);
		var day = ("0" + objToday.getDate()).slice(-2);
		var hour = ("0" + objToday.getHours()).slice(-2);
		var min = ("0" + objToday.getMinutes()).slice(-2);
		var sec = ("0" + objToday.getSeconds()).slice(-2);
		var msec = ("00" + objToday.getMilliseconds()).slice(-3);
		return year + "." + month + "." + day + " " + hour + ":" + min + ":" + sec + ":" + msec;
	},

	deleteCssAllRules: function (aStyleSheet) {
		// aStyleSheet.cssRules may not be available
		try {
			while(aStyleSheet.cssRules.length > 0) {
				aStyleSheet.deleteRule(0);
			}
		} catch(e) {}

	},

	createMarkerRule: function (aStyleSheet, aStyleSheetRuleName) {
		var ruleString = "." + aStyleSheetRuleName + " {}";
		var ruleIndex = aStyleSheet.insertRule(ruleString, aStyleSheet.cssRules.length);
	},

	createCssCardRulesForTable: function (aStyleSheet, aDirPrefId, aColor, aUseColor) {
		let oppositeColor = cardbookHTMLUtils.getTextColorFromBackgroundColor(aColor);
		if (aUseColor == "text") {
			let ruleString1 = ".tableTree tr[data-value='SEARCH color_" + aDirPrefId + "']:not([rowselected='true']) {color: " + aColor + " !important;}";
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);

			let ruleString2 = ".tableTree tr[data-value='SEARCH color_" + aDirPrefId + "']:not([rowselected='true']) {background-color: " + oppositeColor + " !important;}";
			let ruleIndex2 = aStyleSheet.insertRule(ruleString2, aStyleSheet.cssRules.length);
		} else if (aUseColor == "background") {
			let ruleString1 = ".tableTree tr[data-value='SEARCH color_" + aDirPrefId + "']:not([rowselected='true']) {background-color: " + aColor + " !important;}";
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);

			let ruleString2 = ".tableTree tr[data-value='SEARCH color_" + aDirPrefId + "']:not([rowselected='true']) {color: " + oppositeColor + " !important;}";
			let ruleIndex2 = aStyleSheet.insertRule(ruleString2, aStyleSheet.cssRules.length);
		}
	},

	getDateFormat: function (aDirPrefId, aVersion) {
		let dateFormat = cardbookBGPreferences.getDateFormat(aDirPrefId);
		let type = cardbookBGPreferences.getType(aDirPrefId);
		if (dateFormat) {
			return dateFormat;
		} else if ( type == "APPLE" || type == "YAHOO") {
			return "YYYY-MM-DD";
		} else {
			return aVersion;
		}
	},

	base64ToBlob: function (aBase64, aType) {
		const byteCharacters = atob(aBase64);
		const byteArrays = [];
		for (let i = 0; i < byteCharacters.length; i++) {
			byteArrays.push(byteCharacters.charCodeAt(i));
		}
		const byteArray = new Uint8Array(byteArrays);
		return new Blob([byteArray], { type: `image/${aType}` });
	},

	blobToBase64: function (aBlob) {
		return new Promise((resolve, _) => {
			const reader = new FileReader();
			reader.onloadend = () => resolve(reader.result);
			reader.readAsDataURL(aBlob);
		});
	},

	addOrgToCard: function(aCard, aNodeId) {
		let nodeArray = cardbookHTMLUtils.escapeStringSemiColon(aNodeId).split("::");
		nodeArray.shift();
		nodeArray.shift();
		aCard.org = JSON.parse(JSON.stringify(nodeArray));
	},

	removeOrgFromCard: function(aCard, aNodeId) {
		let nodeIdArray = aNodeId.split("::");
		while (aCard.org.length >= nodeIdArray.length - 2) {
			aCard.org.pop();
		}
	},

	addCategoryToCard: function(aCard, aCategoryName) {
		aCard.categories.push(aCategoryName);
		aCard.categories = cardbookHTMLUtils.cleanCategories(aCard.categories);
	},

	removeCategoryFromCard: function(aCard, aCategoryName) {
		aCard.categories = aCard.categories.filter(child => child != aCategoryName);
	},

	cleanCategories: function (aCategoryList) {
		let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
		function filterCategories(element) {
			return (element != uncat);
		}
		return cardbookHTMLUtils.arrayUnique(aCategoryList.filter(filterCategories));
	},

	editCardFromList: async function (event) {
		let cbid = event.target.getAttribute("data-id");
		let cards = await messenger.runtime.sendMessage({ query: "cardbook.getCards", cbids: [ cbid ] });
		let card = cards[0];
		let type = card.isAList ? "List" : "Contact";
		let mode = cardbookBGPreferences.getReadOnly(card.dirPrefId) ? `View${type}` : `Edit${type}`;
		await cardbookHTMLUtils.openEditionWindow(card, mode);
	},

	editCardFromCard: async function (aCard) {
		let outCard = new cardbookCardParser();
		await cardbookBGUtils.cloneCard(aCard, outCard);
		let type = aCard.isAList ? "List" : "Contact";
		let mode = cardbookBGPreferences.getReadOnly(aCard.dirPrefId) ? `View${type}` : `Edit${type}`;
		await cardbookHTMLUtils.openEditionWindow(aCard, mode);
	},

	openEditionWindow: async function(aCard, aMode, aCardContent, aAddEmail, aIds) {
		let url = "chrome/content/HTML/cardEdition/wdw_cardEdition.html";
		let params = new URLSearchParams();
		if (aCard) {
			params.set("cbIdIn", aCard.cbid);
		}
		if (aAddEmail) {
			if (aAddEmail.fn) {
				params.set("fn", aAddEmail.fn);
			}
			if (aAddEmail.lastname) {
				params.set("lastname", aAddEmail.lastname);
			}
			if (aAddEmail.firstname) {
				params.set("firstname", aAddEmail.firstname);
			}
			if (aAddEmail.email) {
				params.set("email", aAddEmail.email);
			}
		}
		if (aCard.categories.length == 1) {
			params.set("category", aCard.categories[0]);
		}
		if (aIds) {
			params.set("ids", aIds);
		}
		params.set("editionMode", aMode);
		if (aCardContent) {
			params.set("cardContent", aCardContent);
		}
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
							url: `${url}?${params.toString()}`,
							type: "popup"});
	},

	formatCategoryForCss: function (aCategory) {
		// avoid starting with number
		return "c" + aCategory.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~ ]/g, "_");
	},

	isFileAlreadyOpen: function (aPath) {
		for (let dirPrefId of cardbookBGPreferences.getAllPrefIds()) {
			let type = cardbookBGPreferences.getType(dirPrefId);
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			if (enabled && (type == "FILE")) {
				if (cardbookBGPreferences.getUrl(dirPrefId) == aPath) {
					return true;
				}
			}
		}
		return false;
	},

	isDirectoryAlreadyOpen: function (aPath) {
		for (let dirPrefId of cardbookBGPreferences.getAllPrefIds()) {
			let type = cardbookBGPreferences.getType(dirPrefId);
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			if (enabled && (type == "DIRECTORY")) {
				if (cardbookBGPreferences.getUrl(dirPrefId) == aPath) {
					return true;
				}
			}
		}
		return false;
	},

	// 2 : HTML, 1 : PlainText, 0 : Unknown
	getMailFormatFromCard: function (aCard) {
		if (aCard) {
			for (let other of aCard.others) {
				let localDelim1 = other.indexOf(":",0);
				if (localDelim1 >= 0) {
					let header = other.substr(0,localDelim1).toUpperCase();
					let trailer = other.substr(localDelim1+1, other.length).toUpperCase();
					if (header == cardbookHTMLUtils.defaultEmailFormat) {
						if (trailer == "TRUE") {
							return "2";
						} else if (trailer == "FALSE") {
							return "1";
						}
						break;
					}
				}
			}
		}
		return "0";
	},

	addEventstoCard: function(aCard, aEventsArray, aPGNextNumber, aEventsDateFormat, aDateFormat) {
		let myEventsArray = [];
		for (let i = 0; i < aEventsArray.length; i++) {
			let value = cardbookHTMLDates.getVCardDateFromDateString(aEventsArray[i][0], aEventsDateFormat);
			let isDate = cardbookHTMLDates.convertDateStringToDateUTC(value);
			if (isDate != "WRONGDATE") {
				value = cardbookHTMLDates.convertUTCDateToDateString(isDate, aDateFormat);
				if (aEventsArray[i][2]) {
					myEventsArray.push("ITEM" + aPGNextNumber + ".X-ABDATE;TYPE=PREF:" + value);
				} else {
					myEventsArray.push("ITEM" + aPGNextNumber + ".X-ABDATE:" + value);
				}
				myEventsArray.push("ITEM" + aPGNextNumber + ".X-ABLABEL:" + aEventsArray[i][1]);
				aPGNextNumber++;
			} else {
				let dirPrefName = cardbookBGPreferences.getName(aCard.dirPrefId);
				let values =  [dirPrefName, aCard.fn, aEventsArray[i][0], aEventsDateFormat];
				messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "dateEntry1Wrong", values: values, error: "Warning"});
			}
		}
		aCard.others = myEventsArray.concat(aCard.others);
	},

	getEventsFromCard: function(aCardNoteArray, aCardOthers) {
		var myResult = [];
		var myRemainingNote = [];
		var myRemainingOthers = [];
		var eventInNoteEventPrefix = messenger.i18n.getMessage("eventInNoteEventPrefix");
		var typesList = [ "Birthday" , eventInNoteEventPrefix ];
		for (var i = 0; i < aCardNoteArray.length; i++) {
			var found = false;
			for (var j in typesList) {
				var myType = typesList[j];
				// compatibility when not localized
				var EmptyParamRegExp1 = new RegExp("^" + myType + ":([^:]*):(.*)", "ig");
				if (aCardNoteArray[i].replace(EmptyParamRegExp1, "$1") != aCardNoteArray[i]) {
					var lNotesName = aCardNoteArray[i].replace(EmptyParamRegExp1, "$1").replace(/^\s+|\s+$/g,"");
					if (aCardNoteArray[i].replace(EmptyParamRegExp1, "$2") != aCardNoteArray[i]) {
						var lNotesDateFound = aCardNoteArray[i].replace(EmptyParamRegExp1, "$2").replace(/^\s+|\s+$/g,"");
						if (lNotesDateFound.endsWith(":PREF")) {
							myResult.push([lNotesDateFound.replace(":PREF", ""), lNotesName, true]);
						} else {
							myResult.push([lNotesDateFound, lNotesName, false]);
						}
						found = true;
						break;
					}
				}
			}
			if (!found) {
				myRemainingNote.push(aCardNoteArray[i]);
			}
		}
		while (myRemainingNote[0] == "") {
			myRemainingNote.shift();
		}
		// should parse this, may in wrong order, maybe in lowercase
		// ITEM1.X-ABDATE;TYPE=PREF:20200910
		// ITEM1.X-ABLABEL:fiesta
		var myPGToBeParsed = {};
		for (var i = 0; i < aCardOthers.length; i++) {
			var relative = []
			relative = aCardOthers[i].match(/^ITEM([0-9]*)\.(.*)\:(.*)/i);
			if (relative && relative[1] && relative[2] && relative[3]) {
				var myPGName = "ITEM" + relative[1];
				var relativeKey = relative[2].match(/^([^\;]*)/i);
				var key = relativeKey[1].toUpperCase();
				if (!myPGToBeParsed[myPGName]) {
					myPGToBeParsed[myPGName] = ["", "", "", ""];
				}
				if (relative[2].toUpperCase().startsWith("X-ABLABEL")) {
					myPGToBeParsed[myPGName][1] = relative[3];
				} else {
					myPGToBeParsed[myPGName][0] = relative[3];
					myPGToBeParsed[myPGName][2] = relative[2].replace(key, "");
					myPGToBeParsed[myPGName][3] = key;
				}
			} else {
				myRemainingOthers.push(aCardOthers[i]);
			}
		}
		for (var i in myPGToBeParsed) {
			if (myPGToBeParsed[i][3] == "X-ABDATE") {
				let pref = myPGToBeParsed[i][2].toUpperCase().includes("PREF");
				myResult.push([myPGToBeParsed[i][0], myPGToBeParsed[i][1], pref]);
			} else {
				if (myPGToBeParsed[i][2]) {
					myRemainingOthers.push(i + "." + myPGToBeParsed[i][3] + myPGToBeParsed[i][2] + ":" + myPGToBeParsed[i][0]);
				} else {
					myRemainingOthers.push(i + "." + myPGToBeParsed[i][3] + ":" + myPGToBeParsed[i][0]);
				}
				myRemainingOthers.push(i + ".X-ABLABEL:" + myPGToBeParsed[i][1]);
			}
		}
		// to change pref for myResult
		return {result: myResult, remainingNote: myRemainingNote, remainingOthers: myRemainingOthers};
	},

	findEvents: async function (aCard, aListOfSelectedEmails, aDisplayName) {
		let listOfEmail = [];
		if (aCard) {
			if (!aCard.isAList) {
				for (let email of aCard.email) {
					listOfEmail.push(email[0][0].toLowerCase());
				}
			} else {
				listOfEmail.push(aCard.fn.replace('"', '\"'));
			}
		} else if (aListOfSelectedEmails) {
			listOfEmail = JSON.parse(JSON.stringify(aListOfSelectedEmails));
		}
		let url = "chrome/content/HTML/contactEvents/wdw_cardbookContactEvents.html";
		let params = new URLSearchParams();
		params.set("displayName", aDisplayName);
		params.set("listOfEmail", listOfEmail.join(","));
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
							url: `${url}?${params.toString()}`,
							type: "popup"});
	},

	getTranslatedField: function (aField, aLocale) {
		for (var i in cardbookHTMLUtils.allColumns) {
			for (var j = 0; j < cardbookHTMLUtils.allColumns[i].length; j++) {
				if (i != "arrayColumns" && i != "categories") {
					if (cardbookHTMLUtils.allColumns[i][j] == aField) {
						return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label");
					}
				} else if (i == "categories") {
					if (cardbookHTMLUtils.allColumns[i][j] + "_0_array" == aField) {
						return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label");
					}
				}
			}
		}
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (var i in customFields) {
			for (var j = 0; j < customFields[i].length; j++) {
				if (customFields[i][j][0] == aField) {
					return customFields[i][j][1];
				}
			}
		}
		for (var i = 0; i < cardbookHTMLUtils.allColumns.arrayColumns.length; i++) {
			if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_all" == aField) {
				return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label");
			} else if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_notype" == aField) {
				return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label") + " (" + messenger.i18n.getMessage("importNoTypeLabel") + ")";
			}
			for (var k = 0; k < cardbookHTMLUtils.allColumns.arrayColumns[i][1].length; k++) {
				if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_all" == aField) {
					return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label");
				} else if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_notype" == aField) {
					return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label") + " (" + messenger.i18n.getMessage("importNoTypeLabel") + ")";
				}
			}
		}
		for (var i = 0; i < cardbookHTMLUtils.allColumns.arrayColumns.length; i++) {
			var prefTypes = cardbookHTMLTypes.getTypesFromDirPrefId(cardbookHTMLUtils.allColumns.arrayColumns[i][0]);
			cardbookHTMLUtils.sortMultipleArrayByString(prefTypes,0,1)
			for (var j = 0; j < prefTypes.length; j++) {
				let typeCode = prefTypes[j][1].replaceAll(" ", "_");
				if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + typeCode == aField) {
					return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label") + " (" + prefTypes[j][0] + ")";
				}
				for (var k = 0; k < cardbookHTMLUtils.allColumns.arrayColumns[i][1].length; k++) {
					if (cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_" + typeCode == aField) {
						return messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label") + " (" + prefTypes[j][0] + ")";
					}
				}
			}
		}
		if ("blank" == aField) {
			return messenger.i18n.getMessage("importBlankColumn");
		} else if (aField.startsWith("org_")) {
			let index = aField.replace(/^org_/, "");
			let orgStructure = cardbookBGPreferences.getPref("orgStructure");
			return orgStructure[index];
		}
		return aField;
	},

	getEditionFieldsList: function() {
		let tmpArray = [];
		tmpArray.push([messenger.i18n.getMessage("addressbookHeader"), "addressbook"]);
		tmpArray.push([messenger.i18n.getMessage("categoriesHeader"), "categories"]);
		tmpArray.push([messenger.i18n.getMessage("fnLabel"), "fn"]);
		tmpArray.push([messenger.i18n.getMessage("noteLabel"), "note"]);

		for (let field of cardbookHTMLUtils.allColumns.personal) {
			if (cardbookHTMLUtils.newFields.includes(field)) {
				tmpArray.push([messenger.i18n.getMessage(field + ".conf.label"), field]);
			} else {
				tmpArray.push([messenger.i18n.getMessage(field + "Label"), field]);
			}
		}
		for (let field of cardbookHTMLUtils.multilineFields) {
			tmpArray.push([messenger.i18n.getMessage(field + "GroupboxLabel"), field]);
		}
		for (let field of ["event", "tz"]) {
			tmpArray.push([messenger.i18n.getMessage(field + "GroupboxLabel"), field]);
		}
		for (let field of cardbookHTMLUtils.allColumns.org) {
			tmpArray.push([messenger.i18n.getMessage(field + "Label"), field]);
		}
		for (let field of cardbookHTMLUtils.adrElements) {
			tmpArray.push([messenger.i18n.getMessage(field + "Label"), field]);
		}
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (let type of ["personal", "org"]) {
			for (let field of customFields[type]) {
				tmpArray.push([field[1], field[0]]);
			}
		}
		let orgStructure = cardbookBGPreferences.getPref("orgStructure");
		for (let field of orgStructure) {
			tmpArray.push([field, "org_" + field]);
		}
		tmpArray.push([messenger.i18n.getMessage("listsTabLabel"), "list"]);
		tmpArray.push([messenger.i18n.getMessage("keyTabLabel"), "key"]);
		cardbookHTMLUtils.sortMultipleArrayByString(tmpArray,0,1);
		return tmpArray;
	},

	getEditionFields: function() {
		let prefFields = [];
		prefFields = cardbookBGPreferences.getEditionFields();
		let editionFields = [];
		editionFields = cardbookHTMLUtils.getEditionFieldsList();
		let result = [];
		for (let field of editionFields) {
			let fieldLabel = field[0];
			let fieldCode = field[1];
			if (prefFields == "allFields") {
				result.push([true, fieldLabel, fieldCode, "", ""]);
			} else {
				if (prefFields[fieldCode]) {
					if (prefFields[fieldCode].function != "" && prefFields[fieldCode].function != null) {
						let convertFuntion = prefFields[fieldCode].function;
						let convertLabel = messenger.i18n.getMessage(`${convertFuntion}Label`);
						result.push([prefFields[fieldCode].displayed, fieldLabel, fieldCode, convertLabel, convertFuntion]);
					} else {
						result.push([prefFields[fieldCode].displayed, fieldLabel,fieldCode, "", ""]);
					}
				} else {
					result.push([false, fieldLabel, fieldCode, "", ""]);
				}
			}
		}
		return result;
	},

	// aMode : export|import|search|cardstree
	getAllAvailableColumns: function (aMode) {
		var result = [];
		for (var i in cardbookHTMLUtils.allColumns) {
			for (var j = 0; j < cardbookHTMLUtils.allColumns[i].length; j++) {
				if (!["arrayColumns", "categories", "calculated", "technicalForTree", "media"].includes(i)) {
					result.push([cardbookHTMLUtils.allColumns[i][j], messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label")]);
				} else if (i == "calculated" && aMode == "search") {
					result.push([cardbookHTMLUtils.allColumns[i][j], messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label")]);
				} else if (i == "calculated" && aMode == "cardstree") {
					result.push([cardbookHTMLUtils.allColumns[i][j], messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label")]);
				} else if (i == "technicalForTree" && aMode == "cardstree") {
					result.push([cardbookHTMLUtils.allColumns[i][j], messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label")]);
				} else if (i == "categories") {
					result.push([cardbookHTMLUtils.allColumns[i][j] + "_0_array", messenger.i18n.getMessage(cardbookHTMLUtils.allColumns[i][j] + "Label")]);
				}
			}
		}
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (var i in customFields) {
			for (var j = 0; j < customFields[i].length; j++) {
				result.push([customFields[i][j][0], customFields[i][j][1]]);
			}
		}
		let orgStructure = cardbookBGPreferences.getPref("orgStructure");
		if (orgStructure.length) {
			for (let i = 0; i < orgStructure.length; i++) {
				result.push(["org_" + i, orgStructure[i]]);
			}
		}
		for (var i = 0; i < cardbookHTMLUtils.allColumns.arrayColumns.length; i++) {
			for (var k = 0; k < cardbookHTMLUtils.allColumns.arrayColumns[i][1].length; k++) {
				result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_all",
											messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label")]);
			}
			if (aMode != "import") {
				if (cardbookHTMLUtils.allColumns.arrayColumns[i][1].length > 1) {
					result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_all",
												messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label")]);
				}
			}
			for (var k = 0; k < cardbookHTMLUtils.allColumns.arrayColumns[i][1].length; k++) {
				result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_notype",
											messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label") + " (" + messenger.i18n.getMessage("importNoTypeLabel") + ")"]);
			}
			if (aMode != "import") {
				if (cardbookHTMLUtils.allColumns.arrayColumns[i][1].length > 1) {
					result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_notype",
												messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label") + " (" + messenger.i18n.getMessage("importNoTypeLabel") + ")"]);
				}
			}
			var prefTypes = cardbookHTMLTypes.getTypesFromDirPrefId(cardbookHTMLUtils.allColumns.arrayColumns[i][0]);
			cardbookHTMLUtils.sortMultipleArrayByString(prefTypes,0,1)
			for (var j = 0; j < prefTypes.length; j++) {
				let typeCode = prefTypes[j][1].replaceAll(" ", "_");
				for (var k = 0; k < cardbookHTMLUtils.allColumns.arrayColumns[i][1].length; k++) {
					result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + k + "_" + typeCode,
												messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][1][k] + "Label") + " (" + prefTypes[j][0] + ")"]);
				}
				if (aMode != "import") {
					if (cardbookHTMLUtils.allColumns.arrayColumns[i][1].length > 1) {
						result.push([cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "_" + typeCode,
													messenger.i18n.getMessage(cardbookHTMLUtils.allColumns.arrayColumns[i][0] + "Label") + " (" + prefTypes[j][0] + ")"]);
					}
				}
			}
		}
		return result;
	},

	convertField: function(aFunction, aValue) {
		switch(aFunction) {
			case "lowercase":
				return aValue.toLowerCase();
			case "uppercase":
				return aValue.toUpperCase();
			case "capitalization":
				return aValue.charAt(0).toUpperCase() + aValue.substr(1).toLowerCase();
		};
	},

	rebuildAllPGs: function (aCard) {
		let myPgNumber = 1;
		for (let field of cardbookHTMLUtils.multilineFields) {
			for (var j = 0; j < aCard[field].length; j++) {
				let myTempString = aCard[field][j][2];
				if (myTempString.startsWith("ITEM")) {
					aCard[field][j][2] = "ITEM" + myPgNumber;
					myPgNumber++;
				}
			}
		}
		let myNewOthers = [];
		let myPGMap = {};
		for (var j = 0; j < aCard.others.length; j++) {
			let myTempString = aCard.others[j];
			var relative = []
			relative = myTempString.match(/^ITEM([0-9]*)\.(.*)/i);
			if (relative && relative[1] && relative[2]) {
				if (myPGMap[relative[1]]) {
					myNewOthers.push("ITEM" + myPGMap[relative[1]] + "." + relative[2]);
				} else {
					myNewOthers.push("ITEM" + myPgNumber + "." + relative[2]);
					myPGMap[relative[1]] = myPgNumber;
					myPgNumber++;
				}
			} else {
				myNewOthers.push(aCard.others[j]);
			}
		}
		aCard.others = JSON.parse(JSON.stringify(myNewOthers));
		return myPgNumber;
	},

	getABTypeFormat: function (aType) {
		switch(aType) {
			case "DIRECTORY":
			case "FILE":
			case "LOCALDB":
			case "CARDDAV":
			case "SEARCH":
				return "CARDDAV";
				break;
		};
		return aType;
	},

	getABStatusType: async function (aDirPrefId) {
		let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: aDirPrefId});
		if (syncing) {
			return ["syncing", messenger.i18n.getMessage("syncingLabel")];
		} else if (cardbookBGPreferences.getReadOnly(aDirPrefId)) {
			return ["readonly", messenger.i18n.getMessage("readonlyLabel")];
		} else if (cardbookBGPreferences.getSyncFailed(aDirPrefId)) {
			let lastsync = cardbookBGPreferences.getLastSync(aDirPrefId);
			if (lastsync) {
				let date = cardbookHTMLDates.getCorrectDatetime(lastsync);
				let lastsyncFormatted = cardbookHTMLDates.getFormattedDateTimeForDateTimeString(date, "2");
				return ["syncfailed", messenger.i18n.getMessage("syncFailedLastSync", [ lastsyncFormatted ])];
			} else {
				return ["syncfailed", messenger.i18n.getMessage("syncFailedNeverSynced")];
			}
		} else {
			return ["readwrite", ""];
		}
	},

	getOnlyTypesFromTypes: function(aArray) {
		function deletePrefs(element) {
			return !(element.toUpperCase().replace(/TYPE=PREF/i,"PREF").replace(/PREF=[0-9]*/i,"PREF") == "PREF");
		}
		let result = [];
		for (let type of aArray) {
			let upperElement = type.toUpperCase();
			if (upperElement == "PREF" || upperElement == "TYPE=PREF") {
				continue;
			} else if (upperElement == "HOME" || upperElement == "FAX" || upperElement == "CELL" || upperElement == "WORK") {
				result.push(type);
			} else if (upperElement.replace(/^TYPE=/i,"") !== upperElement) {
				let tmpArray = type.replace(/^TYPE=/ig,"").split(",").filter(deletePrefs);
				for (let type of tmpArray) {
					if (type == "VOICE" || type == "INTERNET") {
						continue;
					}
					result.push(type);
				}
			}
		}
		return result;
	},

	getNotTypesFromTypes: function(aArray) {
		let result = [];
		for (let element of aArray) {
			var upperElement = element.toUpperCase();
			if (upperElement === "PREF" || upperElement === "TYPE=PREF") {
				continue;
			} else if (upperElement === "HOME" || upperElement === "FAX" || upperElement === "CELL" || upperElement === "WORK") {
				continue;
			} else if (upperElement.replace(/PREF=[0-9]*/i,"PREF") == "PREF") {
				continue;
			} else if (upperElement.replace(/^TYPE=/i,"") === upperElement) {
				result.push(element);
			}
		}
		return result.join(",");
	},

	getPrefBooleanFromTypes: function(aArray) {
		if (aArray) {
			for (let type of aArray) {
				var upperElement = type.toUpperCase();
				if (upperElement === "PREF" || upperElement === "TYPE=PREF") {
					return true;
				} else if (upperElement.replace(/PREF=[0-9]*/i,"PREF") == "PREF") {
					return true;
				} else if (upperElement.replace(/^TYPE=/ig,"") !== upperElement) {
					var tmpArray = type.replace(/^TYPE=/ig,"").split(",");
					for (var j = 0; j < tmpArray.length; j++) {
						var upperElement1 = tmpArray[j].toUpperCase();
						if (upperElement1 === "PREF") {
							return true;
						} else if (upperElement1.replace(/PREF=[0-9]*/i,"PREF") == "PREF") {
							return true;
						}
					}
				}
			}
		}
		return false;
	},

	getPrefValueFromTypes: function(aArray, aVersion) {
		if (aVersion == "3.0") {
			return "";
		} else if (cardbookHTMLUtils.getPrefBooleanFromTypes(aArray)) {
			for (var i = 0; i < aArray.length; i++) {
				var upperElement = aArray[i].toUpperCase();
				if (upperElement === "PREF" || upperElement === "TYPE=PREF") {
					continue;
				} else if (upperElement.replace(/PREF=[0-9]*/i,"PREF") == "PREF") {
					return upperElement.replace(/PREF=/i,"");
				} else if (upperElement.replace(/^TYPE=/i,"") !== upperElement) {
					var tmpArray = aArray[i].replace(/^TYPE=/ig,"").split(",");
					for (var j = 0; j < tmpArray.length; j++) {
						var upperElement1 = tmpArray[j].toUpperCase();
						if (upperElement1 === "PREF") {
							continue;
						} else if (upperElement1.replace(/PREF=[0-9]*/i,"PREF") == "PREF") {
							return upperElement1.replace(/PREF=/i,"");
						}
					}
				}
			}
		}
		return "";
	},

	formatExtension: function (aExtension, aVersion) {
		switch (aExtension) {
			case "pgp":
			case "pub":
				aExtension = "pgp";
				break;
			case "JPG":
			case "jpg":
				aExtension = "jpeg";
				break;
			case "TIF":
			case "tif":
				aExtension = "tiff";
				break;
			case "":
				aExtension = "jpeg";
		}
		if (aExtension.replace(/([\\\/\:\*\?\"\<\>\|]+)/g, "-") != aExtension) {
			aExtension = "jpeg";
		}
		if (aVersion == "4.0") {
			aExtension = aExtension.toLowerCase();
		} else {
			aExtension = aExtension.toUpperCase();
		}
		return aExtension;
	},

	formatTelForSearching: function (aString) {
		// +33 6 45 44 42 25 should be equal to 06 45 44 42 25 should be equal to 00 33 6 45 44 42 25 should be equal to 0645444225
		return aString.replace(/^\+\d+\s+/g, "").replace(/^00\s+\d+\s+/g, "").replace(/\D/g, "").replace(/^0/g, "");
	},

	formatTelForOpenning: function (aString) {
		return aString.replace(/\s*/g, "").replace(/-*/g, "").replace(/\.*/g, "");
	},

	formatIMPPForOpenning: function (aString) {
		return aString.replace(/\s*/g, "");
	},

	// aTypesList should be escaped
	// TYPE="WORK,VOICE" would be splitted into TYPE=WORK,TYPE=HOME
	// the duplicate types would also be removed
	formatTypes: function (aTypesList) {
		var result = [];
		for (var i = 0; i < aTypesList.length; i++) {
			var myTempString = aTypesList[i].replace(/\"/g,"");
			if ((myTempString.indexOf(",") != -1) && (myTempString.indexOf("TYPE=",0) == 0)) {
				var myTempArray = myTempString.replace(/^TYPE=/, "").split(",");
				for (var j = 0; j < myTempArray.length; j++) {
					result.push("TYPE=" + myTempArray[j]);
				}
			} else if (myTempString && myTempString != "") {
				result.push(myTempString);
			}
		}
		return cardbookHTMLUtils.arrayUnique(result);
	},

	formatTypesForDisplay: function (aTypeList) {
		aTypeList = cardbookHTMLUtils.cleanArray(aTypeList);
		return cardbookHTMLUtils.sortArrayByString(aTypeList, 1).join("    ");
	},

	getFileExtension: function (aFile) {
		let fileArray = aFile.split("/");
		let fileArray1 = fileArray[fileArray.length-1].split("\\");
		return cardbookHTMLUtils.getFileNameExtension(fileArray1[fileArray1.length-1]);
	},

	getFileNameExtension: function (aFileName) {
		let fileArray = aFileName.split(".");
		let myExtension = "";
		if (fileArray.length != 1) {
			myExtension = fileArray[fileArray.length - 1];
		}
		return myExtension;
	},

	getName: function (aCard) {
		let showNameAs = cardbookBGPreferences.getPref("showNameAs");
		if (aCard.isAList || showNameAs == "DSP") {
			return aCard.fn;
		}
		if (aCard.lastname != "" && aCard.firstname != "") {
			let result = "";
			if (showNameAs == "LF") {
				result = aCard.lastname + " " + aCard.firstname;
			} else if (showNameAs == "FL") {
				result = aCard.firstname + " " + aCard.lastname;
			} else if (showNameAs == "LFCOMMA") {
				result = aCard.lastname + ", " + aCard.firstname;
			}
			return result.trim();
		} else {
			return aCard.fn;
		}
	},

	convertVCard: async function (aCard, aTargetName, aTargetVersion, aDateFormatSource, aDateFormatTarget, aPossibleCustomFields) {
		let converted = false;
		// basic fields
		if (aCard.version != aTargetVersion) {
			converted = true;
			aCard.version = aTargetVersion;
			for (let newField of cardbookHTMLUtils.newFields) {
				let oldField = "X-" + newField.toUpperCase();
				if (aTargetVersion == "3.0") {
					if (aCard[newField] != "") {
						aCard.others.push(oldField + ":" + aCard[newField]);
						if (!aPossibleCustomFields[oldField].add && !aPossibleCustomFields[oldField].added) {
							aPossibleCustomFields[oldField].add = true;
						}
						aCard[newField] = "";
					}
				} else if (aTargetVersion == "4.0") {
					for (let k = 0; k < aCard.others.length; k++) {
						if (aCard.others[k].startsWith(oldField + ":")) {
							let newFieldRegExp = new RegExp("^" + oldField + ":");
							aCard[newField] = aCard.others[k].replace(newFieldRegExp, "");
							aCard.others.splice(k,1);
							break;
						}
					}
				}
			}

			// lists
			let kindCustom = cardbookBGPreferences.getPref("kindCustom");
			let memberCustom = cardbookBGPreferences.getPref("memberCustom");
			if (aCard.isAList) {
				if (aTargetVersion == "3.0") {
					cardbookHTMLUtils.addMemberstoCard(aCard, aCard.member, aCard.kind);
					aCard.member = "";
					aCard.kind = "";
				} else if (aTargetVersion == "4.0") {
					let myMembers = [];
					let myGroup = "";
					for (let j = 0; j < aCard.others.length; j++) {
						if (aCard.others[j].startsWith(memberCustom + ":")) {
							let myFieldRegExp = new RegExp("^" + memberCustom + ":");
							myMembers.push(aCard.others[j].replace(myFieldRegExp, ""));
							aCard.others.splice(j,1);
							j--;
						} else if (aCard.others[j].startsWith(kindCustom + ":")) {
							let myFieldRegExp = new RegExp("^" + kindCustom + ":");
							myGroup = aCard.others[j].replace(myFieldRegExp, "");
							aCard.others.splice(j,1);
							j--;
						}
					}
					cardbookHTMLUtils.addMemberstoCard(aCard, myMembers, myGroup);
				}
			}
		}
		// date fields
		if (aDateFormatSource != aDateFormatTarget) {
			if (await cardbookHTMLDates.convertCardDate(aCard, aTargetName, aDateFormatSource, aDateFormatTarget)) {
				converted = true;
			}
		}
		return {converted: converted, possibleCustomFields: aPossibleCustomFields};
	},

	getPrefAddressFromCard: function (aCard, aType, aAddressPref) {
		let listOfAddress = [];
		if (aCard) {
			let notfoundOnePrefAddress = true;
			let listOfPrefAddress = [];
			let prefValue;
			let oldPrefValue = 0;
			for (var j = 0; j < aCard[aType].length; j++) {
				var addressText = aCard[aType][j][0][0];
				if (aAddressPref) {
					for (var k = 0; k < aCard[aType][j][1].length; k++) {
						if (aCard[aType][j][1][k].toUpperCase().indexOf("PREF") >= 0) {
							if (aCard[aType][j][1][k].toUpperCase().indexOf("PREF=") >= 0) {
								prefValue = aCard[aType][j][1][k].toUpperCase().replace("PREF=","");
							} else {
								prefValue = 1;
							}
							if (prefValue == oldPrefValue || oldPrefValue === 0) {
								listOfPrefAddress.push(addressText);
								oldPrefValue = prefValue;
							} else if (prefValue < oldPrefValue) {
								listOfPrefAddress = [];
								listOfPrefAddress.push(addressText);
								oldPrefValue = prefValue;
							}
							notfoundOnePrefAddress = false;
						}
					}
				} else {
					listOfAddress.push(addressText);
					notfoundOnePrefAddress = false;
				}
			}
			if (notfoundOnePrefAddress) {
				for (var j = 0; j < aCard[aType].length; j++) {
					listOfAddress.push(aCard[aType][j][0][0]);
				}
			} else {
				for (var j = 0; j < listOfPrefAddress.length; j++) {
					listOfAddress.push(listOfPrefAddress[j]);
				}
			}
		}
		return listOfAddress;
	},

	addMemberstoCard: function(aCard, aMemberLines, aKindValue) {
		if (aCard.version == "4.0") {
			aCard.member = JSON.parse(JSON.stringify(aMemberLines));
			if (aKindValue) {
				aCard.kind = aKindValue;
			} else {
				aCard.kind = "group";
			}
		} else if (aCard.version == "3.0") {
			let kindCustom = cardbookBGPreferences.getPref("kindCustom");
			let memberCustom = cardbookBGPreferences.getPref("memberCustom");
			for (var i = 0; i < aCard.others.length; i++) {
				let localDelim1 = aCard.others[i].indexOf(":",0);
				if (localDelim1 >= 0) {
					let header = aCard.others[i].substr(0, localDelim1).toUpperCase();
					if (header == kindCustom || header == memberCustom) {
						aCard.others.splice(i, 1);
						i--;
						continue;
					}
				}
			}
			for (let member of aMemberLines) {
				if (i === 0) {
					if (aKindValue) {
						aCard.others.push(`${kindCustom}:${aKindValue}`);
					} else {
						aCard.others.push(`${kindCustom}:group`);
					}
				}
				aCard.others.push(`${memberCustom}:${member}`);
			}
		}
	},

	formatAddressForInput: function(aAddress, aAdrFormula) {
		let value = cardbookHTMLUtils.formatAddress(aAddress, aAdrFormula);
		let regExp = new RegExp("\\n", "g");
		return value.replace(regExp, " ");
	},

	formatAddress: function(aAddress, aAdrFormula) {
		let result =  "";
		var adrFormula = cardbookBGPreferences.getPref("adrFormula");
		let defaultAdrFormula = messenger.i18n.getMessage("addressFormatFormula");
		let myAdrFormula = aAdrFormula ||
							adrFormula ||
							defaultAdrFormula;
		let addressArray = JSON.parse(JSON.stringify(aAddress));
		// maybe a country code (Google uses them)
		let lcRegionCode = addressArray[6].toLowerCase();
		if (cardbookHTMLUtils.countriesList.includes(lcRegionCode)) {
			addressArray[6] = messenger.i18n.getMessage("region-name-" + lcRegionCode);
		}
		result = cardbookHTMLFormulas.getStringFromFormula(myAdrFormula, addressArray);
		var re = /[\n\u0085\u2028\u2029]|\r\n?/;
		var myAdrResultArray = result.split(re);
		return cardbookHTMLUtils.cleanArray(myAdrResultArray).join("\n");
	},

	getCardRegion: function (aCard) {
		let i = 0;
		while (true) {
			if (aCard.adr[i] && aCard.adr[i][0]) {
				var country = aCard.adr[i][0][6].toUpperCase();
				if (country != "") {
					let lcRegionCode = country.toLowerCase();
					// maybe a country code
					if (cardbookHTMLUtils.countriesList.includes(lcRegionCode)) {
						return country;
					}
					// let's try to find a known country
					for (let code of cardbookHTMLUtils.countriesList) {
						let locName = messenger.i18n.getMessage("region-name-" + code);
						if (lcRegionCode == locName.toLowerCase()) {
							return code.toUpperCase();
						}
					}
				}
				i++;
			} else {
				return cardbookBGPreferences.getPref("defaultRegion");
			}
		}
		return "";
	},

	getCountries: function (useCodeValues) {
		let result = [];
		for (let code of cardbookHTMLUtils.countriesList) {
			let country = messenger.i18n.getMessage("region-name-" + code);
			if (useCodeValues) {
				result.push([code.toUpperCase(), country]);
			} else {
				result.push([country, country]);
			}
		}
		cardbookHTMLUtils.sortMultipleArrayByString(result,1,1);
		return result;
	},

	openURL: async function (aURL) {
		let localizeTarget = cardbookBGPreferences.getPref("localizeTarget");
		if (localizeTarget == "in") {
			await messenger.runtime.sendMessage({query: "cardbook.openTab", url: aURL});
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.openExternalURL", link: aURL})
		}
	},

	getAddressesFromCards: function (aListOfCards) {
		let listOfAddresses= [];
		if (aListOfCards) {
			for (let card of aListOfCards) {
				for (let adr of card.adr) {
					let adress = adr[0];
					listOfAddresses.push(adress);
				}
			}
		}
		return listOfAddresses;
	},

	getURLsFromCards: function (aListOfCards) {
		let listOfURLs= [];
		if (aListOfCards) {
			for (let card of aListOfCards) {
				for (let url of card.url) {
					listOfURLs.push(url[0][0]);
				}
			}
		}
		return listOfURLs;
	},

	localizeCards: async function (aListOfSelectedCard, aListOfSelectedAddresses) {
		let listOfAddresses = [];
		if (aListOfSelectedCard) {
			listOfAddresses = cardbookHTMLUtils.getAddressesFromCards(aListOfSelectedCard);
		} else if (aListOfSelectedAddresses) {
			listOfAddresses = JSON.parse(JSON.stringify(aListOfSelectedAddresses));
		}
		
		let localizeEngine = cardbookBGPreferences.getPref("localizeEngine");
		let urlEngine = "";
		if (localizeEngine === "GoogleMaps") {
			urlEngine = "https://www.google.com/maps?q=";
		} else if (localizeEngine === "OpenStreetMap") {
			urlEngine = "https://www.openstreetmap.org/search?query=";
		} else if (localizeEngine === "BingMaps") {
			urlEngine = "https://www.bing.com/maps/?q=";
		} else {
			return;
		}

		function getAddressURL(address) {
			let result = JSON.parse(JSON.stringify(address));
			result.shift();
			result = result.join("+").replace(/[\n\u0085\u2028\u2029]|\r\n?/g, "+").replace(/ /g, "+");
			return result;
		}

		if (localizeEngine === "GoogleMaps") {
			let url = urlEngine;
			for (let address of listOfAddresses) {
				url = url + getAddressURL(address) + "/";
			}
			await cardbookHTMLUtils.openURL(url);
		} else {
			for (let address of listOfAddresses) {
				let url = urlEngine + getAddressURL(address);
				await cardbookHTMLUtils.openURL(url);
			}
		}
	},

	// not possible to include prefs for ADR field
	getCardValueByField: function(aCard, aField, aIncludePref) {
		var result = [];
		if (aField.indexOf("_") > 0) {
			var myFieldArray = aField.split("_");
			var myField = myFieldArray[0];
			var myPosition = myFieldArray[1];
			if (myField == "org") {
				let orgStructure = cardbookBGPreferences.getPref("orgStructure");
				if (orgStructure.length) {
					result.push(aCard[myField][myPosition]);
				} else {
					result.push(aCard[myField][0]);
				}
			// adr_all, adr_5_all, adr_adr_privé, adr_5_adr_privé
			} else if (myField == "adr") {
				var myFieldArray = aField.split("_");
				var myField = myFieldArray[0];
				if (aCard[myField].length) {
					if (["0", "1", "2", "3", "4", "5", "6"].includes(myFieldArray[1])) {
						let subField = parseInt(myFieldArray[1]);
						let type = myFieldArray.slice(2).join("_");
						if (type == "all") {
							if (aCard[myField]) {
								for (var i = 0; i < aCard[myField].length; i++) {
									if (aCard[myField][i][0][subField]) {
										result.push(aCard[myField][i][0][subField]);
									}
								}
							}
						} else if (type == "notype") {
							if (aCard[myField]) {
								for (var i = 0; i < aCard[myField].length; i++) {
									let inputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
									let emptyType = inputTypes.length == 0 || inputTypes[0] == "";
									if (emptyType && aCard[myField][i][3].length == 0 && aCard[myField][i][2] == "") {
										if (aCard[myField][i][0][subField]) {
											result.push(aCard[myField][i][0][subField]);
										}
									}
								}
							}
						} else {
							for (var i = 0; i < aCard[myField].length; i++) {
								let found = false;
								if (aCard[myField][i][0][subField]) {
									if (aCard[myField][i][3].length != 0 && aCard[myField][i][2] != "") {
										for (var j = 0; j < aCard[myField][i][3].length; j++) {
											let tmpArray = aCard[myField][i][3][j].split(":");
											if (tmpArray[0] == "X-ABLABEL" && tmpArray[1].replaceAll(" ", "_") == type) {
												found = true;
												break;
											}
										}
									}
									if (found) {
										if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
											result.push(cardbookHTMLUtils.prefCSVPrefix + aCard[myField][i][0][subField]);
										} else {
											result.push(aCard[myField][i][0][subField]);
										}
									} else {
										var ABType = cardbookBGPreferences.getType(aCard.dirPrefId);
										var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
										var myInputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
										if (cardbookHTMLTypes.isMyCodePresent(myField, type, ABTypeFormat, myInputTypes)) {
											if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
												result.push(cardbookHTMLUtils.prefCSVPrefix + aCard[myField][i][0][subField]);
											} else {
												result.push(aCard[myField][i][0][subField]);
											}
										}
									}
								}
							}
						}
					} else {
						let type = myFieldArray.slice(1).join("_");
						if (type == "all") {
							if (aCard[myField]) {
								for (var i = 0; i < aCard[myField].length; i++) {
									let address = cardbookHTMLUtils.formatAddress(aCard[myField][i][0]);
									if (address) {
										result.push(address);
									}
								}
							}
						} else if (type == "notype") {
							if (aCard[myField]) {
								for (var i = 0; i < aCard[myField].length; i++) {
									let inputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
									let emptyType = inputTypes.length == 0 || inputTypes[0] == "";
									if (emptyType && aCard[myField][i][3].length == 0 && aCard[myField][i][2] == "") {
										let address = cardbookHTMLUtils.formatAddress(aCard[myField][i][0]);
										if (address) {
											result.push(address);
										}
									}
								}
							}
						} else {
							for (var i = 0; i < aCard[myField].length; i++) {
								let found = false;
								let address = cardbookHTMLUtils.formatAddress(aCard[myField][i][0]);
								if (address) {
									if (aCard[myField][i][3].length != 0 && aCard[myField][i][2] != "") {
										for (var j = 0; j < aCard[myField][i][3].length; j++) {
											let tmpArray = aCard[myField][i][3][j].split(":");
											if (tmpArray[0] == "X-ABLABEL" && tmpArray[1].replaceAll(" ", "_") == type) {
												found = true;
												break;
											}
										}
									}
									if (found) {
										if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
											result.push(cardbookHTMLUtils.prefCSVPrefix + address);
										} else {
											result.push(address);
										}
									} else {
										var ABType = cardbookBGPreferences.getType(aCard.dirPrefId);
										var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
										var myInputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
										if (cardbookHTMLTypes.isMyCodePresent(myField, type, ABTypeFormat, myInputTypes)) {
											if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
												result.push(cardbookHTMLUtils.prefCSVPrefix + address);
											} else {
												result.push(address);
											}
										}
									}
								}
							}
						}
					}
				}
			} else {
				var myType = myFieldArray.slice(2).join("_");
				if (myType == "all") {
					if (aCard[myField]) {
						for (var i = 0; i < aCard[myField].length; i++) {
							if (aCard[myField][i][0][myPosition] != "") {
								result.push(aCard[myField][i][0][myPosition]);
							}
						}
					}
				} else if (myType == "array") {
					if (aCard[myField].length != 0) {
						result = result.concat(aCard[myField]);
					}
				} else {
					if (aCard[myField]) {
						var ABType = cardbookBGPreferences.getType(aCard.dirPrefId);
						var ABTypeFormat = cardbookHTMLUtils.getABTypeFormat(ABType);
						for (var i = 0; i < aCard[myField].length; i++) {
							if (myType == "notype") {
								let inputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
								let emptyType = inputTypes.length == 0 || inputTypes[0] == "";
								if (emptyType && aCard[myField][i][3].length == 0 && aCard[myField][i][2] == "") {
									result.push(aCard[myField][i][0][myPosition]);
								}
							} else {
								let found = false;
								if (aCard[myField][i][3].length != 0 && aCard[myField][i][2] != "") {
									for (var j = 0; j < aCard[myField][i][3].length; j++) {
										let tmpArray = aCard[myField][i][3][j].split(":");
										if (tmpArray[0] == "X-ABLABEL" && tmpArray[1].replaceAll(" ", "_") == myType) {
											found = true;
											break;
										}
									}
								}
								if (found) {
									if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
										result.push(cardbookHTMLUtils.prefCSVPrefix + aCard[myField][i][0][myPosition]);
									} else {
										result.push(aCard[myField][i][0][myPosition]);
									}
								} else {
									var myInputTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
									if (cardbookHTMLTypes.isMyCodePresent(myField, myType, ABTypeFormat, myInputTypes)) {
										if (aIncludePref && cardbookHTMLUtils.getPrefBooleanFromTypes(aCard[myField][i][1])) {
											result.push(cardbookHTMLUtils.prefCSVPrefix + aCard[myField][i][0][myPosition]);
										} else {
											result.push(aCard[myField][i][0][myPosition]);
										}
									}
								}
							}
						}
					}
				}
			}
		} else if (aField == "age") {
			result.push(cardbookHTMLDates.getAge(aCard));
		} else if (aField == "ABName") {
			result.push(cardbookBGPreferences.getName(aCard.dirPrefId));
		} else {
			if (aCard[aField]) {
				result.push(aCard[aField]);
			} else {
				for (var i = 0; i < aCard.others.length; i++) {
					let delim = aCard.others[i].indexOf(":", 0);
					let header = aCard.others[i].substr(0, delim);
					let value = aCard.others[i].substr(delim+1, aCard.others[i].length);
					let headerTmp = header.split(";");
					if (aField == headerTmp[0]) {
						result.push(value);
						break;
					}
				}
			}
		}
		return result;
	},

	setCardValueByField: function(aCard, aField, aValue) {
		aValue = aValue.replace(/^\"|\"$/g, "").trim();
		if (aValue == "") {
			return;
		} else if (aField == "blank") {
			return;
		} else if (aField == "org") {
			aCard[aField][0] = aValue;
		} else if (aField.indexOf("_") > 0) {
			var myFieldArray = aField.split("_");
			var myField = myFieldArray[0];
			var myPosition = myFieldArray[1];
			var myType = myFieldArray.slice(2).join("_");
			if (aCard[myField]) {
				// adr may only have one value and one type
				if (myField == "org") {
					let orgStructure = cardbookBGPreferences.getPref("orgStructure");
					for (let i = 0; i < orgStructure.length; i++) {
						if (!aCard[myField][i]) {
							aCard[myField][i] = "";
						}
					}
					aCard[myField][myPosition] = aValue;
				} else if (myField == "adr") {
					var myType2 = "";
					if (myType != "notype") {
						for (var j = 0; j < cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField].length; j++) {
							if (cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField][j][0] == myType) {
								var myCode = cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField][j][1].split(";");
								myType2 = myCode[0];
								break;
							}
						}
					}
					var found = false;
					for (var i = 0; i < aCard[myField].length; i++) {
						var myTypes = cardbookHTMLUtils.getOnlyTypesFromTypes(aCard[myField][i][1]);
						if (myTypes.length == 0 && myType == "notype") {
							aCard[myField][i][0][myPosition] = aValue;
							found = true;
							break;
						} else {
							for (var j = 0; j < myTypes.length; j++) {
								if (myType2.toLowerCase() == myTypes[j].toLowerCase()) {
									aCard[myField][i][0][myPosition] = aValue;
									found = true;
									break;
								}
							}
						}
					}
					if (!found) {
						if (myType != "notype") {
							aCard[myField].push([ ["", "", "", "", "", "", ""], ["TYPE=" + myType2], "", [] ]);
						} else {
							aCard[myField].push([ ["", "", "", "", "", "", ""], [], "", [] ]);
						}
						aCard[myField][i][0][myPosition] = aValue;
					} else {
						// now merge the types if possible
						var valueArray = [];
						for (var i = 0; i < aCard[myField].length; i++) {
							valueArray.push([i, aCard[myField][i][0].join()]);
						}
						var found = false;
						if (valueArray.length > 1) {
							for (var i = 0; i < valueArray.length; i++) {
								for (var j = i+1; j < valueArray.length; j++) {
									if (valueArray[i][1] == valueArray[j][1]) {
										aCard[myField][valueArray[i][0]][1] = aCard[myField][valueArray[i][0]][1].concat(aCard[myField][valueArray[j][0]][1]);
										aCard[myField][valueArray[i][0]][1] = cardbookHTMLUtils.arrayUnique(aCard[myField][valueArray[i][0]][1]);
										valueArray.splice(j, 1);
										aCard[myField].splice(j, 1);
										j--;
									}
								}
							}
						}
						
					}
				} else if (myField == "categories") {
					aCard[myField] = cardbookHTMLUtils.unescapeArray(cardbookHTMLUtils.escapeString(aValue).split(","));
				// these fields may have multiples values and multiples types
				} else {
					var re = /[\n\u0085\u2028\u2029]|\r\n?/;
					var aValueArray = aValue.split(re);
					for (var i = 0; i < aValueArray.length; i++) {
						var myPref = false;
						if (aValueArray[i].slice(0, 2) == cardbookHTMLUtils.prefCSVPrefix) {
							aValueArray[i] = aValueArray[i].slice(2);
							myPref = true;
						}
						if (myType == "notype" || myType == "all") {
							if (myPref) {
								aCard[myField].push([ [aValueArray[i]], ["TYPE=PREF"], "", [] ]);
							} else {
								aCard[myField].push([ [aValueArray[i]], [], "", [] ]);
							}
						} else {
							var myType2 = "";
							for (var j = 0; j < cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField].length; j++) {
								if (cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField][j][0] == myType) {
									var myCode = cardbookHTMLTypes.cardbookCoreTypes["CARDDAV"][myField][j][1].split(";");
									myType2 = myCode[0];
									break;
								}
							}
							if (myType2 != "") {
								if (myPref) {
									aCard[myField].push([ [aValueArray[i]], ["TYPE=PREF", "TYPE=" + myType2], "", [] ]);
								} else {
									aCard[myField].push([ [aValueArray[i]], ["TYPE=" + myType2], "", [] ]);
								}
							} else {
								let PGNextNumber = cardbookHTMLUtils.rebuildAllPGs(aCard);
								myType = myType.replaceAll("_", " ");
								if (myPref) {
									aCard[myField].push([ [aValueArray[i]], ["TYPE=PREF"], "ITEM" + PGNextNumber, ["X-ABLABEL:" + myType] ]);
								} else {
									aCard[myField].push([ [aValueArray[i]], [], "ITEM" + PGNextNumber, ["X-ABLABEL:" + myType] ]);
								}
							}
						}
					}
				}
			}
		} else {
			let found = false;
			let customFields = cardbookBGPreferences.getAllCustomFields();
			for (let i in customFields) {
				for (let customField of customFields[i]) {
					if (customField[0] == aField) {
						aCard.others.push(aField + ":" + aValue);
						found = true;
						break;
					}
				}
			}
			if (!found) {
				aCard[aField] = aValue;
			}
		}
	},

	getUUID: function () {
		/*function uuidv4() {
			return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
				(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
			);
		}
		return uuidv4();*/

		return crypto.randomUUID();
	},

	getFieldValue: async function (aCbId, aFieldName, aFieldIndex) {
		let card = await messenger.runtime.sendMessage({query: "cardbook.getCardWithId", cbid: aCbId});
		if (aFieldName == "adr") {
			let value = card[aFieldName][aFieldIndex]
			let formattedValue = cardbookHTMLUtils.formatAddress(card[aFieldName][aFieldIndex][0]);
			return [ formattedValue, value ];
		} else if (cardbookHTMLUtils.multilineFields.includes(aFieldName) || aFieldName == "tz") {
			return [ card[aFieldName][aFieldIndex][0][0], card[aFieldName][aFieldIndex] ];
		} else if (cardbookHTMLUtils.adrElements.includes(aFieldName)) {
			let partialIndex = cardbookHTMLUtils.adrElements.indexOf(aFieldName);
			return [ card.adr[aFieldIndex][0][partialIndex], card.adr[aFieldIndex][0][partialIndex] ];
		} else if (aFieldName == "event") {
			let events = cardbookHTMLUtils.getEventsFromCard(card.note.split("\n"), card.others);
			let event = events.result[aFieldIndex];
			let dateDisplayedFormat = cardbookBGPreferences.getPref("dateDisplayedFormat");
			let myFormattedDate = cardbookHTMLDates.getFormattedDateForDateString(event[0], dateDisplayedFormat);
			return [ `${myFormattedDate} ${event[1]}`, event];
		} else if (aFieldName.startsWith("X-")) {
			let value = cardbookHTMLUtils.getCardValueByField(card, aFieldName, false)[0];
			return [value, value ];
		} else if (aFieldName.startsWith("org_")) {
			let value = cardbookHTMLUtils.getCardValueByField(card, "org", false)[0];
			let orgStructure = cardbookBGPreferences.getPref("orgStructure");
			let arr = aFieldName.split("_");
			arr.shift();
			let name = arr.join("_");
			let index = orgStructure.indexOf(name);
			let result = "";
			for (let j = 0; j <= index; j++) {
				result = result + "::" + value[j];
			}
			return [ value[index], result ];
		} else if (cardbookHTMLDates.dateFields.includes(aFieldName)) {
			let value = cardbookHTMLUtils.getCardValueByField(card, aFieldName, false)[0];
			return [ cardbookHTMLDates.getFormattedDateForCard(card, aFieldName), value ];
		} else {
			let value = cardbookHTMLUtils.getCardValueByField(card, aFieldName, false)[0];
			return [value, value ];
		}
	},

	getCountryCodeFromCountryName: function (aName) {
		for (let code of cardbookHTMLUtils.countriesList) {
			let locName = messenger.i18n.getMessage("region-name-" + code);
			if (locName.toLowerCase() == aName.toLowerCase()) {
				return code.toUpperCase();
			}
		}
		return aName;
	},

	saveWindowSize: async function(aWindowName, aWindowState) {
		let prefName = `window.${aWindowName}.state`;
		await cardbookBGPreferences.setPref(prefName, aWindowState);
	},
};
