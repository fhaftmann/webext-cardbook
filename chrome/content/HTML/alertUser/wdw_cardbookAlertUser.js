import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";

var wdw_cardbookAlertUser = {
	type: "",
	action: "",
	winId: "",
	lineId: "",
	title: "",
	message: "",
	checkboxAction: "",
	checkboxValue: false,
	checkboxMsg: "",
	buttons: "",
	validateMsg: "",

	load: function () {
		let urlParams = new URLSearchParams(window.location.search);
		wdw_cardbookAlertUser.type = urlParams.get("type");
		wdw_cardbookAlertUser.action = urlParams.get("action");
		wdw_cardbookAlertUser.winId = urlParams.get("winId") || "";
		wdw_cardbookAlertUser.lineId = urlParams.get("lineId");
		wdw_cardbookAlertUser.title = urlParams.get("title");
		wdw_cardbookAlertUser.message = urlParams.get("message");
		wdw_cardbookAlertUser.buttons = urlParams.get("buttons") || "";
		wdw_cardbookAlertUser.checkboxAction = urlParams.get("checkboxAction") || "";
		wdw_cardbookAlertUser.checkboxMsg = urlParams.get("checkboxMsg");
		wdw_cardbookAlertUser.checkboxValue = urlParams.get("checkboxValue");
		wdw_cardbookAlertUser.validateMsg = urlParams.get("validateMsg");

		i18n.updateDocument();
		cardbookHTMLRichContext.loadRichContext();
	
		// button
		document.getElementById("validateButton").addEventListener("click", event => wdw_cardbookAlertUser.validate(event));
		document.getElementById("cancelButton").addEventListener("click", event => wdw_cardbookAlertUser.close(event));

		document.title = wdw_cardbookAlertUser.title;
		document.getElementById("messageLabel").textContent = wdw_cardbookAlertUser.message;
		if (wdw_cardbookAlertUser.type == "info") {
			document.getElementById("infoIcon").classList.add("infoIcon");
		} else if (wdw_cardbookAlertUser.type == "confirm") {
			document.getElementById("infoIcon").classList.add("questionIcon");
		} else if (wdw_cardbookAlertUser.type == "confirmEx") {
			document.getElementById("infoIcon").classList.add("questionIcon");
			document.getElementById("checkboxContainer").classList.remove("hidden");
			document.getElementById("confirmCheckBox").checked = (wdw_cardbookAlertUser.checkboxValue === "true");
			document.querySelector(`label[for='confirmCheckBox']`).textContent = wdw_cardbookAlertUser.checkboxMsg;
			if (wdw_cardbookAlertUser.validateMsg) {
				document.getElementById("validateButton").textContent = wdw_cardbookAlertUser.validateMsg;
			}
			document.getElementById("validateButton").classList.add("hidden");
			document.getElementById("cancelButton").classList.add("hidden");
			for (let button of wdw_cardbookAlertUser.buttons.split("|")) {
				document.getElementById(`${button}Button`).classList.remove("hidden");
			}
		} else if (wdw_cardbookAlertUser.type == "alert") {
			document.getElementById("infoIcon").classList.add("warningIcon");
			document.getElementById("cancelButton").classList.add("hidden");
		}
	},

	validate: async function () {
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		if (wdw_cardbookAlertUser.type == "confirm" || wdw_cardbookAlertUser.type == "confirmEx") {
			await messenger.runtime.sendMessage({query: "cardbook.alertUser", action: wdw_cardbookAlertUser.action, checkboxAction: document.getElementById("confirmCheckBox").checked, winId: wdw_cardbookAlertUser.winId, lineId: wdw_cardbookAlertUser.lineId, response: true});
		}
		cardbookHTMLRichContext.closeWindow();
	},

	close: async function () {
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		if (wdw_cardbookAlertUser.type == "confirm" || wdw_cardbookAlertUser.type == "confirmEx") {
			await messenger.runtime.sendMessage({query: "cardbook.alertUser", action: wdw_cardbookAlertUser.action, checkboxAction: document.getElementById("confirmCheckBox").checked, winId: wdw_cardbookAlertUser.winId, lineId: wdw_cardbookAlertUser.lineId, response: false});
		}
		cardbookHTMLRichContext.closeWindow();
	}
};

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

// messenger.windows.onFocusChanged.addListener(async (windowId) => {
// 	await wdw_cardbookAlertUser.close();
// });

wdw_cardbookAlertUser.load();

