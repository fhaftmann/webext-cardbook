import { cardbookHTMLPermissions } from "../utils/scripts/cardbookHTMLPermissions.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";
import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLDirTree } from "../utils/scripts/cardbookHTMLDirTree.mjs";
import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLTypes } from "../utils/scripts/cardbookHTMLTypes.mjs";
import { cardbookHTMLDates } from "../utils/scripts/cardbookHTMLDates.mjs";
import { cardbookHTMLWindowUtils } from "../utils/scripts/cardbookHTMLWindowUtils.mjs";
import { cardbookHTMLImages } from "../utils/scripts/cardbookHTMLImages.mjs";
import { cardbookHTMLToolbar } from "../utils/scripts/cardbookHTMLToolbar.mjs"
import { cardbookHTMLComplexSearch } from "../utils/scripts/cardbookHTMLComplexSearch.mjs"
import { cardbookHTMLPluralRules } from "../utils/scripts/cardbookHTMLPluralRules.mjs"
import { cardbookHTMLDownloads } from "../utils/scripts/cardbookHTMLDownloads.mjs"

import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "../../BG/utils/cardbookBGUtils.mjs";

import { cardbookIDBImage } from "../../BG/indexedDB/cardbookIDBImage.mjs";
import { cardbookIDBCard } from "../../BG/indexedDB/cardbookIDBCard.mjs";
import { cardbookIDBCat } from "../../BG/indexedDB/cardbookIDBCat.mjs";

import { MimeAddressParser } from "../../BG/mimeParser/MimeJSComponents.sys.mjs";

import { CBTree } from "../utils/scripts/cardbookHTMLCardsTree.js";
import { cardbookCardParser } from "../../BG/utils/cardbookCardParser.mjs";

var wdw_mainWindow = {
	initTreesDone: false,
	accountSelected: false,
	currentAccountId: "",
	cardbookComplexSearchMode: "NOSEARCH",
	cardsTree: {},
	searchInput: {},
	searchAllInput: {},
	searchAB: "allAddressBooks",
	matchAll: true,
	rules: [],
	searchTimer: null,
	complexSearchTimer: null,
	winId: "",
	compFields: [],
	context: {},
	copy: {},
	copyField: {},
	displayId: 0,
	searchId: 0,
	accounts: [],

	getAccounts: async function () {
		let accounts = await messenger.runtime.sendMessage({query: "cardbook.getAccounts"});
		wdw_mainWindow.accounts = JSON.parse(JSON.stringify(accounts));
		return accounts;
	},

	setAccountsTreeMenulist: function () {
		let accountsShown = cardbookBGPreferences.getPref("accountsShown");
		let select = document.getElementById("accountsOrCatsTreeSelect");

		let values = [ "all", "enabled", "disabled", "local", "remote", "search" ];
		let options = [];
        for (let value of values) {
            let label = messenger.i18n.getMessage(`${value}AccountsLabel`);
            let checked = false;
             if (accountsShown == value) {
				select.setButtonLabel(label);
                checked = true;
            }
			options.push([label, value, checked]);
        }
		select.loadOptions("accountsOrCatsTreeSelect", options, wdw_mainWindow.changeAddressbookTreeMenu, [], null);
		select.setClass([ "down", "arrow-down" ]);
		select.getButton().classList.add("fullWidth");
		select.setMode(true);
	},

	changeAddressbookTreeMenu: async function () {
		let select = document.getElementById("accountsOrCatsTreeSelect");
		let value = select.querySelector("li.active").id;
		let label = messenger.i18n.getMessage(`${value}AccountsLabel`)
		select.setButtonLabel(label);
		await cardbookBGPreferences.setPref("accountsShown", value);
		cardbookHTMLWindowUtils.clearCard();
		await wdw_mainWindow.refreshWindow();
	},

	createCssCardRules: function (aStyleSheet, aDirPrefId, aColor, aOppositeColor, aUseColor) {
		if (aUseColor == "text") {
			let ruleString1 = "tr[is=\"cb-table-card-row\"]." + aDirPrefId + " {color: " + aColor + ";}";
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);

			let ruleString2 = "tr[is=\"cb-table-card-row\"]." + aDirPrefId + " {background-color: " + aOppositeColor + ";}";
			let ruleIndex2 = aStyleSheet.insertRule(ruleString2, aStyleSheet.cssRules.length);
		} else if (aUseColor == "background") {
			let ruleString1 = "tr[is=\"cb-table-card-row\"]." + aDirPrefId + " {background-color: " + aColor + ";}";
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);

			let ruleString2 = "tr[is=\"cb-table-card-row\"]." + aDirPrefId + " {color: " + aOppositeColor + ";}";
			let ruleIndex2 = aStyleSheet.insertRule(ruleString2, aStyleSheet.cssRules.length);
		}
	},

	createCssCategoryRules2: function (aStyleSheet, aName, aColor, aUseColor) {
		if (aUseColor == "background" || aUseColor == "text") {
			let ruleString1 = `.${aName} {color: ${aColor};}`;
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);
		}
	},

	loadCssRules: function (aAccounts, aNodeColors) {
		let myStyleSheetRuleName = "cardbookEmpty.css";
		for (let styleSheet of document.styleSheets) {
			if (styleSheet.href.endsWith(myStyleSheetRuleName)) {
				let useColor = cardbookBGPreferences.getPref("useColor");
				cardbookHTMLUtils.deleteCssAllRules(styleSheet);
				cardbookHTMLUtils.createMarkerRule(styleSheet, myStyleSheetRuleName);
				for (let account of aAccounts) {
					let dirPrefId = account[1];
					let color = cardbookBGPreferences.getColor(dirPrefId);
					let oppositeColor = cardbookHTMLUtils.getTextColorFromBackgroundColor(color);
					if (account[2]) {
						wdw_mainWindow.createCssCardRules(styleSheet, `color_${dirPrefId}`, color, oppositeColor, useColor);
					}
				}
				for (let category in aNodeColors) {
					let color = aNodeColors[category];
					let oppositeColor = cardbookHTMLUtils.getTextColorFromBackgroundColor(color);
					let cleanName = cardbookHTMLUtils.formatCategoryForCss(category);
					wdw_mainWindow.createCssCardRules(styleSheet, `color_category_${cleanName}`, color, oppositeColor, useColor);
					wdw_mainWindow.createCssCategoryRules2(styleSheet, `${cleanName}_color`, color, useColor);
				}
				return;
			}
		}
	},

	setToolbar: function () {
		let toolbar = document.getElementById("toolbox");
		let items = cardbookHTMLToolbar.getCurrentItemsForPrefs();
		let mode = cardbookBGPreferences.getPref("toolbarMode");
		cardbookHTMLToolbar.constructToolbar(toolbar, items, mode);

		for (let element of toolbar.children) {
			let id = element.id.split("_")[0];
			if (id == "flex" || id == "space" || id == "toolsep") {
				continue;
			} else if (id == "addcontact") {
				element.addEventListener("click", event => wdw_mainWindow.newKey());
			} else if (id == "addlist") {
				element.addEventListener("click", event => wdw_mainWindow.createList());
			} else if (id == "edit") {
				element.addEventListener("click", event => wdw_mainWindow.editCard());
			} else if (id == "delete") {
				element.addEventListener("click", event => wdw_mainWindow.deleteCardsAndValidate());
			} else if (id == "sync") {
				wdw_mainWindow.setSyncButton(element);
			} else if (id == "email") {
                let options = [];
                options.push([messenger.i18n.getMessage("toEmailCardFromCardsLabel"), "to", false]);
                options.push([messenger.i18n.getMessage("ccEmailCardFromCardsLabel"), "cc", false]);
                options.push([messenger.i18n.getMessage("bccEmailCardFromCardsLabel"), "bcc", false]);
                element.loadOptions("emailButton", options, wdw_mainWindow.emailCardsFromMenuEntries, [], wdw_mainWindow.emailCardsFromMenu);
			} else if (id == "layout") {
				wdw_mainWindow.setLayoutButton();
			} else if (id == "prefs") {
				element.addEventListener("click", event => wdw_mainWindow.openPrefs());
			} else if (id == "newAB") {
				element.addEventListener("click", event => wdw_mainWindow.addAddressbook());
			} else if (id == "chat") {
				wdw_mainWindow.setChatButton();
			} else if (id == "template") {
				element.addEventListener("click", event => wdw_mainWindow.createTemplate());
			} else if (id == "print") {
				element.addEventListener("click", event => wdw_mainWindow.printCards());
			} else if (id == "undo") {
				wdw_mainWindow.setUndoAndRedoButton(element, id);
				element.addEventListener("click", event => wdw_mainWindow.undo());
			} else if (id == "redo") {
				wdw_mainWindow.setUndoAndRedoButton(element, id);
				element.addEventListener("click", event => wdw_mainWindow.redo());
			} else if (id == "search") {
				element.addEventListener("click", event => wdw_mainWindow.editComplexSearch());
			} else if (id == "newevent") {
				element.addEventListener("click", event => wdw_mainWindow.createEvent());
			} else if (id == "newtask") {
				element.addEventListener("click", event => wdw_mainWindow.createTodo());
			}
		}
	},

	setUndoAndRedoLabel: async function() {
		await messenger.runtime.sendMessage({query: "cardbook.setUndoAndRedoLabel"});
	},

	searchInputExec: async function () {
		if (wdw_mainWindow.searchInput.value) {
			document.getElementById("clearSearch").classList.remove("hidden");
		}
		cardbookHTMLWindowUtils.clearCard();
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		let ABType = cardbookBGPreferences.getType(dirPrefId);
		let remote = cardbookBGUtils.isMyAccountRemote(ABType);
		let cached = cardbookBGPreferences.getDBCached(dirPrefId);
		if (remote && !cached) {
			await cardbookBGPreferences.setLastSearch(dirPrefId, wdw_mainWindow.searchInput.value);
			if (wdw_mainWindow.searchInput.value) {
				await messenger.runtime.sendMessage({query: "cardbook.searchRemote", dirPrefId: dirPrefId, value: wdw_mainWindow.searchInput.value});
			} else {
				await messenger.runtime.sendMessage({query: "cardbook.emptyUnCachedAccountFromRepository", dirPrefId: dirPrefId});
			}
		}
		await wdw_mainWindow.windowControlShowing();
		await wdw_mainWindow.displayBook();
	},

	displayBook: async function (aMessage) {
		wdw_mainWindow.context.contactSelectedIsAList = false;
		wdw_mainWindow.context.contactSelectedCount = 0;
		if (aMessage) {
			await wdw_mainWindow.cardsTree.displayBook([]);
			document.getElementById("migratingPlaceholder").textContent = aMessage;
			wdw_mainWindow.cardsTree.updatePlaceholder("migratingPlaceholder");
			wdw_mainWindow.disableABAvailable();
			cardbookHTMLWindowUtils.clearCard();
			return
		}
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			wdw_mainWindow.searchId++;
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			if (document.getElementById("showHideTerms").classList.contains("cardbookDown")) {
				wdw_mainWindow.cardsTree.dirPrefId = "windowSearch";
			} else {
				wdw_mainWindow.cardsTree.dirPrefId = dirPrefId;
			}
			let id = document.getElementById("cardbookAccountsTree").selectedRow.id;
			let type = cardbookBGPreferences.getType(dirPrefId);
			let remote = cardbookBGUtils.isMyAccountRemote(type);
			let cached = cardbookBGPreferences.getDBCached(dirPrefId);
			let searchValue = cardbookBGUtils.makeSearchString(wdw_mainWindow.searchInput.value);
			let searchAll = wdw_mainWindow.searchAllInput.classList.contains("allAB") ? "allAB" : "oneAB";
			wdw_mainWindow.setComplexSearchMode(type, searchAll);

			await wdw_mainWindow.cardsTree.displayBook([]);

			if (remote && !cached) {
				wdw_mainWindow.searchAllInput.classList.add("hidden");
				wdw_mainWindow.searchInput.placeholder = messenger.i18n.getMessage("searchRemoteLabel");
				if (cardbookBGPreferences.getLastSearch(dirPrefId)) {
					wdw_mainWindow.searchInput.value = cardbookBGPreferences.getLastSearch(dirPrefId);
				}
				let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: dirPrefId});	
				if (!syncing) {
					wdw_mainWindow.searchInput.disabled = false;
					wdw_mainWindow.cardsTree.updatePlaceholder("searchOnlyABPlaceholder");
					if (!wdw_mainWindow.searchInput.value) {
						return
					}
				} else {
					wdw_mainWindow.searchInput.disabled = true;
					wdw_mainWindow.cardsTree.updatePlaceholder("searchingABPlaceholder");
					return;
				}
			} else {
				wdw_mainWindow.searchAllInput.classList.remove("hidden");
				wdw_mainWindow.cardsTree.updatePlaceholder("searchingABPlaceholder");
				if (document.getElementById("cardbookAccountsTree")?.selectedRow?.root) {
					let name = cardbookBGPreferences.getName(document.getElementById("cardbookAccountsTree").selectedRow.root);
					if (wdw_mainWindow.searchAllInput.classList.contains("oneAB")) {
						wdw_mainWindow.searchInput.placeholder = messenger.i18n.getMessage("searchinAB", [ name ]);
						wdw_mainWindow.searchAllInput.title = messenger.i18n.getMessage("searchinAllAB");
					} else {
						wdw_mainWindow.searchInput.placeholder = messenger.i18n.getMessage("searchinAllAB");
						wdw_mainWindow.searchAllInput.title = messenger.i18n.getMessage("searchinAB", [ name ]);
					}
				}
			}
			let selectedCards = wdw_mainWindow.getSelectedCards();

			if (document.getElementById("showHideTerms").classList.contains("cardbookDown")) {
				let search = cardbookHTMLComplexSearch.getSearch();
				let [ cards, searchId ] = await cardbookIDBCard.getCards(dirPrefId, id, search, searchAll, wdw_mainWindow.searchId);
				if (searchId != wdw_mainWindow.searchId) {
					return
				}
				await wdw_mainWindow.cardsTree.displayBook(cards);
				if (cards.length == 0) {
					wdw_mainWindow.cardsTree.updatePlaceholder("noContactsFoundABPlaceholder");
				}
			} else if (cardbookBGPreferences.getEnabled(dirPrefId)) {
				if (searchValue == "") {
					searchAll = "oneAB";
				}
				let [ cards, searchId ] = await cardbookIDBCard.getCards(dirPrefId, id, searchValue, searchAll, wdw_mainWindow.searchId);
				if (searchId != wdw_mainWindow.searchId) {
					return
				}
				await wdw_mainWindow.cardsTree.displayBook(cards);
				if (cards.length == 0) {
					wdw_mainWindow.cardsTree.updatePlaceholder("noContactsFoundABPlaceholder");
				}
			} else {
				await wdw_mainWindow.cardsTree.displayBook([]);
				wdw_mainWindow.cardsTree.updatePlaceholder("noContactsAvailableABPlaceholder");
			}

			let cbids = [];
			for (let index in wdw_mainWindow.cardsTree.cardsList?.view?._rowMap) {
				cbids.push(wdw_mainWindow.cardsTree.cardsList?.view?._rowMap[index].card.cbid);
			}
			await messenger.runtime.sendMessage({ query: "cardbook.setNextAndPreviousCard", cbids: cbids });
			
			if (wdw_mainWindow.cardsTree.cardsList?.view?._rowMap?.length == 1) {
				let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[0].card;
				wdw_mainWindow.setSelectedCards([card]);
			} else if (wdw_mainWindow.cardsTree.cardsList.selectedIndices.length == 1) {
				let index = wdw_mainWindow.cardsTree.cardsList.selectedIndices[0];
				let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card;
				wdw_mainWindow.setSelectedCards([card]);
			} else if (selectedCards.length) {
				wdw_mainWindow.setSelectedCards(selectedCards);
			} else {
				cardbookHTMLWindowUtils.clearCard();
			}
			wdw_mainWindow.enableABAvailable();
			await wdw_mainWindow.windowControlShowing();
		} else {
			wdw_mainWindow.cardsTree.updatePlaceholder("noContactsAvailableABPlaceholder");
			wdw_mainWindow.disableABAvailable();
			await wdw_mainWindow.cardsTree.displayBook([]);
			cardbookHTMLWindowUtils.clearCard();
		}
    },

	getSortResource: function (aTableName, aDirPrefId) {
		return cardbookBGPreferences.getSortResource(aDirPrefId);
    },

	setSortResource: async function (aValue, aTableName, aDirPrefId) {
		await cardbookBGPreferences.setSortResource(aDirPrefId, aValue);
    },

	getSortDirection: function (aTableName, aDirPrefId) {
		return cardbookBGPreferences.getSortDirection(aDirPrefId);
    },

	setSortDirection: async function (aValue, aTableName, aDirPrefId) {
		await cardbookBGPreferences.setSortDirection(aDirPrefId, aValue);
    },

	getTreeColumns: function (aTableName, aDirPrefId) {
		return cardbookBGPreferences.getDisplayedColumns(aDirPrefId);
    },

	setTreeColumns: async function (aValue, aTableName, aDirPrefId) {
		await cardbookBGPreferences.setDisplayedColumns(aDirPrefId, aValue);
    },

	getCardsFromAccountsOrCats: function () {
		let listOfSelectedCard = [];
		if (wdw_mainWindow.cardsTree.cardsList) {
			for (let index in wdw_mainWindow.cardsTree.cardsList.view._rowMap) {
				let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card;
				listOfSelectedCard.push(card);
			}
		}
		return listOfSelectedCard;
	},

	getCardsFromAccountsOrCatsId: function () {
		let listOfSelectedCard = [];
		if (wdw_mainWindow.cardsTree.cardsList) {
			for (let index in wdw_mainWindow.cardsTree.cardsList.view._rowMap) {
				let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card.cbid;
				listOfSelectedCard.push(card);
			}
		}
		return listOfSelectedCard;
	},

	getSelectedCards: function () {
		let selectedCards = [];
		if (wdw_mainWindow.cardsTree.cardsList) {
			for (let index of wdw_mainWindow.cardsTree.cardsList.selectedIndices) {
				selectedCards.push(wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card);
			}
		}
		return selectedCards;
	},

	getSelectedCardsCount: function () {
		let count = wdw_mainWindow.cardsTree.cardsList ? wdw_mainWindow.cardsTree.cardsList.selectedIndices.length : 0;
		return count;
	},

	getSelectedCardsId: function () {
		let selectedUids = [];
		if (wdw_mainWindow.cardsTree.cardsList) {
			for (let index of wdw_mainWindow.cardsTree.cardsList.selectedIndices) {
				selectedUids.push(wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card.cbid);
			}
		}
		return selectedUids;
	},

	getSelectedCardsDirPrefId: function () {
		let selectedDir = [];
		if (wdw_mainWindow.cardsTree.cardsList) {
			for (let index of wdw_mainWindow.cardsTree.cardsList.selectedIndices) {
				selectedDir.push(wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card.dirPrefId);
			}
		}
		return cardbookHTMLUtils.arrayUnique(selectedDir);
	},

	setSelectedCards: function (aListOfCard) {
		let cards = JSON.parse(JSON.stringify(aListOfCard));
		let indexFound = [];
		for (let index in wdw_mainWindow.cardsTree?.cardsList?.view?._rowMap) {
			let i = cards.length
			while (i--) {
				if (cards[i].cbid == wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card.cbid) {
					indexFound.push(parseInt(index));
					cards.splice(i, 1);
					break;
				}
			}
			if (cards.length == 0) {
				break;
			}
		}
		if (indexFound.length) {
			wdw_mainWindow.cardsTree.cardsList.selectedIndices = indexFound;
		}
	},

	selectTree: async function (aEvent) {
		wdw_mainWindow.selectCard(aEvent);
		wdw_mainWindow.windowControlShowing();
		wdw_mainWindow.context.contactSelectedCount = wdw_mainWindow.cardsTree.cardsList.selectedIndices.length;
		wdw_mainWindow.context.contactSelectedIsAList = false;
		if (wdw_mainWindow.context.contactSelectedCount == 1) {
			let index = wdw_mainWindow.cardsTree.cardsList.selectedIndices[0];
			let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[index].card;
			wdw_mainWindow.context.contactSelectedIsAList = card.isAList;
		}
	},

	selectCard: async function (aEvent) {
		if (wdw_mainWindow.getSelectedCardsCount() != 1) {
			cardbookHTMLWindowUtils.clearCard();
		} else {
			let selectedCard = wdw_mainWindow.getSelectedCards()[0];
			await wdw_mainWindow.displayCard(selectedCard);
		}
	},

	chooseActionCardsTree: async function () {
		let preferEmailEdition = cardbookBGPreferences.getPref("preferEmailEdition");
		if (preferEmailEdition) {
			await wdw_mainWindow.editCard();
		} else {
			await wdw_mainWindow.emailCardsFromAction("to");
		}
	},

	doubleClickCards: async function (aEvent) {
		if (aEvent.button != 0 || aEvent.ctrlKey || aEvent.metaKey || aEvent.shiftKey || aEvent.altKey) {
			return;
		}
		let rowHeader = aEvent.target.closest(`thead[is="cb-tree-view-table-header"]`);
		let rowCard = aEvent.target.closest(`tr[is="cb-table-card-row"]`);
		if (rowCard) {
			wdw_mainWindow.chooseActionCardsTree();
		} else if (rowHeader) {
			return;
		} else {
			if (document.getElementById("cardbookAccountsTree").selectedRow) {
				let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
				if (!cardbookBGPreferences.getReadOnly(dirPrefId) && cardbookBGPreferences.getEnabled(dirPrefId)) {
					await wdw_mainWindow.createContact();
				}
			}
		}
		aEvent.stopImmediatePropagation();
	},

	deleteKey: async function (aEvent) {
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		if (aEvent.target.closest("li") instanceof HTMLLIElement) {
			if (dirPrefId == wdw_mainWindow.currentAccountId) {
				await wdw_mainWindow.removeAddressbook();
			} else {
				await messenger.runtime.sendMessage({query: "cardbook.removeNode", node: wdw_mainWindow.currentAccountId});
			}
		} else if (aEvent.target instanceof HTMLTableRowElement || aEvent.target.tagName.toLowerCase() == "tbody") {
			if (cardbookBGPreferences.getEnabled(dirPrefId)) {
				if (!cardbookBGPreferences.getReadOnly(dirPrefId)) {
					await wdw_mainWindow.deleteCardsAndValidate();
				}
			}
		}
	},

	applyTo: async function () {
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		let url = "chrome/content/HTML/applyColumns/wdw_cardbookApplyColumns.html";
		let params = new URLSearchParams();
		params.set("dirPrefId", dirPrefId);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	startDrag: async function (aEvent) {
		let useOnlyEmail = cardbookBGPreferences.getPref("useOnlyEmail");
		let cards = [];
		let ids = [];
		if (aEvent.target instanceof HTMLLIElement) {
			cards = wdw_mainWindow.getCardsFromAccountsOrCats();
		} else if (aEvent.target instanceof HTMLTableRowElement) {
			cards = wdw_mainWindow.getSelectedCards();
		}
		cards = cards.map(x => x.cbid);
		aEvent.dataTransfer.setData("text/x-cardbook-id", cards.join(" , "));
	},

	dragCards: async function (aEvent) {
		let row = aEvent.target.closest("li");
		let target = "";
		if (row) {
			target = row.id;
		} else {
			return;
		}
		let nodeArray = cardbookHTMLUtils.escapeStringSemiColon(target).split("::");
		let dirPrefId = nodeArray[0];

		let ids = aEvent.dataTransfer.getData("text/x-cardbook-id");
		aEvent.preventDefault();
		aEvent.stopPropagation();

		if (ids.length) {
			let ctrlKey = aEvent.ctrlKey;
			let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsDragged", array: null, refresh: null, totalEstimated: ids.length, total: ids.length});
			await wdw_mainWindow.bulkOperation(actionId);
			await messenger.runtime.sendMessage({query: "cardbook.dragCards", ids: ids, target: target, actionId: actionId, ctrlKey: ctrlKey});
			await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
		} else if (aEvent.dataTransfer.files.length) {
			for (let file of aEvent.dataTransfer.files) {
				let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsImportedFromFile"});
				let fileReader = new FileReader();
				fileReader.readAsText(file);
				fileReader.onload = async function(event) {
					await messenger.runtime.sendMessage({query: "cardbook.importCardsFromFile", dirPrefId: dirPrefId, target: target, content: event.target.result, url: null, filename: file.name, actionId: actionId});
				};
			}
		}
	},

	setCSSRow: async function (aClassList, aCard) {
		aClassList.toggle("Mail", aCard.isEmail === true);
		aClassList.toggle("MailList", aCard.isAList === true);
		aClassList.toggle("Changed", aCard.updated || aCard.created);
		if (wdw_mainWindow.cardbookComplexSearchMode === "SEARCH") {
			let colorStyle = "color_" + aCard.dirPrefId;
			let nodeColors = cardbookBGPreferences.getNodeColors();
			if (aCard.categories.length > 0) {
				for (let category in nodeColors) {
					if (aCard.categories.includes(category)) {
						colorStyle = "color_category_" + cardbookHTMLUtils.formatCategoryForCss(category);
						break;
					}
				}
			} else {
				let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
				if (nodeColors[uncat]) {
					colorStyle = "color_category_" + cardbookHTMLUtils.formatCategoryForCss(uncat);
				} else {
					colorStyle = "color_" + aCard.dirPrefId;
				}
			}
			aClassList.add(colorStyle);
		}
	},

	initCardsTree: function () {
		let sortResource = { "get": wdw_mainWindow.getSortResource, "set": wdw_mainWindow.setSortResource };
		let sortDirection = { "get": wdw_mainWindow.getSortDirection, "set": wdw_mainWindow.setSortDirection };
		let treeColumns = { "get": wdw_mainWindow.getTreeColumns, "set": wdw_mainWindow.setTreeColumns };

		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		let treeEvents = { "select": wdw_mainWindow.selectTree,
							"dblclick": wdw_mainWindow.doubleClickCards,
							"copyKey": wdw_mainWindow.copyCards,
							"cutKey": wdw_mainWindow.cutCards,
							"pasteKey": wdw_mainWindow.pasteCards,
							"deleteKey": wdw_mainWindow.deleteKey,
							"applyTo": wdw_mainWindow.applyTo,
							"dragstart": wdw_mainWindow.startDrag};
		let treeCSS = { "row": wdw_mainWindow.setCSSRow };
		wdw_mainWindow.cardsTree = new CBTree(dirPrefId, "cardbookCardsTree", null, treeEvents, sortResource, sortDirection, treeColumns, treeCSS, true);

		wdw_mainWindow.searchInput = document.getElementById("cardbookSearchInput");
		wdw_mainWindow.searchInput.addEventListener("input", event => {
			if (wdw_mainWindow.searchTimer !== null) {
				clearTimeout(wdw_mainWindow.searchTimer);
			}
			wdw_mainWindow.searchTimer = setTimeout(wdw_mainWindow.searchInputExec, 500);
		});

		wdw_mainWindow.searchAllInput = document.getElementById("searchAllAB");
		wdw_mainWindow.searchAllInput.classList.add(cardbookBGPreferences.getPref("searchAllAB"));
		wdw_mainWindow.searchAllInput
		wdw_mainWindow.searchAllInput.addEventListener("click", async event => {
			if (wdw_mainWindow.searchAllInput.classList.contains("allAB")) {
				wdw_mainWindow.searchAllInput.classList.add("oneAB");
				wdw_mainWindow.searchAllInput.classList.remove("allAB");
				await cardbookBGPreferences.setPref("searchAllAB", "oneAB");
			} else {
				wdw_mainWindow.searchAllInput.classList.remove("oneAB");
				wdw_mainWindow.searchAllInput.classList.add("allAB");
				await cardbookBGPreferences.setPref("searchAllAB", "allAB");
			}
			await wdw_mainWindow.displayBook();
		});

		document.getElementById("clearSearch").addEventListener("click", event => wdw_mainWindow.clearSearch());
		document.getElementById("showHideTerms").addEventListener("click", event => wdw_mainWindow.showHideTerms());

		wdw_mainWindow.cardsTree.cardsList.addEventListener("rowcountchange", () => {
			if (document.activeElement == wdw_mainWindow.cardsTree.cardsList && wdw_mainWindow.cardsTree.cardsList.view.rowCount == 0) {
				wdw_mainWindow.searchInput.focus();
		}});
	},

	load: async function () {
		let win = await messenger.windows.getCurrent();
		wdw_mainWindow.winId = win.id;
		document.title = messenger.i18n.getMessage("cardbookTitle");
		i18n.updateDocument();

		// button
		document.getElementById("importCardsToFileInput").addEventListener("change", wdw_mainWindow.importCardsFromFile);
		document.getElementById("generalTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("generalTabPanel"));
		document.getElementById("noteTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("noteTabPanel"));
		document.getElementById("listTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("listTabPanel"));
		document.getElementById("vcardTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("vcardTabPanel"));
		document.getElementById("technicalTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("technicalTabPanel"));
		document.getElementById("keyTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("keyTabPanel"));
		document.getElementById("addressbooksPane").addEventListener("dblclick", wdw_mainWindow.addAddressbook);

		wdw_mainWindow.showCorrectTabs();
		wdw_mainWindow.initTrees();
		cardbookHTMLWindowUtils.clearCard();
		await wdw_mainWindow.setLayout();
		wdw_mainWindow.setToolbar();
		await wdw_mainWindow.setUndoAndRedoLabel();
		cardbookHTMLWindowUtils.showPane("generalTabPanel");
		await wdw_mainWindow.refreshWindow();
	},

	initTrees() {
		if (!wdw_mainWindow.initTreesDone) {
			wdw_mainWindow.initCardsTree();
			wdw_mainWindow.setAccountsTreeMenulist();
			wdw_mainWindow.initTreesDone = true;
		}
	},

	async clearSearch() {
		wdw_mainWindow.searchInput.value = "";
		document.getElementById("clearSearch").classList.add("hidden");
		cardbookHTMLWindowUtils.clearCard();
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			await wdw_mainWindow.windowControlShowing();
			await wdw_mainWindow.displayBook();
		}
	},

	async showHideTerms() {
		let button = document.getElementById("showHideTerms");
        let terms = document.getElementById("searchTerms");
		if (button.classList.contains("cardbookUp")) {
			terms.classList.remove("hidden");
			button.classList.remove("cardbookUp");
			button.classList.add("cardbookDown");
			button.title = messenger.i18n.getMessage("closeEditionLabel");

			document.getElementById("clearSearch").classList.add("hidden");
			wdw_mainWindow.searchAB = "allAddressBooks";
			wdw_mainWindow.matchAll = true;
			wdw_mainWindow.rules = [{case: "dig", field: "fn", term: "Contains", value: wdw_mainWindow.searchInput.value}];
			wdw_mainWindow.searchInput.value = "";
			wdw_mainWindow.searchInput.disabled= true;
			let ABList = document.getElementById("addressbookMenulist");
			await cardbookHTMLTools.loadAddressBooks(ABList, wdw_mainWindow.searchAB, true, true, true, false, false);
			cardbookHTMLComplexSearch.loadMatchAll(wdw_mainWindow.matchAll);
			cardbookHTMLTools.deleteRows("searchTermsGroupbox");
			cardbookHTMLComplexSearch.constructDynamicRows("searchTerms", wdw_mainWindow.rules, "3.0");
			document.getElementById(`searchTermsGroupbox`).addEventListener("input", wdw_mainWindow.doComplexSearch, false);
			document.getElementById(`addressbooksSearch`).addEventListener("input", wdw_mainWindow.doComplexSearch, false);
			document.getElementById(`andOrSearch`).addEventListener("input", wdw_mainWindow.doComplexSearch, false);
			document.getElementById("searchTerms_0_valueBox").focus();
			wdw_mainWindow.doComplexSearch();
		} else {
			let search = cardbookHTMLComplexSearch.getSearch();
			if (search && search.rules && search.rules.length) {
				if (search.rules[0].field == "fn") {
					wdw_mainWindow.searchInput.value = search.rules[0].value;
				}
			}
			wdw_mainWindow.searchInput.disabled= false;
			terms.classList.add("hidden");
			button.classList.add("cardbookUp");
			button.classList.remove("cardbookDown");
			button.title = messenger.i18n.getMessage("enterSearchTerms");
			wdw_mainWindow.refreshWindow();
		}
	},

	doComplexSearch() {
		if (wdw_mainWindow.complexSearchTimer !== null) {
			clearTimeout(wdw_mainWindow.complexSearchTimer);
		}
		wdw_mainWindow.complexSearchTimer = setTimeout(wdw_mainWindow.refreshWindow, 500);
	},

	addListenersForAccounts() {
		let cardbookAccountsTree = document.getElementById("cardbookAccountsTree");
		cardbookAccountsTree.addEventListener("collapsed", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("expanded", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("dragstart", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("dragover", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("drop", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("select", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("dblclick", wdw_mainWindow.handleAccountsEvents);
		cardbookAccountsTree.addEventListener("keydown", wdw_mainWindow.handleAccountsEvents);
	},

    removeListenersForAccounts() {
        try {
			let cardbookAccountsTree = document.getElementById("cardbookAccountsTree");
			cardbookAccountsTree.removeEventListener("collapsed", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("expanded", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("dragstart", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("dragover", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("drop", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("select", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("dblclick", wdw_mainWindow.handleAccountsEvents);
			cardbookAccountsTree.removeEventListener("keydown", wdw_mainWindow.handleAccountsEvents);
        } catch(e) { console.log(e) }
    },

	refreshAccountsInDirTree: async function(aDisplayId) {
		try {
			let accounts = await wdw_mainWindow.getAccounts();
			await cardbookIDBImage.openImageDB({});
			await cardbookIDBCard.openCardDB({});
			await cardbookIDBCat.openCatDB({});
			let categories = await cardbookIDBCat.getCategories();
			let uncats = await cardbookIDBCard.getUncatFromViews();
			let allNodes = await cardbookIDBCard.getNodesFromViews();
			let nodeColors = cardbookBGPreferences.getNodeColors();

			let oldVisibleData = JSON.parse(JSON.stringify(cardbookHTMLDirTree.visibleData));
			let newVisibleData = cardbookHTMLDirTree.getAccountsData(accounts, categories, uncats, allNodes, nodeColors);
			if (oldVisibleData.length == newVisibleData.length) {
				let pass = false;
				for (let i = 0; i < oldVisibleData.length; i++) {
					if (!oldVisibleData[i].every((value, index) => value === newVisibleData[i][index])) {
						pass = true;
						break;
					}
				}
				if (!pass) {
					return
				}
			}

			if (aDisplayId != wdw_mainWindow.displayId) {
				return
			}
			cardbookHTMLDirTree.visibleData = JSON.parse(JSON.stringify(newVisibleData));
			wdw_mainWindow.removeListenersForAccounts();
			cardbookHTMLTools.deleteRows("cardbookAccountsTreeUL");
			let allRows = [];
			for (let node of cardbookHTMLDirTree.visibleData) {
				let row = await cardbookHTMLDirTree.createRow(node, allRows);
				if (row) {
					allRows.push(row);
				}
			}
			let rows = allRows.map(x => [x.name, x])
			cardbookHTMLUtils.sortMultipleArrayByString(rows,0,1);
			for (let [ name, row ] of rows) {
				document.getElementById("cardbookAccountsTree").querySelector("ul").appendChild(row);
			}

			if (wdw_mainWindow.accountSelected == false) {
				let accountSaved = cardbookBGPreferences.getPref("accountShown");
				let dirPrefId = cardbookBGUtils.getAccountId(accountSaved);
				let name = cardbookBGPreferences.getName(dirPrefId);
				let nodes = [];
				for (let node in allNodes) {
					nodes.push(node.id);
				}
				let categories = await messenger.runtime.sendMessage({ query: "cardbook.getCategories", defaultPrefId: dirPrefId, noUncat: false });
				categories = categories.map(x => x[1]);
				if (!name) {
					if (document.getElementById("cardbookAccountsTree").selectedRow.id) {
						cardbookHTMLDirTree.setSelectedAccount(document.getElementById("cardbookAccountsTree").selectedRow.id);
						wdw_mainWindow.accountSelected = true;
					}
				} else if (accountSaved.includes("::org::") && !nodes.includes(accountSaved)) {
					if (document.getElementById("cardbookAccountsTree").selectedRow.id) {
						cardbookHTMLDirTree.setSelectedAccount(document.getElementById("cardbookAccountsTree").selectedRow.id);
						wdw_mainWindow.accountSelected = true;
					}
				} else if (accountSaved.includes("::categories::") && !categories.includes(accountSaved)) {
					if (document.getElementById("cardbookAccountsTree").selectedRow.id) {
						cardbookHTMLDirTree.setSelectedAccount(document.getElementById("cardbookAccountsTree").selectedRow.id);
						wdw_mainWindow.accountSelected = true;
					}
				} else {
					if (!accountSaved) {
						if (document.getElementById("cardbookAccountsTree")?.selectedRow?.id) {
							accountSaved = document.getElementById("cardbookAccountsTree")?.selectedRow?.root;
						}
					}
					if (document.getElementById("cardbookAccountsTree")?.querySelector("li[id=\"" + accountSaved + "\"]")) {
						cardbookHTMLDirTree.setSelectedAccount(accountSaved);
						wdw_mainWindow.accountSelected = true;
					}
				}
			} else {
				cardbookHTMLDirTree.setSelectedAccount(wdw_mainWindow.currentAccountId);
			}
			if (document.getElementById("cardbookAccountsTree")?.selectedRow?.id) {
				await wdw_mainWindow.setCurrentAccountId(document.getElementById("cardbookAccountsTree").selectedRow.id);
			}

			wdw_mainWindow.loadCssRules(accounts, nodeColors);
			wdw_mainWindow.addListenersForAccounts();
			wdw_mainWindow.setToolbar();
		}
		catch (e) {
			await messenger.runtime.sendMessage({query: "cardbook.updateStatusProgressInformation", string: "wdw_mainWindow.refreshAccountsInDirTree error : " + e, error: "Error"});
		}
	},

	setCurrentAccountId: async function (aAccountOrCat) {
		wdw_mainWindow.currentAccountId = aAccountOrCat;
		await cardbookBGPreferences.setPref("accountShown", aAccountOrCat);
	},

	async handleAccountsEvents(aEvent) {
		let row = aEvent.target.closest("li");
		switch (aEvent.type) {
			case "keydown":
				switch (aEvent.key) {
					case "v":
						if (aEvent.ctrlKey) {
							await wdw_mainWindow.pasteCards()
						}
						break;
					case "Delete":
						await wdw_mainWindow.deleteKey(aEvent);
						aEvent.preventDefault();
						break;
					case "Enter":
						if (!row) {
							wdw_mainWindow.addAddressbook();
						} else {
							let root = row.root;
							let id = row.id;
							if (id == root) {
								wdw_mainWindow.editAddressbook();
							} else {
								await wdw_mainWindow.selectNodeToAction("EDIT", aEvent);
							}
						}
						aEvent.preventDefault();
						break;
				}
				break;
			case "collapsed":
				cardbookBGPreferences.setExpanded(aEvent.target.id, false);
				break;
			case "expanded":
				cardbookBGPreferences.setExpanded(aEvent.target.id, true);
				break;
			case "dragstart":
				wdw_mainWindow.startDrag(aEvent);
				break;
			case "dragover":
				aEvent.dataTransfer.dropEffect = "none";
				aEvent.preventDefault();
				if (row) {
					if (row.nodetype != "SEARCH" && !cardbookBGPreferences.getReadOnly(row.root)) {
							aEvent.dataTransfer.dropEffect = aEvent.ctrlKey ? "copy" : "move";
					}
				}
				break;
			case "drop":
				wdw_mainWindow.dragCards(aEvent);
				break;
			case "select":
				let accountsTree = document.getElementById("cardbookAccountsTree");
				if (accountsTree.selectedRow) {
					if (wdw_mainWindow.accountSelected) {
						await wdw_mainWindow.setCurrentAccountId(accountsTree.selectedRow.id);
					}
					wdw_mainWindow.searchInput.value = "";
					document.getElementById("clearSearch").classList.add("hidden");
					cardbookHTMLWindowUtils.clearCard();
					if (document.getElementById("cardbookAccountsTree").selectedRow) {
						await wdw_mainWindow.windowControlShowing();
						await wdw_mainWindow.displayBook();
					}
				}
				break;
			case "dblclick":
				if (!row) {
					wdw_mainWindow.addAddressbook();
				} else {
					let root = row.root;
					let id = row.id;
					if (id == root) {
						wdw_mainWindow.editAddressbook();
					} else {
						await wdw_mainWindow.selectNodeToAction("EDIT", aEvent);
					}
				}
				break;
		}
	},

	displayCard: async function (aCard) {
		cardbookHTMLWindowUtils.clearCard();

		if (aCard.isAList) {
			document.getElementById("typesGroupbox").classList.add("hidden");
		} else {
			document.getElementById("typesGroupbox").classList.remove("hidden");
		}

		let cardDefaultRegion = cardbookHTMLUtils.getCardRegion(aCard);
		document.getElementById("cardDefaultRegion").value = cardDefaultRegion;

		await cardbookHTMLImages.displayImageCard(aCard, true);
		await cardbookHTMLWindowUtils.displayCard(aCard, true);
		await wdw_mainWindow.displayList(aCard);
		document.getElementById("vCardInputLabel").textContent = await messenger.runtime.sendMessage({ query: "cardbook.cardToVcardData", card: aCard });
	},

	addEmailToList: function (aDirPrefId, aEmail, aList) {
		let exist = aList.filter(card => card.isEmail && card.fn == aEmail);
		if (exist.length) {
			return
		}
		let card = new cardbookCardParser("", "", "", aDirPrefId);
		card.fn = aEmail;
		card.isEmail = true;
		aList.push(card);
	},

	addCardToList: function (aCard, aList) {
		let exist = aList.filter(card => card.uid == aCard.uid);
		if (exist.length) {
			return
		}
		aList.push(aCard);
	},

	displayList: async function (aCard) {
		if (!aCard.isAList) {
			return
		}
		let cards = [];
		let members = await messenger.runtime.sendMessage({query: "cardbook.getMembersFromCard", card: aCard});
		for (let email of members.mails) {
			wdw_mainWindow.addEmailToList(aCard.dirPrefId, email.toLowerCase(), cards);
		}
		for (let card of members.uids) {
			wdw_mainWindow.addCardToList(card, cards);
		}

		await cardbookHTMLWindowUtils.constructStaticList(cards);
	},

	openPrefs: async function (aTab) {
		let url = "chrome/content/HTML/configuration/wdw_cardbookConfiguration.html";
		let params = new URLSearchParams();
		if (aTab) {
			params.set("showTab", aTab);
		}
		await messenger.runtime.sendMessage({query: "cardbook.openTab", url: `${url}?${params.toString()}`});
	},

	newKey: async function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			if (!cardbookBGPreferences.getReadOnly(dirPrefId) && cardbookBGPreferences.getEnabled(dirPrefId)) {
				await wdw_mainWindow.createContact();
			}
		}
	},

	createTemplate: async function () {
		let newCard = new cardbookCardParser();
		let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
		newCard.dirPrefId = dirPrefId;
		newCard.fn = cardbookBGPreferences.getFnFormula(dirPrefId);
		await wdw_mainWindow.createCard(newCard, "EditTemplate");
	},

	createContact: async function () {
		let newCard = new cardbookCardParser();
		await wdw_mainWindow.createCard(newCard, "CreateContact");
	},

	createList: async function () {
		let newCard = new cardbookCardParser();
		newCard.isAList = true;
		await wdw_mainWindow.createCard(newCard, "CreateList");
	},

	createCard: async function (aCard, aEditionMode) {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let myId = document.getElementById("cardbookAccountsTree").selectedRow.id;
			aCard.dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let type = document.getElementById("cardbookAccountsTree").selectedRow.nodetype;
			let name = document.getElementById("cardbookAccountsTree").selectedRow.name;
			if (name != cardbookBGPreferences.getPref("uncategorizedCards")) {
				if (type == "categories") {
					cardbookHTMLUtils.addCategoryToCard(aCard, name);
				} else if (type == "org") {
					cardbookHTMLUtils.addOrgToCard(aCard, myId);
				}
			}
		} else {
			return;
		}
		cardbookBGUtils.setCalculatedFields(aCard);
		await cardbookHTMLUtils.openEditionWindow(aCard, aEditionMode);
	},

	editCard: async function () {
		let cards = wdw_mainWindow.getSelectedCards();
		if (cards.length == 1) {
			await cardbookHTMLUtils.editCardFromCard(cards[0]);
		}
	},

	deleteCardsAndValidate: async function () {
		let selectedCardsId = wdw_mainWindow.getSelectedCardsId();
		await messenger.runtime.sendMessage({query: "cardbook.deleteCardsAndValidate", listOfCardsId: selectedCardsId});
	},

	editComplexSearch: function () {
		wdw_mainWindow.addAddressbook("search");
	},

	addAddressbook: async function (aAction, aDirPrefId) {
		await messenger.runtime.sendMessage({query: "cardbook.addAddressbook", action: aAction, dirPrefId: aDirPrefId});
	},
	
	editAddressbook: async function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let type = cardbookBGPreferences.getType(dirPrefId);
			if (type === "SEARCH") {
				wdw_mainWindow.addAddressbook("search", dirPrefId);
			} else {
				let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: dirPrefId});	
				if (!syncing) {
					await messenger.runtime.sendMessage({query: "cardbook.initMultipleOperations", dirPrefId: dirPrefId});
					let url = "chrome/content/HTML/addressbooksconfiguration/wdw_addressbooksEdit.html";
					let params = new URLSearchParams();
					params.set("dirPrefId", dirPrefId);
					let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
															url: `${url}?${params.toString()}`,
															type: "popup"});
				}
			}
		}
	},

	removeAddressbook: async function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: dirPrefId});	
			if (syncing) {
				return;
			}
			await messenger.runtime.sendMessage({query: "cardbook.removeAddressbook", dirPrefId: dirPrefId, winId: wdw_mainWindow.winId});
		}
	},

	enableOrDisableAddressbook: async function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: dirPrefId});	
			if (syncing) {
				return;
			}
			let value = !cardbookBGPreferences.getEnabled(dirPrefId);
			let name = cardbookBGPreferences.getName(dirPrefId);
			let readonly = cardbookBGPreferences.getReadOnly(dirPrefId);
			let modifiedAccount = {dirPrefId: dirPrefId, name: name, readonly: readonly, enabled: value};
			await messenger.runtime.sendMessage({query: "cardbook.modifyAddressbook", account: modifiedAccount});
		}
	},

	readOnlyOrReadWriteAddressbook: async function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let syncing = await messenger.runtime.sendMessage({query: "cardbook.isMyAccountSyncing", dirPrefId: dirPrefId});	
			if (syncing) {
				return;
			}

			let value = !cardbookBGPreferences.getReadOnly(dirPrefId);
			let name = cardbookBGPreferences.getName(dirPrefId);
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			let modifiedAccount = {dirPrefId: dirPrefId, name: name, readonly: value, enabled: enabled};
			await messenger.runtime.sendMessage({query: "cardbook.modifyAddressbook", account: modifiedAccount});
		}
	},

	createEvent: async function () {
		let listOfSelectedCard = wdw_mainWindow.getSelectedCards();
		let contacts = [];
		for (let card of listOfSelectedCard) {
			contacts.push([card.emails, card.fn]);
		}
		await messenger.runtime.sendMessage({query: "cardbook.createEvent", contacts: contacts});
	},

	createTodo: async function () {
		let listOfSelectedCard = wdw_mainWindow.getSelectedCards();
		let description = "";
		let title = "";
		for (let card of listOfSelectedCard) {
			// todo unify preferEmailPref and preferIMPPPref
			let phone = cardbookHTMLUtils.getPrefAddressFromCard(card, "tel", cardbookBGPreferences.getPref("preferEmailPref"));
			if (phone.length) {
				description = description + card.fn + " (" + phone[0] + ")\r\n";
			} else {
				description = description + card.fn + "\r\n";
			}
		}
		if (listOfSelectedCard.length == 1) {
			title = description;
			description = "";
		}
		await messenger.runtime.sendMessage({query: "cardbook.createToDo", title: title, description: description});
	},

	emailCardsFromMenuEntries: async function (aEvent) {
		let li = aEvent.target;
		li.classList.remove("active");
		await wdw_mainWindow.emailCardsFromAction(li.id);
	},

	emailCardsFromMenu: async function () {
		await wdw_mainWindow.emailCardsFromAction("to");
	},

	emailCardsFromAction: async function (aAction) {
		let action = aAction || "to";
		let cards = wdw_mainWindow.getSelectedCards();
		if (cards.length == 0 && cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId) != wdw_mainWindow.currentAccountId) {
			cards = wdw_mainWindow.getCardsFromAccountsOrCats();
		}
		await wdw_mainWindow.emailCards(cards, null, action);
	},

	emailCards: async function (aListOfSelectedCard, aListOfSelectedMails, aMsgField) {
		let useOnlyEmail = cardbookBGPreferences.getPref("useOnlyEmail");
		let result = {};
		let compFields = [];
		if (aListOfSelectedCard && aListOfSelectedCard.length != 0) {
			result = await messenger.runtime.sendMessage({query: "cardbook.getMimeEmailsFromCardsAndLists",
								listOfCards: aListOfSelectedCard, useOnlyEmail});
		} else if (aListOfSelectedMails && aListOfSelectedMails.length != 0) {
			result.emptyResults = [];
			result.notEmptyResults = [];
			if (useOnlyEmail) {
				result.notEmptyResults.push(aListOfSelectedMails[1]);
			} else {
				result.notEmptyResults.push(MimeAddressParser.prototype.makeMimeAddress(aListOfSelectedMails[0], aListOfSelectedMails[1]));
			}
		// possibility to send email to nobody for the write button
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: compFields});
			return;
		}
		wdw_mainWindow.compFields = [{field: aMsgField, value: result.notEmptyResults}];
		if (result.emptyResults.length != 0 && cardbookBGPreferences.getPref("warnEmptyEmails")) {
			await wdw_mainWindow.warnEmptyEmailContacts(result.emptyResults, result.notEmptyResults);
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: wdw_mainWindow.compFields});
		}
	},

	warnEmptyEmailContacts: async function(aListOfEmptyFn, aListOfNotEmptyEmails) {
		let warningTitle = messenger.i18n.getMessage("warningTitle");
		if (aListOfEmptyFn.length > 1) {
			var warningMsg = messenger.i18n.getMessage("emptyEmailsCardsConfirmMessage", [aListOfEmptyFn.join(", ")]);
		} else {
			var warningMsg = messenger.i18n.getMessage("emptyEmailsCardConfirmMessage", [aListOfEmptyFn.join(", ")]);
		}
		let rememberMsg = messenger.i18n.getMessage("doNotShowAnymore");
		let url = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "confirmEx");
		params.set("action", "emailCards");
		params.set("winId", wdw_mainWindow.winId);
		params.set("title", warningTitle);
		params.set("message", warningMsg);
		params.set("buttons", "cancel");
		params.set("checkboxAction", "warnEmptyEmails");
		params.set("checkboxMsg", rememberMsg);
		params.set("checkboxValue", "false");
		if (aListOfNotEmptyEmails.length != 0) {
			params.set("validateMsg", messenger.i18n.getMessage("sendButtonLabel"));
		}
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	shareCardsByEmail: async function () {
		let cards = wdw_mainWindow.getSelectedCards();
		if (cards.length == 0) {
			cards = wdw_mainWindow.getCardsFromAccountsOrCats();
		}
		let vCards = [];
		for (let card of cards) {
			let listOfNames = Array.from(vCards, x => x.filename);
			let filename = cardbookBGUtils.getFileNameForCard2(card, listOfNames, ".vcf");
			let vCard = await messenger.runtime.sendMessage({query: "cardbook.getvCard", dirPrefId: card.dirPrefId, contactId: card.uid});
			vCards.push({filename: filename, vCard: vCard});
		}
		await messenger.runtime.sendMessage({query: "cardbook.sharevCards", vCards: vCards});
	},

	printCards: async function () {
		let listOfSelectedCard = wdw_mainWindow.getSelectedCardsId();
		if (listOfSelectedCard.length == 0) {
			listOfSelectedCard = wdw_mainWindow.getCardsFromAccountsOrCatsId();
		}
		await messenger.runtime.sendMessage({query: "cardbook.printCards", listOfCards: listOfSelectedCard});
	},

	setLayout: async function () {
		let isModernLayout = (cardbookBGPreferences.getPref("panesView") == "modern");
		document.body.classList.toggle("layout-table", !isModernLayout);

		let addressbooksSplitter = document.getElementById("addressbooksSplitter");
		addressbooksSplitter.width = cardbookBGPreferences.getPref(`${addressbooksSplitter.id}-width`);
		addressbooksSplitter.isCollapsed = !cardbookBGPreferences.getPref("viewABPane");
		addressbooksSplitter.addEventListener("splitter-resized", async () => {
			await cardbookBGPreferences.setPref(`${addressbooksSplitter.id}-width`, addressbooksSplitter.width);
			if (document.getElementById("addressbooksPane").classList.contains("collapsed-by-splitter")) {
				await cardbookBGPreferences.setPref("viewABPane", false);
			} else {
				await cardbookBGPreferences.setPref("viewABPane", true);
			}
			wdw_mainWindow.setLayoutButton();
		});

		wdw_mainWindow.setCardsSplitterProps();
		let cardsSplitter = document.getElementById("cardsSplitter");
		cardsSplitter.width = cardbookBGPreferences.getPref(`${cardsSplitter.id}-width`);
		cardsSplitter.height = cardbookBGPreferences.getPref(`${cardsSplitter.id}-height`);
		cardsSplitter.isCollapsed = !cardbookBGPreferences.getPref("viewABContact");
		cardsSplitter.addEventListener("splitter-resized", async () => {
			if (document.body.classList.contains("layout-table")) {
				await cardbookBGPreferences.setPref(`${cardsSplitter.id}-height`, cardsSplitter.height);
			} else {
				await cardbookBGPreferences.setPref(`${cardsSplitter.id}-width`, cardsSplitter.width);
			}
			if (document.getElementById("detailsPane").classList.contains("collapsed-by-splitter")) {
				await cardbookBGPreferences.setPref("viewABContact", false);
			} else {
				await cardbookBGPreferences.setPref("viewABContact", true);
			}
			wdw_mainWindow.setLayoutButton();
		});

		document.getElementById("totalContacts").addEventListener("click", event => wdw_mainWindow.openLogEdition());
	},

	setLayoutButton: function() {
		let toolbar = document.getElementById("toolbox");
		for (let element of toolbar.children) {
			if (element.id.startsWith("layout")) {
				let options = [];
				let checked = true;
				checked = (cardbookBGPreferences.getPref("panesView") == "classical") ? true: false;
				options.push([messenger.i18n.getMessage("ClassicViewLabel"), "classic", checked]);
				checked = (cardbookBGPreferences.getPref("panesView") == "classical") ? false : true;
				options.push([messenger.i18n.getMessage("VerticalViewLabel"), "vertical", checked]);
				checked = cardbookBGPreferences.getPref("viewABPane");
				options.push([messenger.i18n.getMessage("ABPaneLabel"), "viewABPane", checked]);
				checked = cardbookBGPreferences.getPref("viewABContact");
				options.push([messenger.i18n.getMessage("contactPaneLabel"), "viewABContact", checked]);
				element.loadOptions("layoutButton", options, wdw_mainWindow.changeLayout, [], null);
				break;
			}
		}
	},

	setChatButton: function() {
		let toolbar = document.getElementById("toolbox");
		for (let element of toolbar.children) {
			if (element.id.startsWith("chat")) {
				let options = [];
				let button = element.querySelector("button");
				if (wdw_mainWindow.context.IMPPs && wdw_mainWindow.context.IMPPs.length) {
					let index = 0;
					for (let IMPP of wdw_mainWindow.context.IMPPs) {
						options.push([IMPP.label, index, false]);
						index++;
					}
					button.disabled = false;
				} else {
					button.disabled = true;
				}
				element.loadOptions("chatButton", options, wdw_mainWindow.connectCardsFromChatButton, [], wdw_mainWindow.connectCardsFromChatButton);
				break;
			}
		}
	},

	setSyncButton: function(aElement) {
		let options = [];
		let button = aElement.querySelector("button");
		if (button) {
			let visibleAccounts = cardbookHTMLDirTree.visibleData.filter(x => x[1] == x[5]);
			for (let account of visibleAccounts) {
				let name = account[0];
				let dirPrefId = account[5];
				let type = cardbookBGPreferences.getType(dirPrefId);
				let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
				if (enabled && cardbookBGUtils.isMyAccountRemote(type)) {
					options.push([name, dirPrefId, false]);
				}
			}
			if (options.length) {
				cardbookHTMLUtils.sortMultipleArrayByString(options, 0, 1);
				button.disabled = false;
			} else {
				button.disabled = true;
			}
			aElement.loadOptions("syncButton", options, wdw_mainWindow.syncAccountsFromMenuEntries, [], wdw_mainWindow.syncAccounts);
		}
	},

	setUndoAndRedoButton: function(aButton, aCode) {
		if (aButton && wdw_mainWindow.context[aCode]) {
			aButton.disabled = !wdw_mainWindow.context[aCode].enabled;
			aButton.title = wdw_mainWindow.context[aCode].message;
		}
	},

	changeLayout: async function(aEvent) {
		let id = aEvent.target.id;
		switch (id) {
			case "classic":
				await cardbookBGPreferences.setPref("panesView", "classical");
				document.getElementById("wdw_mainWindowBody").classList.add("layout-table");
				break;
			case "vertical":
				await cardbookBGPreferences.setPref("panesView", "modern");
				document.getElementById("wdw_mainWindowBody").classList.remove("layout-table");
				break;
			case "viewABPane":
			case "viewABContact":
				let splitter = (id == "viewABPane") ? "addressbooksSplitter" : "cardsSplitter";
				let checked = aEvent.target.classList.contains("active");
				if (checked == false) {
					document.getElementById(splitter).isCollapsed = true;
				} else {
					document.getElementById(splitter).isCollapsed = false;
				}
				await cardbookBGPreferences.setPref(id, checked);
				break;
		}
		wdw_mainWindow.setCardsSplitterProps();
		wdw_mainWindow.setLayoutButton();
	},

	connectCardsFromChatButton: async function (aEvent) {
		if (aEvent) {
			let index = 0;
			let id = aEvent.target.id;
			if (id != "chatButton-Button") {
				index = parseInt(id);
			}
			if (wdw_mainWindow.context.IMPPs[index]) {
				let type = wdw_mainWindow.context.IMPPs[index].type;
				let value = wdw_mainWindow.context.IMPPs[index].value;
				if (type == "tel") {
					await messenger.runtime.sendMessage({query: "cardbook.connectTel", tel: value});
				} else {
					await messenger.runtime.sendMessage({query: "cardbook.openExternalURL", link: cardbookHTMLUtils.formatIMPPForOpenning(value)})
				}
			}
		}
	},

	setCardsSplitterProps: function() {
		let isModernLayout = (cardbookBGPreferences.getPref("panesView") == "modern");
		let cardsSplitter = document.getElementById("cardsSplitter");
		cardsSplitter.resizeDirection = isModernLayout ? "horizontal" : "vertical";
	},

	bulkOperation: async function (aActionId) {
		let show = await messenger.runtime.sendMessage({query: "cardbook.displayBulkWindow", actionId: aActionId});
		if (show) {
			let url = "chrome/content/HTML/bulkOperation/wdw_bulkOperation.html";
			let win = messenger.runtime.sendMessage({query: "cardbook.openWindow",
															url: url,
															type: "popup"});
		}
	},

	prepareSubMenus: async function() {
		await wdw_mainWindow.prepareCategorySubMenus();
		await wdw_mainWindow.prepareIMPPSubMenus();
		wdw_mainWindow.context.findEmailsEnabled = await messenger.runtime.sendMessage({ query: "cardbook.getSystemPref", type: "boolean", value: "mailnews.database.global.indexer.enabled" });
	},

	prepareUndoAndRedoSubMenus: function(aCode, aValue) {
		wdw_mainWindow.context[aCode] = {};
		wdw_mainWindow.context[aCode].label = aValue;
		let sortMg = messenger.i18n.getMessage(`${aCode}ShortLabel`);
		wdw_mainWindow.context[aCode].enabled = (aValue == sortMg) ? false : true;
		wdw_mainWindow.context[aCode].message = (aValue == sortMg) ? sortMg : aValue;
		let button = document.getElementById("toolbox").querySelector(`button.${aCode}`);
		wdw_mainWindow.setUndoAndRedoButton(button, aCode);
	},

	prepareCategorySubMenus: async function() {
		wdw_mainWindow.context.categories = [];
		let listOfDirPrefId = wdw_mainWindow.getSelectedCardsDirPrefId();
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length > 0) {
			let allCategories = [];
			for (let dirPrefId of listOfDirPrefId) {
				let categories = await messenger.runtime.sendMessage({ query: "cardbook.getCategories", defaultPrefId: dirPrefId, noUncat: true });
				allCategories = allCategories.concat(categories);
			}
			allCategories = cardbookHTMLUtils.cleanCategories(allCategories);
			cardbookHTMLUtils.sortMultipleArrayByString(allCategories, 0, 1);
			for (let category of allCategories) {
				let categoryCount = 0;
				for (let card of selectedCards) {
					if (card.categories.includes(category[0])) {
						categoryCount++;
					}
				}
				if (categoryCount == 0) {
					wdw_mainWindow.context.categories.push({ name: category[0], checked: false, enabled: true });
				} else if (categoryCount == selectedCards.length) {
					wdw_mainWindow.context.categories.push({ name: category[0], checked: true, enabled: true });
				} else {
					wdw_mainWindow.context.categories.push({ name: category[0], checked: false, enabled: false });
				}						
			}
		}
	},

	prepareIMPPSubMenus: async function() {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		wdw_mainWindow.context.IMPPs = [];
		if (selectedCards.length == 1) {
			let card = selectedCards[0];
			let telProtocolLine = cardbookBGPreferences.getPref("tels.0");
			let preferIMPPPref = cardbookBGPreferences.getPref("preferIMPPPref");
			let tels = cardbookHTMLUtils.getPrefAddressFromCard(card, "tel", preferIMPPPref);
			if (telProtocolLine) {
				let telProtocolLineArray = telProtocolLine.split(":");
				let telLabel = telProtocolLineArray[1];
				let telProtocol = telProtocolLineArray[2];
				for (let tel of tels) {
					let regExp = new RegExp(`^${telProtocol}:`);
					let address = tel.replace(regExp, "");
					wdw_mainWindow.context.IMPPs.push({ type: "tel", label: `${telLabel}: ${address}`, value: address });
				}
			} else {
				for (let tel of tels) {
					wdw_mainWindow.context.IMPPs.push({ type: "tel", label: tel, value: tel });
				}
			}
			let IMPPs = cardbookHTMLUtils.getPrefAddressFromCard(card, "impp", preferIMPPPref);
			for (let IMPP of IMPPs) {
				let serviceProtocol = cardbookHTMLTypes.getIMPPProtocol([IMPP]);
				let serviceLine = [];
				serviceLine = cardbookHTMLTypes.getIMPPLineForProtocol(serviceProtocol)
				if (serviceLine[0]) {
					let regExp = new RegExp(`^${serviceLine[2]}:`);
					let address = IMPP.replace(regExp, "");
					wdw_mainWindow.context.IMPPs.push({ type: "IMPP", label: `${serviceLine[1]}: ${address}`, value: `${serviceLine[2]}:${address}` });
				}
			}
		}
	},

	prepareSyncContext: function (aDirPrefId, aStatus) {
		if (!wdw_mainWindow.context.syncing) {
			wdw_mainWindow.context.syncing = {};
		}
		wdw_mainWindow.context.syncing[aDirPrefId] = aStatus;
	},

	addCategoryToSelectedCards: async function (aCategory, aCategorySelect) {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let topic = (aCategorySelect) ? "categoryCreated" : "categorySelected";
		let length = 0;
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			length++
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, array: [aCategory], refresh: null, totalEstimated: length, total: length});
		await wdw_mainWindow.bulkOperation(actionId);
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			let outCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(card, outCard);
			cardbookHTMLUtils.addCategoryToCard(outCard, aCategory);
			await messenger.runtime.sendMessage({query: "cardbook.saveCardFromUpdate", card: card, outCard: outCard, actionId: actionId, checkCategory: false});
		}
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	removeCategoryFromSelectedCards: async function (aCategory) {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let topic = "categoryUnselected";
		let length = 0;
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			length++
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, array: [aCategory], refresh: null, totalEstimated: length, total: length});
		await wdw_mainWindow.bulkOperation(actionId);
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			let outCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(card, outCard);
			cardbookHTMLUtils.removeCategoryFromCard(outCard, aCategory);
			await messenger.runtime.sendMessage({query: "cardbook.saveCardFromUpdate", card: card, outCard: outCard, actionId: actionId, checkCategory: true});
		}
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	findEmailsFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length == 1) {
			let card = selectedCards[0];
			await messenger.runtime.sendMessage({query: "cardbook.findEmails", card: [ card ], email: []});
		}
	},

	findEventsFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length == 1) {
			let card = selectedCards[0];
			await cardbookHTMLUtils.findEvents(card, [], card.fn);
		}
	},

	localizeCardsFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		await cardbookHTMLUtils.localizeCards(selectedCards, []);
	},

	openURLFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let listOfURLs = cardbookHTMLUtils.getURLsFromCards(selectedCards);
		for (let url of listOfURLs) {
			await cardbookHTMLUtils.openURL(url);
		}
	},

	searchForOnlineKeyFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length == 1) {
			let card = selectedCards[0];
			let key = await messenger.runtime.sendMessage({query: "cardbook.searchForOnlineKey", card: [ card ], email: []});
			if (key) {
				// test to do wdw_cardEdition.addKeyToEdit(key);
			}
		}
	},

	cutCards: async function () {
		let selectedCardsId = wdw_mainWindow.getSelectedCardsId();
		if (selectedCardsId.length == 0) {
			selectedCardsId = wdw_mainWindow.getCardsFromAccountsOrCatsId();
		}
		await wdw_mainWindow.cutOrCopyCards(selectedCardsId, "CUT");
	},

	copyCards: async function () {
		let selectedCardsId = wdw_mainWindow.getSelectedCardsId();
		if (selectedCardsId.length == 0) {
			selectedCardsId = wdw_mainWindow.getCardsFromAccountsOrCatsId();
		}
		await wdw_mainWindow.cutOrCopyCards(selectedCardsId, "COPY");
	},

	cutOrCopyCards: async function (aListOfSelectedCardsId, aMode) {
		if (aListOfSelectedCardsId.length) {
			let message = (aListOfSelectedCardsId.length > 1) ? messenger.i18n.getMessage("contactsCopied") : messenger.i18n.getMessage("contactCopied"); // test to pluralize
			wdw_mainWindow.copy = { mode: aMode, ids: aListOfSelectedCardsId };
			await messenger.runtime.sendMessage({query: "cardbook.updateStatusProgressInformation", string: message});
		}
	},

	pasteCards: async function () {
		if (wdw_mainWindow.copy.ids) {
			let target = document.getElementById("cardbookAccountsTree").selectedRow.id;
			let nodeArray = cardbookHTMLUtils.escapeStringSemiColon(target).split("::");
			let dirPrefId = nodeArray[0];
			let type = cardbookBGPreferences.getType(dirPrefId);
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			let readonly = cardbookBGPreferences.getReadOnly(dirPrefId);
			if (type !== "SEARCH") {
				if (enabled) {
					if (!readonly) {
						let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsPasted", array: [], refresh: target, totalEstimated: wdw_mainWindow.copy.ids.length, total: wdw_mainWindow.copy.ids.length});
						await wdw_mainWindow.bulkOperation(actionId);
						await messenger.runtime.sendMessage({query: "cardbook.pasteCards", target: target, actionId: actionId, ids: wdw_mainWindow.copy.ids, mode: wdw_mainWindow.copy.mode});
					} else {
						let name = cardbookBGPreferences.getName(dirPrefId);
						await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "addressbookReadOnly", values: [name]});
					}
				} else {
					let name = cardbookBGPreferences.getName(dirPrefId);
					await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "addressbookDisabled", values: [name]});
				}
			}
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "clipboardEmpty", values: []});
		}
		wdw_mainWindow.copy = { };
	},

	copyFieldValue: async function (aFieldName, aFieldIndex, aFieldLabel) {
		let cbid = document.getElementById("dirPrefIdInputText").value + "::" + document.getElementById("uidInputText").value;
		let [ clipValue, dummy ] = await cardbookHTMLUtils.getFieldValue(cbid, aFieldName, aFieldIndex);
		wdw_mainWindow.copyField = { field: aFieldName, index: aFieldIndex, label: aFieldLabel, cbid: cbid}
		await navigator.clipboard.writeText(clipValue);
		await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "lineCopied"});
	},

	pasteFieldValue: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let length = 0;
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			length++
		}
		if (!wdw_mainWindow.copyField.field || !length) {
			await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "clipboardEmpty"});
			return;
		}
		let [ dummy, value ] = await cardbookHTMLUtils.getFieldValue(wdw_mainWindow.copyField.cbid, wdw_mainWindow.copyField.field, wdw_mainWindow.copyField.index);
		if (!value) {
			await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "clipboardEmpty"});
			return;
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "linePasted", array: null, refresh: null, totalEstimated: length, total: length});
		await wdw_mainWindow.bulkOperation(actionId);

		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			let outCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(card, outCard);
			let dateFormat = cardbookHTMLUtils.getDateFormat(outCard.dirPrefId, outCard.version);

			if (cardbookHTMLUtils.multilineFields.includes(wdw_mainWindow.copyField.field) || wdw_mainWindow.copyField.field == "tz") {
				outCard[wdw_mainWindow.copyField.field].push(value);
			} else if (cardbookHTMLUtils.adrElements.includes(wdw_mainWindow.copyField.field)) {
				let partialIndex = cardbookHTMLUtils.adrElements.indexOf(wdw_mainWindow.copyField.field);
				if (outCard.adr.length) {
					for (let i = 0; i < outCard.adr.length; i++) {
						outCard.adr[i][0][partialIndex] = value;
					}
				} else {
					outCard.adr.push([["", "", "", "", "", "", ""], [], "", []]);
					outCard.adr[0][0][partialIndex] = value;
				}
			} else if (wdw_mainWindow.copyField.field == "event") {
				let PGNextNumber = cardbookHTMLUtils.rebuildAllPGs(outCard);
				cardbookHTMLUtils.addEventstoCard(outCard, [ value ], PGNextNumber, dateFormat, dateFormat);
			} else if (wdw_mainWindow.copyField.field.startsWith("X-")) {
				let migratedItems = Array.from(cardbookHTMLUtils.newFields, item => "X-" + item.toUpperCase());
				if (migratedItems.includes(wdw_mainWindow.copyField.field)) {
					if (outCard.version == "3.0") {
						outCard.others.push(`${wdw_mainWindow.copyField.field}:${value}`);
					} else {
						let field = wdw_mainWindow.copyField.field.replace(/^X\-/, "").toLowerCase();
						outCard[field] = value;
					}
				} else {
					outCard.others.push(`${wdw_mainWindow.copyField.field}:${value}`);
				}
			} else if (wdw_mainWindow.copyField.field.startsWith("org_")) {
				cardbookHTMLUtils.addOrgToCard(outCard, `org::org${value}`);
			} else if (cardbookHTMLUtils.newFields.includes(wdw_mainWindow.copyField.field)) {
				if (outCard.version == "4.0") {
					outCard[wdw_mainWindow.copyField.field] = value;
				} else {
					continue;
				}
			} else if (cardbookHTMLDates.dateFields.includes(wdw_mainWindow.copyField.field)) {
				let newDate = cardbookHTMLDates.convertDateStringToDateUTC(value);
				outCard[wdw_mainWindow.copyField.field] = cardbookHTMLDates.convertUTCDateToDateString(newDate, dateFormat);
			} else {
				outCard[wdw_mainWindow.copyField.field] = value;
			}
			
			cardbookHTMLTypes.rebuildAllPGs(outCard);
			await messenger.runtime.sendMessage({query: "cardbook.saveCardFromUpdate", card: card, outCard: outCard, actionId: actionId, checkCategory: true});
			let message = messenger.i18n.getMessage("linePastedToCard", [outCard.fn])
			await messenger.runtime.sendMessage({query: "cardbook.updateStatusProgressInformation", string: message});
		}
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	duplicateCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsDuplicated", array: null, refresh: null, totalEstimated: selectedCards.length});
		await wdw_mainWindow.bulkOperation(actionId);
		for (let card of selectedCards) {
			if (cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
				continue;
			}
			let outCard = new cardbookCardParser();
			await cardbookBGUtils.cloneCard(card, outCard);
			outCard.fn = outCard.fn + " " + messenger.i18n.getMessage("fnDuplicatedMessage");
			outCard.cardurl = "";
			outCard.etag = "0";
			cardbookBGUtils.setCardUUID(outCard);
			await messenger.runtime.sendMessage({query: "cardbook.saveCardFromUpdate", card: {}, outCard: outCard, actionId: actionId, checkCategory: true});
		}
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	findMergeableContacts: function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		let contacts = 0;
		let lists = 0;
		let ABid = [];
		for (let contact of selectedCards) {
			if (contact.isAList) {
				lists++;
				ABid.push(contact.dirPrefId);
			} else {
				contacts++;
			}
		}
		if (contacts >= 2) {
			return selectedCards.filter(card => card.isAList == false);
		} else if (contacts == 1) {
			return [];
		} else if (lists >= 2 && cardbookHTMLUtils.arrayUnique(ABid).length == 1) {
			return selectedCards;
		} else {
			return [];
		}
	},

	mergeCards: async function () {
		let listOfSelectedCard = wdw_mainWindow.findMergeableContacts();
		if (listOfSelectedCard.length) {
			let ids = listOfSelectedCard.map(card => card.cbid);
			let mode = listOfSelectedCard.filter(card => card.isAList).length > 0 ? "LIST" : "CONTACT";
			let url = "chrome/content/HTML/mergeCards/wdw_mergeCards.html";
			let params = new URLSearchParams();
			params.set("hideCreate", false);
			params.set("source", "MERGE");
			params.set("mode", mode);
			params.set("ids", ids.join(","));
			params.set("mergeId", cardbookHTMLUtils.getUUID());
			let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
													url: `${url}?${params.toString()}`,
													type: "popup"});
		}
	},

	convertListToCategory: async function () {
		async function addMember(aDirPrefId, aUid, aCategoryName, aActionId) {
			let memberCard = await messenger.runtime.sendMessage({query: "cardbook.getCardWithId", cbid: `${aDirPrefId}::${aUid}`});
			if (memberCard) {
				let name = cardbookBGPreferences.getName(aDirPrefId);
				let outCard = new cardbookCardParser();
				await cardbookBGUtils.cloneCard(memberCard, outCard);
				cardbookHTMLUtils.addCategoryToCard(outCard, aCategoryName);
				await messenger.runtime.sendMessage({query: "cardbook.saveCardFromUpdate", card: memberCard, outCard: outCard, actionId: aActionId, checkCategory: true});
				await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "cardAddedToCategory", values: [name, outCard.fn, aCategoryName]});
			}
		}
		let cbid = document.getElementById("dirPrefIdInputText").value + "::" + document.getElementById("uidInputText").value;
		let card = await messenger.runtime.sendMessage({query: "cardbook.getCardWithId", cbid: cbid});
		if (!card.isAList || cardbookBGPreferences.getReadOnly(card.dirPrefId)) {
			return;
		} else {
			let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "listConvertedToCategory", array: [card.fn], refresh: null});
			await wdw_mainWindow.bulkOperation(actionId);
			let categoryName = card.fn;                                                                                                                                                                                                                                              
			if (card.version == "4.0") {
				for (let member of card.member) {
					let uid = member.replace("urn:uuid:", "");
					await addMember(card.dirPrefId, uid, categoryName, actionId);
				}
			} else if (card.version == "3.0") {
				let memberCustom = cardbookBGPreferences.getPref("memberCustom");
				for (let other of card.others) {
					let localDelim1 = other.indexOf(":",0);
					if (localDelim1 >= 0) {
						let header = other.substr(0, localDelim1);
						let trailer = other.substr(localDelim1+1, other.length);
						if (header == memberCustom) {
							let uid = trailer.replace("urn:uuid:", "");
							await addMember(card.dirPrefId, uid, categoryName, actionId);
						}
					}
				}
			}
			await messenger.runtime.sendMessage({query: "cardbook.deleteCards", actionId: actionId, cards: [card]});
		}
	},

	exportCardsToFileFromAccountsOrCats: async function (aType) {
		let selectedCards = wdw_mainWindow.cardsTree.cardsList.view._rowMap;
		if (selectedCards.length) {
			let name = document.getElementById("cardbookAccountsTree").selectedRow.name;
			let defaultFileName = `${name}.${aType}`;
			await wdw_mainWindow.exportCardsToFileNext(aType, defaultFileName, "accounts");
		}
	},

	exportCardsToFileFromCards: async function (aType) {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length) {
			let defaultFileName = (selectedCards.length == 1) ? `${selectedCards[0].fn}.${aType}` : `export.${aType}`;
			await wdw_mainWindow.exportCardsToFileNext(aType, defaultFileName, "cards");
		}
	},

	exportCardsToFileNext: async function (aType, aFileName, aSource) {
		let filename = cardbookBGUtils.getFormattedFileName(aFileName);
		if (aType == "csv") {
			let url = "chrome/content/HTML/csvTranslator/wdw_csvTranslator.html";
			let params = new URLSearchParams();
			params.set("mode", "export");
			params.set("includePref", false);
			params.set("lineHeader", true);
			params.set("columnSeparator", cardbookBGPreferences.getPref("exportDelimiter"));
			params.set("filename", filename);
			params.set("source", aSource);
			let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
													url: `${url}?${params.toString()}`,
													type: "popup"});
		} else {
			let selectedCards = (aSource == "accounts") ? wdw_mainWindow.getCardsFromAccountsOrCats() : wdw_mainWindow.getSelectedCards();
			let topic = "cardsExportedToVcf";
			let length = selectedCards.length;
			let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, totalEstimated: length, total: length});
			await wdw_mainWindow.bulkOperation(actionId);
			let content = await cardbookBGUtils.getDataForUpdatingFile({}, selectedCards, actionId);
			let file = new Blob([content], { type: "text/plain" });
			let url = URL.createObjectURL(file);
			await cardbookHTMLDownloads.download(url, filename, actionId, topic, length);
		}
	},

	exportCardsToZipFromAccountsOrCats: async function () {
		let selectedCards = wdw_mainWindow.cardsTree.cardsList.view._rowMap;
		if (selectedCards.length) {
			let name = document.getElementById("cardbookAccountsTree").selectedRow.name;
			let defaultFileName = `${name}.cards.zip`;
			await wdw_mainWindow.exportCardsToZipNext(defaultFileName, "accounts");
		}
	},

	exportCardsToZipFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length) {
			let defaultFileName = (selectedCards.length == 1) ? `${selectedCards[0].fn}.cards.zip` : "export.cards.zip";
			await wdw_mainWindow.exportCardsToZipNext(defaultFileName, "cards");
		}
	},

	exportCardsToZipNext: async function (aFileName, aSource) {
		let selectedCards = (aSource == "accounts") ? wdw_mainWindow.getCardsFromAccountsOrCats() : wdw_mainWindow.getSelectedCards();
		let creationDate = new Date();
		let blobWriter = new zip.BlobWriter("application/zip");
		let zipWriter = new zip.ZipWriter(blobWriter, { bufferedWrite: true, useCompressionStream: false });
		let listOfFiles = [];
		let topic = "cardsExportedToZip";
		let length = selectedCards.length;
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, totalEstimated: length, total: length});
		await wdw_mainWindow.bulkOperation(actionId);
		for (let card of selectedCards) {
			let filename = cardbookBGUtils.getFormattedFileName(`${card.fn}.vcf`);
			if (listOfFiles.includes(filename)) {
				filename = cardbookBGUtils.getFormattedFileName(`${card.fn}.${card.uid}.vcf`);
			}
			let vCard = await cardbookBGUtils.cardToVcardData({}, card);
			let data = new Blob([vCard], {type: "text;charset=utf-8"});
			await zipWriter.add(filename, new zip.BlobReader(data), { level: 0, lastModDate: creationDate, useWebWorkers: false });
			listOfFiles.push(filename);
			if (listOfFiles.length % 100 == 0) {
				await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: listOfFiles.length});
			}
		}
		await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: listOfFiles.length});
        let zipFile = await zipWriter.close();
		let url = URL.createObjectURL(zipFile);
		await cardbookHTMLDownloads.download(url, aFileName, actionId, topic, length);
	},

	exportCardsImagesFromAccountsOrCats: async function () {
		let selectedCards = wdw_mainWindow.cardsTree.cardsList.view._rowMap;
		if (selectedCards.length) {
			let name = document.getElementById("cardbookAccountsTree").selectedRow.name;
			let defaultFileName = `${name}.photo.zip`;
			await wdw_mainWindow.exportCardsImagesNext(defaultFileName, "accounts");
		}
	},

	exportCardsImagesFromCards: async function () {
		let selectedCards = wdw_mainWindow.getSelectedCards();
		if (selectedCards.length) {
			let defaultFileName = (selectedCards.length == 1) ? `${selectedCards[0].fn}.photo.zip` : "export.photo.zip";
			await wdw_mainWindow.exportCardsImagesNext(defaultFileName, "cards");
		}
	},

	exportCardsImagesNext: async function (aFileName, aSource) {
		let selectedCards = (aSource == "accounts") ? wdw_mainWindow.getCardsFromAccountsOrCats() : wdw_mainWindow.getSelectedCards();
		let creationDate = new Date();
		let blobWriter = new zip.BlobWriter("application/zip");
		let zipWriter = new zip.ZipWriter(blobWriter, { bufferedWrite: true, useCompressionStream: false });
		let listOfFiles = [];
		let topic = "cardsImagesExported";
		let length = selectedCards.length;
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, totalEstimated: length});
		await wdw_mainWindow.bulkOperation(actionId);
		for (let card of selectedCards) {
			let dirname = cardbookBGPreferences.getName(card.dirPrefId);
			let image = await cardbookIDBImage.getImage({}, "photo", dirname, card.cbid, card.fn);
			if (image && image.content && image.extension) {
				let extension = cardbookHTMLUtils.formatExtension(image.extension);
				let filename = cardbookBGUtils.getFormattedFileName(`${card.fn}.${extension}`);
				if (listOfFiles.includes(filename)) {
					filename = cardbookBGUtils.getFormattedFileName(`${card.fn}.${card.uid}.${extension}`);
				}
				let data = cardbookHTMLUtils.base64ToBlob(image.content, extension);
				await zipWriter.add(filename, new zip.BlobReader(data), { level: 0, lastModDate: creationDate, useWebWorkers: false });
				listOfFiles.push(filename);
				if (listOfFiles.length % 100 == 0) {
					await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: listOfFiles.length});
				}
			}
		}
		await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: listOfFiles.length});
        let zipFile = await zipWriter.close();
		let url = URL.createObjectURL(zipFile);
		await cardbookHTMLDownloads.download(url, aFileName, actionId, topic, listOfFiles.length);
	},

	importCardsFromFile: async function () {
		let filepicker = document.getElementById("importCardsToFileInput");
		if (filepicker.files.length == 1) {
			let file = filepicker.files[0];
			let fileReader = new FileReader();
			fileReader.readAsText(file, "UTF-8");
			fileReader.onload = function(event) {
				if (event.target.result) {
					wdw_mainWindow.importCardsFromFileNext(file.name, event.target.result);
				}
			};
		}
	},

	importCardsFromFileNext: async function (aFileName, aContent) {
		let target = document.getElementById("cardbookAccountsTree").selectedRow.id;
		let dirPrefId = cardbookBGUtils.getAccountId(target);
		if (!aContent) {
			await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "fileEmpty", values: [aFileName]});
			return;
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsImportedFromFile", array: [aFileName], totalEstimated: 10});
		await wdw_mainWindow.bulkOperation(actionId);
		if (cardbookHTMLUtils.getFileNameExtension(aFileName).toLowerCase() == "csv") {
			await messenger.runtime.sendMessage({query: "cardbook.prepareCSVImport", target: target, content: aContent, actionId: actionId});
			let delimiter = cardbookBGPreferences.getPref("exportDelimiter")
			let parser = d3.dsvFormat(delimiter);
			let result = parser.parseRows(aContent)
			let headers = result[0].join(delimiter);
			let url = "chrome/content/HTML/csvTranslator/wdw_csvTranslator.html";
			let params = new URLSearchParams();
			params.set("mode", "import");
			params.set("includePref", false);
			params.set("lineHeader", true);
			params.set("columnSeparator", delimiter);
			params.set("actionId", actionId);
			params.set("filename", aFileName);
			params.set("foundColumns", headers);
			let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
													url: `${url}?${params.toString()}`,
													type: "popup"});
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.importCardsFromFile", dirPrefId: dirPrefId, target: target, content: aContent, filename: aFileName, actionId: actionId});
		}
	},

	importCardsFromDir: async function () {
		let result = await messenger.runtime.sendMessage({ query: "cardbook.callFilePicker", result: "path", title: "dirImportTitle", mode: "DIR"});
		await wdw_mainWindow.importCardsFromDirNext(result.file, result.filename);
	},

	importCardsFromDirNext: async function (aDirectory, aDirectoryName) {
		let target = document.getElementById("cardbookAccountsTree").selectedRow.id;
		let dirPrefId = cardbookBGUtils.getAccountId(target);
		let url = cardbookBGPreferences.getUrl(dirPrefId);
		// search if file is already open
		if (aDirectory == url) {
			await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "importNotIntoSameDir"});
			return;
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsImportedFromDir", array: [aDirectory], totalEstimated: 10});
		await wdw_mainWindow.bulkOperation(actionId);
		await messenger.runtime.sendMessage({query: "cardbook.importCardsFromDir", dirPrefId: dirPrefId, target: target, url: aDirectory, actionId: actionId, directoryname: aDirectoryName});
	},

    finishImport: async function(aAlertTitle, aAlertMessage) {
        let url = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		params.set("type", "info");
		params.set("title", aAlertTitle);
		params.set("message", aAlertMessage);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	selectNodeToAction: async function (aAction, aEvent) {
		let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
		let nodeType = cardbookBGPreferences.getNode(dirPrefId);

		if (aAction == "CREATE") {
			if (nodeType != "categories") {
				return;
			}
			await wdw_mainWindow.createNode(dirPrefId, nodeType);
		} else if (cardbookHTMLUtils.possibleNodes.includes(nodeType)) {
			let name = document.getElementById("cardbookAccountsTree").selectedRow.name;
			let nodeId = document.getElementById("cardbookAccountsTree").selectedRow.id;
			if (aAction == "REMOVE") {
				await messenger.runtime.sendMessage({query: "cardbook.removeNode", node: nodeId});
			} else if (aAction == "CONVERT") {
				if (nodeType == "org") {
					await wdw_mainWindow.createListFromNode(dirPrefId, nodeId, name);
				} else {
					await wdw_mainWindow.convertNodeToList(dirPrefId, nodeId, name, nodeType);
				}
			} else if (aAction == "EDIT") {
				await messenger.runtime.sendMessage({query: "cardbook.renameNode", node: nodeId});
			}
		}
	},

	createNode: async function (aDirPrefId, aNodeType) {
		let dirPrefId = aDirPrefId || cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		if (aNodeType != "categories" ) {
			return
		} else if (dirPrefId) {
			if (cardbookBGPreferences.getReadOnly(dirPrefId) || cardbookBGPreferences.getType(dirPrefId) == "SEARCH") {
				return;
			}
		}

		let url = "chrome/content/HTML/renameField/wdw_cardbookRenameField.html";
		let params = new URLSearchParams();
		params.set("dirPrefId", dirPrefId);
		params.set("type", aNodeType);
		params.set("context", "CreateCat");
		params.set("showColor", "true");
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	createListFromNode: async function (aDirPrefId, aNodeId, aNodeName) {
		if ((aNodeName == cardbookBGPreferences.getPref("uncategorizedCards")) || cardbookBGPreferences.getReadOnly(aDirPrefId)) {
			return;
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "listCreatedFromNode", array: [aNodeName]});
		await wdw_mainWindow.bulkOperation(actionId);
		await messenger.runtime.sendMessage({query: "cardbook.createListFromNode", actionId: actionId, name: aNodeName, nodeId: aNodeId});
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	convertNodeToList: async function (aDirPrefId, aNodeId, aNodeName, aNodeType) {
		if ((aNodeName == cardbookBGPreferences.getPref("uncategorizedCards")) || cardbookBGPreferences.getReadOnly(aDirPrefId)) {
			return;
		}
		let topic = (aNodeType != "categories") ? "listCreatedFromNode" : "categoryConvertedToList";
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, array: [aNodeName]});
		await wdw_mainWindow.bulkOperation(actionId);
		await messenger.runtime.sendMessage({query: "cardbook.convertNodeToList", actionId: actionId, nodeName: aNodeName, nodeId: aNodeId, nodeType: aNodeType});
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	syncAccountsFromMenuEntries: async function (aEvent) {
		let dirPrefId = aEvent.target.id;
		wdw_mainWindow.syncAccount(dirPrefId);
	},

	syncAccounts: async function () {
		let syncButton = document.getElementById("toolbox").querySelector("div.sync");
		let options = syncButton.querySelectorAll(("[role='option']"),);
		let accounts = Array.from(options).map(x => x.id);
		let result = await cardbookHTMLPermissions.requestAllURLPermissions(accounts);
		if (result === true) {
			for (let account of accounts) {
				wdw_mainWindow.syncAccount(account, false);
			}
		}
	},

	syncAccount: async function (aDirPrefId, aAskPerm = true) {
		let dirPrefId = aDirPrefId || cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		let type = cardbookBGPreferences.getType(dirPrefId);
		if (!cardbookBGPreferences.getEnabled(dirPrefId) || !cardbookBGUtils.isMyAccountRemote(type)) {
			return;
		}
		let result = true;
		if (aAskPerm) {
			result = await cardbookHTMLPermissions.requestURLPermission(dirPrefId, null, type);
		}
		if (result === true) {
			await messenger.runtime.sendMessage({query: "cardbook.syncAccount", dirPrefId: dirPrefId});
		}
	},

	formatData: async function () {
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		if (cardbookBGPreferences.getReadOnly(dirPrefId)) {
			return;
		}
		let url = "chrome/content/HTML/formatData/wdw_formatData.html";
		let params = new URLSearchParams();
		params.set("dirPrefId", dirPrefId);
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	findOnlyDuplicates: async function () {
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		if (cardbookBGPreferences.getReadOnly(dirPrefId)) {
			return;
		}
		await wdw_mainWindow.findDuplicates(dirPrefId);
	},

	findAllDuplicates: async function () {
		await wdw_mainWindow.findDuplicates();
	},

	findDuplicates: async function (aDirPrefId) {
		let url = "chrome/content/HTML/findDuplicates/wdw_findDuplicates.html";
		let params = new URLSearchParams();
		if (aDirPrefId) {
			params.set("dirPrefId", aDirPrefId);
		}
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: `${url}?${params.toString()}`,
												type: "popup"});
	},

	generateFn: async function () {
		let dirPrefId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
		if (cardbookBGPreferences.getReadOnly(dirPrefId)) {
			return;
		}
		let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "displayNameGenerated"});
		await wdw_mainWindow.bulkOperation(actionId);
		await messenger.runtime.sendMessage({query: "cardbook.generateFn", actionId: actionId, accountId: wdw_mainWindow.currentAccountId});
		await messenger.runtime.sendMessage({query: "cardbook.endAction", actionId: actionId});
	},

	birthdaysList: async function() {
		let url = "chrome/content/HTML/birthdays/wdw_birthdayList.html";
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: url,
												type: "popup"});
	},

	birthdaysAdd: async function() {
		let url = "chrome/content/HTML/birthdays/wdw_birthdaySync.html";
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
												url: url,
												type: "popup"});
	},

	undo: async function() {
		await messenger.runtime.sendMessage({query: "cardbook.undo"});
	},

	redo: async function() {
		await messenger.runtime.sendMessage({query: "cardbook.redo"});
	},

	showCorrectTabs: function () {
		let showNewTab = false;
		for (let i of [ "listTab", "technicalTab", "vcardTab", "keyTab" ]) {
			let node = document.getElementById(i);
			if (cardbookBGPreferences.getPref(`${i}View`) === true) {
				node.classList.remove("hidden");
			} else {
				if (node.hasAttribute("visuallyselected")) {
					showNewTab = true;
				}
				node.classList.add("hidden");
			}
		}
		if (showNewTab === true) {
			cardbookHTMLWindowUtils.showPane("generalTabPanel");
		}
	},

	setComplexSearchMode: function (aType, aAllAB) {
		if (document.getElementById("showHideTerms").classList.contains("cardbookDown")) {
			if (document.getElementById("addressbookMenulist").value == "allAddressBooks") {
				wdw_mainWindow.cardbookComplexSearchMode = "SEARCH";
			} else {
				wdw_mainWindow.cardbookComplexSearchMode = "NOSEARCH";
			}
		} else if (aType == "SEARCH") {
			wdw_mainWindow.cardbookComplexSearchMode = "SEARCH";
		} else if (aAllAB == "allAB") {
			wdw_mainWindow.cardbookComplexSearchMode = "SEARCH";
		} else {
			wdw_mainWindow.cardbookComplexSearchMode = "NOSEARCH";
		}
	},

	openLogEdition: async function () {
		let url = "chrome/content/HTML/log/wdw_logEdition.html";
		let win = await messenger.runtime.sendMessage({query: "cardbook.openWindow",
														url: url,
														type: "popup"});
	},

	isThereRemoteAccountToSync: function () {
		for (let account of wdw_mainWindow.accounts) {
			if (account[2] && cardbookBGUtils.isMyAccountRemote(account[3])) {
				return true;
			}
		}
		return false;
	},

	getAvailableAccountNumber: function () {
		let result = 0;
		let visibleAccounts = cardbookHTMLDirTree.visibleData.filter(x => x[1] == x[5]);
		for (let account of visibleAccounts) {
			if (cardbookBGPreferences.getEnabled(account[5]) && cardbookBGPreferences.getType(account[5]) != "SEARCH") {
				result++;
			}
		}
		return result;
	},

	setElementIdLabel: function (aElementId, aValue) {
		if (document.getElementById(aElementId)) {
			wdw_mainWindow.setElementLabel(document.getElementById(aElementId), aValue);
		}
	},

	setElementLabel: function (aElement, aValue) {
		if (aElement) {
			aElement.textContent = aValue;
		}
	},
	
	enableOrDisableElement: function (aButtonArray, aContextArray, aValue) {
		for (let element of aButtonArray) {
			if (element) {
				element.disabled = aValue;
			}
		}
		for (let element of aContextArray) {
			wdw_mainWindow.context[element] = !aValue;
		}
	},

	enableCardDeletion: function () {
		let accountsNumber = wdw_mainWindow.getAvailableAccountNumber();
		if (accountsNumber === 0) {
			wdw_mainWindow.disableCardDeletion();
		} else {
			let deleteButton = document.getElementById("toolbox").querySelector("button.delete");
			wdw_mainWindow.enableOrDisableElement([deleteButton], ["delete"], false);
		}
	},

	enableCardCreation: function () {
		let addListButton = document.getElementById("toolbox").querySelector("button.addlist");
		let addContactButton = document.getElementById("toolbox").querySelector("button.addcontact");
		wdw_mainWindow.enableOrDisableElement([addContactButton, addListButton], ["addContact", "addList"], false);
	},

	enableListCreation: function () {
		let addListButton = document.getElementById("toolbox").querySelector("button.addlist");
		wdw_mainWindow.enableOrDisableElement([addListButton], ["addList"], false);
	},

	enableCardModification: function () {
		let editButton = document.getElementById("toolbox").querySelector("button.edit");
		wdw_mainWindow.enableOrDisableElement([editButton], ["edit"], false);
		if (editButton) {
			let label = wdw_mainWindow.context.ABRW ? "editCardButtonLabel" : "viewCardButtonLabel";
			editButton.textContent = messenger.i18n.getMessage(label);
		}
		wdw_mainWindow.setChatButton();
	},

	enableABModification: function () {
		wdw_mainWindow.context.ABRW = true;
	},

	enableABSync: function () {
		wdw_mainWindow.context.ABSync = true;
	},

	enableABAvailable: function () {
		wdw_mainWindow.context.ABAvailable = true;
	},

	disableCardDeletion: function () {
		let deleteButton = document.getElementById("toolbox").querySelector("button.delete");
		wdw_mainWindow.enableOrDisableElement([deleteButton], ["delete"], true);
	},
	
	disableCardCreation: function () {
		let addListButton = document.getElementById("toolbox").querySelector("button.addlist");
		let addContactButton = document.getElementById("toolbox").querySelector("button.addcontact");
		wdw_mainWindow.enableOrDisableElement([addContactButton, addListButton], ["addContact", "addList"], true);
	},
	
	disableListCreation: function () {
		let addListButton = document.getElementById("toolbox").querySelector("button.addlist");
		wdw_mainWindow.enableOrDisableElement([addListButton], ["addList"], true);
	},
	
	disableCardModification: function () {
		let editButton = document.getElementById("toolbox").querySelector("button.edit");
		wdw_mainWindow.enableOrDisableElement([editButton], ["edit"], true);
		if (editButton) {
			let label = wdw_mainWindow.context.ABRW ? "editCardButtonLabel" : "viewCardButtonLabel";
			editButton.textContent = messenger.i18n.getMessage(label);
		}
		wdw_mainWindow.setChatButton();
	},
	
	disableABModification: function () {
		wdw_mainWindow.context.ABRW = false;
	},
	
	disableABSync: function () {
		wdw_mainWindow.context.ABSync = false;
	},

	disableABAvailable: function () {
		wdw_mainWindow.context.ABAvailable = false;
	},

	setABSelection: function () {
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let accountId = document.getElementById("cardbookAccountsTree").selectedRow.id;
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			wdw_mainWindow.context.ABSelected = accountId == dirPrefId;
			wdw_mainWindow.context.NodeSelected = accountId != dirPrefId;
		} else {
			wdw_mainWindow.context.ABSelected = false;
			wdw_mainWindow.context.NodeSelected = false;
		}
	},

	updateStatusInformation: async function() {
		if (document.getElementById("cardbookAccountsTree").selectedRow && wdw_mainWindow.cardsTree.cardsList &&
				wdw_mainWindow.cardsTree.cardsList.view) {
			let message = "";
			let accountId = document.getElementById("cardbookAccountsTree").selectedRow.id;
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let cached = cardbookBGPreferences.getDBCached(dirPrefId);
			if (wdw_mainWindow.searchInput.value != "" || !cached) {
				let length = wdw_mainWindow.cardsTree.cardsList.view._rowMap.length;
				let modified = 0;
				for (let i in wdw_mainWindow.cardsTree.cardsList.view._rowMap) {
					let card = wdw_mainWindow.cardsTree.cardsList.view._rowMap[i].card;
					if (card.updated || card.created) {
						modified++;
					}
				}
				if (modified) {
					message = messenger.i18n.getMessage("numberContactsFoundModified", [length, modified]);
				} else {
					message = messenger.i18n.getMessage("numberContactsFound", [length]);
				}
			} else {
				if (wdw_mainWindow.cardsTree.cardsList.view) {
					let length = wdw_mainWindow.cardsTree.cardsList.view._rowMap.length;
					let modified = await messenger.runtime.sendMessage({query: "cardbook.getModifiedCards", account: accountId});
					if (modified) {
						message = messenger.i18n.getMessage("numberContactsModified", [length, modified]);
					} else {
						message = messenger.i18n.getMessage("numberContacts", [length]);
					}
				}
			}
			let selectedLength = wdw_mainWindow.getSelectedCards().length;
			if (selectedLength > 1) {
				document.getElementById("selectedContacts").classList.remove("hidden");
				document.getElementById("selectedContacts").textContent = messenger.i18n.getMessage("numberContactsSelected", [selectedLength]);
			} else {
				document.getElementById("selectedContacts").classList.add("hidden");
			}
			document.getElementById("totalContacts").classList.remove("hidden");
			document.getElementById("totalContacts").textContent = message;
		} else {
			document.getElementById("totalContacts").classList.add("hidden");
		}
	},

	windowControlShowing: async function () {
		wdw_mainWindow.setABSelection();
		if (cardbookHTMLDirTree.visibleData.length == 0) {
			wdw_mainWindow.disableABModification();
			wdw_mainWindow.disableABSync();
			wdw_mainWindow.disableCardCreation();
			wdw_mainWindow.disableCardModification();
			wdw_mainWindow.disableCardDeletion();
		} else if (wdw_mainWindow.searchInput.value != "" || wdw_mainWindow.cardbookComplexSearchMode === "SEARCH") {
			wdw_mainWindow.enableABModification();
			wdw_mainWindow.enableABSync();
			wdw_mainWindow.enableCardCreation();
			if (wdw_mainWindow.getSelectedCardsCount() >= 2 || wdw_mainWindow.getSelectedCardsCount() == 0) {
				wdw_mainWindow.disableCardModification();
			} else {
				wdw_mainWindow.enableCardModification();
			}
			if (wdw_mainWindow.getSelectedCardsCount() == 0) {
				wdw_mainWindow.disableCardDeletion();
			} else {
				wdw_mainWindow.enableCardDeletion();
			}
			wdw_mainWindow.disableListCreation();
		}
		if (document.getElementById("cardbookAccountsTree").selectedRow) {
			let dirPrefId = document.getElementById("cardbookAccountsTree").selectedRow.root;
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			let ABType = cardbookBGPreferences.getType(dirPrefId);
			let remote = cardbookBGUtils.isMyAccountRemote(ABType);
			if (enabled) {
				if (cardbookBGPreferences.getReadOnly(dirPrefId)) {
					wdw_mainWindow.disableABModification();
					wdw_mainWindow.disableCardCreation();
					wdw_mainWindow.disableCardDeletion();
					wdw_mainWindow.disableCardModification();
				} else {
					wdw_mainWindow.enableABModification();
					wdw_mainWindow.enableCardCreation();
					wdw_mainWindow.enableListCreation();
					if (wdw_mainWindow.getSelectedCardsCount() == 0) {
						wdw_mainWindow.disableCardDeletion();
					} else {
						wdw_mainWindow.enableCardDeletion();
					}
					if (wdw_mainWindow.getSelectedCardsCount() >= 2 || wdw_mainWindow.getSelectedCardsCount() == 0) {
						wdw_mainWindow.disableCardModification();
					} else {
						wdw_mainWindow.enableCardModification();
					}
				}
				if (cardbookBGPreferences.getType(dirPrefId).startsWith("GOOGLE")) {
					wdw_mainWindow.disableListCreation();
				}
				if (remote) {
					wdw_mainWindow.enableABSync();
				} else {
					wdw_mainWindow.disableABSync();
				}
			} else {
				wdw_mainWindow.disableABModification();
				wdw_mainWindow.disableABSync();
				wdw_mainWindow.disableCardCreation();
				wdw_mainWindow.disableCardModification();
				wdw_mainWindow.disableCardDeletion();
			}
		} else {
			wdw_mainWindow.disableABModification();
			wdw_mainWindow.disableABSync();
			wdw_mainWindow.disableCardCreation();
			wdw_mainWindow.disableCardModification();
			wdw_mainWindow.disableCardDeletion();
		}
		let elements = Array.from(document.getElementById("toolbox").children);
		let syncElement = elements.filter(x => x.id.startsWith("sync_"))[0];
		if (syncElement) {
			wdw_mainWindow.setSyncButton(syncElement);
		}
		let newABButton = document.getElementById("toolbox").querySelector("button.newAB");
		let prefsButton = document.getElementById("toolbox").querySelector("button.prefs");
		let printButton = document.getElementById("toolbox").querySelector("button.print");
		let searchButton = document.getElementById("toolbox").querySelector("button.search");
		let emailButton = document.getElementById("toolbox").querySelector(".email > button");
		wdw_mainWindow.enableOrDisableElement([newABButton, prefsButton, printButton, searchButton, emailButton],
			["newAB", "prefs", "print", "search", "email"], false);
		await wdw_mainWindow.updateStatusInformation();
		await wdw_mainWindow.prepareSubMenus();
	},

	refreshWindow: async function (aParams, aMessage) {
		wdw_mainWindow.initTrees();
		wdw_mainWindow.displayId++;
		let selectionBefore = wdw_mainWindow.accountSelected;
		await wdw_mainWindow.refreshAccountsInDirTree(wdw_mainWindow.displayId);
		let selectionAfter = wdw_mainWindow.accountSelected;
		await wdw_mainWindow.windowControlShowing();
		
		let mySyncCondition = false;
		if (selectionBefore != selectionAfter) {
			mySyncCondition = true;
		} else if (wdw_mainWindow.cardbookComplexSearchMode == "SEARCH") {
			mySyncCondition = true;
		} else {
			if (aParams) {
				if (aParams.startsWith("forceAccount::")) {
					mySyncCondition = true;
				} else {
					let myDirPredId = cardbookBGUtils.getAccountId(aParams);
					let myCurrentDirPredId = cardbookBGUtils.getAccountId(wdw_mainWindow.currentAccountId);
					mySyncCondition = (myCurrentDirPredId == myDirPredId);
				}
			} else {
				mySyncCondition = true;
			}
		}

		// for search mode the reselection is done inside their functions
		if (mySyncCondition) {
			await wdw_mainWindow.displayBook(aMessage);
		}
		await wdw_mainWindow.windowControlShowing();
	}
};

wdw_mainWindow.load();

messenger.runtime.onMessage.addListener( (info) => {
	switch (info.query) {
		// test case "cardbook.importKeyFromValue":
		// test 	let values = JSON.parse(aData);
		// test 	let confirmTitle = cardbookRepository.extension.localeData.localizeMessage("confirmTitle");
		// test 	let confirmMsg = cardbookRepository.extension.localeData.localizeMessage("importKeyFromCards.label");
		// test 	if (Services.prompt.confirm(window, confirmTitle, confirmMsg)) {
		// test 		wdw_cardbook.importKeyFromValue(
		// test 			values.keyValue,
		// test 			{
		// test 				"dirPrefId": values.dirPrefId,
		// test 				"fn": values.fn
		// test 			}
		// test 		);
		// test 	}
		// test 	break;
		case "cardbook.pref.preferencesChanged": {
			let dummy = new Promise(async (resolve, reject) => {
				wdw_mainWindow.setToolbar();
				wdw_mainWindow.showCorrectTabs();
				await wdw_mainWindow.refreshWindow();
			});
			break;
		}
		case "cardbook.DBMigrateFinished":
		case "cardbook.accountLoaded": {
			let dummy = new Promise(async (resolve, reject) => {
				await wdw_mainWindow.refreshWindow(info.aData);
			});
			break;
		}
		case "cardbook.addressbookDeleted":
			document.getElementById("cardbookAccountsTree").selectedRow = null;
		case "cardbook.addressbookCreated":
			cardbookHTMLWindowUtils.clearCard();
		case "cardbook.addressbookModified":
		case "cardbook.complexSearchLoaded":
		case "cardbook.syncFinished": {
			let dummy = new Promise(async (resolve, reject) => {
				wdw_mainWindow.prepareSyncContext(info.aData, false);
				await wdw_mainWindow.refreshWindow(info.aData);
				await cardbookHTMLDirTree.setSyncingIcon(info.aData);
			});
			break;
		}
		case "cardbook.syncRunning": {
			let dummy = new Promise(async (resolve, reject) => {
				wdw_mainWindow.prepareSyncContext(info.aData, true);
				await cardbookHTMLDirTree.setSyncingIcon(info.aData);
			});
			break;
		}
		case "cardbook.DBMigrate": {
			let dummy = new Promise(async (resolve, reject) => {
				let label = messenger.i18n.getMessage("migrating", [info.aData]);
				await wdw_mainWindow.refreshWindow(null, label);
			});
			break;
		}
		case "cardbook.categoryDeleted":
			document.getElementById("cardbookAccountsTree").selectedRow = null;
		case "cardbook.categoryCreated": {
			let dummy = new Promise(async (resolve, reject) => {
				let account = info.aData.replace("forceAccount::", "");
				await wdw_mainWindow.refreshWindow(info.aData);
				await wdw_mainWindow.setCurrentAccountId(account);
			});
			break;
		}
		case "cardbook.cardCreated":
		case "cardbook.cardEdited":
		case "cardbook.cardModified":
		case "cardbook.cardsConverted":
		case "cardbook.cardsDeleted":
		case "cardbook.cardsDragged":
		case "cardbook.cardsMerged":
		case "cardbook.cardsDuplicated":
		case "cardbook.cardsImportedFromDir":
		case "cardbook.cardsImportedFromFile":
		case "cardbook.cardsPasted":
		case "cardbook.categoryConvertedToList":
		case "cardbook.categoryRenamed":
		case "cardbook.categorySelected":
		case "cardbook.categoryUnselected":
		case "cardbook.displayNameGenerated":
		case "cardbook.emailCollectedByFilter":
		case "cardbook.emailDeletedByFilter":
		case "cardbook.linePasted":
		case "cardbook.listConvertedToCategory":
		case "cardbook.outgoingEmailCollected":
		case "cardbook.redoActionDone":
		case "cardbook.undoActionDone":
		case "cardbook.listCreatedFromNode":
		case "cardbook.nodeDeleted":
		case "cardbook.nodeRenamed":
		case "cardbook.cardsFormatted": {
			let dummy = new Promise(async (resolve, reject) => {
				await wdw_mainWindow.refreshWindow(info.aData);
			});
			break;
		}
		case "cardbook.writeCardsToCSVFile": {
			let dummy = new Promise(async (resolve, reject) => {
				let selectedCards = (info.params.source == "accounts") ? wdw_mainWindow.getCardsFromAccountsOrCats() : wdw_mainWindow.getSelectedCards();
				let topic = "cardsExportedToCsv";
				let length = selectedCards.length;
				let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: topic, totalEstimated: length, total: length});
				await wdw_mainWindow.bulkOperation(actionId);
				let output = "";
				let k = 0;
				let labels = info.params.labels.split("|");
				for (let column of labels) {
					if (k === 0) {
						output = "\"" + column + "\"";
						k++;
					} else {
						output = output + info.params.columnSeparator + "\"" + column + "\"";
					}
				}
				k = 0;
				let count = 0;
				let fields = info.params.fields.split("|");
				for (let card of selectedCards) {
					for (let column of fields) {
						let tmpValue;
						if (column == "categories_0_array") {
							tmpValue = cardbookHTMLUtils.getCardValueByField(card, column, false);
							tmpValue = cardbookBGUtils.escapeArrayComma(tmpValue).join(",");
						} else {
							tmpValue = cardbookHTMLUtils.getCardValueByField(card, column, info.params.includePref).join("\r\n");
						}
						if (tmpValue.includes('"')) {
							tmpValue = tmpValue.replace(/"/g, '""');
						}
						let tmpResult = "\"" + tmpValue + "\"";
						if (k === 0) {
							output = output + "\r\n" + tmpResult;
							k++;
						} else {
							output = output + info.params.columnSeparator + tmpResult;
						}
					}
					k = 0;
					count++;
					if (count % 100 == 0) {
						await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: count});
					}
				}
				await messenger.runtime.sendMessage({query: "cardbook.updateCurrentAction", actionId: actionId, doneCards: count});

				// a final blank line
				output = output + "\r\n";
				let file = new Blob([output], { type: "text/plain" });
				let url = URL.createObjectURL(file);
				await cardbookHTMLDownloads.download(url, info.params.filename, actionId, topic, length);
			});
			break;
		}
		case "cardbook.finishImport": {
			let dummy = new Promise(async (resolve, reject) => {
				await wdw_mainWindow.finishImport(info.title, info.message);
			});
			break;
		}
		case "cardbook.setUndoAndRedoLabel":
			wdw_mainWindow.prepareUndoAndRedoSubMenus(info.code, info.value);
			break;
		case "cardbook.alertUser":
			if (wdw_mainWindow.winId == info.winId) {
				if (info.action == "deleteCard" && info.response === true) {
					return new Promise(async (resolve, reject) => {
						let listOfSelectedCard = wdw_mainWindow.getSelectedCards();
						let actionId = await messenger.runtime.sendMessage({query: "cardbook.startAction", actionCode: "cardsDeleted", array: listOfSelectedCard, refresh: null, totalEstimated: listOfSelectedCard.length, total: listOfSelectedCard.length});
						await wdw_mainWindow.bulkOperation(actionId);
						await messenger.runtime.sendMessage({query: "cardbook.deleteCards", actionId: actionId, cards: listOfSelectedCard});
					});
				} else if (info.action == "emailCards") {
					if (info.checkboxAction === true) {
						let dummy = new Promise(async (resolve, reject) => {
							await cardbookBGPreferences.setPref("warnEmptyEmails", !info.checkboxAction);
						});
					}
					if (info.response === true) {
						return new Promise(async (resolve, reject) => {
							await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: wdw_mainWindow.compFields});
						});
					}
				}
			}
			break;
		}
});

let lastContextElement;
let currentTab = await messenger.tabs.getCurrent();
document.addEventListener("contextmenu", async (event) => {
	lastContextElement = event.target;

	browser.menus.overrideContext({context: "tab", tabId: currentTab.id});
	browser.menus.removeAll();
	let properties = { contexts: ["tab"], enabled: true, visible: true, viewTypes: ["tab", "popup"] };
	let toolbox = event.target.closest("#toolbox");
	let table = event.target.closest("tree-view");
	let row = event.target.closest("tr");
	let dir = event.target.closest(`ul[is="cb-tree-listbox"]`);

	if (toolbox) {
		let customizeTitle = messenger.i18n.getMessage("CustomizeCardBookToolbarLabel");
		let customizeProperties = { ...properties, id: "customize", title: customizeTitle};
		await browser.menus.create(customizeProperties);
	} else if (event.target.tagName.toUpperCase() == "IMG") {
		let defaultCardImage = "chrome/content/skin/contact-generic.svg";
		if (!event.target.src.endsWith(defaultCardImage)) {
			let saveImageCardTitle = messenger.i18n.getMessage("saveImageCardLabel");
			let saveImageCardProperties = { ...properties, id: "saveImageCard", title: saveImageCardTitle};
			await browser.menus.create(saveImageCardProperties);

			let copyImageCardTitle = messenger.i18n.getMessage("copyImageCardLabel");
			let copyImageCardProperties = { ...properties, id: "copyImageCard", title: copyImageCardTitle};
			await browser.menus.create(copyImageCardProperties);
		}
	} else if (row?.getAttribute("data-field-name") == "email") {
		let type = row.getAttribute("data-field-name");
		let toEmailTitle = messenger.i18n.getMessage("toEmailEmailTreeLabel");
		let toEmailProperties = { ...properties, id: "toEmail", title: toEmailTitle};
		await browser.menus.create(toEmailProperties);

		let ccEmailTitle = messenger.i18n.getMessage("ccEmailEmailTreeLabel");
		let ccEmailProperties = { ...properties, id: "ccEmail", title: ccEmailTitle};
		await browser.menus.create(ccEmailProperties);

		let bccEmailTitle = messenger.i18n.getMessage("bccEmailEmailTreeLabel");
		let bccEmailProperties = { ...properties, id: "bccEmail", title: bccEmailTitle};
		await browser.menus.create(bccEmailProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let findEmailsTitle = messenger.i18n.getMessage("findemailemailTreeLabel");
		let findEmailsProperties = { ...properties, id: "findEmails", title: findEmailsTitle, enabled: wdw_mainWindow.context.findEmailsEnabled};
		await browser.menus.create(findEmailsProperties);

		let findEventsTitle = messenger.i18n.getMessage("findeventemailTreeLabel");
		let findEventsProperties = { ...properties, id: "findEvents", title: findEventsTitle};
		await browser.menus.create(findEventsProperties);

		let separator2Properties = { ...properties, id: "separator2", type: "separator"};
		await browser.menus.create(separator2Properties);
		
		let searchForOnlineKeyTitle = messenger.i18n.getMessage("searchForOnlineKeyTreeLabel");
		let searchForOnlineKeyProperties = { ...properties, id: "searchForOnlineKey", title: searchForOnlineKeyTitle};
		await browser.menus.create(searchForOnlineKeyProperties);

		let separator3Properties = { ...properties, id: "separator3", type: "separator"};
		await browser.menus.create(separator3Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyEmailTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyEmailProperties = { ...properties, id: "copyField", title: copyEmailTitle};
		await browser.menus.create(copyEmailProperties);
	} else if (row?.getAttribute("data-field-name") == "url") {
		let type = row.getAttribute("data-field-name");
		let openURLTitle = messenger.i18n.getMessage("openURLTreeLabel");
		let openURLProperties = { ...properties, id: "openURL", title: openURLTitle};
		await browser.menus.create(openURLProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyURLTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyURLProperties = { ...properties, id: "copyField", title: copyURLTitle};
		await browser.menus.create(copyURLProperties);
	} else if (row?.getAttribute("data-field-name") == "impp") {
		let type = row.getAttribute("data-field-name");
		let connectIMPPTitle = messenger.i18n.getMessage("IMPPMenuLabel");
		let connectIMPPProperties = { ...properties, id: "connectIMPP", title: connectIMPPTitle};
		await browser.menus.create(connectIMPPProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyIMPPTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyIMPPProperties = { ...properties, id: "copyField", title: copyIMPPTitle};
		await browser.menus.create(copyIMPPProperties);
	} else if (row?.getAttribute("data-field-name") == "adr") {
		let type = row.getAttribute("data-field-name");
		let localizeAdrTitle = messenger.i18n.getMessage("localizeadrTreeLabel");
		let localizeAdrProperties = { ...properties, id: "localizeAdr", title: localizeAdrTitle};
		await browser.menus.create(localizeAdrProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		for (let element of cardbookHTMLUtils.adrElements) {
			let copyElementLabel = messenger.i18n.getMessage(`${element}Label`);
			let copyElementTitle = messenger.i18n.getMessage("copyFieldValue", [copyElementLabel]);
			let copyElementProperties = { ...properties, id: `copyField${element}`, title: copyElementTitle};
			await browser.menus.create(copyElementProperties);
		}

		let separator2Properties = { ...properties, id: "separator2", type: "separator"};
		await browser.menus.create(separator2Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyAdrTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyAdrProperties = { ...properties, id: "copyField", title: copyAdrTitle};
		await browser.menus.create(copyAdrProperties);
	} else if (row?.getAttribute("data-field-name") == "tel") {
		let type = row.getAttribute("data-field-name");
		let connectTelTitle = messenger.i18n.getMessage("IMPPMenuLabel");
		let connectTelProperties = { ...properties, id: "connectTel", title: connectTelTitle};
		await browser.menus.create(connectTelProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyTelTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyTelProperties = { ...properties, id: "copyField", title: copyTelTitle};
		await browser.menus.create(copyTelProperties);
	} else if (row?.getAttribute("data-field-name")) {
		let field = row.getAttribute("data-field-name");
		let fieldLabel = row.getAttribute("data-field-label") || messenger.i18n.getMessage(`${field}Label`);
		let copyTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyProperties = { ...properties, id: "copyField", title: copyTitle};
		await browser.menus.create(copyProperties);
	} else if (lastContextElement.id == "noteInputLabel") {
		let field = lastContextElement.getAttribute("data-field-name");
		let fieldLabel = messenger.i18n.getMessage(`${field}Label`);
		let copyTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyProperties = { ...properties, id: "copyField", title: copyTitle};
		await browser.menus.create(copyProperties);
	} else if (table?.id == "cardbookCardsTree") {
		let addContactTitle = messenger.i18n.getMessage("cardbookToolbarAddContactButtonLabel");
		let addContactProperties = { ...properties, id: "addContact", title: addContactTitle, enabled: wdw_mainWindow.context.addContact};
		await browser.menus.create(addContactProperties);

		let addListTitle = messenger.i18n.getMessage("cardbookToolbarAddListButtonLabel");
		let addListProperties = { ...properties, id: "addList", title: addListTitle, enabled: wdw_mainWindow.context.addList};
		await browser.menus.create(addListProperties);

		let label = wdw_mainWindow.context.ABRW ? "editCardButtonLabel" : "viewCardButtonLabel";
		let editCardTitle = messenger.i18n.getMessage(label);
		let editCardProperties = { ...properties, id: "editCard", title: editCardTitle, enabled: wdw_mainWindow.context.edit};
		await browser.menus.create(editCardProperties);

		let removeCardTitle = messenger.i18n.getMessage("cardbookToolbarRemoveButtonLabel");
		let removeCardProperties = { ...properties, id: "removeCard", title: removeCardTitle, enabled: wdw_mainWindow.context.delete};
		await browser.menus.create(removeCardProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let categoriesTitle = messenger.i18n.getMessage("categoryHeader");
		let categoriesProperties = { ...properties, id: "categories", title: categoriesTitle, enabled: wdw_mainWindow.context.contactSelectedCount != 0};
		await browser.menus.create(categoriesProperties);

		let addCategoryTitle = messenger.i18n.getMessage("categoryMenuLabel");
		let addCategoryProperties = { ...properties, id: "addCategory", parentId: "categories", title: addCategoryTitle};
		await browser.menus.create(addCategoryProperties);

		let separator2Properties = { ...properties, id: "separator2", parentId: "categories", type: "separator"};
		await browser.menus.create(separator2Properties);

		let catCount = 0;
		for (let category of wdw_mainWindow.context.categories) {
			let assignCategoryTitle = category.name;
			let catProperties = { ...properties, type: "checkbox", checked: category.checked, enabled: category.enabled};
			let assignCategoryProperties = { ...catProperties, id: `assignCategory${catCount}`, parentId: "categories", title: assignCategoryTitle};
			await browser.menus.create(assignCategoryProperties);
			catCount++;
		}

		let separator3Properties = { ...properties, id: "separator3", type: "separator"};
		await browser.menus.create(separator3Properties);

		let toEmailCardsTitle = messenger.i18n.getMessage("toEmailCardFromCardsLabel");
		let toEmailCardsProperties = { ...properties, id: "toEmailCards", title: toEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(toEmailCardsProperties);

		let ccEmailCardsTitle = messenger.i18n.getMessage("ccEmailCardFromCardsLabel");
		let ccEmailCardsProperties = { ...properties, id: "ccEmailCards", title: ccEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(ccEmailCardsProperties);

		let bccEmailCardsTitle = messenger.i18n.getMessage("bccEmailCardFromCardsLabel");
		let bccEmailCardsProperties = { ...properties, id: "bccEmailCards", title: bccEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(bccEmailCardsProperties);

		let shareCardsByEmailTitle = messenger.i18n.getMessage("shareCardByEmailFromCardsLabel");
		let shareCardsByEmailProperties = { ...properties, id: "shareCardsByEmail", title: shareCardsByEmailTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(shareCardsByEmailProperties);

		let findEmailsFromCardsTitle = messenger.i18n.getMessage("findEmailsFromCardsLabel");
		let findEmailsFromCardsProperties = { ...properties, id: "findEmailsFromCards", title: findEmailsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(findEmailsFromCardsProperties);

		let separator4Properties = { ...properties, id: "separator4", type: "separator"};
		await browser.menus.create(separator4Properties);

		let IMPPsTitle = messenger.i18n.getMessage("IMPPMenuLabel");
		let IMPPsProperties = { ...properties, id: "IMPPs", title: IMPPsTitle};
		await browser.menus.create(IMPPsProperties);

		let IMMPcount = 0;
		for (let IMPP of wdw_mainWindow.context.IMPPs) {
			let IMPPTitle = IMPP.label;
			let IMPPProperties = { ...properties, id: `runIMPP${IMMPcount}`, parentId: "IMPPs", title: IMPPTitle};
			await browser.menus.create(IMPPProperties);
			IMMPcount++;
		}

		let separator5Properties = { ...properties, id: "separator5", type: "separator"};
		await browser.menus.create(separator5Properties);

		let eventsAndTasksTitle = messenger.i18n.getMessage("eventsAndTasksLabel");
		let eventsAndTasksProperties = { ...properties, id: "eventsAndTasks", title: eventsAndTasksTitle};
		await browser.menus.create(eventsAndTasksProperties);

		let addEventTitle = messenger.i18n.getMessage("addEventFromCardsLabel");
		let addEventProperties = { ...properties, id: "addEvent", parentId: "eventsAndTasks", title: addEventTitle};
		await browser.menus.create(addEventProperties);

		let addTodoTitle = messenger.i18n.getMessage("addTodoFromCardsLabel");
		let addTodoProperties = { ...properties, id: "addTodo", parentId: "eventsAndTasks", title: addTodoTitle};
		await browser.menus.create(addTodoProperties);

		let findEventsFromCardsTitle = messenger.i18n.getMessage("findEventsFromCardsLabel");
		let findEventsFromCardsProperties = { ...properties, id: "findEventsFromCards", parentId: "eventsAndTasks", title: findEventsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(findEventsFromCardsProperties);

		let separator6Properties = { ...properties, id: "separator6", type: "separator"};
		await browser.menus.create(separator6Properties);

		let localizeCardsFromCardsTitle = messenger.i18n.getMessage("localizeCardFromCardsLabel");
		let localizeCardsFromCardsProperties = { ...properties, id: "localizeCardsFromCards", title: localizeCardsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(localizeCardsFromCardsProperties);

		let separator7Properties = { ...properties, id: "separator7", type: "separator"};
		await browser.menus.create(separator7Properties);

		let openURLFromCardsTitle = messenger.i18n.getMessage("openURLCardFromCardsLabel");
		let openURLFromCardsProperties = { ...properties, id: "openURLFromCards", title: openURLFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(openURLFromCardsProperties);

		let separator8Properties = { ...properties, id: "separator8", type: "separator"};
		await browser.menus.create(separator8Properties);

		let publicKeysTitle = messenger.i18n.getMessage("publicKeysFromCards.label");
		let publicKeysProperties = { ...properties, id: "publicKeys", title: publicKeysTitle};
		await browser.menus.create(publicKeysProperties);

		let searchForOnlineKeyFromCardsTitle = messenger.i18n.getMessage("searchForOnlineKeyFromCards.label");
		let searchForOnlineKeyFromCardsProperties = { ...properties, id: "searchForOnlineKeyFromCards", parentId: "publicKeys", title: searchForOnlineKeyFromCardsTitle};
		await browser.menus.create(searchForOnlineKeyFromCardsProperties);

		let importKeyFromCardsTitle = messenger.i18n.getMessage("importKeyFromCards.label");
		let importKeyFromCardsProperties = { ...properties, id: "importKeyFromCards", parentId: "publicKeys", title: importKeyFromCardsTitle};
		await browser.menus.create(importKeyFromCardsProperties);

		let separator9Properties = { ...properties, id: "separator9", type: "separator"};
		await browser.menus.create(separator9Properties);

		let cutCardsTitle = messenger.i18n.getMessage("cutCardFromCardsLabel");
		let cutCardsProperties = { ...properties, id: "cutCards", title: cutCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1 && wdw_mainWindow.context.ABRW};
		await browser.menus.create(cutCardsProperties);

		let copyCardsTitle = messenger.i18n.getMessage("copyCardFromCardsLabel");
		let copyCardsProperties = { ...properties, id: "copyCards", title: copyCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(copyCardsProperties);

		let pasteCardsTitle = messenger.i18n.getMessage("pasteCardFromCardsLabel");
		let pasteCardsProperties = { ...properties, id: "pasteCards", title: pasteCardsTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(pasteCardsProperties);

		let pasteLabel = wdw_mainWindow.copyField?.label;
		let pasteEntryFromCardsTitle = (pasteLabel) ? messenger.i18n.getMessage("pasteFieldValue", [pasteLabel]) :  messenger.i18n.getMessage("pasteEntryLabel");
		let pasteEntryFromCardsProperties = { ...properties, id: "pasteEntryFromCards", title: pasteEntryFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1 && wdw_mainWindow.context.ABRW};
		await browser.menus.create(pasteEntryFromCardsProperties);

		let separator10Properties = { ...properties, id: "separator10", type: "separator"};
		await browser.menus.create(separator10Properties);

		let duplicateCardsFromCardsTitle = messenger.i18n.getMessage("duplicateCardFromCardsLabel");
		let duplicateCardsFromCardsProperties = { ...properties, id: "duplicateCardsFromCards", title: duplicateCardsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1 && wdw_mainWindow.context.ABRW};
		await browser.menus.create(duplicateCardsFromCardsProperties);

		let mergeCardsFromCardsTitle = messenger.i18n.getMessage("mergeCardsFromCardsLabel");
		let mergeCardsFromCardsProperties = { ...properties, id: "mergeCardsFromCards", title: mergeCardsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 2};
		await browser.menus.create(mergeCardsFromCardsProperties);

		let separator11Properties = { ...properties, id: "separator11", type: "separator"};
		await browser.menus.create(separator11Properties);

		let convertListToCategoryTitle = messenger.i18n.getMessage("convertListToCategoryFromCardsLabel");
		let convertListToCategoryProperties = { ...properties, id: "convertListToCategory", title: convertListToCategoryTitle, enabled: wdw_mainWindow.context.contactSelectedIsAList};
		await browser.menus.create(convertListToCategoryProperties);

		let separator12Properties = { ...properties, id: "separator12", type: "separator"};
		await browser.menus.create(separator12Properties);

		let printFromCardsTitle = messenger.i18n.getMessage("cardbookToolbarPrintButtonLabel");
		let printFromCardsProperties = { ...properties, id: "printFromCards", title: printFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(printFromCardsProperties);

		let separator13Properties = { ...properties, id: "separator13", type: "separator"};
		await browser.menus.create(separator13Properties);

		let exportsFromCardsTitle = messenger.i18n.getMessage("exportsMenu");
		let exportsFromCardsProperties = { ...properties, id: "exportsFromCards", title: exportsFromCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(exportsFromCardsProperties);

		let exportCardToCSVFileTitle = messenger.i18n.getMessage("exportCardToCSVFileLabel");
		let exportCardToCSVFileProperties = { ...properties, id: "exportCardsToCSVFileFromCards", parentId: "exportsFromCards", title: exportCardToCSVFileTitle};
		await browser.menus.create(exportCardToCSVFileProperties);

		let exportCardToVCFFileTitle = messenger.i18n.getMessage("exportCardToVCFFileLabel");
		let exportCardToVCFFileProperties = { ...properties, id: "exportCardsToVCFFileFromCards", parentId: "exportsFromCards", title: exportCardToVCFFileTitle};
		await browser.menus.create(exportCardToVCFFileProperties);

		let exportCardsToZipTitle = messenger.i18n.getMessage("exportCardToZIPFileLabel");
		let exportCardsToZipProperties = { ...properties, id: "exportCardsToZipFromCards", parentId: "exportsFromCards", title: exportCardsToZipTitle};
		await browser.menus.create(exportCardsToZipProperties);

		let exportCardImagesTitle = messenger.i18n.getMessage("exportCardImagesLabel");
		let exportCardImagesProperties = { ...properties, id: "exportCardsImagesFromCards", parentId: "exportsFromCards", title: exportCardImagesTitle};
		await browser.menus.create(exportCardImagesProperties);
	} else if (dir?.id == "cardbookAccountsTree") {
		let dirPrefId = document.getElementById("cardbookAccountsTree")?.selectedRow?.root || "";
		let syncing = false;
		let cached = cardbookBGPreferences.getDBCached(dirPrefId);
		if (wdw_mainWindow.context.syncing && wdw_mainWindow.context.syncing[dirPrefId]) {
			syncing = wdw_mainWindow.context.syncing[dirPrefId];
		}

		let addAccountTitle = messenger.i18n.getMessage("cardbookToolbarAddServerButtonLabel");
		let addAccountProperties = { ...properties, id: "addAccount", title: addAccountTitle};
		await browser.menus.create(addAccountProperties);

		let editAccountTitle = messenger.i18n.getMessage("editAccountFromAccountsOrCatsLabel");
		let editAccountProperties = { ...properties, id: "editAccount", title: editAccountTitle, enabled: wdw_mainWindow.context.ABAvailable && !syncing};
		await browser.menus.create(editAccountProperties);

		let removeAccountTitle = messenger.i18n.getMessage("removeAccountFromAccountsOrCatsLabel");
		let removeAccountProperties = { ...properties, id: "removeAccount", title: removeAccountTitle, enabled: wdw_mainWindow.context.ABAvailable && !syncing};
		await browser.menus.create(removeAccountProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
		let enableOrDisableAccountTitle = enabled == true ? messenger.i18n.getMessage("disableFromAccountsOrCats") : messenger.i18n.getMessage("enableFromAccountsOrCats");
		let enableOrDisableAccountProperties = { ...properties, id: "enableOrDisableAccount", title: enableOrDisableAccountTitle, enabled: wdw_mainWindow.context.ABAvailable};
		await browser.menus.create(enableOrDisableAccountProperties);

		let separator2Properties = { ...properties, id: "separator2", type: "separator"};
		await browser.menus.create(separator2Properties);

		let readonly = cardbookBGPreferences.getReadOnly(dirPrefId);
		let readOnlyOrReadWriteAccountTitle = readonly == true ? messenger.i18n.getMessage("readWriteFromAccountsOrCats") : messenger.i18n.getMessage("readOnlyFromAccountsOrCats");
		let readOnlyOrReadWriteAccountProperties = { ...properties, id: "readOnlyOrReadWriteAccount", title: readOnlyOrReadWriteAccountTitle, enabled: wdw_mainWindow.context.ABAvailable && cached};
		await browser.menus.create(readOnlyOrReadWriteAccountProperties);

		let separator3Properties = { ...properties, id: "separator3", type: "separator"};
		await browser.menus.create(separator3Properties);

		let syncAccountTitle = messenger.i18n.getMessage("syncAccountFromAccountsOrCatsLabel");
		let syncAccountProperties = { ...properties, id: "syncAccount", title: syncAccountTitle, enabled: wdw_mainWindow.context.ABSync};
		await browser.menus.create(syncAccountProperties);

		let separator4Properties = { ...properties, id: "separator4", type: "separator"};
		await browser.menus.create(separator4Properties);

		let toEmailCardsTitle = messenger.i18n.getMessage("toEmailCardFromAccountsOrCatsLabel");
		let toEmailCardsProperties = { ...properties, id: "toEmailCards", title: toEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(toEmailCardsProperties);

		let ccEmailCardsTitle = messenger.i18n.getMessage("ccEmailCardFromAccountsOrCatsLabel");
		let ccEmailCardsProperties = { ...properties, id: "ccEmailCards", title: ccEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(ccEmailCardsProperties);

		let bccEmailCardsTitle = messenger.i18n.getMessage("bccEmailCardFromAccountsOrCatsLabel");
		let bccEmailCardsProperties = { ...properties, id: "bccEmailCards", title: bccEmailCardsTitle, enabled: wdw_mainWindow.context.email};
		await browser.menus.create(bccEmailCardsProperties);

		let separator5Properties = { ...properties, id: "separator5", type: "separator"};
		await browser.menus.create(separator5Properties);

		let shareCardsByEmailTitle = messenger.i18n.getMessage("shareCardByEmailFromAccountsOrCatsLabel");
		let shareCardsByEmailProperties = { ...properties, id: "shareCardsByEmail", title: shareCardsByEmailTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(shareCardsByEmailProperties);

		let separator6Properties = { ...properties, id: "separator6", type: "separator"};
		await browser.menus.create(separator6Properties);

		let cutCardsTitle = messenger.i18n.getMessage("cutCardFromAccountsOrCatsLabel");
		let cutCardsProperties = { ...properties, id: "cutCards", title: cutCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1 && wdw_mainWindow.context.ABRW};
		await browser.menus.create(cutCardsProperties);

		let copyCardsTitle = messenger.i18n.getMessage("copyCardFromAccountsOrCatsLabel");
		let copyCardsProperties = { ...properties, id: "copyCards", title: copyCardsTitle, enabled: wdw_mainWindow.context.contactSelectedCount >= 1};
		await browser.menus.create(copyCardsProperties);

		let pasteCardsTitle = messenger.i18n.getMessage("pasteCardFromAccountsOrCatsLabel");
		let pasteCardsProperties = { ...properties, id: "pasteCards", title: pasteCardsTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(pasteCardsProperties);

		let separator7Properties = { ...properties, id: "separator7", type: "separator"};
		await browser.menus.create(separator7Properties);

		let addCategoryTitle = messenger.i18n.getMessage("createCategoryFromAccountsOrCats");
		let addCategoryProperties = { ...properties, id: "addCategory", title: addCategoryTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(addCategoryProperties);

		let nodeType = cardbookBGPreferences.getNode(dirPrefId);
		let editNodeTitle = (nodeType != "categories") ? messenger.i18n.getMessage("editNodeFromAccountsOrCats") : messenger.i18n.getMessage("editCategoryFromAccountsOrCats");
		let editNodeProperties = { ...properties, id: "editNode", title: editNodeTitle, enabled: wdw_mainWindow.context.ABRW && wdw_mainWindow.context.NodeSelected};
		await browser.menus.create(editNodeProperties);

		let removeNodeTitle = (nodeType != "categories") ? messenger.i18n.getMessage("removeNodeFromAccountsOrCats") : messenger.i18n.getMessage("removeCategoryFromAccountsOrCats");
		let removeNodeProperties = { ...properties, id: "removeNode", title: removeNodeTitle, enabled: wdw_mainWindow.context.ABRW && wdw_mainWindow.context.NodeSelected};
		await browser.menus.create(removeNodeProperties);

		let convertNodeTitle = (nodeType != "categories") ? messenger.i18n.getMessage("convertNodeFromAccountsOrCats") : messenger.i18n.getMessage("convertCategoryFromAccountsOrCats");
		let convertNodeProperties = { ...properties, id: "convertNode", title: convertNodeTitle, enabled: wdw_mainWindow.context.ABRW && wdw_mainWindow.context.NodeSelected};
		await browser.menus.create(convertNodeProperties);

		let separator8Properties = { ...properties, id: "separator8", type: "separator"};
		await browser.menus.create(separator8Properties);

		let toolsFromAccountsOrCatsTitle = messenger.i18n.getMessage("cardbookToolsMenuLabel");
		let toolsFromAccountsOrCatsProperties = { ...properties, id: "toolsFromAccountsOrCats", title: toolsFromAccountsOrCatsTitle};
		await browser.menus.create(toolsFromAccountsOrCatsProperties);

		let formatDataFromAccountsOrCatsTitle = messenger.i18n.getMessage("formatDataFromAccountsOrCatsLabel");
		let formatDataFromAccountsOrCatsProperties = { ...properties, id: "formatData", parentId: "toolsFromAccountsOrCats", title: formatDataFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABAvailable};
		await browser.menus.create(formatDataFromAccountsOrCatsProperties);

		let separator9Properties = { ...properties, id: "separator9", parentId: "toolsFromAccountsOrCats", type: "separator"};
		await browser.menus.create(separator9Properties);

		let findOnlyDuplicatesFromAccountsOrCatsTitle = messenger.i18n.getMessage("cardbookToolsMenuFindSingleDuplicatesLabel");
		let findOnlyDuplicatesFromAccountsOrCatsProperties = { ...properties, id: "findOnlyDuplicates", parentId: "toolsFromAccountsOrCats", title: findOnlyDuplicatesFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABAvailable};
		await browser.menus.create(findOnlyDuplicatesFromAccountsOrCatsProperties);

		let findAllDuplicatesFromAccountsOrCatsTitle = messenger.i18n.getMessage("cardbookToolsMenuFindAllDuplicatesLabel");
		let findAllDuplicatesFromAccountsOrCatsProperties = { ...properties, id: "findAllDuplicates", parentId: "toolsFromAccountsOrCats", title: findAllDuplicatesFromAccountsOrCatsTitle};
		await browser.menus.create(findAllDuplicatesFromAccountsOrCatsProperties);

		let separator10Properties = { ...properties, id: "separator10", parentId: "toolsFromAccountsOrCats", type: "separator"};
		await browser.menus.create(separator10Properties);

		let generateFnFromAccountsOrCatsTitle = messenger.i18n.getMessage("generateFnFromAccountsOrCatsLabel");
		let generateFnFromAccountsOrCatsProperties = { ...properties, id: "generateFn", parentId: "toolsFromAccountsOrCats", title: generateFnFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(generateFnFromAccountsOrCatsProperties);

		let separator11Properties = { ...properties, id: "separator11", parentId: "toolsFromAccountsOrCats", type: "separator"};
		await browser.menus.create(separator11Properties);

		let birthdaysListTitle = messenger.i18n.getMessage("cardbookToolsMenuBirthdayListLabel");
		let birthdaysListProperties = { ...properties, id: "birthdaysList", parentId: "toolsFromAccountsOrCats", title: birthdaysListTitle};
		await browser.menus.create(birthdaysListProperties);

		let birthdaysAddTitle = messenger.i18n.getMessage("cardbookToolsMenuSyncLightningLabel");
		let birthdaysAddProperties = { ...properties, id: "birthdaysAdd", parentId: "toolsFromAccountsOrCats", title: birthdaysAddTitle};
		await browser.menus.create(birthdaysAddProperties);

		let separator12Properties = { ...properties, id: "separator12", parentId: "toolsFromAccountsOrCats", type: "separator"};
		await browser.menus.create(separator12Properties);

		let undoTitle = wdw_mainWindow.context.undo.label;
		let undoProperties = { ...properties, id: "undo", parentId: "toolsFromAccountsOrCats", title: undoTitle, enabled: wdw_mainWindow.context.undo.enabled};
		await browser.menus.create(undoProperties);

		let redoTitle = wdw_mainWindow.context.redo.label;
		let redoProperties = { ...properties, id: "redo", parentId: "toolsFromAccountsOrCats", title: redoTitle, enabled: wdw_mainWindow.context.redo.enabled};
		await browser.menus.create(redoProperties);

		let separator13Properties = { ...properties, id: "separator13", parentId: "toolsFromAccountsOrCats", type: "separator"};
		await browser.menus.create(separator13Properties);

		let cardbookLogTitle = messenger.i18n.getMessage("wdw_logEditionTitle");
		let cardbookLogProperties = { ...properties, id: "cardbookLog", parentId: "toolsFromAccountsOrCats", title: cardbookLogTitle};
		await browser.menus.create(cardbookLogProperties);

		let separator14Properties = { ...properties, id: "separator14", type: "separator"};
		await browser.menus.create(separator14Properties);

		let cardbookPrefsTitle = messenger.i18n.getMessage("cardbookToolsMenuPrefsLabel");
		let cardbookPrefsProperties = { ...properties, id: "cardbookPrefs", parentId: "toolsFromAccountsOrCats", title: cardbookPrefsTitle};
		await browser.menus.create(cardbookPrefsProperties);

		let separator15Properties = { ...properties, id: "separator15", type: "separator"};
		await browser.menus.create(separator15Properties);

		let printFromAccountsOrCatsTitle = messenger.i18n.getMessage("cardbookToolbarPrintButtonLabel");
		let printFromAccountsOrCatsProperties = { ...properties, id: "printFromAccountsOrCats", title: printFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABAvailable};
		await browser.menus.create(printFromAccountsOrCatsProperties);

		let separator16Properties = { ...properties, id: "separator16", type: "separator"};
		await browser.menus.create(separator16Properties);

		let exportFromAccountsOrCatsTitle = messenger.i18n.getMessage("exportsMenu");
		let exportFromAccountsOrCatsProperties = { ...properties, id: "exportFromAccountsOrCats", title: exportFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABAvailable};
		await browser.menus.create(exportFromAccountsOrCatsProperties);

		let exportCardsToCSVFileFromAccountsOrCatsTitle = messenger.i18n.getMessage("exportCardToCSVFileLabel");
		let exportCardsToCSVFileFromAccountsOrCatsProperties = { ...properties, id: "exportCardsToCSVFileFromAccountsOrCats", parentId: "exportFromAccountsOrCats", title: exportCardsToCSVFileFromAccountsOrCatsTitle};
		await browser.menus.create(exportCardsToCSVFileFromAccountsOrCatsProperties);

		let exportCardsToVCFFileFromAccountsOrCatsTitle = messenger.i18n.getMessage("exportCardToVCFFileLabel");
		let exportCardsToVCFFileFromAccountsOrCatsProperties = { ...properties, id: "exportCardsToVCFFileFromAccountsOrCats", parentId: "exportFromAccountsOrCats", title: exportCardsToVCFFileFromAccountsOrCatsTitle};
		await browser.menus.create(exportCardsToVCFFileFromAccountsOrCatsProperties);

		let exportCardsToZipFromAccountsOrCatsTitle = messenger.i18n.getMessage("exportCardToZIPFileLabel");
		let exportCardsToZipFromAccountsOrCatsProperties = { ...properties, id: "exportCardsToZipFromAccountsOrCats", parentId: "exportFromAccountsOrCats", title: exportCardsToZipFromAccountsOrCatsTitle};
		await browser.menus.create(exportCardsToZipFromAccountsOrCatsProperties);

		let exportCardsImagesFromAccountsOrCatsTitle = messenger.i18n.getMessage("exportCardImagesLabel");
		let exportCardsImagesFromAccountsOrCatsProperties = { ...properties, id: "exportCardsImagesFromAccountsOrCats", parentId: "exportFromAccountsOrCats", title: exportCardsImagesFromAccountsOrCatsTitle};
		await browser.menus.create(exportCardsImagesFromAccountsOrCatsProperties);

		let separator17Properties = { ...properties, id: "separator17", type: "separator"};
		await browser.menus.create(separator17Properties);

		let importFromAccountsOrCatsTitle = messenger.i18n.getMessage("importsMenu");
		let importFromAccountsOrCatsProperties = { ...properties, id: "importFromAccountsOrCats", title: importFromAccountsOrCatsTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(importFromAccountsOrCatsProperties);

		let importCardsFromFileTitle = messenger.i18n.getMessage("importCardFromFileLabel");
		let importCardsFromFileProperties = { ...properties, id: "importCardsFromFile", parentId: "importFromAccountsOrCats", title: importCardsFromFileTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(importCardsFromFileProperties);

		let importCardsFromDirTitle = messenger.i18n.getMessage("importCardFromDirLabel");
		let importCardsFromDirProperties = { ...properties, id: "importCardsFromDir", parentId: "importFromAccountsOrCats", title: importCardsFromDirTitle, enabled: wdw_mainWindow.context.ABRW};
		await browser.menus.create(importCardsFromDirProperties);
	}
});

window.addEventListener("keydown", async event => {
	const { key } = event;
	let searchTerms = event.target.closest("#searchTerms");
	let searchBox = event.target.closest("#searchBox");
	switch (key) {
		case "ArrowUp":
		case "Escape":
			if (searchTerms) {
				event.preventDefault();
				event.stopImmediatePropagation();
				await wdw_mainWindow.showHideTerms();
				wdw_mainWindow.searchInput.focus();
			} else if (searchBox) {
				wdw_mainWindow.searchInput.value = "";
				wdw_mainWindow.searchInputExec();
			}
			break;
		case "ArrowDown":
			if (searchBox) {
				event.preventDefault();
				event.stopImmediatePropagation();
				await wdw_mainWindow.showHideTerms();
			}
			break;
		}
});

messenger.menus.onClicked.addListener(async (info, tab) => {
	if (currentTab.id != tab.id) {
		return;
	}
	if (info.menuItemId == "customize") {
		await wdw_mainWindow.openPrefs("cardbook-toolbarPane")
	} else if (info.menuItemId == "saveImageCard") {
		await cardbookHTMLImages.saveImageCard();
	} else if (info.menuItemId == "copyImageCard") {
		cardbookHTMLImages.copyImageCard();
	} else if (info.menuItemId.startsWith("copyField")) {
		let element = info.menuItemId.replace(/^copyField/, "").toLowerCase();
		let row = lastContextElement.closest("tr");
		if (row) {
			let field = element || row.getAttribute(`data-field-name`);
			let index = row.getAttribute(`data-field-index`);
			let label = row.getAttribute(`data-field-label`) || messenger.i18n.getMessage(`${field}Label`);
			await wdw_mainWindow.copyFieldValue(field, index, label);
		} else if (lastContextElement.id == "noteInputLabel") {
			let field = lastContextElement.getAttribute(`data-field-name`);
			let index = "";
			let label = messenger.i18n.getMessage(`${field}Label`);
			await wdw_mainWindow.copyFieldValue(field, index, label);
		}
	} else if (info.menuItemId == "toEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "to", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "ccEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "cc", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "bccEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "bcc", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "findEmails") {
		await messenger.runtime.sendMessage({query: "cardbook.findEmails", card: [], email: [lastContextElement.textContent]});
	} else if (info.menuItemId == "findEvents") {
		await cardbookHTMLUtils.findEvents(null, [lastContextElement.textContent], lastContextElement.textContent);
	} else if (info.menuItemId == "searchForOnlineKey") {
		await messenger.runtime.sendMessage({query: "cardbook.searchForOnlineKey", card: [], email: [lastContextElement.textContent]});
	} else if (info.menuItemId == "openURL") {
		if (lastContextElement.textContent.startsWith("http")) {
			await cardbookHTMLUtils.openURL(lastContextElement.textContent);
		}
	} else if (info.menuItemId == "localizeAdr") {
		let row = lastContextElement.closest("tr");
		let index = row.getAttribute("data-field-index");
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let uid = document.getElementById("uidInputText").value;
		let cbid = `${dirPrefId}::${uid}`;
		let adr = [];
		for (let field of cardbookHTMLUtils.adrElements) {
			let [ clipValue, dummy ] = await cardbookHTMLUtils.getFieldValue(cbid, field, index);
			adr.push(clipValue);
		}
		await cardbookHTMLUtils.localizeCards(null, [adr]);
	} else if (info.menuItemId == "connectIMPP") {
		let row = lastContextElement.closest("tr");
		let index = row.getAttribute("data-field-index");
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let uid = document.getElementById("uidInputText").value;
		let cbid = `${dirPrefId}::${uid}`;
		await messenger.runtime.sendMessage({query: "cardbook.connectIMPP", cbid: cbid, index: index});
	} else if (info.menuItemId == "connectTel") {
		await messenger.runtime.sendMessage({query: "cardbook.connectTel", tel: lastContextElement.textContent});
	} else if (info.menuItemId == "addContact") {
		await wdw_mainWindow.newKey();
	} else if (info.menuItemId == "addList") {
		await wdw_mainWindow.createList();
	} else if (info.menuItemId == "editCard") {
		await wdw_mainWindow.editCard();
	} else if (info.menuItemId == "removeCard") {
		await wdw_mainWindow.deleteCardsAndValidate();
	} else if (info.menuItemId == "addCategory") {
		await wdw_mainWindow.createNode(null, "categories");
	} else if (info.menuItemId.startsWith("assignCategory")) {
		let index = parseInt(info.menuItemId.replace("assignCategory", ""));
		let cat = wdw_mainWindow.context.categories[index].name;
		if (info.wasChecked) {
			await wdw_mainWindow.removeCategoryFromSelectedCards(cat);
		} else {
			await wdw_mainWindow.addCategoryToSelectedCards(cat, false);
		}
	} else if (info.menuItemId == "toEmailCards") {
		await wdw_mainWindow.emailCardsFromAction("to");
	} else if (info.menuItemId == "ccEmailCards") {
		await wdw_mainWindow.emailCardsFromAction("cc");
	} else if (info.menuItemId == "bccEmailCards") {
		await wdw_mainWindow.emailCardsFromAction("bcc");
	} else if (info.menuItemId == "shareCardsByEmail") {
		await wdw_mainWindow.shareCardsByEmail();
	} else if (info.menuItemId == "findEmailsFromCards") {
		await wdw_mainWindow.findEmailsFromCards();
	} else if (info.menuItemId.startsWith("runIMPP")) {
		let index = parseInt(info.menuItemId.replace("runIMPP", ""));
		let type = wdw_mainWindow.context.IMPPs[index].type;
		let value = wdw_mainWindow.context.IMPPs[index].value;
		if (type == "tel") {
			await messenger.runtime.sendMessage({query: "cardbook.connectTel", tel: value});
		} else {
			await messenger.runtime.sendMessage({query: "cardbook.openExternalURL", link: cardbookHTMLUtils.formatIMPPForOpenning(value)})
		}
	} else if (info.menuItemId == "addEvent") {
		await wdw_mainWindow.createEvent();
	} else if (info.menuItemId == "addTodo") {
		await wdw_mainWindow.createTodo();
	} else if (info.menuItemId == "findEventsFromCards") {
		await wdw_mainWindow.findEventsFromCards();
	} else if (info.menuItemId == "localizeCardsFromCards") {
		await wdw_mainWindow.localizeCardsFromCards();
	} else if (info.menuItemId == "openURLFromCards") {
		await wdw_mainWindow.openURLFromCards();
	} else if (info.menuItemId == "searchForOnlineKeyFromCards") {
		await wdw_mainWindow.searchForOnlineKeyFromCards();
	} else if (info.menuItemId == "importKeyFromCards") {
		await wdw_mainWindow.importKeyFromCards(); // test to do
	} else if (info.menuItemId == "cutCards") {
		await wdw_mainWindow.cutCards();
	} else if (info.menuItemId == "copyCards") {
		await wdw_mainWindow.copyCards();
	} else if (info.menuItemId == "pasteCards") {
		await wdw_mainWindow.pasteCards();
	} else if (info.menuItemId == "pasteEntryFromCards") {
		await wdw_mainWindow.pasteFieldValue();
	} else if (info.menuItemId == "duplicateCardsFromCards") {
		await wdw_mainWindow.duplicateCards();
	} else if (info.menuItemId == "mergeCardsFromCards") {
		await wdw_mainWindow.mergeCards();
	} else if (info.menuItemId == "convertListToCategory") {
		await wdw_mainWindow.convertListToCategory();
	} else if (info.menuItemId == "printFromCards") {
		await wdw_mainWindow.printCards();
	} else if (info.menuItemId == "exportCardsToCSVFileFromCards") {
		await wdw_mainWindow.exportCardsToFileFromCards("csv");
	} else if (info.menuItemId == "exportCardsToVCFFileFromCards") {
		await wdw_mainWindow.exportCardsToFileFromCards("vcf");
	} else if (info.menuItemId == "exportCardsToZipFromCards") {
		await wdw_mainWindow.exportCardsToZipFromCards();
	} else if (info.menuItemId == "exportCardsImagesFromCards") {
		await wdw_mainWindow.exportCardsImagesFromCards();
	} else if (info.menuItemId == "addAccount") {
		await wdw_mainWindow.addAddressbook();
	} else if (info.menuItemId == "editAccount") {
		await wdw_mainWindow.editAddressbook();
	} else if (info.menuItemId == "removeAccount") {
		await wdw_mainWindow.removeAddressbook();
	} else if (info.menuItemId == "enableOrDisableAccount") {
		await wdw_mainWindow.enableOrDisableAddressbook();
	} else if (info.menuItemId == "readOnlyOrReadWriteAccount") {
		await wdw_mainWindow.readOnlyOrReadWriteAddressbook();
	} else if (info.menuItemId == "syncAccount") {
		await wdw_mainWindow.syncAccount();
	} else if (info.menuItemId == "editNode") {
		await wdw_mainWindow.selectNodeToAction("EDIT");
	} else if (info.menuItemId == "removeNode") {
		await wdw_mainWindow.selectNodeToAction("REMOVE");
	} else if (info.menuItemId == "convertNode") {
		await wdw_mainWindow.selectNodeToAction("CONVERT");
	} else if (info.menuItemId == "formatData") {
		await wdw_mainWindow.formatData();
	} else if (info.menuItemId == "findOnlyDuplicates") {
		await wdw_mainWindow.findOnlyDuplicates();
	} else if (info.menuItemId == "findAllDuplicates") {
		await wdw_mainWindow.findAllDuplicates();
	} else if (info.menuItemId == "generateFn") {
		await wdw_mainWindow.generateFn();
	} else if (info.menuItemId == "birthdaysList") {
		await wdw_mainWindow.birthdaysList();
	} else if (info.menuItemId == "birthdaysAdd") {
		await wdw_mainWindow.birthdaysAdd();
	} else if (info.menuItemId == "undo") {
		await wdw_mainWindow.undo();
	} else if (info.menuItemId == "redo") {
		await wdw_mainWindow.redo();
	} else if (info.menuItemId == "cardbookLog") {
		await wdw_mainWindow.openLogEdition();
	} else if (info.menuItemId == "cardbookPrefs") {
		await wdw_mainWindow.openPrefs();
	} else if (info.menuItemId == "printFromAccountsOrCats") {
		await wdw_mainWindow.printCards();
	} else if (info.menuItemId == "exportCardsToCSVFileFromAccountsOrCats") {
		await wdw_mainWindow.exportCardsToFileFromAccountsOrCats("csv");
	} else if (info.menuItemId == "exportCardsToVCFFileFromAccountsOrCats") {
		await wdw_mainWindow.exportCardsToFileFromAccountsOrCats("vcf");
	} else if (info.menuItemId == "exportCardsToZipFromAccountsOrCats") {
		await wdw_mainWindow.exportCardsToZipFromAccountsOrCats();
	} else if (info.menuItemId == "exportCardsImagesFromAccountsOrCats") {
		await wdw_mainWindow.exportCardsImagesFromAccountsOrCats();
	} else if (info.menuItemId == "importCardsFromFile") {
		let filepicker = document.getElementById("importCardsToFileInput");
		filepicker.click();
	} else if (info.menuItemId == "importCardsFromDir") {
		await wdw_mainWindow.importCardsFromDir();
	}
});
