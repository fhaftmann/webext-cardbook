import { cardbookHTMLRichContext } from "../utils/scripts/cardbookHTMLRichContext.mjs";
import { cardbookHTMLUtils } from "../utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookHTMLTools } from "../utils/scripts/cardbookHTMLTools.mjs";
import { cardbookHTMLDates } from "../utils/scripts/cardbookHTMLDates.mjs";
import { cardbookHTMLFormulas } from "../utils/scripts/cardbookHTMLFormulas.mjs";
import { cardbookHTMLNotification } from "../utils/scripts/cardbookHTMLNotification.mjs";
import { cardbookHTMLWindowUtils } from "../utils/scripts/cardbookHTMLWindowUtils.mjs"
import { cardbookHTMLValidations } from "../utils/scripts/cardbookHTMLValidations.mjs";
import { cardbookHTMLImages } from "../utils/scripts/cardbookHTMLImages.mjs";
import { cardbookHTMLDownloads } from "../utils/scripts/cardbookHTMLDownloads.mjs";

import { cardbookIDBCard } from "../../BG/indexedDB/cardbookIDBCard.mjs";

import { cardbookBGPreferences } from "../../BG/utils/cardbookBGPreferences.mjs";
import { cardbookCardParser } from "../../BG/utils/cardbookCardParser.mjs";
import { cardbookBGUtils } from "../../BG/utils/cardbookBGUtils.mjs";
import { CBTree } from "../utils/scripts/cardbookHTMLCardsTree.js";

var wdw_cardEdition = {
	contactNotLoaded: true,
	editionFields: [],
	customFields: {},
	cardbookeditlists: {},
	availableCards: {},
	addedCards: {},
	workingCard: {},
	defaultEmailFormat: "X-MOZILLA-HTML",
	editionAction: "CANCEL",
	editionMode: "",
	cardContent: "",
	cbIdIn: "",
	cardIn: {},
	ids: "",
	fn: "",
	firstname: "",
	lastname: "",
	email: "",
	category: "",

	addImageCardFromFile: async function () {
		let filepicker = document.getElementById("loadImageInput");
		filepicker.click();
	},

	loadImageInput: async function () {
		let filepicker = document.getElementById("loadImageInput");
		if (filepicker.files.length == 1) {
			let file = filepicker.files[0];
			let fileReader = new FileReader();
			fileReader.readAsDataURL(file);
			fileReader.onload = function(event) {
				cardbookHTMLImages.resizeImage64Card(wdw_cardEdition.workingCard, event.target.result);
			};
		}
	},	

	dragImageCard: function (aEvent) {
		aEvent.preventDefault();
		let content = aEvent.dataTransfer.getData("text/plain");
		if (content) {
			cardbookHTMLImages.resizeImage64Card(wdw_cardEdition.workingCard, content);
		} else if (aEvent.dataTransfer.files.length) {
			let file = aEvent.dataTransfer.files[0];
			let fileReader = new FileReader();
			fileReader.readAsDataURL(file);
			fileReader.onload = function(event) {
				cardbookHTMLImages.resizeImage64Card(wdw_cardEdition.workingCard, event.target.result);
			};
		}
	},

	pasteImageCard: async function () {
		try {
			let clipboardItems = await navigator.clipboard.read();
			for (const item of clipboardItems) {
				for (let type of [ "image/png", "image/jpg", "image/jpeg", "image/gif", "text/plain" ]) {
					if (item.types.includes(type)) {
						let blob = await item.getType(type);
						let content = await cardbookHTMLUtils.blobToBase64(blob);
						cardbookHTMLImages.resizeImage64Card(wdw_cardEdition.workingCard, content);
						break;
					}
				}
			}
		}
		catch (e) {
			await messenger.runtime.sendMessage({ query: "cardbook.updateStatusProgressInformation", string: `wdw_cardEdition.pasteImageCard error : ${e}`, error: "Error" });
		}
	},

	getEmails: function () {
		let emails = [];
		let cardEmails = cardbookHTMLWindowUtils.getAllTypes("email", true);
		for (let cardRow of cardEmails) {
			emails.push(cardRow[0][0]);
		}
		return emails;
	},

	addNoteDate: function () {
		let element = document.getElementById("noteInputText");
		let currentValue = element.value;
		let date = new Date();
		let dateSplitted = cardbookHTMLDates.splitDateIntoComponents(date);
		let format = cardbookBGPreferences.getPref("noteDateFormat");
		let value = format.replace("YYYY", dateSplitted.year).replace("MM", dateSplitted.month).replace("DD", dateSplitted.day);

		if (typeof element.selectionStart == "number" && typeof element.selectionEnd == "number") {
			let endIndex = element.selectionEnd;
			element.value = currentValue.slice(0, endIndex) + value + currentValue.slice(endIndex);
			element.selectionStart = element.selectionEnd = endIndex + value.length;
		} else if (doc.selection != "undefined" && doc.selection.createRange) {
			element.focus();
			let range = doc.selection.createRange();
			range.collapse(false);
			range.text = value;
			range.select();
		}
		element.focus();
	},
	
	searchForOnlineKey: async function () {
		let emails = wdw_cardEdition.getEmails();
		if (emails.length) {
			let key = await messenger.runtime.sendMessage({query: "cardbook.searchForOnlineKey", card: [], email: emails});
			if (key) {
				wdw_cardEdition.addKeyToEdit(key);
			}
		}
	},

	searchForThKeyEdit: async function () {
		let emails = wdw_cardEdition.getEmails();
		if (emails.length) {
			let key = await messenger.runtime.sendMessage({query: "cardbook.searchForThKeyEdit", card: [], email: emails});
			if (key) {
				wdw_cardEdition.addKeyToEdit(key);
			}
		}
	},

	searchForLocalKeyEdit: async function () {
		let filepicker = document.getElementById("loadKeyInput");
		filepicker.click();
	},

	loadKeyInput: async function () {
		let filepicker = document.getElementById("loadKeyInput");
		if (filepicker.files.length == 1) {
			let file = filepicker.files[0];
			let fileReader = new FileReader();
			fileReader.readAsText(file, "UTF-8");
			fileReader.onload = function(event) {
				if (event.target.result) {
					wdw_cardEdition.addKeyToEdit(event.target.result);
				}
			};
		}
	},

	addKeyToEdit: function (aKey) {
		let type = "key";
		let re = /[\n\u0085\u2028\u2029]|\r\n?/g;
		aKey = aKey.replace(/-----(BEGIN|END) PGP PUBLIC KEY BLOCK-----/g, "").trim().replace(re, "\\r\\n");
		let allKeyArray = cardbookHTMLWindowUtils.getAllKeys(false);
		allKeyArray = allKeyArray.filter(child => (child.value != "" || child.URI != ""));
		allKeyArray.push({ types: [], value: aKey, URI: "", extension: "" });
		cardbookHTMLTools.deleteRows(type + "ReadWriteGroupbox");
		cardbookHTMLWindowUtils.constructDynamicKeysRows(type, allKeyArray, wdw_cardEdition.workingCard.version);
	},
	
	displayLists: async function (aCard, aDirPrefId) {
		if (!aCard.isAList) {
			return
		}
		let dirname = cardbookBGPreferences.getName(aDirPrefId);
		document.getElementById("searchAvailableCardsInputText").placeholder = messenger.i18n.getMessage("searchinAB", [ dirname ]);
		document.getElementById("searchAvailableCardsInputText").value = "";
		document.getElementById("kindInputText").value = "";

		wdw_cardEdition.cardbookeditlists.availableCardsTree = [];
		wdw_cardEdition.cardbookeditlists.addedCardsTree = [];

		let searchId;
		[ wdw_cardEdition.cardbookeditlists.availableCardsTree, searchId ] = await cardbookIDBCard.getCards(aDirPrefId, aDirPrefId, "", "", 0);

		let uid = document.getElementById("uidInputText").value;
		wdw_cardEdition.cardbookeditlists.availableCardsTree = wdw_cardEdition.cardbookeditlists.availableCardsTree.filter(card => card.uid != uid);
		let members = await messenger.runtime.sendMessage({query: "cardbook.getMembersFromCard", card: aCard});
		document.getElementById("kindInputText").value = members.kind;
		for (let email of members.mails) {
			wdw_cardEdition.addEmailToAdded(email.toLowerCase());
		}
		for (let card of members.uids) {
			wdw_cardEdition.removeCardFromAvailable(card);
			wdw_cardEdition.addCardToAdded(card);
		}

		if (cardbookBGPreferences.getReadOnly(wdw_cardEdition.cardIn.dirPrefId)) {
			await cardbookHTMLWindowUtils.constructStaticList(wdw_cardEdition.cardbookeditlists.addedCardsTree);
		} else {
			let availableEvents = { "enterKey": wdw_cardEdition.enterKeyTree,
									"dblclick": wdw_cardEdition.doubleClickTree,
									"dragstart": wdw_cardEdition.startDragTree,
									"dragover": wdw_cardEdition.dragOverTree,
									"drop": wdw_cardEdition.dragCardsTree };
			let availableCSS = { "row": wdw_cardEdition.setCSSRow };
			wdw_cardEdition.availableCards = new CBTree(aDirPrefId, "availableCardsTree", null, availableEvents, null, null, null, availableCSS, false);

			let addedAllColumns =  [ ["cardIcon", messenger.i18n.getMessage("cardIconLabel")],
									["fn", messenger.i18n.getMessage("fnLabel")] ]
			let addedEvents = { "enterKey": wdw_cardEdition.enterKeyTree,
								"dblclick": wdw_cardEdition.doubleClickTree,
								"deleteKey": wdw_cardEdition.deleteKeyTree,
								"dragstart": wdw_cardEdition.startDragTree,
								"dragover": wdw_cardEdition.dragOverTree,
								"drop": wdw_cardEdition.dragCardsTree  };
			let addedCSS = { "row": wdw_cardEdition.setCSSRow };
			wdw_cardEdition.addedCards = new CBTree(aDirPrefId, "addedCardsTree", addedAllColumns, addedEvents, null, null, null, addedCSS, false);

			await wdw_cardEdition.availableCards.displayBook(wdw_cardEdition.cardbookeditlists.availableCardsTree);
			await wdw_cardEdition.addedCards.displayBook(wdw_cardEdition.cardbookeditlists.addedCardsTree);
		}
	},

	setCSSRow: function (aClassList, aCard) {
		aClassList.toggle("Mail", aCard.isEmail === true);
		aClassList.toggle("MailList", aCard.isAList === true);
		aClassList.toggle("Changed", aCard.updated || aCard.created);
	},

	deleteKeyTree: function (aEvent) {
		let treeview = aEvent.target.closest("tree-view");
		if (treeview.id == "addedCardsTree") {
			wdw_cardEdition.modifyLists("deleteListTree");
		}
	},

	enterKeyTree: function (aEvent) {
		let treeview = aEvent.target.closest("tree-view");
		if (treeview.id == "availableCardsTree") {
			wdw_cardEdition.modifyLists("appendCardTree");
		} else if (treeview.id == "addedCardsTree") {
			wdw_cardEdition.modifyLists("deleteListTree");
		}
	},

	doubleClickTree: function (aEvent) {
		let treeview = aEvent.target.closest("tree-view");
		if (treeview.id == "availableCardsTree") {
			wdw_cardEdition.modifyLists("appendCardTree");
		} else if (treeview.id == "addedCardsTree") {
			wdw_cardEdition.modifyLists("deleteListTree");
		}
		aEvent.preventDefault();
	},

	startDragTree: async function (aEvent) {
		try {
			let result = [];
			let treeview = aEvent.target.closest("tree-view");
			if (treeview.id == "availableCardsTree") {
				let indexes = wdw_cardEdition.availableCards.cardsList.selectedIndices;
				for (let index of indexes) {
					let card = wdw_cardEdition.availableCards.cardsList.view._rowMap[index].card;
					result.push(card.cbid);
				}
			} else if (treeview.id == "addedCardsTree") {
				let indexes = wdw_cardEdition.addedCards.cardsList.selectedIndices;
				for (let index of indexes) {
					let card = wdw_cardEdition.addedCards.cardsList.view._rowMap[index].card;
					result.push(card.cbid);
				}
			}
			aEvent.dataTransfer.setData("text/plain", result.join(","));
		}
		catch (e) {
			await messenger.runtime.sendMessage({ query: "cardbook.updateStatusProgressInformation", string: `wdw_cardEdition.startDrag error : ${e}`, error: "Error" });
		}
	},

	dragOverTree: function (aEvent) {
		let treeview = aEvent.target.closest("tree-view");
		if (treeview.id == "availableCardsTree") {
			aEvent.preventDefault();
		} else if (treeview.id == "addedCardsTree") {
			aEvent.preventDefault();
		}
	},

	dragCardsTree: async function (aEvent) {
		try {
			aEvent.preventDefault();
			let treeview = aEvent.target.closest("tree-view");
			let data = aEvent.dataTransfer.getData("text/plain");
			let cbids = data.split(",");
			for (let cbid of cbids) {
				if (treeview.id == "availableCardsTree") {
					let arrayTmp = wdw_cardEdition.addedCards.cardsList.view._rowMap.filter(x => x.card.cbid == cbid);
					let card = arrayTmp[0].card;
					wdw_cardEdition.addCardToAvailable(card);
					wdw_cardEdition.removeCardFromAdded(card);
				} else if (treeview.id == "addedCardsTree") {
					let arrayTmp = wdw_cardEdition.availableCards.cardsList.view._rowMap.filter(x => x.card.cbid == cbid);
					let card = arrayTmp[0].card;
					wdw_cardEdition.removeCardFromAvailable(card);
					wdw_cardEdition.addCardToAdded(card);
				}	
			}
			await wdw_cardEdition.availableCards.displayBook(wdw_cardEdition.cardbookeditlists.availableCardsTree);
			await wdw_cardEdition.addedCards.displayBook(wdw_cardEdition.cardbookeditlists.addedCardsTree);
		}
		catch (e) {
			await messenger.runtime.sendMessage({ query: "cardbook.updateStatusProgressInformation", string: `wdw_cardEdition.dragCards error : ${e}`, error: "Error" });
		}
	},

	addCardToAdded: function (aCard) {
		let exist = wdw_cardEdition.cardbookeditlists.addedCardsTree.filter(card => card.uid == aCard.uid);
		if (exist.length) {
			return
		}
		wdw_cardEdition.cardbookeditlists.addedCardsTree.push(aCard);
	},

	addEmailToAdded: function (aEmail) {
		let exist = wdw_cardEdition.cardbookeditlists.addedCardsTree.filter(card => card.isEmail && card.fn == aEmail);
		if (exist.length) {
			return
		}
		let card = new cardbookCardParser("", "", "", wdw_cardEdition.workingCard.dirPrefId);
		card.fn = aEmail;
		card.isEmail = true;
		wdw_cardEdition.cardbookeditlists.addedCardsTree.push(card);
	},

	addInputToAdded: function () {
		let email = document.getElementById("addEmailInputText").value;
		if (email.includes("@")) {
			wdw_cardEdition.addEmailToAdded(email.toLowerCase());
		};
		document.getElementById("addEmailInputText").value = "";
	},

	removeCardFromAdded: function (aCard) {
		wdw_cardEdition.cardbookeditlists.addedCardsTree = wdw_cardEdition.cardbookeditlists.addedCardsTree.filter(card => card.uid != aCard.uid);
	},

	addCardToAvailable: function (aCard) {
		let exist = wdw_cardEdition.cardbookeditlists.availableCardsTree.filter(card => card.uid == aCard.uid);
		if (exist.length) {
			return
		} else if (aCard.isEmail) {
			return
		} else if (aCard.uid == document.getElementById("uidInputText").value) {
			return
		}
		wdw_cardEdition.cardbookeditlists.availableCardsTree.push(aCard);
	},

	removeCardFromAvailable: function (aCard) {
		wdw_cardEdition.cardbookeditlists.availableCardsTree = wdw_cardEdition.cardbookeditlists.availableCardsTree.filter(card => card.uid != aCard.uid);
	},

	modifyLists: async function (aObjectId, aEmail) {
		switch (aObjectId) {
			case "appendCardTree":
			case "appendCardButton":
			case "appendListMenu": {
				if (document.getElementById("addEmailInputText").value != "") {
					wdw_cardEdition.addInputToAdded();
				} else {
					let indexes = wdw_cardEdition.availableCards.cardsList.selectedIndices;
					for (let index of indexes) {
						let card = wdw_cardEdition.availableCards.cardsList.view._rowMap[index].card;
						wdw_cardEdition.addCardToAdded(card);
						wdw_cardEdition.removeCardFromAvailable(card);
					}
				}
				break;
			}
			case "appendCardEmailMenu":
				if (aEmail.includes("@")) {
					wdw_cardEdition.addEmailToAdded(aEmail.toLowerCase());
				}
				break;
			case "addEmailInputText":
				if (document.getElementById("addEmailInputText").value != "") {
					wdw_cardEdition.addInputToAdded();
				}
				break;
			case "appendCardEmailsMenu":
			case "appendCardEmailsButton": {
				if (document.getElementById("addEmailInputText").value != "") {
					wdw_cardEdition.addInputToAdded();
				} else {
					let indexes = wdw_cardEdition.availableCards.cardsList.selectedIndices;
					for (let index of indexes) {
						let card = wdw_cardEdition.availableCards.cardsList.view._rowMap[index].card;
						for (let emailLine of card.email) {
							wdw_cardEdition.addEmailToAdded(emailLine[0][0].toLowerCase());
						}
					}
				}
				break;
			}
			case "deleteListTree":
			case "deleteListButton":
			case "deleteListMenu": {
				let indexes = wdw_cardEdition.addedCards.cardsList.selectedIndices;
				for (let index of indexes) {
					let card = wdw_cardEdition.addedCards.cardsList.view._rowMap[index].card;
					wdw_cardEdition.addCardToAvailable(card);
					wdw_cardEdition.removeCardFromAdded(card);
				}
				break;
			}
			default:
				break;
		}
		await wdw_cardEdition.availableCards.displayBook(wdw_cardEdition.cardbookeditlists.availableCardsTree);
		await wdw_cardEdition.addedCards.displayBook(wdw_cardEdition.cardbookeditlists.addedCardsTree);
	},

	searchAvailableCards: async function () {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value
		let search = document.getElementById("searchAvailableCardsInputText").value;

		let [ cards, searchId ] = await cardbookIDBCard.getCards(dirPrefId, dirPrefId, search, "", 0);

		wdw_cardEdition.cardbookeditlists.availableCardsTree = [];
		for (let card of cards) {
			let found = false;
			for (let added of wdw_cardEdition.cardbookeditlists.addedCardsTree) {
				if (added.uid == card.uid) {
					found = true;
					break;
				}
			}
			if (!found && card.uid != document.getElementById("uidInputText").value) {
				wdw_cardEdition.cardbookeditlists.availableCardsTree.push(card);
			}
		}
		await wdw_cardEdition.availableCards.displayBook(wdw_cardEdition.cardbookeditlists.availableCardsTree);
	},

	loadEditionMode: function () {
		let titleString = "wdw_cardEdition" + wdw_cardEdition.editionMode + "Title";
		document.title = messenger.i18n.getMessage(titleString, [wdw_cardEdition.workingCard.fn]);
		document.getElementById("loadEditionMode").value = wdw_cardEdition.editionMode;
		if (wdw_cardEdition.editionMode == "ViewResult") {
			document.getElementById("addressbookHeader").textContent = messenger.i18n.getMessage("addToAddressbook");
			document.getElementById("addressbookMenulist").classList.remove("hidden");
			document.getElementById("addressbookInputLabel").classList.add("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.remove("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.remove("hidden");
			document.getElementById("saveEditionButton").classList.add("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "ViewResultHideCreate") {
			document.getElementById("addressbookRow").classList.add("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.remove("hidden");
			document.getElementById("saveEditionButton").classList.add("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
			document.getElementById("cardbookSwitchButtonDown").classList.add("hidden");
			document.getElementById("cardbookSwitchButtonUp").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "ViewContact" || wdw_cardEdition.editionMode == "ViewList") {
			document.getElementById("addressbookMenulist").classList.add("hidden");
			document.getElementById("addressbookInputLabel").classList.remove("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("fnInputText").setAttribute("class", "indent");
			document.getElementById("listReadOnlyGroupbox").classList.remove("hidden");
			document.getElementById("listReadWriteGroupbox").classList.add("hidden");
			document.getElementById("keyToolsGroupbox").classList.add("hidden");
			document.getElementById("defaultCardImage").removeAttribute("editionMode");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.add("hidden");
			document.getElementById("saveEditionButton").classList.add("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
			document.getElementById("cardbookSwitchButtonDown").classList.add("hidden");
			document.getElementById("cardbookSwitchButtonUp").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "EditContact" || wdw_cardEdition.editionMode == "EditList") {
			document.getElementById("addressbookMenulist").classList.remove("hidden");
			document.getElementById("addressbookInputLabel").classList.add("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.remove("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "CreateContact" || wdw_cardEdition.editionMode == "CreateList") {
			document.getElementById("addressbookHeader").textContent = messenger.i18n.getMessage("addToAddressbook");
			document.getElementById("addressbookMenulist").classList.remove("hidden");
			document.getElementById("addressbookInputLabel").classList.add("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.remove("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "AddEmail") {
			document.getElementById("addressbookHeader").textContent = messenger.i18n.getMessage("addToAddressbook");
			document.getElementById("addressbookMenulist").classList.remove("hidden");
			document.getElementById("addressbookInputLabel").classList.add("hidden");
			document.getElementById("contactsRow").classList.remove("hidden");
			document.getElementById("contactMenulist").classList.remove("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.add("hidden");
			document.getElementById("helpTab").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.remove("hidden");
			document.getElementById("saveTemplateButton").classList.add("hidden");
		} else if (wdw_cardEdition.editionMode == "EditTemplate") {
			document.getElementById("addressbookRow").classList.add("hidden");
			document.getElementById("contactsRow").classList.add("hidden");
			document.getElementById("contactMenulist").classList.add("hidden");
			document.getElementById("listReadOnlyGroupbox").classList.add("hidden");
			document.getElementById("listReadWriteGroupbox").classList.remove("hidden");
			document.getElementById("keyToolsGroupbox").classList.remove("hidden");
			document.getElementById("createEditionButton").classList.add("hidden");
			document.getElementById("createAndReplaceEditionButton").classList.add("hidden");
			document.getElementById("saveEditionButton").classList.add("hidden");
			document.getElementById("helpTab").classList.remove("hidden");
			document.getElementById("applyTemplateButton").classList.add("hidden");
			document.getElementById("autoComputeFnButton").classList.remove("hidden");
			document.getElementById("saveTemplateButton").classList.remove("hidden");
		}
	},

	setFieldsAsDefault: async function () {
		let result = {};
		for (let field of wdw_cardEdition.editionFields) {
			if (field[0] == "allFields") {
				result = "allFields";
				break;
			} else {
				result[field[2]] = { displayed: field[0], function: field[4] };
			}
		}
		await cardbookBGPreferences.setPref("fieldsNameList", JSON.stringify(result));
	},

	checkEditionFields: function (aField) {
		if (cardbookHTMLUtils.newFields.includes(aField) 
			&& document.getElementById("versionInputText").value == "3.0") {
			return false;
		}
		if (wdw_cardEdition.editionFields[0][0] == "allFields") {
			return true;
		}
		for (let field of wdw_cardEdition.editionFields) {
			if (field[2] == aField) {
				return field[0];
			}
		}
		return false;
	},

	loadFieldSelector: function () {
		let name = "fieldsMenulist";
		let fieldsSelector = document.getElementById(name);

		let fieldsSelectorLabel = messenger.i18n.getMessage("editionGroupboxLabel");
		fieldsSelector.setButtonLabel(fieldsSelectorLabel);
		fieldsSelector.setClass(["up", "arrow-up"]);

		let options = [];
		for (let [checked, label, value] of wdw_cardEdition.editionFields) {
			value = `CBField-${value}`; // to avoid core css definition #categories
			options.push([label, value, checked]);
        }
		let restId = "CBField-SetAsDefault";
		let restLabel = messenger.i18n.getMessage("fieldsButtonLabel");

		fieldsSelector.loadOptions("CBField", options, wdw_cardEdition.changeEditionFields, [ { restId, restLabel, restFunction: wdw_cardEdition.setFieldsAsDefault } ], null);
	},

	changeEditionFields: function (event) {
		let value = event.target.id.replace(/^CBField-/, "");
		for (let i = 0; i < wdw_cardEdition.editionFields.length; i++) {
			if (value == wdw_cardEdition.editionFields[i][2]) {
				wdw_cardEdition.editionFields[i][0] = !wdw_cardEdition.editionFields[i][0];
				break;
			}
		}
		wdw_cardEdition.loadEditionFields();
	},

	loadEditionFields: function () {
		switch (wdw_cardEdition.editionMode) {
			case "ViewResultHideCreate":
			case "ViewContact":
			case "ViewList":
				cardbookHTMLWindowUtils.showPane("generalTabPanel");
				return;
		}

		if (wdw_cardEdition.checkEditionFields("addressbook") && wdw_cardEdition.editionMode != "EditTemplate") {
			document.getElementById("addressbookRow").classList.remove("hidden");
		} else {
			document.getElementById("addressbookRow").classList.add("hidden");
		}
		if (wdw_cardEdition.checkEditionFields("categories") || wdw_cardEdition.workingCard.categories.length != 0) {
			document.getElementById("categoriesRow").classList.remove("hidden");
		} else {
			document.getElementById("categoriesRow").classList.add("hidden");
		}
		if (wdw_cardEdition.checkEditionFields("note") || wdw_cardEdition.workingCard.note) {
			document.getElementById("noteTab").classList.remove("hidden");
		} else {
			document.getElementById("noteTab").classList.add("hidden");
		}
		if (wdw_cardEdition.checkEditionFields("list") && wdw_cardEdition.editionMode != "EditTemplate") {
			document.getElementById("listTab").classList.remove("hidden");
		} else {
			document.getElementById("listTab").classList.add("hidden");
		}
		if (wdw_cardEdition.checkEditionFields("key") && wdw_cardEdition.editionMode != "EditTemplate") {
			document.getElementById("keyTab").classList.remove("hidden");
		} else {
			document.getElementById("keyTab").classList.add("hidden");
		}
		if (wdw_cardEdition.checkEditionFields("fn") || wdw_cardEdition.workingCard.fn) {
			document.getElementById("fnRow").classList.remove("hidden");
		} else {
			document.getElementById("fnRow").classList.add("hidden");
		}

		for (let field of cardbookHTMLUtils.allColumns.personal) {
			if (wdw_cardEdition.checkEditionFields(field) || wdw_cardEdition.workingCard[field]) {
				document.getElementById(`${field}Row`).classList.remove("hidden");
			} else {
				document.getElementById(`${field}Row`).classList.add("hidden");
			}
		}
		if (document.getElementById("firstnameRow").hasAttribute("hidden") ||
			document.getElementById("firstnameInputText").hasAttribute("hidden") ||
			document.getElementById("lastnameRow").hasAttribute("hidden") ||
			document.getElementById("lastnameInputText").hasAttribute("hidden")) {
			document.getElementById("cardbookSwitchButtonUp").classList.add("hidden");
			document.getElementById("cardbookSwitchButtonDown").classList.add("hidden");
		} else {
			document.getElementById("cardbookSwitchButtonUp").classList.remove("hidden");
			document.getElementById("cardbookSwitchButtonDown").classList.remove("hidden");
		}
		if (!wdw_cardEdition.workingCard.isAList) {
			for (let field of cardbookHTMLUtils.multilineFields) {
				if (wdw_cardEdition.checkEditionFields(field) || document.getElementById(`${field}_0_valueBox`).value) {
					document.getElementById(`${field}Groupbox`).classList.remove("hidden");
				} else {
					document.getElementById(`${field}Groupbox`).classList.add("hidden");
				}
			}
			for (let field of ["event"]) {
				if (wdw_cardEdition.checkEditionFields(field) || document.getElementById(`${field}_0_valueBox`).value || document.getElementById(`${field}_0_InputDate`).getValue()) {
					document.getElementById(`${field}Groupbox`).classList.remove("hidden");
				} else {
					document.getElementById(`${field}Groupbox`).classList.add("hidden");
				}
			}
			for (let field of ["tz"]) {
				let ABType = cardbookBGPreferences.getType(wdw_cardEdition.workingCard.dirPrefId);
				if (ABType.startsWith("GOOGLE") || ABType == "APPLE" || ABType == "OFFICE365" || ABType == "YAHOO") {
					document.getElementById(`${field}Groupbox`).classList.add("hidden");
				} else if (wdw_cardEdition.checkEditionFields(field) || document.getElementById(`${field}_0_menulistTz`).value) {
					document.getElementById(`${field}Groupbox`).classList.remove("hidden");
				} else {
					document.getElementById(`${field}Groupbox`).classList.add("hidden");
				}
			}
		}
		for (let type of ["personal", "org"]) {
			for (let i = 0; i < wdw_cardEdition.customFields[type].length; i++) {
				let customFieldName = `customField${i}${type}InputText`;
				let customFieldRowName = `customField${i}${type}Row`;
				if (wdw_cardEdition.checkEditionFields(wdw_cardEdition.customFields[type][i][0]) || document.getElementById(customFieldName).value) {
					document.getElementById(customFieldRowName).classList.remove("hidden");
				} else {
					document.getElementById(customFieldRowName).classList.add("hidden");
				}
			}
		}
		let orgStructure = cardbookBGPreferences.getPref("orgStructure");
		if (orgStructure.length) {
			for (let i = 0; i < orgStructure.length; i++) {
				if (wdw_cardEdition.checkEditionFields(`org_${orgStructure[i]}`) || document.getElementById(`orgInputText_${i}`).value) {
					document.getElementById(`orgRow_${i}`).classList.remove("hidden");
				} else {
					document.getElementById(`orgRow_${i}`).classList.add("hidden");
				}
			}
		} else {
			if (wdw_cardEdition.checkEditionFields("org") || document.getElementById("orgInputText_0").value) {
				document.getElementById("orgRow_0").classList.remove("hidden");
			} else {
				document.getElementById("orgRow_0").classList.add("hidden");
			}
		}
		for (let field of ["title", "role"]) {
			if (wdw_cardEdition.checkEditionFields(field) || wdw_cardEdition.workingCard[field]) {
				document.getElementById(field + "Row").classList.remove("hidden");
			} else {
				document.getElementById(field + "Row").classList.add("hidden");
			}
		}
		cardbookHTMLWindowUtils.showPane("generalTabPanel");
	},

	setConvertButtons: function () {
		let forceHide = false;
		switch (wdw_cardEdition.editionMode) {
			case "ViewResult":
			case "ViewResultHideCreate":
			case "ViewContact":
			case "ViewList":
				forceHide = true;
		}
		let itemsList = document.querySelectorAll("img.cardbookProcessClass");
		for (let item of itemsList) {
			if (forceHide) {
				item.classList.add("hidden");
			} else {
				let code = item.id.replace("ProcessButton", "");
				for (let field of wdw_cardEdition.editionFields) {
					if (field[0] == "allFields") {
						item.classList.add("hidden");
						break;
					} else if (code == field[2]) {
						if (field[3] != "") {
							item.classList.remove("hidden");
						} else {
							item.classList.add("hidden");
						}
						break;
					}
				}
			}
		}
	},

	onInputField: function (aEvent) {
		let textbox = aEvent.target;
		let row = aEvent.target.closest("tr");
		let field = row.getAttribute("data-field-name");
		if (document.getElementById(`${field}ProcessButton`)) {
			let button = document.getElementById(`${field}ProcessButton`);
			if (!button.classList.contains("hidden") && button.hasAttribute("autoConvertField")) {
				let tmpArray = wdw_cardEdition.editionFields.filter(x => x[2] == field);
				let convertionFunction = tmpArray[0][4];
				let value = cardbookHTMLUtils.convertField(convertionFunction, textbox.value);
				textbox.value = value;
			}
			let orgStructure = cardbookBGPreferences.getPref("orgStructure");
			if (["lastname", "firstname", "othername", "prefixname", "suffixname", "nickname"].includes(field) ||
				(orgStructure.length == 0 && cardbookHTMLUtils.allColumns.org.includes(field)) ||
				(orgStructure.length != 0 && orgStructure.includes(field.replace(/^org\./, "")))) {
				wdw_cardEdition.setDisplayName();
			}
		}
	},

	loadHelpTab: function () {
		let orgStructure = cardbookBGPreferences.getPref("orgStructure");
		document.getElementById("formulaMemberLabel1").textContent = "{{1}} : " + messenger.i18n.getMessage("prefixnameLabel");
		document.getElementById("formulaMemberLabel2").textContent = "{{2}} : " + messenger.i18n.getMessage("firstnameLabel");
		document.getElementById("formulaMemberLabel3").textContent = "{{3}} : " + messenger.i18n.getMessage("othernameLabel");
		document.getElementById("formulaMemberLabel4").textContent = "{{4}} : " + messenger.i18n.getMessage("lastnameLabel");
		document.getElementById("formulaMemberLabel5").textContent = "{{5}} : " + messenger.i18n.getMessage("suffixnameLabel");
		document.getElementById("formulaMemberLabel6").textContent = "{{6}} : " + messenger.i18n.getMessage("nicknameLabel");

		let count = 6;
		let table = document.getElementById("formulaSampleTable");
		if (orgStructure.length == 0) {
			count++;
			let row = cardbookHTMLTools.addHTMLTR(table, `formulaSampleTextRow${count}`);
			let labelData = cardbookHTMLTools.addHTMLTD(row, `formulaMemberLabel${count}.1`);
			let label = cardbookHTMLTools.addHTMLLABEL(labelData, `formulaMemberLabel${count}`, "{{" + count + "}} : " + messenger.i18n.getMessage("orgLabel"), {});
		} else {
			for (let org of orgStructure) {
				count++;
				let row = cardbookHTMLTools.addHTMLTR(table, `formulaSampleTextRow${count}`);
				let labelData = cardbookHTMLTools.addHTMLTD(row, `formulaMemberLabel${count}.1`);
				let label = cardbookHTMLTools.addHTMLLABEL(labelData, `formulaMemberLabel${count}`, "{{" + count + "}} : " + org, {});
			}
		}
		count++;
		let rowTitle = cardbookHTMLTools.addHTMLTR(table, `formulaSampleTextRow${count}`);
		let titleData = cardbookHTMLTools.addHTMLTD(rowTitle, `formulaMemberLabel${count}.2`);
		let labelTitle = cardbookHTMLTools.addHTMLLABEL(titleData, `formulaMemberLabel${count}`, "{{" + count + "}} : " + messenger.i18n.getMessage("titleLabel"), {});
		count++;
		let rowRole = cardbookHTMLTools.addHTMLTR(table, `formulaSampleTextRow${count}`);
		let roleData = cardbookHTMLTools.addHTMLTD(rowRole, `formulaMemberLabel${count}.3`);
		let labelRole = cardbookHTMLTools.addHTMLLABEL(roleData, `formulaMemberLabel${count}`, "{{" + count + "}} : " + messenger.i18n.getMessage("roleLabel"), {});
	},

	setEditionFields: function () {
		wdw_cardEdition.editionFields = cardbookHTMLUtils.getEditionFields();
	},

	loadDefaultVersion: async function () {
		if (wdw_cardEdition.workingCard.version == "") {
			let dirPrefId = document.getElementById("addressbookMenulist").value;
			document.getElementById("versionInputText").value = cardbookBGPreferences.getVCardVersion(dirPrefId);
			wdw_cardEdition.workingCard.version = document.getElementById("versionInputText").value;
		} else {
			document.getElementById("versionInputText").value = wdw_cardEdition.workingCard.version;
		}
	},

	removeContacts: function () {
		document.getElementById("contactMenulist").selectedIndex = 0;
		cardbookHTMLTools.deleteRows("contactMenupopup");
		wdw_cardEdition.contactNotLoaded = true;
	},

	loadContacts: async function () {
		if (wdw_cardEdition.contactNotLoaded) {
			let contactMenulist = document.getElementById("contactMenulist");
			let dirPrefId = document.getElementById("addressbookMenulist").value;
			cardbookHTMLTools.loadContacts(contactMenulist, dirPrefId, "", true);
			wdw_cardEdition.contactNotLoaded = false;
		}
	},

	changeAddressbook: async function () {
		wdw_cardEdition.removeContacts();
		document.getElementById("dirPrefIdInputText").value = document.getElementById("addressbookMenulist").value;
		if (wdw_cardEdition.editionMode == "AddEmail") {
			wdw_cardEdition.workingCard = null;
			wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.cardIn, wdw_cardEdition.workingCard);
		} else {
			// keep the current changes
			let outCard = new cardbookCardParser();
			outCard = await wdw_cardEdition.calculateResult(outCard);
			// convertion if AB changed
			let myTargetName = cardbookBGPreferences.getName(outCard.dirPrefId);
			let myTargetVersion = cardbookBGPreferences.getVCardVersion(outCard.dirPrefId);
			let mySourceDateFormat = cardbookHTMLUtils.getDateFormat(wdw_cardEdition.workingCard.dirPrefId, cardbookBGPreferences.getVCardVersion(wdw_cardEdition.workingCard.dirPrefId));
			let myTargetDateFormat = cardbookHTMLUtils.getDateFormat(outCard.dirPrefId, myTargetVersion);
			let possibleCustomFields = await messenger.runtime.sendMessage({query: "cardbook.getPossibleCustomFields"});
			let result = await cardbookHTMLUtils.convertVCard(outCard, myTargetName, myTargetVersion, mySourceDateFormat, myTargetDateFormat, possibleCustomFields);
			await messenger.runtime.sendMessage({query: "cardbook.writePossibleCustomFields", possibleCustomFields: result.possibleCustomFields});

			wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(outCard, wdw_cardEdition.workingCard);
			outCard = null;
		}
		wdw_cardEdition.workingCard.dirPrefId = document.getElementById("addressbookMenulist").value;
		wdw_cardEdition.workingCard.cbid = `${wdw_cardEdition.workingCard.dirPrefId}::${wdw_cardEdition.workingCard.uid}`;

		await wdw_cardEdition.loadContacts();
		await wdw_cardEdition.displayCard(wdw_cardEdition.workingCard);
	},

	changeContact: async function () {
		let dirPrefId = document.getElementById("addressbookMenulist").value;
		let myUid = document.getElementById("contactMenulist").value;
		if (myUid) {
			wdw_cardEdition.workingCard = null;
			let results = await messenger.runtime.sendMessage({ query: "cardbook.getCards", cbids: [dirPrefId + "::" + myUid] });
			let card = results[0];
			wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(card, wdw_cardEdition.workingCard);
			if (wdw_cardEdition.editionMode == "AddEmail") {
				if (wdw_cardEdition.workingCard.isAList) {
					cardbookHTMLUtils.addMemberstoCard(wdw_cardEdition.workingCard, [`mailto:${wdw_cardEdition.email}`]);
				} else {
					wdw_cardEdition.workingCard.email.push([[wdw_cardEdition.email], [], "", []]);
				}
			}
		} else {
			wdw_cardEdition.workingCard = null;
			wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.cardIn, wdw_cardEdition.workingCard);
		}
		wdw_cardEdition.loadEditionMode();
		await wdw_cardEdition.displayCard(wdw_cardEdition.workingCard);
		await wdw_cardEdition.displayLists(wdw_cardEdition.workingCard, dirPrefId);
	},

	switchLastnameAndFirstname: function () {
		var tmpValue = document.getElementById("lastnameInputText").value;
		document.getElementById("lastnameInputText").value = document.getElementById("firstnameInputText").value;
		document.getElementById("firstnameInputText").value = tmpValue;
		document.getElementById("lastnameInputText").focus();
		document.getElementById("lastnameInputText").dispatchEvent(new Event("input"));
		document.getElementById("firstnameInputText").dispatchEvent(new Event("input"));
	},

	autoComputeFn: async function (aButton, aForce) {
		if ("undefined" == typeof (aForce)) {
			if (!aButton.hasAttribute("autoComputeFn")) {
				aButton.setAttribute("autoComputeFn", "true");
				aButton.setAttribute("title", messenger.i18n.getMessage("dontAutoComputeFn"));
			} else {
				aButton.removeAttribute("autoComputeFn");
				aButton.setAttribute("title", messenger.i18n.getMessage("autoComputeFn"));
			}
			let autoComputeFn = cardbookBGPreferences.getPref("autoComputeFn");
			await cardbookBGPreferences.setPref("autoComputeFn", !autoComputeFn);
		} else {
			if (aForce == true) {
				aButton.setAttribute("autoComputeFn", "true");
				aButton.setAttribute("title", messenger.i18n.getMessage("dontAutoComputeFn"));
			} else {
				aButton.removeAttribute("autoComputeFn");
				aButton.setAttribute("title", messenger.i18n.getMessage("autoComputeFn"));
			}
		}
	},

	expandButton: function (aButton, aForce) {
		let table = document.getElementById(aButton.id.replace(/^expand/, "").replace(/Image$/, "").toLowerCase() + "Table");
		if ("undefined" == typeof (aForce)) {
			if (aButton.getAttribute("state") == "right") {
				table.classList.remove("hidden");
				aButton.setAttribute("state", "down");
			} else {
				table.classList.add("hidden");
				aButton.setAttribute("state", "right");
			}
		} else {
			if (aForce == true) {
				table.classList.remove("hidden");
				aButton.setAttribute("state", "down");
			} else {
				table.classList.add("hidden");
				aButton.setAttribute("state", "right");
			}
		}
	},

	displayCard: async function (aCard) {
		cardbookHTMLWindowUtils.clearCard();

		if (aCard.isAList) {
			document.getElementById("typesGroupbox").classList.add("hidden");
			wdw_cardEdition.expandButton(document.getElementById("expandPersonalImage"), false);
			wdw_cardEdition.expandButton(document.getElementById("expandOrgImage"), false);
			document.getElementById("preferDisplayNameDiv").classList.add("hidden");
		} else {
			document.getElementById("typesGroupbox").classList.remove("hidden");
			wdw_cardEdition.expandButton(document.getElementById("expandPersonalImage"), true);
			wdw_cardEdition.expandButton(document.getElementById("expandOrgImage"), true);
			document.getElementById("preferDisplayNameDiv").classList.remove("hidden");
		}
		document.getElementById("lastnameInputText").focus();
		let autoComputeFn = cardbookBGPreferences.getPref("autoComputeFn");
		wdw_cardEdition.autoComputeFn(document.getElementById("autoComputeFnButton"), autoComputeFn);

		let cardDefaultRegion = cardbookHTMLUtils.getCardRegion(aCard);
		document.getElementById("cardDefaultRegion").value = cardDefaultRegion;
		wdw_cardEdition.loadCssRules();

		let ABList = document.getElementById("addressbookMenulist");
		let readonly = wdw_cardEdition.editionMode == "ViewContact" || wdw_cardEdition.editionMode == "ViewList";
		document.getElementById("addressbookInputLabel").textContent = cardbookBGPreferences.getName(aCard.dirPrefId);
		await cardbookHTMLTools.loadAddressBooks(ABList, aCard.dirPrefId, true, false, readonly, false, false);

		cardbookHTMLTools.loadGender("genderMenulist", aCard.gender);
		await wdw_cardEdition.loadContacts();

		// the dirPrefId may be different from the one loaded in case of a complex search
		aCard.dirPrefId = document.getElementById("addressbookMenulist").value;

		let aReadOnly = cardbookBGPreferences.getReadOnly(aCard.dirPrefId);
		await cardbookHTMLImages.displayImageCard(aCard, !aReadOnly);
		await cardbookHTMLWindowUtils.displayCard(aCard, aReadOnly, wdw_cardEdition.onInputField, wdw_cardEdition.setDisplayName);

		document.getElementById("preferDisplayNameCheckBox").checked = true;
		aCard.version = document.getElementById("versionInputText").value;

		for (let email of aCard.emails) {
			if (await messenger.runtime.sendMessage({ query: "cardbook.cardbookPreferDisplayNameIndex", email: email.toLowerCase() })) {
				document.getElementById("preferDisplayNameCheckBox").checked = false;
				break;
			}
		}

		wdw_cardEdition.loadEditionFields();
		wdw_cardEdition.loadFieldSelector();
		wdw_cardEdition.setConvertButtons();
	},

	getOrg: function () {
		let org = [];
		let i = 0;
		while (true) {
			if (document.getElementById(`orgRow_${i}`)) {
				org.push(document.getElementById(`orgInputText_${i}`).value.trim());
				i++;
			} else {
				break;
			}
		}
		return cardbookHTMLUtils.rtrimArray(org);
	},

	setDisplayName: function () {
		if (document.getElementById("autoComputeFnButton").hasAttribute("autoComputeFn")) {
			var myNewOrg = wdw_cardEdition.getOrg();
			var myNewFn = cardbookHTMLFormulas.getDisplayedNameFromFormula(document.getElementById("dirPrefIdInputText").value, [document.getElementById("prefixnameInputText").value.trim(),
			document.getElementById("firstnameInputText").value.trim(),
			document.getElementById("othernameInputText").value.trim(),
			document.getElementById("lastnameInputText").value.trim(),
			document.getElementById("suffixnameInputText").value.trim(),
			document.getElementById("nicknameInputText").value.trim()],
				[myNewOrg,
					document.getElementById("titleInputText").value.trim(),
					document.getElementById("roleInputText").value.trim()]);
			document.getElementById("fnInputText").value = myNewFn;
			wdw_cardEdition.workingCard.lastname = document.getElementById("lastnameInputText").value.trim();
			wdw_cardEdition.workingCard.firstname = document.getElementById("firstnameInputText").value.trim();
			wdw_cardEdition.workingCard.othername = document.getElementById("othernameInputText").value.trim();
			wdw_cardEdition.workingCard.suffixname = document.getElementById("suffixnameInputText").value.trim();
			wdw_cardEdition.workingCard.prefixname = document.getElementById("prefixnameInputText").value.trim();
			wdw_cardEdition.workingCard.nickname = document.getElementById("nicknameInputText").value.trim();
			wdw_cardEdition.workingCard.org = myNewOrg;
			wdw_cardEdition.workingCard.fn = myNewFn;
		}
	},

	cloneCard: async function (aSourceCard, aTargetCard) {
		// we need to keep the list flag as the normal cloneCard function may not find this information
		// for new cards
		aTargetCard = new cardbookCardParser();
		await cardbookBGUtils.cloneCard(aSourceCard, aTargetCard);
		aTargetCard.isAList = aSourceCard.isAList;
		return aTargetCard;
	},

	createCssCategoryRules: function (aStyleSheet, aName, aColor, aUseColor) {
		let oppositeColor = cardbookHTMLUtils.getTextColorFromBackgroundColor(aColor);
		if (aUseColor == "background" || aUseColor == "text") {
			let ruleString1 = `.${aName} {color: ${aColor};}`;
			let ruleIndex1 = aStyleSheet.insertRule(ruleString1, aStyleSheet.cssRules.length);
		}
	},

	loadCssRules: function () {
		let myStyleSheetRuleName = "cardbookCategoriesEdition.css";
		for (let styleSheet of document.styleSheets) {
			if (styleSheet.href.endsWith(myStyleSheetRuleName)) {
				cardbookHTMLUtils.deleteCssAllRules(styleSheet);
				cardbookHTMLUtils.createMarkerRule(styleSheet, myStyleSheetRuleName);
				let useColor = cardbookBGPreferences.getPref("useColor");
				let nodeColors = cardbookBGPreferences.getNodeColors();
				for (let category in nodeColors) {
					let color = nodeColors[category];
					let cleanName = cardbookHTMLUtils.formatCategoryForCss(category);
					wdw_cardEdition.createCssCategoryRules(styleSheet, `${cleanName}_color`, color, useColor);
				}
				return;
			}
		}
	},

	copyFieldValue: async function (aFieldName, aFieldIndex, aFieldLabel) {
		let cbid = document.getElementById("dirPrefIdInputText").value + "::" + document.getElementById("uidInputText").value;
		let [ clipValue, dummy ] = await cardbookHTMLUtils.getFieldValue(cbid, aFieldName, aFieldIndex);
		await navigator.clipboard.writeText(clipValue);
		await messenger.runtime.sendMessage({query: "cardbook.formatStringForOutput", string: "lineCopied"});
	},

	createTemplate: async function () {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value
		let card = new cardbookCardParser("", "", "", dirPrefId);
		await cardbookHTMLUtils.openEditionWindow(card, "EditTemplate");
	},

	loadTemplateButton: async function () {
		let filepicker = document.getElementById("loadTemplateInput");
		filepicker.click();
	},
	
	loadTemplateInput: async function () {
		let filepicker = document.getElementById("loadTemplateInput");
		if (filepicker.files.length == 1) {
			let file = filepicker.files[0];
			let fileReader = new FileReader();
			fileReader.readAsText(file, "UTF-8");
			fileReader.onload = async function(event) {
				if (event.target.result) {
					let dirPrefId = document.getElementById("dirPrefIdInputText").value
					let card = new cardbookCardParser(event.target.result, "", "", dirPrefId);
					await cardbookHTMLUtils.openEditionWindow(card, "EditTemplate", event.target.result);
				}
			};
		}
	},

	applyTemplateButton: async function () {
		let filepicker = document.getElementById("applyTemplateInput");
		filepicker.click();
	},
	
	applyTemplateInput: async function () {
		let filepicker = document.getElementById("applyTemplateInput");
		if (filepicker.files.length == 1) {
			let file = filepicker.files[0];
			let fileReader = new FileReader();
			fileReader.readAsText(file, "UTF-8");
			fileReader.onload = async function(event) {
				if (event.target.result) {
					wdw_cardEdition.applyTemplateNext(event.target.result);
				}
			};
		}
	},

	applyTemplateNext: async function (aContent) {
		if (aContent) {
			let re = /[\n\u0085\u2028\u2029]|\r\n?/;
			let fileContentArray = cardbookHTMLUtils.cleanArrayWithoutTrim(aContent.split(re));
			let fileContentArrayLength = fileContentArray.length
			let cardContent = "";
			for (let i = 0; i < fileContentArrayLength; i++) {
				if (fileContentArray[i].toUpperCase().startsWith("BEGIN:VCARD")) {
					cardContent = fileContentArray[i];
				} else if (fileContentArray[i].toUpperCase().startsWith("END:VCARD")) {
					cardContent = cardContent + "\r\n" + fileContentArray[i];
					let myTempCard = new cardbookCardParser();
					myTempCard = await wdw_cardEdition.calculateResult(myTempCard);

					let myTemplateCard = new cardbookCardParser(cardContent, "", "", myTempCard.dirPrefId);
					if (myTemplateCard.isAList != myTempCard.isAList || myTempCard.isAList) {
						return;
					}

					let listFields = ["prefixname", "firstname", "othername", "lastname", "suffixname", "nickname", "org", "title", "role"]
					for (let field of listFields) {
						if (myTempCard[field] == "" && myTemplateCard[field] != "") {
							myTempCard[field] = myTemplateCard[field];
						}
					}
					myTempCard.categories = myTempCard.categories.concat(myTemplateCard.categories);
					myTempCard.categories = cardbookHTMLUtils.cleanCategories(myTempCard.categories);

					let myOrg = [wdw_cardEdition.getOrg(), myTempCard.title, myTempCard.role];
					let myN = [myTempCard.prefixname, myTempCard.firstname, myTempCard.othername, myTempCard.lastname,
					myTempCard.suffixname, myTempCard.nickname];
					let data = cardbookHTMLFormulas.getFnDataForFormula(myN, myOrg);

					if (myTemplateCard.fn.includes("{{")) {
						myTempCard.fn = cardbookHTMLFormulas.getStringFromFormula(myTemplateCard.fn, data);
					} else {
						let myFnFormula = cardbookBGPreferences.getFnFormula(myTempCard.dirPrefId);
						myTempCard.fn = cardbookHTMLFormulas.getStringFromFormula(myFnFormula, data);
					}

					function stringify(aEntryLine) {
						return aEntryLine[0].join(",");
					};
					function addIfMissing(aEntryLine, aEntryLines) {
						let lineToAdd = stringify(aEntryLine);
						for (let entryLine of aEntryLines) {
							if (stringify(entryLine) == lineToAdd) {
								return;
							}
						}
						aEntryLines.push(aEntryLine);
					};
					for (let type of cardbookHTMLUtils.multilineFields) {
						if (type == "adr") {
							for (let entryLine of myTemplateCard[type]) {
								addIfMissing(entryLine, myTempCard[type])
							}
						} else {
							for (let entryLine of myTemplateCard[type]) {
								entryLine[0][0] = cardbookHTMLFormulas.getStringFromFormula(entryLine[0][0], data).toLowerCase();
								addIfMissing(entryLine, myTempCard[type])
							}
						}
					}

					if (myTempCard.note == "" && myTemplateCard.note != "") {
						myTempCard.note = cardbookHTMLFormulas.getStringFromFormula(myTemplateCard.note, data);
					}

					let dateFormat = cardbookHTMLUtils.getDateFormat(myTempCard.dirPrefId, myTempCard.version);
					let myEvents = cardbookHTMLUtils.getEventsFromCard(myTemplateCard.note.split("\n"), myTemplateCard.others);
					let myPGNextNumber = cardbookHTMLUtils.rebuildAllPGs(myTempCard);
					for (let entryLine of myEvents.result) {
						entryLine[1] = cardbookHTMLFormulas.getStringFromFormula(entryLine[1], data);
					}
					cardbookHTMLUtils.addEventstoCard(myTempCard, myEvents.result, myPGNextNumber, dateFormat, dateFormat);

					for (let type of ["personal", "org"]) {
						for (let custom of wdw_cardEdition.customFields[type]) {
							let tempCustomValue = cardbookHTMLUtils.getCardValueByField(myTempCard, custom[0], false);
							let templateCustomValue = cardbookHTMLUtils.getCardValueByField(myTemplateCard, custom[0], false);
							if (tempCustomValue.length == 0 && templateCustomValue.length != 0) {
								let value = cardbookHTMLFormulas.getStringFromFormula(templateCustomValue[0], data);
								myTempCard = cardbookHTMLUtils.setCardValueByField(myTempCard, custom[0], value);
							}
						}
					}

					wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(myTempCard, wdw_cardEdition.workingCard);
					await wdw_cardEdition.displayCard(wdw_cardEdition.workingCard);
					myTempCard = null;

					// first vCard shown
					return;
				} else if (fileContentArray[i] == "") {
					continue;
				} else {
					cardContent = cardContent + "\r\n" + fileContentArray[i];
				}
			}
		}
	},

	load: async function (noParams = true) {
		if (noParams === true) {
			let urlParams = new URLSearchParams(window.location.search);
			wdw_cardEdition.editionMode = urlParams.get("editionMode");
			wdw_cardEdition.cardContent = urlParams.get("cardContent");
			wdw_cardEdition.cbIdIn = urlParams.get("cbIdIn");
            wdw_cardEdition.ids = urlParams.get("ids");
			wdw_cardEdition.fn = urlParams.get("fn") || "";
			wdw_cardEdition.firstname = urlParams.get("firstname") || "";
			wdw_cardEdition.lastname = urlParams.get("lastname") || "";
			wdw_cardEdition.email = urlParams.get("email") || "";
			wdw_cardEdition.category = urlParams.get("category") || "";

			i18n.updateDocument();

			wdw_cardEdition.customFields = cardbookBGPreferences.getAllCustomFields();

			// checkbox
			document.getElementById("preferDisplayNameCheckBox").addEventListener("input", wdw_cardEdition.savePreferDisplayName);

			// input
			let inputs = document.querySelectorAll("input");
			for (let input of inputs) {
				let row = input.closest("tr");
				if (row && row.hasAttribute("data-field-name") && row.id != "fnRow") {
					input.addEventListener("input", event => wdw_cardEdition.onInputField(event));
				}
			}
			document.getElementById("searchAvailableCardsInputText").addEventListener("input", event => wdw_cardEdition.searchAvailableCards(event));
			
			// button
			document.getElementById("generalTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("generalTabPanel"));
			document.getElementById("noteTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("noteTabPanel"));
			document.getElementById("listTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("listTabPanel"));
			document.getElementById("keyTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("keyTabPanel"));
			document.getElementById("helpTab").addEventListener("click", event => cardbookHTMLWindowUtils.showPane("helpTabPanel"));
			
			document.getElementById("noteDateButton").addEventListener("click", wdw_cardEdition.addNoteDate);
			document.getElementById("searchForOnlineKeyButton").addEventListener("click", wdw_cardEdition.searchForOnlineKey);
			document.getElementById("searchForThKeyEditButton").addEventListener("click", wdw_cardEdition.searchForThKeyEdit);
			document.getElementById("searchForLocalKeyEditButton").addEventListener("click", wdw_cardEdition.searchForLocalKeyEdit);
			document.getElementById("loadKeyInput").addEventListener("change", event => wdw_cardEdition.loadKeyInput());

			document.getElementById("autoComputeFnButton").addEventListener("click", event => wdw_cardEdition.autoComputeFn(event.target));
			document.getElementById("expandPersonalImage").addEventListener("click", event => wdw_cardEdition.expandButton(event.target));
			document.getElementById("expandOrgImage").addEventListener("click", event => wdw_cardEdition.expandButton(event.target));
			document.getElementById("cardbookSwitchButtonDown").addEventListener("click", wdw_cardEdition.switchLastnameAndFirstname);
			document.getElementById("cardbookSwitchButtonUp").addEventListener("click", wdw_cardEdition.switchLastnameAndFirstname);
			let convertButtons = document.querySelectorAll("img.cardbookProcessClass");
			for (let button of convertButtons) {
				button.addEventListener("click", cardbookHTMLWindowUtils.setConvertFunction);
			}

			document.getElementById("defaultCardImage").addEventListener("dblclick", event => wdw_cardEdition.addImageCardFromFile());
			document.getElementById("defaultCardImage").addEventListener("dragover", event => event.preventDefault());
			document.getElementById("defaultCardImage").addEventListener("drop", event => wdw_cardEdition.dragImageCard(event));
			document.getElementById("loadImageInput").addEventListener("change", event => wdw_cardEdition.loadImageInput());

			document.getElementById("createTemplateButtonImg").addEventListener("click", wdw_cardEdition.createTemplate);
			document.getElementById("openTemplateButtonImg").addEventListener("click", wdw_cardEdition.loadTemplateButton);
			document.getElementById("loadTemplateInput").addEventListener("change", wdw_cardEdition.loadTemplateInput);
			document.getElementById("applyTemplateButtonImg").addEventListener("click", wdw_cardEdition.applyTemplateButton);
			document.getElementById("applyTemplateInput").addEventListener("change", wdw_cardEdition.applyTemplateInput);

			document.getElementById("previousEditButton").addEventListener("click", event => wdw_cardEdition.changeContactFromOrder("previous"));
			document.getElementById("nextEditButton").addEventListener("click", event => wdw_cardEdition.changeContactFromOrder("next"));
			document.getElementById("createEditionButton").addEventListener("click", wdw_cardEdition.create);
			document.getElementById("createAndReplaceEditionButton").addEventListener("click", wdw_cardEdition.createAndReplace);
			document.getElementById("saveEditionButton").addEventListener("click", wdw_cardEdition.save);
			document.getElementById("saveTemplateButton").addEventListener("click", wdw_cardEdition.saveTemplateButton);
			document.getElementById("cancelButton").addEventListener("click", wdw_cardEdition.cancel);

			document.getElementById("appendCardButton").addEventListener("click", event => wdw_cardEdition.modifyLists(event.target.id));
			document.getElementById("appendCardEmailsButton").addEventListener("click", event => wdw_cardEdition.modifyLists(event.target.id));
			document.getElementById("deleteListButton").addEventListener("click", event => wdw_cardEdition.modifyLists(event.target.id));

			// select
			document.getElementById("addressbookMenulist").addEventListener("change", wdw_cardEdition.changeAddressbook);
			document.getElementById("contactMenulist").addEventListener("change", wdw_cardEdition.changeContact);
		}

		await cardbookIDBCard.openCardDB({});
		wdw_cardEdition.workingCard = {};
		let dirPrefId = wdw_cardEdition.cbIdIn.split("::")[0];
		if (wdw_cardEdition.editionMode == "EditTemplate") {
			wdw_cardEdition.cardIn = new cardbookCardParser(wdw_cardEdition.cardContent, "", "", dirPrefId);
			wdw_cardEdition.cardIn.fn = cardbookBGPreferences.getFnFormula(dirPrefId);
			wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.cardIn, wdw_cardEdition.workingCard);
		} else {
			let results = await messenger.runtime.sendMessage({ query: "cardbook.getCards", cbids: [ wdw_cardEdition.cbIdIn ] });
			let result = results[0];
			if (results.length && Object.keys(result).length !== 0) {
				wdw_cardEdition.cardIn = result;
				wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.cardIn, wdw_cardEdition.workingCard);
			} else {
				wdw_cardEdition.cardIn = new cardbookCardParser("", "", "", dirPrefId);
				wdw_cardEdition.cardIn.fn = wdw_cardEdition.fn;
				wdw_cardEdition.cardIn.lastname = wdw_cardEdition.lastname;
				wdw_cardEdition.cardIn.firstname = wdw_cardEdition.firstname;
				if (wdw_cardEdition.category != "") {
					wdw_cardEdition.cardIn.categories.push(wdw_cardEdition.category);
				}
				if (wdw_cardEdition.email) {
					if (wdw_cardEdition.cardIn.isAList) {
						cardbookHTMLUtils.addMemberstoCard(wdw_cardEdition.cardIn, [`mailto:${wdw_cardEdition.email}`]);
					} else {
						wdw_cardEdition.cardIn.email.push([[wdw_cardEdition.email], [], "", []]);
					}
				}
				if (wdw_cardEdition.editionMode == "CreateList") {
					wdw_cardEdition.cardIn.isAList = true;
				}
				wdw_cardEdition.workingCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.cardIn, wdw_cardEdition.workingCard);
			}
		}

		wdw_cardEdition.loadEditionMode();
		wdw_cardEdition.setEditionFields();
		await wdw_cardEdition.changePreviousNext();
		wdw_cardEdition.loadHelpTab();

		await wdw_cardEdition.displayCard(wdw_cardEdition.workingCard);
		await wdw_cardEdition.displayLists(wdw_cardEdition.workingCard, dirPrefId);
	},

	saveMailPopularity: async function () {
		let i = 0;
		while (true) {
			if (document.getElementById(`emailproperty_${i}_Row`)) {
				let email = document.getElementById(`email_${i}_inputText`).textContent.toLowerCase();
				let emailValue = parseInt(document.getElementById(`popularity_${i}_inputText`).value) || "0";
				await messenger.runtime.sendMessage({ query: "cardbook.updateMailPop", email: email, value: emailValue});
				i++;
			} else {
				break;
			}
		}
	},

	savePreferDisplayName: async function () {
		let i = 0;
		while (true) {
			if (document.getElementById(`email_${i}_valueBox`)) {
				let email = document.getElementById(`email_${i}_valueBox`).value.toLowerCase();
				if (document.getElementById("preferDisplayNameCheckBox").checked == true) {
					await messenger.runtime.sendMessage({ query: "cardbook.removePrefDispName", email: email});
				} else {
					await messenger.runtime.sendMessage({ query: "cardbook.addPrefDispName", email: email});
				}
				i++;
			} else {
				break;
			}
		}
	},

	calculateResult: async function (aCard) {
		aCard = await wdw_cardEdition.cloneCard(wdw_cardEdition.workingCard, aCard);
		aCard.dirPrefId = document.getElementById("addressbookMenulist").value;

		aCard.version = document.getElementById("versionInputText").value;
		aCard.categories = document.getElementById("categoriesInputDropDown").getValues();

		aCard.org = wdw_cardEdition.getOrg();
		aCard.title = document.getElementById("titleInputText").value.trim();
		aCard.role = document.getElementById("roleInputText").value.trim();

		aCard.fn = document.getElementById("fnInputText").value.trim();

		aCard.lastname = document.getElementById("lastnameInputText").value.trim();
		aCard.firstname = document.getElementById("firstnameInputText").value.trim();
		aCard.othername = document.getElementById("othernameInputText").value.trim();
		aCard.suffixname = document.getElementById("suffixnameInputText").value.trim();
		aCard.prefixname = document.getElementById("prefixnameInputText").value.trim();
		aCard.nickname = document.getElementById("nicknameInputText").value.trim();
		aCard.gender = document.getElementById("genderMenulist").value.trim();

		var dateFormat = cardbookHTMLUtils.getDateFormat(document.getElementById("dirPrefIdInputText").value, document.getElementById("versionInputText").value);
		for (var field of cardbookHTMLDates.dateFields) {
			let value = document.getElementById(`${field}InputDate`).getValue();
			let value1 = cardbookHTMLDates.getVCardDateFromDateString(value, "4.0");
			let isDate = cardbookHTMLDates.convertDateStringToDateUTC(value1);
			if (isDate != "WRONGDATE") {
				aCard[field] = cardbookHTMLDates.convertUTCDateToDateString(isDate, dateFormat);
			}
		}

		aCard.birthplace = document.getElementById("birthplaceInputText").value.trim();
		aCard.deathplace = document.getElementById("deathplaceInputText").value.trim();

		aCard.note = document.getElementById("noteInputText").value.trim();

		for (let media of cardbookHTMLUtils.allColumns.media) {
			if (media == "photo") {
				aCard[media] = { types: [], value: "", URI: "", extension: "", attachmentId: "" };
				aCard[media].value = document.getElementById(`${media}URIInputText`).value;
				aCard[media].extension = document.getElementById(`${media}ExtensionInputText`).value;
				if (document.getElementById(`${media}AttachmentIdInputText`)) {
					aCard[media].attachmentId = document.getElementById(`${media}AttachmentIdInputText`).value;
				}
			} else {
				aCard[media] = { types: [], value: "", URI: "", extension: "" };
				aCard[media].value = document.getElementById(`${media}URIInputText`).value;
				aCard[media].extension = document.getElementById(`${media}ExtensionInputText`).value;
			}
		}

		for (let field of cardbookHTMLUtils.multilineFields) {
			aCard[field] = cardbookHTMLWindowUtils.getAllTypes(field, true);
		}
		aCard.tz = cardbookHTMLWindowUtils.getAllTz(true);

		var keys = cardbookHTMLWindowUtils.getAllKeys(true);
		var re = /[\n\u0085\u2028\u2029]|\r\n?/g;
		keys = keys.map(key => {
			key.value = key.value.replace(/-----(BEGIN|END) PGP PUBLIC KEY BLOCK-----/g, "").trim().replace(re, "\\r\\n"); //key.value.replaceAll("\n", "\\n").replaceAll("\r", "\\r");
			return key;
		});
		aCard.key = keys;

		var othersTemp1 = [];
		for (var i in wdw_cardEdition.customFields) {
			for (var j = 0; j < wdw_cardEdition.customFields[i].length; j++) {
				let customFieldNumber = wdw_cardEdition.customFields[i][j][2];
				let customFieldName = `customField${customFieldNumber}${i}InputText`;
				if (document.getElementById(customFieldName)) {
					var customValue = document.getElementById(customFieldName).value.trim();
					if (customValue) {
						let customField = wdw_cardEdition.customFields[i][j][0];
						othersTemp1.push(`${customField}:${customValue}`);
					}
				}
			}
		}
		var re = /[\n\u0085\u2028\u2029]|\r\n?/;
		var othersTemp3 = [];
		var othersTemp2 = document.getElementById("othersInputText").value;
		if (othersTemp2) {
			othersTemp3 = othersTemp2.split(re);
		}
		aCard.others = othersTemp1.concat(othersTemp3);

		aCard.others = aCard.others.filter(element => !element.toUpperCase().startsWith(wdw_cardEdition.defaultEmailFormat));
		if (document.getElementById("select_1_preferMailFormat")?.value == "1") {
			aCard.others.push(wdw_cardEdition.defaultEmailFormat + ":FALSE");
		} else if (document.getElementById("select_1_preferMailFormat")?.value == "2") {
			aCard.others.push(wdw_cardEdition.defaultEmailFormat + ":TRUE");
		}

		var myPGNextNumber = cardbookHTMLUtils.rebuildAllPGs(aCard);
		var myEvents = cardbookHTMLWindowUtils.getAllEvents(true);
		cardbookHTMLUtils.addEventstoCard(aCard, myEvents, myPGNextNumber, "4.0", dateFormat);

		// trying desesperately to find a Fn
		if (aCard.fn == "") {
			cardbookHTMLFormulas.getDisplayedName(aCard, document.getElementById("dirPrefIdInputText").value, [document.getElementById("prefixnameInputText").value.trim(),
			document.getElementById("firstnameInputText").value.trim(),
			document.getElementById("othernameInputText").value.trim(),
			document.getElementById("lastnameInputText").value.trim(),
			document.getElementById("suffixnameInputText").value.trim(),
			document.getElementById("nicknameInputText").value.trim()],
				[wdw_cardEdition.getOrg(),
				document.getElementById("titleInputText").value.trim(),
				document.getElementById("roleInputText").value.trim()]);
		}

		if (aCard.isAList) {
			let members = [];
			for (let card of wdw_cardEdition.cardbookeditlists.addedCardsTree) {
				if (card.isEmail) {
					members.push("mailto:" + card.fn);
				} else {
					let ABType = cardbookBGPreferences.getType(wdw_cardEdition.workingCard.dirPrefId);
					if (ABType == "OFFICE365") {
						for (let email of card.emails) {
							members.push("mailto:" + email);
						}
					} else {
						members.push("urn:uuid:" + card.uid);
					}
				}
			}
			let kind = document.getElementById("kindInputText").value.trim();
			cardbookHTMLUtils.addMemberstoCard(aCard, members, kind);
		}
		return aCard;
	},

	changePreviousNext: async function () {
		document.getElementById("previousEditButton").classList.add("hidden");
		document.getElementById("nextEditButton").classList.add("hidden");
		switch (wdw_cardEdition.editionMode) {
			case "ViewResult":
			case "ViewResultHideCreate":
			case "CreateContact":
			case "CreateList":
			case "AddEmail":
			case "EditTemplate":
				return;
		}
		document.getElementById("previousEditButton").classList.add("hidden");
		document.getElementById("nextEditButton").classList.add("hidden");
		let cards = await messenger.runtime.sendMessage({ query: "cardbook.getNextAndPreviousCard", cbid: wdw_cardEdition.cardIn.cbid });
		if (cards.previous) {
			document.getElementById("previousEditButton").classList.remove("hidden");
		}
		if (cards.next) {
			document.getElementById("nextEditButton").classList.remove("hidden");
		}
	},

	changeContactFromOrder: async function (aOrder) {
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let readonly = cardbookBGPreferences.getReadOnly(dirPrefId);
		if (!readonly) {
			await wdw_cardEdition.saveFinal(false);
		}
		let cards = await messenger.runtime.sendMessage({ query: "cardbook.getNextAndPreviousCard", cbid: wdw_cardEdition.cardIn.cbid });
		wdw_cardEdition.cardIn = new cardbookCardParser();
		if (aOrder == "next") {
			wdw_cardEdition.cardIn = await wdw_cardEdition.cloneCard(cards.next, wdw_cardEdition.cardIn);
		} else {
			wdw_cardEdition.cardIn = await wdw_cardEdition.cloneCard(cards.previous, wdw_cardEdition.cardIn);
		}
		wdw_cardEdition.cbIdIn = wdw_cardEdition.cardIn.cbid;

		if (wdw_cardEdition.cardIn.isAList) {
			var myType = "List";
		} else {
			var myType = "Contact";
		}
		if (cardbookBGPreferences.getReadOnly(wdw_cardEdition.cardIn.dirPrefId)) {
			wdw_cardEdition.editionMode = "View" + myType;
		} else {
			wdw_cardEdition.editionMode = "Edit" + myType;
		}
		await wdw_cardEdition.load(false);
	},

	validate: async function (aCard) {
		let notificationMessage = document.getElementById("notificationMessage");
		if (await cardbookHTMLValidations.validateOffice365(aCard, wdw_cardEdition.cardbookeditlists.addedCardsTree, notificationMessage) &&
			cardbookHTMLValidations.validateNodesNumber(notificationMessage) &&
			cardbookHTMLValidations.validateEvents(notificationMessage) &&
			wdw_cardEdition.editionMode != "ViewContact" &&
			wdw_cardEdition.editionMode != "ViewList") {
			return true;
		} else {
			return false;
		}
	},

	saveFinalGetOutCard: async function (aClose) {
		var outCard = new cardbookCardParser();
		outCard = await wdw_cardEdition.calculateResult(outCard);
		if (await wdw_cardEdition.validate(outCard)) {
			await wdw_cardEdition.saveMailPopularity();

			wdw_cardEdition.workingCard = null;
			// no change, no save
			if (wdw_cardEdition.editionMode != "ViewResult" && wdw_cardEdition.editionMode != "ViewResultHideCreate") {
				cardbookHTMLUtils.sortArrayByString(wdw_cardEdition.cardIn.categories, 1)
				let cardin = await messenger.runtime.sendMessage({ query: "cardbook.cardToVcardData", card: wdw_cardEdition.cardIn });
				cardbookHTMLUtils.sortArrayByString(outCard.categories, 1)
				let cardout = await messenger.runtime.sendMessage({ query: "cardbook.cardToVcardData", card: outCard, mediaFromDB: false });
				if (cardin == cardout && wdw_cardEdition.cardIn.dirPrefId == outCard.dirPrefId) {
					if (aClose) {
						await wdw_cardEdition.cancel();
					}
					await messenger.runtime.sendMessage({ query: "cardbook.notifyObserver", value: "cardbook.cardEdited" });
					return;
				}
			}

			outCard.uid = outCard.uid.replace(/^urn:uuid:/i, "");
			if (cardbookBGPreferences.getUrnuuid(outCard.dirPrefId)) {
				outCard.uid = "urn:uuid:" + outCard.uid;
			}

			if (wdw_cardEdition.editionMode == "AddEmail") {
				await wdw_cardEdition.cloneCard(outCard, wdw_cardEdition.cardIn);
			}
			outCard = cardbookHTMLWindowUtils.sortPrefFieldsFirstForGoogle(outCard);
			return outCard;
		}
		return null;
	},

	saveFinal: async function (aClose = true) {
		let outCard = await wdw_cardEdition.saveFinalGetOutCard(aClose);
		if (outCard instanceof Object) {
			if (wdw_cardEdition.editionMode == "ViewResult" || wdw_cardEdition.editionMode == "ViewResultHideCreate") {
				await messenger.runtime.sendMessage({query: "cardbook.mergeCards.closeViewCardResult", ids: wdw_cardEdition.ids, action: wdw_cardEdition.editionAction, cardOut: outCard});
			} else {
				await messenger.runtime.sendMessage({ query: "cardbook.saveEditionWindow", cardIn: wdw_cardEdition.cardIn, cardOut: outCard, editionMode: wdw_cardEdition.editionMode, action: wdw_cardEdition.editionAction});
			}
			
			if (aClose) {
				wdw_cardEdition.closeWindow();
			}
		}
	},

	saveTemplateButton: async function () {
		let outCard = await wdw_cardEdition.saveFinalGetOutCard();
		if (outCard instanceof Object) {
			let content = await messenger.runtime.sendMessage({ query: "cardbook.cardToVcardData", card: outCard });
			let file = new Blob([content], { type: "text/plain" });
			let url = URL.createObjectURL(file);
			let filename = messenger.i18n.getMessage("templateLabel") + ".tpl";
			await cardbookHTMLDownloads.download(url, filename, "saveTemplate", null);
			wdw_cardEdition.closeWindow();
		}
	},

	create: function () {
		wdw_cardEdition.editionAction = "CREATE";
		wdw_cardEdition.saveFinal();
	},

	createAndReplace: function () {
		wdw_cardEdition.editionAction = "CREATEANDREPLACE";
		wdw_cardEdition.saveFinal();
	},

	save: function () {
		wdw_cardEdition.editionAction = "SAVE";
		wdw_cardEdition.saveFinal();
	},

	cancel: async function () {
		wdw_cardEdition.editionAction = "CANCEL";
		await messenger.runtime.sendMessage({ query: "cardbook.saveEditionWindow", cardIn: wdw_cardEdition.cardIn, cardOut: null, editionMode: wdw_cardEdition.editionMode, action: wdw_cardEdition.editionAction});
		wdw_cardEdition.closeWindow();
	},

	closeWindow: async function () {
		let win = await cardbookHTMLRichContext.getWindow();
		await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
		cardbookHTMLRichContext.closeWindow();
	}
};

await wdw_cardEdition.load();

messenger.runtime.onMessage.addListener( (info) => {
	switch (info.query) {
		case "cardbook.pref.preferencesChanged":
			wdw_cardEdition.load(false);
			break;
	}
});

window.addEventListener("beforeunload", async function() {
	let win = await cardbookHTMLRichContext.getWindow();
	await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
});

window.addEventListener("keydown", async event => {
	let notificationMessage = document.getElementById("notificationMessage");
	cardbookHTMLNotification.setNotification(notificationMessage, "OK");
	switch (event.key) {
		case "Enter":
			let treeview = event.target.closest("tree-view");
			let dropdown = event.target.closest(".dropdownContainer");
			if (event.target.id == "addEmailInputText") {
				await wdw_cardEdition.modifyLists(event.target.id, event.target.value);
				return;
			} else if (event.target.id == "noteInputText") {
				return;
			} else if (event.target.id == "streetInputText") {
				return;
			} else if (treeview) {
				return;
			} else if (dropdown) {
				return;
			} else if (wdw_cardEdition.editionMode == "ViewResult" || wdw_cardEdition.editionMode == "ViewResultHideCreate") {
				return;
			} else {
				wdw_cardEdition.save();
				event.preventDefault();
			}
			break;
		case "Escape":
			if (wdw_cardEdition.editionMode == "ViewResult" || wdw_cardEdition.editionMode == "ViewResultHideCreate") {
				await messenger.runtime.sendMessage({query: "cardbook.mergeCards.closeViewCardResult", ids: wdw_cardEdition.ids, action: wdw_cardEdition.editionAction});
			}
			let win = await cardbookHTMLRichContext.getWindow();
			await cardbookHTMLUtils.saveWindowSize(win.name, win.state);
			messenger.menus.removeAll();
			messenger.windows.remove(win.id);
			break;
		}
});

let currentTab = await messenger.tabs.getCurrent();
let findEmailsEnabled = await messenger.runtime.sendMessage({ query: "cardbook.getSystemPref", type: "boolean", value: "mailnews.database.global.indexer.enabled" });
let lastemail = [];
let lastContextElement;
window.addEventListener("contextmenu", async (event) => {
	lastemail = [];
	lastContextElement = event.target;

	browser.menus.overrideContext({context: "tab", tabId: currentTab.id});
	browser.menus.removeAll();
	let properties = { contexts: ["tab"], enabled: true, visible: true, viewTypes: ["tab", "popup"] };
	let row = event.target.closest("tr");

	if ((event.target.tagName.toUpperCase() == "INPUT" && event.target.type.toUpperCase() == "TEXT") ||
		(event.target.tagName.toUpperCase() == "TEXTAREA" && event.target.type.toUpperCase() == "TEXTAREA")) {
		let copyTitle = messenger.i18n.getMessage("copy");
		let copyProperties = { ...properties, id: "cardbookCopyMenuId", title: copyTitle};
		await browser.menus.create(copyProperties);

		let pasteTitle = messenger.i18n.getMessage("paste");
		let pasteProperties = { ...properties, id: "cardbookPasteMenuId", title: pasteTitle};
		await browser.menus.create(pasteProperties);

		let upperTitle = messenger.i18n.getMessage("toUpperCase");
		let upperProperties = { ...properties, id: "cardbookUppercaseMenuId", title: upperTitle};
		await browser.menus.create(upperProperties);

		let lowerTitle = messenger.i18n.getMessage("toLowerCase");
		let lowerProperties = { ...properties, id: "cardbookLowercaseMenuId", title: lowerTitle};
		await browser.menus.create(lowerProperties);
	} else if (event.target.tagName.toUpperCase() == "IMG" && event.target.hasAttribute("editionMode")) {
		let defaultCardImage = "chrome/content/skin/contact-generic.svg";

		let addImageCardFromFileTitle = messenger.i18n.getMessage("addImageCardFromFileLabel");
		let addImageCardFromFileProperties = { ...properties, id: "addImageCardFromFile", title: addImageCardFromFileTitle};
		await browser.menus.create(addImageCardFromFileProperties);

		let pasteImageCardTitle = messenger.i18n.getMessage("pasteImageCardLabel");
		let pasteImageCardProperties = { ...properties, id: "pasteImageCard", title: pasteImageCardTitle};
		await browser.menus.create(pasteImageCardProperties);

		if (!event.target.src.endsWith(defaultCardImage)) {
			let saveImageCardTitle = messenger.i18n.getMessage("saveImageCardLabel");
			let saveImageCardProperties = { ...properties, id: "saveImageCard", title: saveImageCardTitle};
			await browser.menus.create(saveImageCardProperties);

			let copyImageCardTitle = messenger.i18n.getMessage("copyImageCardLabel");
			let copyImageCardProperties = { ...properties, id: "copyImageCard", title: copyImageCardTitle};
			await browser.menus.create(copyImageCardProperties);

			let deleteImageCardTitle = messenger.i18n.getMessage("deleteImageCardLabel");
			let deleteImageCardProperties = { ...properties, id: "deleteImageCard", title: deleteImageCardTitle};
			await browser.menus.create(deleteImageCardProperties);
		}
	} else if (event.target.tagName.toUpperCase() == "IMG") {
		let defaultCardImage = "chrome/content/skin/contact-generic.svg";
		if (!event.target.src.endsWith(defaultCardImage)) {
			let saveImageCardTitle = messenger.i18n.getMessage("saveImageCardLabel");
			let saveImageCardProperties = { ...properties, id: "saveImageCard", title: saveImageCardTitle};
			await browser.menus.create(saveImageCardProperties);

			let copyImageCardTitle = messenger.i18n.getMessage("copyImageCardLabel");
			let copyImageCardProperties = { ...properties, id: "copyImageCard", title: copyImageCardTitle};
			await browser.menus.create(copyImageCardProperties);
		}
	} else if (row?.getAttribute("data-field-name") == "email") {
		let type = row.getAttribute("data-field-name");
		let toEmailTitle = messenger.i18n.getMessage("toEmailEmailTreeLabel");
		let toEmailProperties = { ...properties, id: "toEmail", title: toEmailTitle};
		await browser.menus.create(toEmailProperties);

		let ccEmailTitle = messenger.i18n.getMessage("ccEmailEmailTreeLabel");
		let ccEmailProperties = { ...properties, id: "ccEmail", title: ccEmailTitle};
		await browser.menus.create(ccEmailProperties);

		let bccEmailTitle = messenger.i18n.getMessage("bccEmailEmailTreeLabel");
		let bccEmailProperties = { ...properties, id: "bccEmail", title: bccEmailTitle};
		await browser.menus.create(bccEmailProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let findEmailsTitle = messenger.i18n.getMessage("findemailemailTreeLabel");
		let findEmailsProperties = { ...properties, id: "findEmails", title: findEmailsTitle, enabled: findEmailsEnabled};
		await browser.menus.create(findEmailsProperties);

		let findEventsTitle = messenger.i18n.getMessage("findeventemailTreeLabel");
		let findEventsProperties = { ...properties, id: "findEvents", title: findEventsTitle};
		await browser.menus.create(findEventsProperties);

		let separator2Properties = { ...properties, id: "separator2", type: "separator"};
		await browser.menus.create(separator2Properties);
		
		let searchForOnlineKeyTitle = messenger.i18n.getMessage("searchForOnlineKeyTreeLabel");
		let searchForOnlineKeyProperties = { ...properties, id: "searchForOnlineKey", title: searchForOnlineKeyTitle};
		await browser.menus.create(searchForOnlineKeyProperties);

		let separator3Properties = { ...properties, id: "separator3", type: "separator"};
		await browser.menus.create(separator3Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyEmailTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyEmailProperties = { ...properties, id: "copyField", title: copyEmailTitle};
		await browser.menus.create(copyEmailProperties);
	} else if (row?.getAttribute("data-field-name") == "url") {
		let type = row.getAttribute("data-field-name");
		let openURLTitle = messenger.i18n.getMessage("openURLTreeLabel");
		let openURLProperties = { ...properties, id: "openURL", title: openURLTitle};
		await browser.menus.create(openURLProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyURLTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyURLProperties = { ...properties, id: "copyField", title: copyURLTitle};
		await browser.menus.create(copyURLProperties);
	} else if (row?.getAttribute("data-field-name") == "impp") {
		let type = row.getAttribute("data-field-name");
		let connectIMPPTitle = messenger.i18n.getMessage("IMPPMenuLabel");
		let connectIMPPProperties = { ...properties, id: "connectIMPP", title: connectIMPPTitle};
		await browser.menus.create(connectIMPPProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyIMPPTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyIMPPProperties = { ...properties, id: "copyField", title: copyIMPPTitle};
		await browser.menus.create(copyIMPPProperties);
	} else if (row?.getAttribute("data-field-name") == "adr") {
		let type = row.getAttribute("data-field-name");
		let localizeAdrTitle = messenger.i18n.getMessage("localizeadrTreeLabel");
		let localizeAdrProperties = { ...properties, id: "localizeAdr", title: localizeAdrTitle};
		await browser.menus.create(localizeAdrProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		for (let element of cardbookHTMLUtils.adrElements) {
			let copyElementLabel = messenger.i18n.getMessage(`${element}Label`);
			let copyElementTitle = messenger.i18n.getMessage("copyFieldValue", [copyElementLabel]);
			let copyElementProperties = { ...properties, id: `copyField${element}`, title: copyElementTitle};
			await browser.menus.create(copyElementProperties);
		}

		let separator2Properties = { ...properties, id: "separator2", type: "separator"};
		await browser.menus.create(separator2Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyAdrTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyAdrProperties = { ...properties, id: "copyField", title: copyAdrTitle};
		await browser.menus.create(copyAdrProperties);
	} else if (row?.getAttribute("data-field-name") == "tel") {
		let type = row.getAttribute("data-field-name");
		let connectTelTitle = messenger.i18n.getMessage("IMPPMenuLabel");
		let connectTelProperties = { ...properties, id: "connectTel", title: connectTelTitle};
		await browser.menus.create(connectTelProperties);

		let separator1Properties = { ...properties, id: "separator1", type: "separator"};
		await browser.menus.create(separator1Properties);

		let fieldLabel = messenger.i18n.getMessage(`${type}Label`);
		let copyTelTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyTelProperties = { ...properties, id: "copyField", title: copyTelTitle};
		await browser.menus.create(copyTelProperties);
	} else if (row?.getAttribute("data-field-name")) {
		let field = row.getAttribute("data-field-name");
		let fieldLabel = row.getAttribute("data-field-label") || messenger.i18n.getMessage(`${field}Label`);
		let copyTitle = messenger.i18n.getMessage("copyFieldValue", [fieldLabel]);
		let copyProperties = { ...properties, id: "copyField", title: copyTitle};
		await browser.menus.create(copyProperties);
	} else {
		if (treeview) {
			if (treeview.id == "availableCardsTree") {
				let appendlistTitle = messenger.i18n.getMessage("appendContactLabel");
				let appendlistProperties = { ...properties, id: "appendListMenu", title: appendlistTitle};
				await browser.menus.create(appendlistProperties);

				if (wdw_cardEdition.availableCards.cardsList.selectedIndices.length == 1) {
					let index = wdw_cardEdition.availableCards.cardsList.selectedIndices[0];
					let card = wdw_cardEdition.availableCards.cardsList.view._rowMap[index].card;
					lastemail = JSON.parse(JSON.stringify(card.email));
					for (var i = 0; i < card.email.length; i++) {
						let email = card.email[i][0][0].toLowerCase();
						let appendCardEmailTitle = messenger.i18n.getMessage("appendEmailLabel", [email])
						let appendCardEmailProperties = { ...properties, id: `appendCardEmailMenu${i}`, title: appendCardEmailTitle};
						await browser.menus.create(appendCardEmailProperties);
					}
				} else {
					let appendCardEmailsTitle = messenger.i18n.getMessage("buttonAppendEmailsToListLabel")
					let appendCardEmailsProperties = { ...properties, id: "appendCardEmailsMenu", title: appendCardEmailsTitle};
					await browser.menus.create(appendCardEmailsProperties);
				}
			} else 	if (treeview.id == "addedCardsTree") {
				let deletelistTitle = messenger.i18n.getMessage("deletelistTreeLabel");
				let deletelistProperties = { ...properties, id: "deleteListMenu", title: deletelistTitle};
				await browser.menus.create(deletelistProperties);
			}
		}
	}
});

messenger.menus.onClicked.addListener(async (info, tab) => {
	if (currentTab.id != tab.id) {
		return;
	}
	if (info.menuItemId == "cardbookUppercaseMenuId") {
		let result = "";
		let value = lastContextElement.value;
		if (lastContextElement.selectionStart == lastContextElement.selectionEnd) {
			result = value.toUpperCase();
		} else {
			for (var i = 0; i < value.length; i++) {
				if (i >= lastContextElement.selectionStart && i < lastContextElement.selectionEnd) {
					result = result + value[i].toUpperCase();
				} else {
					result = result + value[i];
				}
			}
		}
		lastContextElement.value = result;
		lastContextElement.dispatchEvent(new Event("input"));
	} else if (info.menuItemId == "cardbookLowercaseMenuId") {
		let result = "";
		let value = lastContextElement.value;
		if (lastContextElement.selectionStart == lastContextElement.selectionEnd) {
			result = value.toLowerCase();
		} else {
			for (var i = 0; i < value.length; i++) {
				if (i >= lastContextElement.selectionStart && i < lastContextElement.selectionEnd) {
					result = result + value[i].toLowerCase();
				} else {
					result = result + value[i];
				}
			}
		}
		lastContextElement.value = result;
		lastContextElement.dispatchEvent(new Event("input"));
	} else if (info.menuItemId == "cardbookCopyMenuId") {
		let result = "";
		let value = lastContextElement.value;
		if (lastContextElement.selectionStart == lastContextElement.selectionEnd) {
			result = value;
		} else {
			for (var i = 0; i < value.length; i++) {
				if (i >= lastContextElement.selectionStart && i < lastContextElement.selectionEnd) {
					result = result + value[i];
				}
			}
		}
		await navigator.clipboard.writeText(result);
		lastContextElement.dispatchEvent(new Event("input"));
	} else if (info.menuItemId == "cardbookPasteMenuId") {
		let text = await navigator.clipboard.readText();
		let result = "";
		let value = lastContextElement.value;
		result = value.slice(0, lastContextElement.selectionStart) + text + value.slice(lastContextElement.selectionEnd);
		lastContextElement.value = result;
		lastContextElement.dispatchEvent(new Event("input"));
	} else if (info.menuItemId == "addImageCardFromFile") {
		wdw_cardEdition.addImageCardFromFile();
	} else if (info.menuItemId == "pasteImageCard") {
		wdw_cardEdition.pasteImageCard();
	} else if (info.menuItemId == "saveImageCard") {
		await cardbookHTMLImages.saveImageCard();
	} else if (info.menuItemId == "copyImageCard") {
		cardbookHTMLImages.copyImageCard();
	} else if (info.menuItemId == "deleteImageCard") {
		cardbookHTMLImages.deleteImageCard();
	} else if (info.menuItemId == "appendListMenu") {
		wdw_cardEdition.modifyLists(info.menuItemId);
	} else if (info.menuItemId == "deleteListMenu") {
		wdw_cardEdition.modifyLists(info.menuItemId);
	} else if (info.menuItemId == "appendCardEmailsMenu") {
		wdw_cardEdition.modifyLists(info.menuItemId);
	} else if (info.menuItemId.startsWith("appendCardEmailMenu")) {
		let index = info.menuItemId.replace("appendCardEmailMenu", "");
		let action = info.menuItemId.replace(index, "");
		wdw_cardEdition.modifyLists(action, lastemail[parseInt(index)][0][0]);
	} else if (info.menuItemId.startsWith("copyField")) {
		let element = info.menuItemId.replace(/^copyField/, "").toLowerCase();
		let row = lastContextElement.closest("tr");
		let field = element || row.getAttribute(`data-field-name`);
		let index = row.getAttribute(`data-field-index`);
		let label = row.getAttribute(`data-field-label`) || messenger.i18n.getMessage(`${field}Label`);
		await wdw_cardEdition.copyFieldValue(field, index, label);
	} else if (info.menuItemId == "toEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "to", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "ccEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "cc", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "bccEmail") {
		await messenger.runtime.sendMessage({query: "cardbook.emailCards", compFields: [{field: "bcc", value: lastContextElement.textContent}]});
	} else if (info.menuItemId == "findEmails") {
		await messenger.runtime.sendMessage({query: "cardbook.findEmails", card: [], email: [lastContextElement.textContent]});
	} else if (info.menuItemId == "findEvents") {
		await cardbookHTMLUtils.findEvents(null, [lastContextElement.textContent], lastContextElement.textContent);
	} else if (info.menuItemId == "searchForOnlineKey") {
		await messenger.runtime.sendMessage({query: "cardbook.searchForOnlineKey", card: [], email: [lastContextElement.textContent]});
	} else if (info.menuItemId == "openURL") {
		if (lastContextElement.textContent.startsWith("http")) {
			await cardbookHTMLUtils.openURL(lastContextElement.textContent);
		}
	} else if (info.menuItemId == "localizeAdr") {
		let adr = [];
		for (let field of cardbookHTMLUtils.adrElements) {
			let value = lastContextElement.getAttribute(`data-${field}`);
			adr.push(value);
		}
		await cardbookHTMLUtils.localizeCards(null, [adr]);
	} else if (info.menuItemId == "connectIMPP") {
		let row = lastContextElement.closest("tr");
		let index = row.getAttribute("data-field-index");
		let dirPrefId = document.getElementById("dirPrefIdInputText").value;
		let uid = document.getElementById("uidInputText").value;
		let cbid = `${dirPrefId}::${uid}`;
		await messenger.runtime.sendMessage({query: "cardbook.connectIMPP", cbid: cbid, index: index});
	} else if (info.menuItemId == "connectTel") {
		await messenger.runtime.sendMessage({query: "cardbook.connectTel", tel: lastContextElement.textContent});
	}
});
