ChromeUtils.defineESModuleGetters(this, {DeferredTask: "resource://gre/modules/DeferredTask.sys.mjs"});
var { ThreadPaneColumns } = ChromeUtils.importESModule("chrome://messenger/content/ThreadPaneColumns.mjs");
var { ExtensionCommon } = ChromeUtils.importESModule("resource://gre/modules/ExtensionCommon.sys.mjs");
var { EventEmitter } = ChromeUtils.importESModule("resource://gre/modules/EventEmitter.sys.mjs");
  
var { makeWidgetId } = ExtensionCommon;
var customColumns = new Map();

function makeUniqueColumnId(extensionId, columnId) {
    return `${makeWidgetId(extensionId)}-${columnId}`;
}
class CachedCustomColumn {
  constructor(extension, columnId, properties) {

    this.extensionId = extension.id;
    this.columnId = columnId;
    this.uniqueColumnId = makeUniqueColumnId(extension.id, columnId);
    this.extension=extension;

    this.cellData = new Map();
    // The DeferredTask is used to collapse multiple cell update requests
    // into a single request send to the WebExtension, to minimize the
    // number of process boundary crossings.
    this.updateRequest = new DeferredTask(
      () => this.emitDeferredUpdateRequests(),
      50
    );
    this.deferredUpdateRequests = new Set();

    const {
      hidden,
      name: columnName,
      resizable,
      sortable,
    } = properties;

    ThreadPaneColumns.addCustomColumn(this.uniqueColumnId, {
      name: columnName,
      hidden,
      resizable,
      sortable,
      // The sortCallback is used for numeric sort.
      sortCallback:
        sortable == "bySortKey"
          ? msgHdr => this.getCache(msgHdr)?.sort || ""
          : null,
      textCallback: msgHdr => this.getCache(msgHdr)?.text || "",
    });
  }

  getCache(msgHdr) {
    // For unknown messages, we cannot have a cached value. Skip converting
    // unknown messages here for performance reasons.
    const { messageManager } = this.extension;
    const messageHeader = messageManager.convert(msgHdr);
    const messageId = messageHeader.id;


    const cachedData = messageId ? this.cellData.get(messageId) : null;
    // If not cached or cache not updated recently, request update from the
    // WebExtension.
    const now = Date.now();
    if (!cachedData || cachedData.lastUpdated + 2000 < now) {
      this.updateRequest.disarm();
      this.deferredUpdateRequests.add(msgHdr);
      this.updateRequest.arm();
    }

    if (messageId) {
      // Return the cached data for the requested messageId.
      return cachedData;
    }
    return null;

  }
  emitDeferredUpdateRequests() {
    const requests = Array.from(this.deferredUpdateRequests);
    if (requests.length > 0) {
      uiListener.emit("listed-messages-changed", this.uniqueColumnId, requests);
      this.deferredUpdateRequests = new Set();
    }
  }
}

var uiListener = new (class extends EventEmitter {
  constructor() {
    super();
    this.listenerCounts = new Map();
    this.handleEvent = this.handleEvent.bind(this);
    this.lastSelected = new WeakMap();
  }

  handleEvent(event) {
    const browser = event.target.browsingContext.embedderElement;
    const tabmail = browser.ownerGlobal.top.document.getElementById("tabmail");
    const nativeTab = tabmail.tabInfo.find(
      t =>
        t.chromeBrowser == browser ||
        t.chromeBrowser == browser.browsingContext.parent.embedderElement
    );

    if (nativeTab.mode.name != "mail3PaneTab") {
      return;
    }

    const tabId = tabTracker.getId(nativeTab);
    const tab = tabTracker.getTab(tabId);

    if (event.type == "folderURIChanged") {
      const folderURI = event.detail;
      const folder = MailServices.folderLookup.getFolderForURL(folderURI);
      if (this.lastSelected.get(tab) == folder) {
        return;
      }
      this.lastSelected.set(tab, folder);
      this.emit("folder-changed", tab, folder);
      //if (this.has("listed-messages-changed")) {
      //  this.emit("listed-messages-changed", []);
      //}
    } else if (event.type == "messageURIChanged") {
      const messages =
        nativeTab.chromeBrowser.contentWindow.gDBView?.getSelectedMsgHdrs();
      if (messages) {
        this.emit("messages-changed", tab, messages);
      }
    }
  }

  has(name) {
    return !!(this.listenerCounts.get(name) || 0);
  }

  on(name, listener) {
    this._incrementListeners(name);
    return super.on(name, listener);
  }

  off(name, listener) {
    this._decrementListeners(name);
    return super.off(name, listener);
  }

  _incrementListeners(name) {
    let counts = this.listenerCounts.get(name) || 0;
    if (++counts == 1) {
      switch (name) {
        case "folder-changed":
          this.lastSelected = new WeakMap();
          windowTracker.addListener("folderURIChanged", this);
          break;
        case "messages-changed":
          windowTracker.addListener("messageURIChanged", this);
          break;
      }
    }
    this.listenerCounts.set(name, counts);
  }
  _decrementListeners(name) {
    let counts = this.listenerCounts.get(name);
    if (--counts == 0) {
      switch (name) {
        case "folder-changed":
          this.lastSelected = new WeakMap();
          windowTracker.removeListener("folderURIChanged", this);
          break;
        case "messages-changed":
          windowTracker.removeListener("messageURIChanged", this);
          break;
      }
    }
    this.listenerCounts.set(name, counts);
  }

})();

var cc = class extends ExtensionAPIPersistent {
  PERSISTENT_EVENTS = {
    // For primed persistent events (deactivated background), the context is only
    // available after fire.wakeup() has fulfilled (ensuring the convert() function
    // has been called).

    onCellEntriesShown({ context, fire }) {
      const { extension } = this;
      const { messageManager } = extension;

      async function listener(event, uniqueColumnId, msgHdrs) {
        if (fire.wakeup) {
          await fire.wakeup();
        }
        const customColumn = customColumns.get(uniqueColumnId);
        if (customColumn && customColumn.extensionId == extension.id) {
          const entries = [];
          for (const msgHdr of msgHdrs) {   //nsIMsgDBHdr

            //TB115 only:
            //At least for IMAP, when a new message arrives in the currently opened folder,
            //  (the Sent folder) this listener fires two times.
            //The first time immediately after sending the message. In this case, we get
            //  a completely empty msgHdr. The message is not yet vissible in the folder
            //The second time, when the message has been loaded from the server and is visible
            //  in the folder
            if (!msgHdr.messageId) return;  //Ignore the first case

            const messageHeader = messageManager.convert(msgHdr);
            entries.push({
              messageHeader,
              cachedData: customColumn.cellData.get(messageHeader.id),
            });
          }
          fire.sync(customColumn.columnId, entries);
        }
      }

      uiListener.on("listed-messages-changed", listener);
      return {
        unregister: () => {
          uiListener.off("listed-messages-changed", listener);
        },
        convert(newFire, extContext) {
          fire = newFire;
          context = extContext;
        },
      };

    }
  };
  getAPI(context) {
    // mailnews.customHeaders;X-Spam-Status
    context.callOnClose(this);
    return {
      cc: {
        async addColumn(columnId, properties) {
          const customColumn = new CachedCustomColumn(context.extension, columnId, properties);
          customColumns.set(customColumn.uniqueColumnId, customColumn);
        },

        async removeColumn(columnId) {
          const uniqueColumnId = makeUniqueColumnId(context.extension.id, columnId);
          const customColumn = customColumns.get(uniqueColumnId);
          if (!customColumn) {
            throw new ExtensionError(`Unknown custom column id: ${columnId}`);
          }
          ThreadPaneColumns.removeCustomColumn(uniqueColumnId);
          customColumns.delete(uniqueColumnId);
        },

        async setCellData(columnId, entries) {
            const uniqueColumnId = makeUniqueColumnId(context.extension.id, columnId);
            const customColumn = customColumns.get(uniqueColumnId);
          if (!customColumn) {
            throw new ExtensionError(`Unknown custom column id: ${columnId}`);
          }

          let modified = false;
          const lastUpdated = Date.now();
          for (const { messageId, data: updatedData } of entries) {
            const cachedData = customColumn.cellData.get(messageId);
            customColumn.cellData.set(messageId, {
              ...updatedData,
              lastUpdated,
            });

            if (modified) {
              continue;
            }
            modified =
              !cachedData ||
              cachedData.text != updatedData.text ||
              cachedData.sort != updatedData.sort;
          }
          if (modified) {
            ThreadPaneColumns.refreshCustomColumn(uniqueColumnId);
          }
        },

        onCellEntriesShown: new ExtensionCommon.EventManager({
          context,
          module: "cc",
          event: "onCellEntriesShown",
          extensionApi: this,
        }).api(),
      },
    };
  }

  close() {
    customColumns.forEach((v, k) => {
      ThreadPaneColumns.removeCustomColumn(k);
      customColumns.delete(k);
    })
  }
};
