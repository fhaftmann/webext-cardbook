export var cardbookBGPreferences = {

	allPrefs: {},
	prefCardBookData: "data.",
	prefCardBookTels: "tels.",
	prefCardBookIMPPs: "impps.",
	prefCardBookCustomFields: "customFields.",
	prefCardBookAccountVCards: "vcards.",
	prefCardBookAccountRestrictions: "accountsRestrictions.",
	prefCardBookEmailsCollection: "emailsCollection.",
	prefCardBookCustomTypes: "customTypes.",
	changedPrefs: {
		"addonVersion": "97.7",
	},
	defaultPrefs: {
		"autocompletion": true,
		"autocompleteSortByPopularity": true,
		"proposeConcatEmails": false,
		"autocompleteShowAddressbook": false,
		"autocompleteShowEmailType": false,
		"autocompleteShowPopularity": false,
		"autocompleteWithColor": true,
		"autocompleteRestrictSearch": false,
		"autocompleteRestrictSearchFields": "firstname|lastname",
		"useColor": "background",
		"exclusive": false,
		"requestsTimeout": "120",
		"statusInformationLineNumber": "250",
		"debugMode": false,
		"preferEmailEdition": true,
		"listTabView": true,
		"technicalTabView": false,
		"vcardTabView": false,
		"keyTabView": false,
		"panesView": "modern",
		"syncAfterChange": true,
		"initialSync": true,
		"initialSyncDelay": "0",
		"solveConflicts": "User",
		"multiget": "100",
		"maxModifsPushed": "100",
		"decodeReport": true,
		"preferEmailPref": true,
		"preferIMPPPref": true,
		"warnEmptyEmails": true,
		"useOnlyEmail": false,
		"fieldsNameList": "allFields",
		"fieldsNameListUpdate1": true,
		"fieldsNameListUpdate2": true,
		"fieldsNameListUpdate3": false,
		"autoComputeFn": true,
		"usePreferenceValue": false,
		"firstRun": true,
		"kindCustom": "X-ADDRESSBOOKSERVER-KIND",
		"memberCustom": "X-ADDRESSBOOKSERVER-MEMBER",
		"localizeEngine": "OpenStreetMap",
		"localizeTarget": "out",
		"showNameAs": "DSP",
		"adrFormula": messenger.i18n.getMessage("addressFormatFormula"),
		"defaultFnFormula": "({{1}} |)({{2}} |)({{3}} |)({{4}} |)({{5}} |)({{6}} |)({{7}}|)",
		"dateDisplayedFormat": "0",
		"addressBooksNameList": "allAddressBooks",
		"birthday.bday": true,
		"birthday.anniversary": true,
		"birthday.deathdate": true,
		"birthday.events": true,
		"numberOfDaysForSearching": "30",
		"showPopupOnStartup": false,
		"showPeriodicPopup": false,
		"periodicPopupIime": "08:00",
		"showPopupEvenIfNoBirthday": true,
		"syncWithLightningOnStartup": false,
		"numberOfDaysForWriting": "365",
		"eventEntryTitle": messenger.i18n.getMessage("eventEntryTitleMessage"),
		"calendarEntryCategories": messenger.i18n.getMessage("anniversaryCategory"),
		"eventEntryTime": "00:00",
		"repeatingEvent": true,
		"eventEntryWholeDay": false,
		"calendarEntryAlarm": "168",
		"viewABPane": true,
		"viewABContact": true,
		"accountsShown": "all",
		"defaultRegion": "NOTSET",
		"localDataEncryption": false,
		"localDataEncryption.validatedVersion": "",
		"localDataEncryption.counter": "",
		"uncategorizedCards": messenger.i18n.getMessage("uncategorizedCards"),
		"maxUndoChanges": "100",
		"currentUndoId": "0",
		"setupCardDAVAccounts": "",
		"URLPhoneURL": "",
		"URLPhoneUser": "",
		"URLPhoneBackground": false,
		"cardbookToolbar.currentset": "",
		"cardbookToolbar.mode": "",
		"discoveryAccountsNameList": "",
		"preferenceValueLabel": "",
		"orgStructure": [],
		"orgStructureMigrated": false,
		"calendarsNameList": "",
		"accountShown": "",
		"supportedVersion": ["3.0", "4.0"],
		"prefs.lastSelected": "cardbook-generalPane",
		"searchAllAB": "oneAB",
		"cardbookAccountsTreeWidth": "300",
		"cardbookCardsTreeWidth": "500",
		"cardbookCardsTreeHeight": "500",
		"exportDelimiter": ";",
		"availableCardsTreeDefaultColumns": "cardIcon,fn:150",
		"availableCardsTreeSortResource": "fn",
		"availableCardsTreeSortDirection": "ascending",
		"addedCardsTreeDefaultColumns": "cardIcon,fn:150",
		"addedCardsTreeSortResource": "fn",
		"addedCardsTreeSortDirection": "ascending",
		"addedCardsTreeDefaultColumns": "cardIcon,fn:150",
		"cardbookCardsTreeDefaultColumns": "cardIcon,fn:150,email_0_all:200,tel_0_all:200,bday:100,rev:150",
		"cardbookCardsTreeSortResource": "fn",
		"cardbookCardsTreeSortDirection": "ascending",
		"defaultSyncInterval": "60",
		"defaultToolbarItems": "layout,sync,email,prefs,undo,redo,flex,addcontact,addlist,edit,delete",
		"toolbarMode": "iconsBesideText",
		"indexedDB.cardsMigrated": false,
		"indexedDB.cardsMigrated2": false,
		"indexedDB.cardsMigrated3": false,
		"indexedDB.catsMigrated": false,
		"indexedDB.duplicatesMigrated": false,
		"indexedDB.imagesMigrated": false,
		"indexedDB.mailpopsMigrated": false,
		"indexedDB.prefDispNamesMigrated": false,
		"indexedDB.searchesMigrated": false,
		"indexedDB.undosMigrated": false,
		"addressbooksSplitter-width": "300",
		"cardsSplitter-width": "300",
		"cardsSplitter-height": "200",
		"categoryColors": "{}",
		"window.wdw_cardbookConfigurationAddCustomField.html.state": {"width": 500, "height": 300},
		"window.wdw_csvTranslator.html.state": {"width": 700, "height": 600},
		"window.wdw_cardbookConfigurationEditField.html.state": {"width": 600, "height": 500},
		"window.wdw_cardbookConfigurationAddVcards.html.state": {"width": 600, "height": 500},
		"window.wdw_cardbookConfigurationAddEmails.html.state": {"width": 600, "height": 400},
		"window.wdw_cardbookConfigurationRenameField.html.state": {"width": 500, "height": 300},
		"window.wdw_cardbookConfigurationAddIMPP.html.state": {"width": 600, "height": 300},
		"window.wdw_cardbookConfiguration.html.state": {"width": 600, "height": 500},
		"window.wdw_addressbooksAdd.html.state": {"width": 900, "height": 800},
		"window.wdw_formatData.html.state": {"width": 900, "height": 600},
		"window.wdw_logEdition.html.state": {"width": 900, "height": 600},
		"window.wdw_cardbookRenameField.html.state": {"width": 400, "height": 100},
		"window.wdw_cardbookAskUser.html.state": {"width": 900, "height": 300},
		"window.wdw_bulkOperation.html.state": {"width": 400, "height": 200},
		"window.wdw_birthdayList.html.state": {"width": 600, "height": 500},
		"window.wdw_birthdaySync.html.state": {"width": 400, "height": 300},
		"window.wdw_cardbookContactEvents.html.state": {"width": 600, "height": 500},
		"window.wdw_cardEdition.html.state": {"width": 600, "height": 700},
		"window.wdw_cardbookAlertUser.html.state": {"width": 500, "height": 300},
		"window.wdw_cardbookAskPassword.html.state": {"width": 700, "height": 300},
		"window.wdw_cardbookApplyColumns.html.state": {"width": 600, "height": 500},
	},

	get complexPrefs() {
		let result = {};
		let lang = navigator.language;
		lang = lang.toUpperCase();
		
		result["noteDateFormat"] = lang.startsWith("FR") ? "YYYY-MM-DD : " : "YYYY-MM-DD: ";
		return result;
	},

	initPrefs: async function () {
		let data = await browser.storage.local.get();
		for (const [key, value] of Object.entries(data)) {
			cardbookBGPreferences.allPrefs[key] = value;
		}
		for (const [key, value] of Object.entries(this.defaultPrefs)) {
			if ("undefined" == typeof(data[key])) {
				await this.setPref(key, value);
				cardbookBGPreferences.allPrefs[key] = value;
			}
		}
		for (const [key, value] of Object.entries(this.complexPrefs)) {
			if ("undefined" == typeof(data[key])) {
				await this.setPref(key, value);
				cardbookBGPreferences.allPrefs[key] = value;
			}
		}
		for (const [key, value] of Object.entries(this.changedPrefs)) {
			await this.setPref(key, value);
			cardbookBGPreferences.allPrefs[key] = value;
		}
	},

	loadPrefs: async function () {
		let data = await browser.storage.local.get();
		for (const [key, value] of Object.entries(data)) {
			cardbookBGPreferences.allPrefs[key] = value;
		}
	},
	
	removePrefs: async function (keys) {
		await browser.storage.local.remove(keys);
	},

	clear: async function () {
		await browser.storage.local.clear();
	},

	getAllPrefs: function () {
		return cardbookBGPreferences.allPrefs;
	},

	getPref: function (key) {
		return cardbookBGPreferences.allPrefs[key];
	},

	setPref: async function (key, value) {
		cardbookBGPreferences.allPrefs[key] = value;
		await browser.storage.local.set({ [key]: value});
	},

	getBranch: function (aStartingPoint) {
		let childList = this.getAllPrefs();
		let values = [];
		for (const [key, value] of Object.entries(childList)) {
			if (key.startsWith(aStartingPoint)) {
				values.push([key, value]);
			}
		}
		return values;
	},

	getBranchKeys: function (aStartingPoint) {
		let childList = this.getAllPrefs();
		let keys = [];
		for (const [key, value] of Object.entries(childList)) {
			if (key.startsWith(aStartingPoint)) {
				keys.push(key);
			}
		}
		return keys;
	},

	delBranch: async function (aStartingPoint) {
		let keys = this.getBranchKeys(aStartingPoint);
		await this.removePrefs(keys);
	},

	sortMultipleArrayByNumber: function (aArray, aIndex, aInvert) {
		function compare1(a, b) { return (a[aIndex] - b[aIndex])*aInvert; };
		function compare2(a, b) { return (a - b)*aInvert; };
		if (aIndex != -1) {
			return aArray.sort(compare1);
		} else {
			return aArray.sort(compare2);
		}
	},

	// return { pers: [], personal: [ [ "X-CUSTOM2", "Custom2", 0 ] ], org: [] }
	getAllCustomFields: function () {
		var finalResult = {};
		// to delete pers
		for (let type of [ "pers", "personal", "org" ]) {
			finalResult[type] = [];
			let search = `${this.prefCardBookCustomFields}${type}.`;
			let childList = this.getBranch(search);
			for (let result of childList) {
				let rank = result[0].replace(search, "");
				let values = result[1].split(":");
				finalResult[type].push([values[0], values[1], parseInt(rank)]);
			}
			this.sortMultipleArrayByNumber(finalResult[type],2,1);
		}
		return finalResult;
	},

	getCustomFields: function (aType) {
		let value = this.getPref(`${this.prefCardBookCustomFields}${aType}`);
		return value;
	},

	setCustomFields: async function (aType, aField, aValue) {
		await this.setPref(`${this.prefCardBookCustomFields}${aType}.${aField}`, aValue);
	},

	// return Array [ [ "true", "allMailAccounts", "fe6bbe94-4114-45b0-a596-37082163b7db", "1a4fb09f-7df4-4fc5-995a-b7262de591dd", "file.vcf" ], ]
	getAllVCards: function () {
		var finalResult = [];
		let search = this.prefCardBookAccountVCards;
		let childList = this.getBranch(search);
		for (let result of childList) {
			finalResult.push(result[1].split("::"));
		}
		return finalResult;
	},

	delVCards: async function (aVCardId) {
		if (aVCardId) {
			await this.delBranch(`${this.prefCardBookAccountVCards}${aVCardId}`);
		} else {
			await this.delBranch(this.prefCardBookAccountVCards);
		}
	},

	setVCard: async function (aVCardId, aVCardValue) {
		await this.setPref(`${this.prefCardBookAccountVCards}${aVCardId}`, aVCardValue);
	},

	// return Array [ [ "true", "exclude", "allMailAccounts", "fe6bbe94-4114-45b0-a596-37082163b7db", "cat" ], ]
	getAllRestrictions: function () {
		var finalResult = [];
		let search = this.prefCardBookAccountRestrictions;
		let childList = this.getBranch(search);
		for (let result of childList) {
			finalResult.push(result[1].split("::"));
		}
		return finalResult;
	},

	delRestrictions: async function (aRestrictionId) {
		if (aRestrictionId) {
			await this.delBranch(`${this.prefCardBookAccountRestrictions}${aRestrictionId}`);
		} else {
			await this.delBranch(this.prefCardBookAccountRestrictions);
		}
	},

	setRestriction: async function (aRestrictionId, aRestrictionValue) {
		await this.setPref(`${this.prefCardBookAccountRestrictions}${aRestrictionId}`, aRestrictionValue);
	},

	// return Array [ [ "true", "allMailAccounts", "fe6bbe94-4114-45b0-a596-37082163b7db", "cat" ], ]
	getAllEmailsCollections: function () {
		var finalResult = [];
		let search = this.prefCardBookEmailsCollection;
		let childList = this.getBranch(search);
		for (let result of childList) {
			finalResult.push(result[1].split("::"));
		}
		return finalResult;
	},

	delEmailsCollection: async function (aRestrictionId) {
		if (aRestrictionId) {
			await this.delBranch(`${this.prefCardBookEmailsCollection}${aRestrictionId}`);
		} else {
			await this.delBranch(this.prefCardBookEmailsCollection);
		}
	},

	setEmailsCollection: async function (aRestrictionId, aRestrictionValue) {
		await this.setPref(`${this.prefCardBookEmailsCollection}${aRestrictionId}`, aRestrictionValue);
	},

	insertIMPPsSeed: async function () {
		await this.setIMPPs(0,"skype:" + messenger.i18n.getMessage("impp.skype") + ":skype");
		await this.setIMPPs(1,"jabber:" + messenger.i18n.getMessage("impp.jabber") + ":xmpp");
		await this.setIMPPs(2,"googletalk:" + messenger.i18n.getMessage("impp.googletalk") + ":gtalk");
		await this.setIMPPs(3,"qq:" + messenger.i18n.getMessage("impp.qq") + ":qq");
		await this.setIMPPs(4,"jami:" + messenger.i18n.getMessage("impp.jami") + ":jami");
	},

	// return Array [ [ "imppcode", "impplabel", "imppprot", 0 ], ]
	getAllIMPPs: function () {
		var finalResult = [];
		let search = this.prefCardBookIMPPs;
		let childList = this.getBranch(search);
		for (let result of childList) {
			let rank = result[0].replace(search, "");
			let values = result[1].split(":");
			finalResult.push([values[0], values[1], values[2], parseInt(rank)]);
		}
		this.sortMultipleArrayByNumber(finalResult,3,1);
		return finalResult;
	},

	setIMPPs: async function (prefName, value) {
		await this.setPref(`${this.prefCardBookIMPPs}${prefName}`, value);
	},

	// return [ [ "telcode", "tellabel", "telprot", 0 ], ]
	getAllTels: function () {
		var finalResult = [];
		let search = this.prefCardBookTels;
		let childList = this.getBranch(search);
		for (let result of childList) {
			let rank = result[0].replace(search, "");
			let values = result[1].split(":");
			finalResult.push([values[0], values[1], values[2], parseInt(rank)]);
		}
		this.sortMultipleArrayByNumber(finalResult,3,1);
		return finalResult;
	},

	getAllPrefIds: function () {
		let finalResult = [];
		let search = this.prefCardBookData;
		let childList = this.getBranch(search);
		for (let result of childList) {
			let prop = result[0].replace(search, "");
			let tmpArray = prop.split(".");
			if (tmpArray[1] == "id") {
				finalResult.push(result[1]);
			}
		}
		return finalResult;
	},

	getABProperty: function (aDirPrefId, aProperty) {
		return cardbookBGPreferences.allPrefs[`${this.prefCardBookData}${aDirPrefId}.${aProperty}`];
	},

	setABProperty: async function (aDirPrefId, aProperty, aValue) {
		await this.setPref(`${this.prefCardBookData}${aDirPrefId}.${aProperty}`, aValue);
	},

	setId: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "id", value);
	},

	getName: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "name");
	},

	setName: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "name", value);
	},

	getType: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "type");
	},

	setType: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "type", value);
	},

	getEnabled: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "enabled");
	},

	setEnabled: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "enabled", value);
	},

	getVCardVersion: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "vCard");
	},

	setVCardVersion: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "vCard", value);
	},

	getDateFormat: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "dateFormat");
	},

	setDateFormat: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "dateFormat", value);
	},

	getColor: function (aDirPrefId) {
		let color = this.getABProperty(aDirPrefId, "color");
		if (color) {
			return color;
		} else {
			return "#A8C2E1";
		}
	},

	setColor: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "color", value);
	},

	getUrl: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "url");
	},

	setUrl: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "url", value);
	},

	getUser: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "user");
	},

	setUser: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "user", value);
	},

	getReadOnly: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "readonly");
	},

	setReadOnly: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "readonly", value);
	},

	getUrnuuid: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "urnuuid");
	},

	setUrnuuid: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "urnuuid", value);
	},

	getDBCached: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "DBcached");
	},

	setDBCached: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "DBcached", value);
	},

	getAutoSyncEnabled: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "autoSyncEnabled");
	},

	setAutoSyncEnabled: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "autoSyncEnabled", value);
	},

	getLastSync: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "lastsync");
	},

	setLastSync: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "lastsync", value);
	},

	getSourceId: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "sourceId");
	},

	setSourceId: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "sourceId", value);
	},

	getSyncFailed: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "syncfailed");
	},

	setSyncFailed: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "syncfailed", value);
	},
	
	getExpanded: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "expanded");
	},

	setExpanded: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "expanded", value);
	},

	getLastSearch: function (aDirPrefId) {
		return this.getABProperty(aDirPrefId, "lastSearch");
	},

	setLastSearch: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "lastSearch", value);
	},

	getSortDirection: function (aDirPrefId) {
		if (aDirPrefId) {
			let sortDirection = this.getABProperty(aDirPrefId, "sortDirection");
			if (sortDirection) {
				return sortDirection;
			} else {
				return this.getPref("cardbookCardsTreeSortDirection");
			}
		} else {
			return this.getPref("cardbookCardsTreeSortDirection");
		}
	},

	setSortDirection: async function (aDirPrefId, value) {
		if (value) {
			await this.setABProperty(aDirPrefId, "sortDirection", value);
		}
	},

	getSortResource: function (aDirPrefId) {
		if (aDirPrefId) {
			let sortResource = this.getABProperty(aDirPrefId, "sortResource");
			if (sortResource) {
				return sortResource;
			} else {
				return this.getPref("cardbookCardsTreeSortResource");
			}
		} else {
			return this.getPref("cardbookCardsTreeSortResource");
		}
	},

	setSortResource: async function (aDirPrefId, value) {
		if (value) {
			await this.setABProperty(aDirPrefId, "sortResource", value);
		}
	},

	getDisplayedColumns: function (aDirPrefId) {
		if (aDirPrefId) {
			let displayedColumns = this.getABProperty(aDirPrefId, "displayedColumns");
			if (displayedColumns) {
				return displayedColumns;
			} else {
				return this.getPref("cardbookCardsTreeDefaultColumns");
			}
		} else {
			return this.getPref("cardbookCardsTreeDefaultColumns");
		}
	},

	setDisplayedColumns: async function (aDirPrefId, value) {
		if (value) {
			await this.setABProperty(aDirPrefId, "displayedColumns", value);
		}
	},

	getAutoSyncInterval: function (aDirPrefId) {
		let autoSyncInterval = this.getABProperty(aDirPrefId, "autoSyncInterval");
		if (autoSyncInterval) {
			return autoSyncInterval;
		} else {
			return this.getPref("defaultSyncInterval");
		}
	},
	
	setAutoSyncInterval: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "autoSyncInterval", value);
	},

	getToolbarItems: function () {
		let toolbarItems = this.getPref("toolbarItems");
		if (toolbarItems) {
			return toolbarItems;
		} else {
			return this.getPref("defaultToolbarItems");
		}
	},

	setToolbarItems: async function (value) {
		if (value) {
			await this.setPref("toolbarItems", value);
		}
	},

	getNode: function (aDirPrefId) {
		let node = this.getABProperty(aDirPrefId, "node");
		if (node) {
			return node;
		} else {
			return "categories";
		}
	},

	setNode: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "node", value);
	},

	getDefaultFnFormula: function () {
		return cardbookBGPreferences.allPrefs["defaultFnFormula"];
	},

	getFnFormula: function (aDirPrefId) {
		if (aDirPrefId) {
			let fnFormula = this.getABProperty(aDirPrefId, "fnFormula");
			if (fnFormula) {
				return fnFormula;
			} else {
				return this.getDefaultFnFormula();
			}
		} else {
			return this.getDefaultFnFormula();
		}
	},

	setFnFormula: async function (aDirPrefId, value) {
		await this.setABProperty(aDirPrefId, "fnFormula", value);
	},

	// return  [ "c35db08b-3bd9-467b-a8d3-cc4051c97671",  ]
	getAllComplexSearchIds: function () {
		let finalResult = [];
		let search = this.prefCardBookData;
		let childList = this.getBranch(search);
		for (let result of childList) {
			let prop = result[0].replace(search, "");
			let tmpArray = prop.split(".");
			if (tmpArray[1] == "type" && result[1] == "SEARCH") {
				finalResult.push(tmpArray[0]);
			}
		}
		return finalResult;
	},

	getDiscoveryAccounts: function () {
		let finalResult = [];
		let tmpResult1 = [];
		let tmpResult2 = [];
		let tmpValue = this.getPref("discoveryAccountsNameList");
		if (tmpValue && tmpValue != "") {
			tmpResult1 = tmpValue.split(",");
			for (var i = 0; i < tmpResult1.length; i++) {
				tmpResult2 = tmpResult1[i].split("::");
				finalResult.push([tmpResult2[1], tmpResult2[0]]);
			}
		}
		return finalResult;
	},

	getEditionFields: function () {
		try {
			let fields = this.getPref("fieldsNameList");
			if (fields == "allFields") {
				return fields;
			} else {
				return JSON.parse(fields);
			}
		}
		catch(e) {
			return "allFields";
		}
	},

	getMaxModifsPushed: function () {
		let maxModifsPushed = this.getPref("maxModifsPushed");
		if (maxModifsPushed) {
			return maxModifsPushed;
		} else {
			return "60";
		}
	},

	getNodeColors: function () {
		let categoryColors = this.getPref("categoryColors");
		if (categoryColors) {
			return JSON.parse(categoryColors);
		} else {
			return {};
		}
	},

	setNodeColors: async function (value) {
		await this.setPref("categoryColors", JSON.stringify(value));
	},

	delAccount: function (aDirPrefId) {
		this.delBranch(this.prefCardBookData + aDirPrefId);
	},
};

await cardbookBGPreferences.loadPrefs();

browser.storage.local.onChanged.addListener((info => {
	for (const [key, value] of Object.entries(info)) {
		if (value.oldValue && value.newValue) {
			cardbookBGPreferences.allPrefs[key] = value.newValue;
		} else if (value.oldValue) {
			delete cardbookBGPreferences.allPrefs[key];
		} else {
			cardbookBGPreferences.allPrefs[key] =  value.newValue;
		}
	}
}));
