import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGSynchronizationUtils } from "./cardbookBGSynchronizationUtils.mjs";

import { cardbookBGActions } from "./cardbookBGActions.mjs";
import { cardbookIDBCard } from "../indexedDB/cardbookIDBCard.mjs";
import { cardbookIDBCat } from "../indexedDB/cardbookIDBCat.mjs";
import { cardbookIDBImage } from "../indexedDB/cardbookIDBImage.mjs";

import { cardbookCategoryParser } from "./cardbookCategoryParser.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";

export var cardbookBGRepo = {
	userAgent: "",
	prodid: "",

	cardbookAccounts: [],
	cardbookAccountsCategories: {},
	cardbookCards: {},
	cardbookCategories: {},
	cardbookDisplayCards: {},
	cardbookCardLongSearch: {},
	cardbookCardShortSearch: {},
	cardbookCardEmails: {},
	cardbookFileCacheCards: {},
	cardbookFileCacheCategories: {},
	cardbookComplexSearch: {},
	cardbookTempCards: {},

	cardbookMailPopularityIndex: {},
	cardbookMailPopularityLastIndex: 1,

	cardbookPreferDisplayNameIndex: {},
	cardbookPreferDisplayNameLastIndex: 1,

	cardbookCardsFromCache: {},
	cardbookCategoriesFromCache: {},
	
	cardbookServerValidation: {},

	cardbookAccessTokenRequest: {},
	cardbookAccessTokenResponse: {},
	cardbookAccessTokenError: {},
	cardbookRefreshTokenRequest: {},
	cardbookRefreshTokenResponse: {},
	cardbookRefreshTokenError: {},
	cardbookServerDiscoveryRequest: {},
	cardbookServerDiscoveryResponse: {},
	cardbookServerDiscoveryError: {},
	cardbookServerSyncMerge: {},
	cardbookServerSyncRequest: {},
	cardbookServerSyncResponse: {},
	cardbookServerCardSyncDone: {},
	cardbookServerCardSyncTotal: {},
	cardbookServerCardSyncError: {},
	cardbookServerCatSyncDone: {},
	cardbookServerCatSyncTotal: {},
	cardbookServerCatSyncError: {},
	cardbookServerSyncNotUpdatedCard: {},
	cardbookServerSyncNotUpdatedCat: {},
	cardbookServerSyncNewCardOnServer: {},
	cardbookServerSyncNewCatOnServer: {},
	cardbookServerSyncNewCardOnDisk: {},
	cardbookServerSyncNewCatOnDisk: {},
	cardbookServerSyncUpdatedCardOnServer: {},
	cardbookServerSyncUpdatedCatOnServer: {},
	cardbookServerSyncUpdatedCardOnDisk: {},
	cardbookServerSyncUpdatedCatOnDisk: {},
	cardbookServerSyncUpdatedCardOnBoth: {},
	cardbookServerSyncUpdatedCatOnBoth: {},
	cardbookServerSyncUpdatedCardOnDiskDeletedCardOnServer: {},
	cardbookServerSyncUpdatedCatOnDiskDeletedCatOnServer: {},
	cardbookServerSyncDeletedCardOnDisk: {},
	cardbookServerSyncDeletedCatOnDisk: {},
	cardbookServerSyncDeletedCardOnDiskUpdatedCardOnServer: {},
	cardbookServerSyncDeletedCatOnDiskUpdatedCatOnServer: {},
	cardbookServerSyncDeletedCardOnServer: {},
	cardbookServerSyncDeletedCatOnServer: {},
	cardbookServerSyncAgain: {},
	cardbookServerSyncCompareCardWithCacheDone: {},
	cardbookServerSyncCompareCardWithCacheTotal: {},
	cardbookServerSyncCompareCatWithCacheDone: {},
	cardbookServerSyncCompareCatWithCacheTotal: {},
	cardbookServerSyncHandleRemainingCardDone: {},
	cardbookServerSyncHandleRemainingCardTotal: {},
	cardbookServerSyncHandleRemainingCatDone: {},
	cardbookServerSyncHandleRemainingCatTotal: {},
	cardbookServerGetCardRequest: {},
	cardbookServerGetCardResponse: {},
	cardbookServerGetCardError: {},
	cardbookServerGetCardForMergeRequest: {},
	cardbookServerGetCardForMergeResponse: {},
	cardbookServerGetCardForMergeError: {},
	cardbookServerMultiGetArray: {},
	cardbookServerMultiGetGoogleArray: {},
	cardbookServerMultiPutGoogleArray: {},
	cardbookServerSyncParams: {},
	cardbookServerMultiGetRequest: {},
	cardbookServerMultiGetResponse: {},
	cardbookServerMultiGetError: {},
	cardbookServerUpdatedCardRequest: {},
	cardbookServerUpdatedCardResponse: {},
	cardbookServerUpdatedCardError: {},
	cardbookServerGetCardPhotoRequest: {},
	cardbookServerGetCardPhotoResponse: {},
	cardbookServerGetCardPhotoError: {},
	cardbookServerUpdatedCardPhotoRequest: {},
	cardbookServerUpdatedCardPhotoResponse: {},
	cardbookServerUpdatedCardPhotoError: {},
	cardbookServerUpdatedCatRequest: {},
	cardbookServerUpdatedCatResponse: {},
	cardbookServerUpdatedCatError: {},
	cardbookServerCreatedCardRequest: {},
	cardbookServerCreatedCardResponse: {},
	cardbookServerCreatedCardError: {},
	cardbookServerCreatedCatRequest: {},
	cardbookServerCreatedCatResponse: {},
	cardbookServerCreatedCatError: {},
	cardbookServerDeletedCardRequest: {},
	cardbookServerDeletedCardResponse: {},
	cardbookServerDeletedCardError: {},
	cardbookServerDeletedCatRequest: {},
	cardbookServerDeletedCatResponse: {},
	cardbookServerDeletedCatError: {},
	cardbookImageGetRequest: {},
	cardbookImageGetResponse: {},
	cardbookImageGetError: {},
	cardbookSyncMode: {},
	cardbookServerNotPushed: {},
	// used to remember the choice of overriding or not cards
	// while importing, dragging, copying or duplicating
	importConflictChoice: {},
	importConflictChoiceImportValues: ["keep", "update", "duplicate", "merge"],
	importConflictChoiceSync1Values: ["keep", "delete"],
	importConflictChoiceSync2Values: ["local", "remote", "merge"],
	importConflictChoiceSync3Values: ["local", "remote"],
	//used to get a new password
	askPasswordChoice: {},

	cardbookServerChangedPwd: {},
	
	displayedIds: {},

	birthdayTimerCreated: false,
	autoSync: {},
	autoSyncInterval: {},
	autoSyncId: {},

	lTimerLoadCacheAll: {},
	lTimerSyncAll: {},
	lComplexSearchAll: {},
	lTimerNoSyncModeAll: {},
	lTimerNewRefreshTokenAll: {},
	
	// used to ensure that the initial load is done only once
	firstLoad: false,
	filtersInitialized: false,

	statusInformation: [],

	supportedConnections:[
							{ id: "GOOGLE2", type: "GOOGLE2", url: [ ], vCard: [ "3.0" ], email: "true", nopwd: "true"},
							{ id: "GOOGLE3", type: "GOOGLE3", url: [ ], vCard: [ "3.0" ], email: "true", nopwd: "true"},
							{ id: "CARDDAV", type: "CARDDAV", url: [ ]},
							{ id: "APPLE", type: "APPLE", url: [ "https://contacts.icloud.com" ], vCard: [ "3.0" ], pwdapp: "true"},
							{ id: "YAHOO", type: "YAHOO", url: [ "https://carddav.address.yahoo.com" ], vCard: [ "3.0" ], pwdapp: "true"},
							{ id: "AOL.COM", type: "CARDDAV", url: [ "https://carddav.aol.com" ], vCard: [ "3.0" ], pwdapp: "true"},
							{ id: "ECLIPSO", type: "CARDDAV", url: [ "https://www.eclipso.de" ], vCard: [ "3.0" ]},
							{ id: "FASTMAIL", type: "CARDDAV", url: [ "https://carddav.fastmail.com" ], vCard: [ "3.0" ], pwdapp: "true"},
							{ id: "FRUUX", type: "CARDDAV", url: [ "https://dav.fruux.com" ], vCard: [ "3.0" ], pwdapp: "false"},
							{ id: "GMX", type: "CARDDAV", url: [ "https://carddav.gmx.net", "https://carddav.gmx.com" ], vCard: [ "3.0" ]},
							{ id: "LAPOSTE.NET", type: "CARDDAV", url: [ "https://webmail.laposte.net/dav/%EMAILADDRESS%/contacts" ], vCard: [ "3.0" ]},
							{ id: "LIBERTA.VIP", type: "CARDDAV", url: [ "https://cloud.liberta.vip/" ], vCard: [ "3.0" ], pwdapp: "false"},
							{ id: "MAIL.DE", type: "CARDDAV", url: [ "https://adressbuch.mail.de" ], vCard: [ "3.0" ]},
							{ id: "MAILBOX.ORG", type: "CARDDAV", url: [ "https://dav.mailbox.org" ], vCard: [ "3.0" ]},
							{ id: "MAILFENCE.COM", type: "CARDDAV", url: [ "https://mailfence.com" ], vCard: [ "3.0" ]},
							{ id: "MYKOLAB.COM", type: "CARDDAV", url: [ "https://carddav.mykolab.com" ], vCard: [ "3.0" ]},
							{ id: "ORANGE.FR", type: "CARDDAV", url: [ "https://carddav.orange.fr/addressbooks/%EMAILADDRESS%" ], vCard: [ "3.0" ]},
							{ id: "POLARISMAIL", type: "CARDDAV", url: [ "https://%EMAILDOMAIN1%.w.emailarray.com/carddav" ], vCard: [ "3.0" ], email: "true"},
							{ id: "POSTEO", type: "CARDDAV", url: [ "https://posteo.de:8843" ], vCard: [ "3.0" ]},
							{ id: "WEB.DE", type: "CARDDAV", url: [ "https://carddav.web.de" ], vCard: [ "3.0" ]},
							{ id: "YANDEX.RU", type: "CARDDAV", url: [ "https://carddav.yandex.ru/addressbook/%EMAILADDRESS%/addressbook" ], vCard: [ "3.0" ]},
							{ id: "ZACLYS", type: "CARDDAV", url: [ "https://ncloud.zaclys.com" ], vCard: [ "3.0" ], pwdapp: "false"},
							{ id: "ZOHO.COM", type: "CARDDAV", url: [ "https://contacts.zoho.com" ], vCard: [ "3.0" ]},
							{ id: "ZIGGO.NL", type: "CARDDAV", url: [ "https://sync.ziggo.nl" ], vCard: [ "3.0" ], pwdapp: "false"}
						],
	
	// actions
	currentActionId: 0,
	currentAction: {},
	
	// undos
	currentUndoId: 0,

	// preferences
	cardbookPrefs: {},

	possibleCustomFields: { "X-CUSTOM1": {add: false}, "X-CUSTOM2": {add: false}, "X-CUSTOM3": {add: false}, "X-CUSTOM4": {add: false},
							"X-PHONETIC-FIRST-NAME": {add: false}, "X-PHONETIC-LAST-NAME": {add: false}, "X-BIRTHPLACE": {add: false},
							"X-ANNIVERSARY": {add: false}, "X-DEATHDATE": {add: false}, "X-DEATHPLACE": {add: false}, "X-GENDER": {add: false} },

	writePossibleCustomFields: async function (aPossibleCustomFields) {
		let fields = aPossibleCustomFields || cardbookBGRepo.possibleCustomFields;
		let customFields = cardbookBGPreferences.getAllCustomFields();
		let count = customFields.personal.length;
		for (let code in cardbookBGRepo.possibleCustomFields) {
			if (fields[code].add && !cardbookBGRepo.possibleCustomFields[code].added) {
				await cardbookBGPreferences.setCustomFields("personal", count, `${code}:${fields[code].label}`);
				cardbookBGRepo.possibleCustomFields[code].added = true;
				count++;
			}
		}
	},

	verifyABRestrictions: function (aDirPrefId, aSearchAB, aABExclRestrictions, aABInclRestrictions) {
		if (aABExclRestrictions[aDirPrefId]) {
			return false;
		}
		if (((aABInclRestrictions.length == 0) && ((aSearchAB == aDirPrefId) || (aSearchAB === "allAddressBooks"))) ||
			((aABInclRestrictions.length > 0) && ((aSearchAB == aDirPrefId) || ((aSearchAB === "allAddressBooks") && aABInclRestrictions[aDirPrefId])))) {
			return true;
		} else {
			return false;
		}
	},
	
	verifyCatRestrictions: function (aDirPrefId, aCategory, aSearchInput, aABExclRestrictions, aCatExclRestrictions, aCatInclRestrictions) {
		if (aABExclRestrictions[aDirPrefId]) {
			return false;
		}
		if (aCatExclRestrictions[aDirPrefId] && aCatExclRestrictions[aDirPrefId][aCategory]) {
			return false;
		}
		if (((!(aCatInclRestrictions[aDirPrefId])) && (aCategory.replace(/[\s+\-+\.+\,+\;+]/g, "").toUpperCase().indexOf(aSearchInput) >= 0 || aSearchInput == "")) ||
				((aCatInclRestrictions[aDirPrefId]) && (aCatInclRestrictions[aDirPrefId][aCategory]))) {
			return true;
		} else {
			return false;
		}
	},

	isEmailRegistered: function(aEmail, aIdentityKey) {
		let ABInclRestrictions = {};
		let ABExclRestrictions = {};
		let catInclRestrictions = {};
		let catExclRestrictions = {};

		function _loadRestrictions(aIdentityKey) {
			let result = [];
			result = cardbookBGPreferences.getAllRestrictions();
			ABInclRestrictions = {};
			ABExclRestrictions = {};
			catInclRestrictions = {};
			catExclRestrictions = {};
			if (!aIdentityKey) {
				ABInclRestrictions["length"] = 0;
				return;
			}
			for (var i = 0; i < result.length; i++) {
				let resultArray = result[i];
				if ((resultArray[0] == "true") && (resultArray[3] != "") && ((resultArray[2] == aIdentityKey) || (resultArray[2] == "allMailAccounts"))) {
					if (resultArray[1] == "include") {
						ABInclRestrictions[resultArray[3]] = 1;
						if (resultArray[4]) {
							if (!(catInclRestrictions[resultArray[3]])) {
								catInclRestrictions[resultArray[3]] = {};
							}
							catInclRestrictions[resultArray[3]][resultArray[4]] = 1;
						}
					} else {
						if (resultArray[4]) {
							if (!(catExclRestrictions[resultArray[3]])) {
								catExclRestrictions[resultArray[3]] = {};
							}
							catExclRestrictions[resultArray[3]][resultArray[4]] = 1;
						} else {
							ABExclRestrictions[resultArray[3]] = 1;
						}
					}
				}
			}
			ABInclRestrictions["length"] = cardbookBGUtils.sumElements(ABInclRestrictions);
		};
		
		_loadRestrictions(aIdentityKey);
		
		if (aEmail) {
			let email = aEmail.toLowerCase();
			for (let account of cardbookBGRepo.cardbookAccounts) {
				if (account[2] && (account[3] != "SEARCH")) {
					let dirPrefId = account[1];
					if (cardbookBGRepo.verifyABRestrictions(dirPrefId, "allAddressBooks", ABExclRestrictions, ABInclRestrictions)) {
						if (cardbookBGRepo.cardbookCardEmails[dirPrefId]) {
							if (cardbookBGRepo.cardbookCardEmails[dirPrefId][email]) {
								for (let card of cardbookBGRepo.cardbookCardEmails[dirPrefId][email]) {
									if (catExclRestrictions[dirPrefId]) {
										let add = true;
										for (var l in catExclRestrictions[dirPrefId]) {
											if (card.categories.includes(l)) {
												add = false;
												break;
											}
										}
										if (!add) {
											continue;
										}
									}
									if (catInclRestrictions[dirPrefId]) {
										let add = false;
										for (var l in catInclRestrictions[dirPrefId]) {
											if (card.categories.includes(l)) {
												add = true;
												break;
											}
										}
										if (!add) {
											continue;
										}
									}
									return true;
								}
							}
						}
					}
				}
			}
		}
		return false;
	},

	getCardFromEmail: function(aEmail, aDirPrefId) {
		let checkString = aEmail.toLowerCase();
		if (aDirPrefId) {
			if (cardbookBGRepo.cardbookCardEmails[aDirPrefId]) {
				if (cardbookBGRepo.cardbookCardEmails[aDirPrefId][checkString]) {
					return cardbookBGRepo.cardbookCardEmails[aDirPrefId][checkString][0];
				}
			}
		} else {
			for (let account of cardbookBGRepo.cardbookAccounts) {
				if (account[2] && (account[3] != "SEARCH")) {
					let dirPrefId = account[1];
					if (cardbookBGRepo.cardbookCardEmails[dirPrefId]) {
						if (cardbookBGRepo.cardbookCardEmails[dirPrefId][checkString]) {
							return cardbookBGRepo.cardbookCardEmails[dirPrefId][checkString][0];
						}
					}
				}
			}
		}
	},

	updateCategoryFromRepository: async function (aNewCategory, aOldCategory, aDirPrefId) {
		try {
			await cardbookBGRepo.updateCategoryFromDisplay(aNewCategory, aOldCategory, aDirPrefId);
			cardbookBGRepo.removeCategoryFromCache(aOldCategory);
			await cardbookBGRepo.addCategoryToCache(aNewCategory, true, aDirPrefId);
			cardbookBGRepo.removeCategoryFromList(aOldCategory);
			cardbookBGRepo.addCategoryToList(aNewCategory);
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.updateCategoryFromRepository error : " + e, "Error");
		}
	},

	updateCategoryFromDisplay: async function(aNewCategory, aOldCategory, aDirPrefId) {
		if (!cardbookBGRepo.cardbookAccountsCategories[aDirPrefId]) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = [];
		}
		cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].filter(child => child != aOldCategory.name);
		if (!cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].includes(aNewCategory.name)) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].push(aNewCategory.name);
			cardbookHTMLUtils.sortArrayByString(cardbookBGRepo.cardbookAccountsCategories[aDirPrefId],1);
		}
		let id = aDirPrefId+"::categories::"+aOldCategory.name;
		if (cardbookBGRepo.cardbookDisplayCards[id]) {
			let oldDisplay = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[id]));
			delete cardbookBGRepo.cardbookDisplayCards[id];
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aNewCategory.name] = oldDisplay;
			await cardbookIDBCard.removeDisplayFromViews(id);
			for (let card of cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aNewCategory.name].cards) {
				await cardbookIDBCard.addCardToViews(aDirPrefId+"::categories::"+aNewCategory.name, card);
			}
		}
	},

	removeCardsFromCategory: async function(aDirPrefId, aCategoryName) {
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategoryName]) {
			let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategoryName].cards));
			for (let card of cards) {
				let outCard = new cardbookCardParser();
				await cardbookBGUtils.cloneCard(card, outCard);
				cardbookBGRepo.removeCategoryFromCard(outCard, aCategoryName);
				cardbookBGUtils.setCalculatedFields(outCard);
				await cardbookBGUtils.changeMediaFromFileToContent(outCard);
				if (!outCard.created) {
					cardbookBGUtils.addTagUpdated(outCard);
				}
				await cardbookBGRepo.removeCardFromRepository(card, true);
				await cardbookBGRepo.addCardToRepository(outCard, true);
			}
		}
	},

	removeCategoryFromCard: function(aCard, aCategoryName) {
		aCard.categories = aCard.categories.filter(child => child != aCategoryName);
	},

	addCardFromDisplayAndEmail: async function (aDirPrefId, aDisplayName, aEmail, aCategory, aActionId) {
		if (!aDisplayName) {
			if (!aEmail) {
				return;
			} else {
				aDisplayName = aEmail;
			}
		}
		let name = cardbookBGPreferences.getName(aDirPrefId);
		let version = cardbookBGPreferences.getVCardVersion(aDirPrefId);
		let readOnly = cardbookBGPreferences.getReadOnly(aDirPrefId);
		if (!readOnly) {
			let newCard = new cardbookCardParser();
			newCard.dirPrefId = aDirPrefId;
			newCard.version = version;
			newCard.fn = aDisplayName;
			if (newCard.fn == "") {
				newCard.fn = aEmail.substr(0, aEmail.indexOf("@")).replace("."," ").replace("_"," ");
			}
			var displayNameArray = aDisplayName.split(" ");
			if (displayNameArray.length > 1) {
				newCard.lastname = displayNameArray[displayNameArray.length - 1];
				var removed = displayNameArray.splice(displayNameArray.length - 1, 1);
				newCard.firstname = displayNameArray.join(" ");
			}
			newCard.email = [ [ [aEmail], [] ,"", [] ] ];
			if (aCategory) {
				cardbookHTMLUtils.addCategoryToCard(newCard, aCategory);
			}
			await cardbookBGRepo.saveCardFromUpdate({}, newCard, aActionId, true);
		} else {
			cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "addressbookReadOnly", [name]);
		}
	},

	addCardToEmails: async function(aCard) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.addCardToEmails", card: aCard});
		for (var i = 0; i < aCard.email.length; i++) {
			let email = aCard.email[i][0][0].toLowerCase();
			if (email) {
				if (!cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId]) {
					cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId] = {};
				}
				if (!cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email]) {
					cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email] = [];
				}
				cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email].push(aCard);
			}
		}
	},

	addCardToLongSearch: function(aCard) {
		let myLongText = cardbookBGUtils.getLongSearchString(aCard);
		if (myLongText) {
			if (!cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId]) {
				cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId] = {};
			}
			if (!cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText]) {
				cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText] = [];
			}
			cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText].push(aCard);
		}
	},

	getShortSearchString: function(aCard) {
		let lResult = "";
		let sep = "|";
		let fields = cardbookBGPreferences.getPref("autocompleteRestrictSearchFields").split("|");
		for (let field of fields) {
			let value = cardbookHTMLUtils.getCardValueByField(aCard, field, false).join();
			if (value) {
				lResult = lResult + value + sep;
			}
		}
		lResult = lResult.slice(0, -1);
		return cardbookBGUtils.makeSearchString(lResult);
	},

	addCardToShortSearch: function(aCard) {
		if (cardbookBGPreferences.getPref("autocompleteRestrictSearch")) {
			let myShortText = cardbookBGRepo.getShortSearchString(aCard);
			if (myShortText) {
				if (!cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId]) {
					cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId] = {};
				}
				if (!cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText]) {
					cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText] = [];
				}
				cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText].push(aCard);
			}
		} else {
			cardbookBGRepo.cardbookCardShortSearch = {};
		}
	},

	addCardToList: function(aCard) {
		cardbookBGRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid] = aCard;
	},

	addMediaToCache: async function(aCard, aField, aDirPrefName) {
		try {
			if (aCard[aField].value != "") {
				await cardbookIDBImage.addImage(cardbookBGRepo.statusInformation, aField, aDirPrefName, 
											{cbid: aCard.dirPrefId+"::"+aCard.uid, dirPrefId: aCard.dirPrefId, extension: aCard[aField].extension, content: aCard[aField].value},
											aCard.fn);
				aCard[aField].value = "";
			} else if (aCard[aField].localURI) {
				let base64 = await messenger.NotifyTools.notifyExperiment({ query: "cardbook.getImageFromURI", URI: aCard[aField].localURI });
				if (base64) {
					let extension = cardbookHTMLUtils.getFileExtension(aCard[aField].localURI);
					await cardbookIDBImage.addImage(cardbookBGRepo.statusInformation, aField, aDirPrefName,
												{cbid: aCard.dirPrefId+"::"+aCard.uid, dirPrefId: aCard.dirPrefId, extension: extension, content: base64},
												aCard.fn);
					aCard[aField].localURI = "";
				} else {
					aCard[aField].localURI = "";
				}
			} else if (aCard[aField].URI != "") {
				if (!aCard[aField].URI.startsWith("http")) {
					let base64 = await messenger.NotifyTools.notifyExperiment({ query: "cardbook.getImageFromURI", URI: aCard[aField].URI });
					let filenameArray = aCard[aField].URI.split(".");
					let extension = filenameArray[filenameArray.length-1];
					await cardbookIDBImage.addImage(cardbookBGRepo.statusInformation, aField, aDirPrefName,
						{cbid: aCard.dirPrefId+"::"+aCard.uid, dirPrefId: aCard.dirPrefId, extension: extension, content: base64},
						aCard.fn);
				}
			}
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addMediaToCache error : " + e, "Error");
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addMediaToCache aCard : " + aCard.cbid, "Error");
		}
	},

	addCardToCache: async function(aCard, aAddCardToCache) {
		try {
			let name = cardbookBGPreferences.getName(aCard.dirPrefId);
			let type = cardbookBGPreferences.getType(aCard.dirPrefId);

			for (let media of cardbookHTMLUtils.allColumns.media) {
				if (aAddCardToCache) {
					await cardbookBGRepo.addMediaToCache(aCard, media, name);
				}
			}
			cardbookBGUtils.setCacheURIFromCard(aCard, type);
			
			if (type == "DIRECTORY") {
				if (aAddCardToCache) {
					let data = await cardbookBGUtils.getDataForUpdatingFile(cardbookBGRepo.statusInformation, [aCard]);
					let result = await messenger.NotifyTools.notifyExperiment({query: "cardbook.writeContentToFile", url: aCard.cacheuri, data: data, utf8: "UTF8"});
					cardbookBGLog.updateStatusProgressInformationWithDebug2(cardbookBGRepo.statusInformation, name + " : debug mode : Contact " + aCard.fn + " written to directory");
				}
			}
			if (!cardbookBGRepo.cardbookFileCacheCards[aCard.dirPrefId]) {
				cardbookBGRepo.cardbookFileCacheCards[aCard.dirPrefId] = {};
			}
			cardbookBGRepo.cardbookFileCacheCards[aCard.dirPrefId][aCard.cacheuri] = aCard;
			if (aAddCardToCache) {
				await cardbookIDBCard.addCardToCards(cardbookBGRepo.statusInformation, name, aCard);
			}
		}	catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addCardToCache error : " + e, "Error");
		}
	},

	addCardToDisplayModified: function(aCard, aAccountId) {
		if (aCard.updated || aCard.created) {
			cardbookBGRepo.cardbookDisplayCards[aAccountId].modified = cardbookBGRepo.cardbookDisplayCards[aAccountId].modified + 1;
		}
	},

	addCategoryToList: function(aCategory) {
		cardbookBGRepo.cardbookCategories[aCategory.cbid] = aCategory;
	},

	addCategoryToCache: async function(aCategory, aAdddCategoryToCache, aDirPrefId) {
		try {
			let name = cardbookBGPreferences.getName(aDirPrefId);

			if (!cardbookBGRepo.cardbookFileCacheCategories[aDirPrefId]) {
				cardbookBGRepo.cardbookFileCacheCategories[aDirPrefId] = {};
			}
			cardbookBGRepo.cardbookFileCacheCategories[aDirPrefId][aCategory.href] = aCategory;

			let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
			if (aAdddCategoryToCache && aCategory.name != uncat) {
				await cardbookIDBCat.addCategory(cardbookBGRepo.statusInformation, name, aCategory);
			}
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addCategoryToCache error : " + e, "Error");
		}
	},

	addCategoryToDisplay: function(aCategory, aDirPrefId) {
		if (!cardbookBGRepo.cardbookAccountsCategories[aDirPrefId]) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = [];
		}
		if (!cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].includes(aCategory.name)) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].push(aCategory.name);
			cardbookHTMLUtils.sortArrayByString(cardbookBGRepo.cardbookAccountsCategories[aDirPrefId],1);
		}
		if (!cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategory.name]) {
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategory.name] = {modified: 0, cards: []};
		}
	},

	addCategoryToRepository: async function (aCategory, aMode, aDirPrefId) {
		try {
			cardbookBGRepo.addCategoryToList(aCategory);
			await cardbookBGRepo.addCategoryToCache(aCategory, aMode, aDirPrefId);
			cardbookBGRepo.addCategoryToDisplay(aCategory, aDirPrefId);
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addCategoryToRepository error : " + e, "Error");
		}
	},

	addCardToDisplayAndCat: async function(aCard, aDirPrefId, aAddView) {
		if (!cardbookBGRepo.cardbookDisplayCards[aDirPrefId]) {
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId] = {modified: 0, cards: []};
		}
		cardbookBGRepo.cardbookDisplayCards[aDirPrefId].cards.push(aCard);
		cardbookBGRepo.addCardToDisplayModified(aCard, aDirPrefId);
		if (aAddView) {
			await cardbookIDBCard.addCardToViews(aDirPrefId, aCard);
		}

		async function addProcess(aId, aCategory) {
			if (!cardbookBGRepo.cardbookCategories[aDirPrefId+"::"+aCategory]) {
				var category = new cardbookCategoryParser(aCategory, aDirPrefId);
				cardbookBGUtils.addTagCreated(category);
				await cardbookBGRepo.addCategoryToRepository(category, true, aDirPrefId);
			}
			if (!cardbookBGRepo.cardbookDisplayCards[aId]) {
				cardbookBGRepo.cardbookDisplayCards[aId] = {modified: 0, cards: []};
			}
			cardbookBGRepo.cardbookDisplayCards[aId].cards.push(aCard);
			cardbookBGRepo.addCardToDisplayModified(aCard, aId);
			if (aAddView) {
				await cardbookIDBCard.addCardToViews(aId, aCard);
			}
		}
		if (aCard.categories.length != 0) {
			for (let category of aCard.categories) {
				await addProcess(aDirPrefId+"::categories::"+category, category);
			}
		} else {
			let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
			await addProcess(aDirPrefId+"::categories::"+uncat, uncat);
		}
	},

	addCardToOrg: async function(aCard, aDirPrefId, aAddView) {
		if (cardbookBGPreferences.getNode(aDirPrefId) != "org") {
			return;
		}
		async function addProcess(aId, aName) {
			if (!cardbookBGRepo.cardbookDisplayCards[aId]) {
				cardbookBGRepo.cardbookDisplayCards[aId] = {modified: 0, cards: []};
			}
			cardbookBGRepo.cardbookDisplayCards[aId].cards.push(aCard);
			if (aAddView) {
				await cardbookIDBCard.addCardToViews(aId, aCard);
			}
		}
		let parent = aDirPrefId + "::org";
		if (aCard.org.length && aCard.org[0]) {
			for (let org of aCard.org) {
				let id = parent + "::" + org;
				await addProcess(id, org);
				parent = id;
			}
		} else {
			let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
			let id = parent + "::" + uncat;
			await addProcess(id, uncat);
		}
	},

	isMyCardFound: function (aCard, aComplexSearchDirPrefId) {
		if (!cardbookBGPreferences.getEnabled(aComplexSearchDirPrefId)) {
			return false;
		}
		if (cardbookBGRepo.cardbookComplexSearch[aComplexSearchDirPrefId]) {
			if ((cardbookBGRepo.cardbookComplexSearch[aComplexSearchDirPrefId].searchAB == aCard.dirPrefId) || (cardbookBGRepo.cardbookComplexSearch[aComplexSearchDirPrefId].searchAB === "allAddressBooks")) {
				let matchAll = cardbookBGRepo.cardbookComplexSearch[aComplexSearchDirPrefId].matchAll;
				let rules = cardbookBGRepo.cardbookComplexSearch[aComplexSearchDirPrefId].rules
				return cardbookBGUtils.isMyCardFoundInRules(aCard, rules, matchAll);
			}
		}
		return false;
	},

	addCardToRepository: async function (aCard, aMode) {
		try {
			await cardbookBGRepo.addCardToEmails(aCard);
			cardbookBGRepo.addCardToLongSearch(aCard);
			cardbookBGRepo.addCardToShortSearch(aCard);
			cardbookBGRepo.addCardToList(aCard);
			let type = cardbookBGPreferences.getType(aCard.dirPrefId);
			let add = aMode || type == "FILE" || type == "DIRECTORY";
			await cardbookBGRepo.addCardToCache(aCard, add);
			await cardbookBGRepo.addCardToDisplayAndCat(aCard, aCard.dirPrefId, add);
			await cardbookBGRepo.addCardToOrg(aCard, aCard.dirPrefId, add);
			for (let dirPrefId in cardbookBGRepo.cardbookComplexSearch) {
				if (cardbookBGPreferences.getEnabled(dirPrefId) && cardbookBGRepo.isMyCardFound(aCard, dirPrefId)) {
					await cardbookBGRepo.addCardToDisplayAndCat(aCard, dirPrefId, add);
				}
			}
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.addCardToRepository error : " + e, "Error");
		}
	},

	addCardToTemp: function (aCard) {
		try {
			cardbookBGRepo.cardbookTempCards[aCard.dirPrefId+"::"+aCard.uid] = aCard;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation("cardbookBGRepo.addCardToTemp error : " + e, "Error");
		}
	},

	removeCardFromTemp: function (aCard) {
		try {
			delete cardbookBGRepo.cardbookTempCards[aCard.dirPrefId+"::"+aCard.uid];
			aCard = null;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation("cardbookBGRepo.removeCardFromTemp error : " + e, "Error");
		}
	},

	addCardToAction: async function (aActionId, aType, aCard) {
		let action = cardbookBGRepo.currentAction[aActionId];
		let tempCard = new cardbookCardParser();
		await cardbookBGUtils.cloneCard(aCard, tempCard);
		tempCard.cacheuri = aCard.cacheuri;
		action[aType].push(tempCard);
		cardbookBGRepo.currentAction[aActionId].files.push(tempCard.dirPrefId);
		if (aType == "newCards") {
			cardbookBGRepo.currentAction[aActionId].doneCards++;
		}
	},

	removeCardFromLongSearch: function(aCard) {
		var myLongText = cardbookBGUtils.getLongSearchString(aCard);
		if (myLongText) {
			if (cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId] && cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText]) {
				if (cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText].length == 1) {
					delete cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText];
				} else {
					function searchCard(element) {
						return (element.dirPrefId+"::"+element.uid != aCard.dirPrefId+"::"+aCard.uid);
					}
					cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText] = cardbookBGRepo.cardbookCardLongSearch[aCard.dirPrefId][myLongText].filter(searchCard);
				}
			}
		}
	},

	removeCardFromShortSearch: function(aCard) {
		if (cardbookBGPreferences.getPref("autocompleteRestrictSearch")) {
			var myShortText = cardbookBGRepo.getShortSearchString(aCard);
			if (myShortText) {
				if (cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText]) {
					if (cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText].length == 1) {
						delete cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText];
					} else {
						function searchCard(element) {
							return (element.dirPrefId+"::"+element.uid != aCard.dirPrefId+"::"+aCard.uid);
						}
						cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText] = cardbookBGRepo.cardbookCardShortSearch[aCard.dirPrefId][myShortText].filter(searchCard);
					}
				}
			}
		}
	},

	removeCardFromEmails: async function(aCard) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.removeCardFromEmails", card: aCard});
		if (cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId]) {
			for (var i = 0; i < aCard.email.length; i++) {
				let email = aCard.email[i][0][0].toLowerCase();
				if (email) {
					if (cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email]) {
						if (cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email].length == 1) {
							delete cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email];
						} else {
							function searchCard(element) {
								return (element.dirPrefId+"::"+element.uid != aCard.dirPrefId+"::"+aCard.uid);
							}
							cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email] = cardbookBGRepo.cardbookCardEmails[aCard.dirPrefId][email].filter(searchCard);
						}
					}
				}
			}
		}
	},

	removeCardFromDisplayModified: function(aCard, aAccountId) {
		if (aCard.updated || aCard.created) {
			cardbookBGRepo.cardbookDisplayCards[aAccountId].modified = cardbookBGRepo.cardbookDisplayCards[aAccountId].modified - 1 ;
		}
	},

	removeCategoryFromDisplay: async function(aCategory, aDirPrefId, aCacheDeletion) {
		cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = cardbookBGRepo.cardbookAccountsCategories[aDirPrefId].filter(child => child != aCategory.name);
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategory.name]) {
			delete cardbookBGRepo.cardbookDisplayCards[aDirPrefId+"::categories::"+aCategory.name];
			if (aCacheDeletion) {
				await cardbookIDBCard.removeDisplayFromViews(aDirPrefId+"::categories::"+aCategory.name);
			}
		}
	},

	removeCategoryFromCache: function(aCategory) {
		try {
			let name = cardbookBGPreferences.getName(aCategory.dirPrefId);
			cardbookIDBCat.removeCategory(cardbookBGRepo.statusInformation, name, aCategory);
			if (cardbookBGRepo.cardbookFileCacheCategories[aCategory.dirPrefId][aCategory.href]) {
				delete cardbookBGRepo.cardbookFileCacheCategories[aCategory.dirPrefId][aCategory.href];
			}
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.removeCategoryFromCache error : " + e, "Error");
		}
	},

	removeCategoryFromList: function(aCategory) {
		delete cardbookBGRepo.cardbookCategories[aCategory.cbid];
	},

	removeCategoryFromRepository: async function (aCategory, aCacheDeletion, aDirPrefId) {
		try {
			await cardbookBGRepo.removeCategoryFromDisplay(aCategory, aDirPrefId, aCacheDeletion);
			for (let dirPrefId in cardbookBGRepo.cardbookComplexSearch) {
				if (cardbookBGPreferences.getEnabled(aDirPrefId) &&
					cardbookBGRepo.cardbookDisplayCards[dirPrefId+"::categories::"+aCategory.name] &&
					cardbookBGRepo.cardbookDisplayCards[dirPrefId+"::categories::"+aCategory.name].cards.length == 0 &&
					cardbookBGRepo.cardbookCategories[dirPrefId+"::"+aCategory.name]) {
					let otherCat = cardbookBGRepo.cardbookCategories[dirPrefId+"::"+aCategory.name];
					await cardbookBGRepo.removeCategoryFromDisplay(otherCat, dirPrefId, aCacheDeletion);
					cardbookBGRepo.removeCategoryFromCache(otherCat);
					cardbookBGRepo.removeCategoryFromList(otherCat);
				}
			}
			if (aCacheDeletion) {
				cardbookBGRepo.removeCategoryFromCache(aCategory);
			} else {
				cardbookBGUtils.addTagDeleted(aCategory);
				await cardbookBGRepo.addCategoryToCache(aCategory, true, aCategory.dirPrefId);
			}
			cardbookBGRepo.removeCategoryFromList(aCategory);
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.removeCategoryFromRepository error : " + e, "Error");
		}
	},

	removeCardFromDisplayAndCat: async function(aCard, aDirPrefId, aCacheDeletion) {
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId]) {
			let type = cardbookBGPreferences.getType(aDirPrefId);
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId].cards = cardbookBGRepo.cardbookDisplayCards[aDirPrefId].cards.filter(child => child.dirPrefId + child.uid != aCard.dirPrefId + aCard.uid);
			if (aCacheDeletion) {
				await cardbookIDBCard.removeCardFromViews(aDirPrefId, aCard);
			}
			cardbookBGRepo.removeCardFromDisplayModified(aCard, aDirPrefId);
			async function deleteProcess(aId, aCategoryName) {
				if (cardbookBGRepo.cardbookDisplayCards[aId]) {
					cardbookBGRepo.cardbookDisplayCards[aId].cards = cardbookBGRepo.cardbookDisplayCards[aId].cards.filter(child => child.dirPrefId + child.uid != aCard.dirPrefId + aCard.uid);
					if (aCacheDeletion) {
						await cardbookIDBCard.removeCardFromViews(aId, aCard);
					}
					if (cardbookBGRepo.cardbookDisplayCards[aId].cards.length == 0 && type == "SEARCH") {
						let category = cardbookBGRepo.cardbookCategories[aDirPrefId+"::"+aCategoryName];
						await cardbookBGRepo.removeCategoryFromRepository(category, true, aDirPrefId);
					} else {
						cardbookBGRepo.removeCardFromDisplayModified(aCard, aId);
					}
				}
			}
			async function deleteProcessUncategorized(aId) {
				if (cardbookBGRepo.cardbookDisplayCards[aId]) {
					cardbookBGRepo.cardbookDisplayCards[aId].cards = cardbookBGRepo.cardbookDisplayCards[aId].cards.filter(child => child.dirPrefId + child.uid != aCard.dirPrefId + aCard.uid);
					if (aCacheDeletion) {
						await cardbookIDBCard.removeCardFromViews(aId, aCard);
					}
					if (cardbookBGRepo.cardbookDisplayCards[aId].cards.length == 0) {
						let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
						let category = cardbookBGRepo.cardbookCategories[aDirPrefId+"::"+uncat];
						await cardbookBGRepo.removeCategoryFromRepository(category, true, aDirPrefId);
					} else {
						cardbookBGRepo.removeCardFromDisplayModified(aCard, aId);
					}
				}
			}
			if (aCard.categories.length != 0) {
				for (var category of aCard.categories) {
					await deleteProcess(aDirPrefId+"::categories::"+category, category);
				}
			} else {
				let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
				await deleteProcessUncategorized(aDirPrefId+"::categories::"+uncat);
			}
		}
	},

	removeCardFromOrg: async function(aCard, aDirPrefId, aCacheDeletion) {
		if (cardbookBGPreferences.getNode(aDirPrefId) != "org") {
			return;
		}
		async function deleteProcess(aId, aName, aParentId) {
			if (cardbookBGRepo.cardbookDisplayCards[aId]) {
				cardbookBGRepo.cardbookDisplayCards[aId].cards = cardbookBGRepo.cardbookDisplayCards[aId].cards.filter(child => child.dirPrefId + child.uid != aCard.dirPrefId + aCard.uid);
				if (cardbookBGRepo.cardbookDisplayCards[aId].cards.length == 0) {
					if (aId != aDirPrefId) {
						delete cardbookBGRepo.cardbookDisplayCards[aId];
					}
				} else {
					cardbookBGRepo.removeCardFromDisplayModified(aCard, aId);
				}
			}
			if (aCacheDeletion) {
				await cardbookIDBCard.removeCardFromViews(aId, aCard);
			}
		}
		if (aCard.org) {
			let orgArray = JSON.parse(JSON.stringify(aCard.org));
			orgArray.splice(0, 0, aCard.dirPrefId, "org" );
			let id = orgArray.join("::");
			let parent = cardbookBGUtils.getParentOrg(id);
			for (var i = orgArray.length - 1; i >= 2; i--) {
				let data = orgArray[i];
				await deleteProcess(id, data, parent);
				id = parent;
				parent = cardbookBGUtils.getParentOrg(id);
			}
		} else {
			let uncat = cardbookBGPreferences.getPref("uncategorizedCards");
			await deleteProcess(aCard.dirPrefId + "::org::" + uncat, uncat, aCard.dirPrefId);
		}
	},

	cacheDeleteMediaCard: async function(aCard, aDirPrefName) {
		try {
			for (let media of cardbookHTMLUtils.allColumns.media) {
				await cardbookIDBImage.removeImage(cardbookBGRepo.statusInformation, media, aDirPrefName,
														{cbid: aCard.dirPrefId+"::"+aCard.uid, dirPrefId: aCard.dirPrefId, extension: aCard[media].extension, content: aCard[media].value},
														aCard.fn);
			}
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.cacheDeleteMediaCard error : " + e, "Error");
		}
	},

	removeCardFromCache: async function(aCard) {
		try {
			let name = cardbookBGPreferences.getName(aCard.dirPrefId);
			let type = cardbookBGPreferences.getType(aCard.dirPrefId);

			await cardbookBGRepo.cacheDeleteMediaCard(aCard, name);

			if (type === "DIRECTORY") {
				await messenger.NotifyTools.notifyExperiment({query: "cardbook.deleteFile", url: aCard.cacheuri});
				cardbookBGLog.updateStatusProgressInformationWithDebug2(cardbookBGRepo.statusInformation, name + " : debug mode : Contact " + aCard.fn + " deleted from directory");
			}
			cardbookIDBCard.removeCardFromCards(cardbookBGRepo.statusInformation, name, aCard);
			if (cardbookBGRepo.cardbookFileCacheCards[aCard.dirPrefId][aCard.cacheuri]) {
				delete cardbookBGRepo.cardbookFileCacheCards[aCard.dirPrefId][aCard.cacheuri];
			}
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.removeCardFromCache error : " + e, "Error");
		}
	},

	removeCardFromList: function(aCard) {
		delete cardbookBGRepo.cardbookCards[aCard.dirPrefId+"::"+aCard.uid];
	},

	removeCardFromRepository: async function (aCard, aMode) {
		try {
			cardbookBGRepo.removeCardFromLongSearch(aCard);
			cardbookBGRepo.removeCardFromShortSearch(aCard);
			await cardbookBGRepo.removeCardFromEmails(aCard);
			let type = cardbookBGPreferences.getType(aCard.dirPrefId);
			let aCacheDeletion = aMode || type == "FILE" || type == "DIRECTORY";
			await cardbookBGRepo.removeCardFromDisplayAndCat(aCard, aCard.dirPrefId, aCacheDeletion);
			await cardbookBGRepo.removeCardFromOrg(aCard, aCard.dirPrefId, aCacheDeletion);
			for (let dirPrefId in cardbookBGRepo.cardbookComplexSearch) {
				if (cardbookBGPreferences.getEnabled(dirPrefId)) {
					await cardbookBGRepo.removeCardFromDisplayAndCat(aCard, dirPrefId, aCacheDeletion);
				}
			}
			if (aCacheDeletion) {
				await cardbookBGRepo.removeCardFromCache(aCard);
			}
			cardbookBGRepo.removeCardFromList(aCard);
			aCard = null;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.removeCardFromRepository error : " + e, "Error");
		}
	},

	saveCategory: async function(aOldCat, aNewCat, aActionId) {
		try {
			if (cardbookBGPreferences.getReadOnly(aNewCat.dirPrefId) || !cardbookBGPreferences.getEnabled(aNewCat.dirPrefId)) {
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].doneCats++;
				}
				return;
			}

			if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
				cardbookBGRepo.currentAction[aActionId].totalCats++;
				cardbookBGRepo.currentAction[aActionId].files.push(aNewCat.dirPrefId);
			}

			let name = cardbookBGPreferences.getName(aNewCat.dirPrefId);
			// Existing category
			if (aOldCat.dirPrefId && cardbookBGRepo.cardbookCategories[aOldCat.cbid]) {
				await cardbookBGRepo.removeCategoryFromRepository(aOldCat, true, aOldCat.dirPrefId);
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].oldCats.push(aOldCat);
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "categoryRenamed", [name, aNewCat.name]);
				if (!aNewCat.created) {
					cardbookBGUtils.addTagUpdated(aNewCat);
				}
				await cardbookBGRepo.addCategoryToRepository(aNewCat, true, aNewCat.dirPrefId);
			// New category
			} else {
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "categoryCreated", [name, aNewCat.name]);
				cardbookBGUtils.addTagCreated(aNewCat);
				await cardbookBGRepo.addCategoryToRepository(aNewCat, true, aNewCat.dirPrefId);
			}
			if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
				cardbookBGRepo.currentAction[aActionId].newCats.push(aNewCat);
				cardbookBGRepo.currentAction[aActionId].doneCats++;
			}
			aOldCat = null;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.saveCategory error : " + e, "Error");
		}
	},

	deleteCategories: async function (aListOfCategories, aActionId) {
		try {
			for (let category of aListOfCategories) {
				let dirPrefId = category.dirPrefId;
				if (cardbookBGPreferences.getReadOnly(dirPrefId) || !cardbookBGPreferences.getEnabled(dirPrefId)) {
					if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
						cardbookBGRepo.currentAction[aActionId].doneCats++;
					}
					continue;
				}
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].oldCats.push(category);
					cardbookBGRepo.currentAction[aActionId].files.push(dirPrefId);
				}
				var name = cardbookBGPreferences.getName(dirPrefId);
				var type = cardbookBGPreferences.getType(dirPrefId);
				if (type == "GOOGLE2" || type == "GOOGLE3") {
					if (category.created) {
						await cardbookBGRepo.removeCategoryFromRepository(category, true, dirPrefId);
					} else {
						cardbookBGUtils.addTagDeleted(category);
						await cardbookBGRepo.removeCategoryFromRepository(category, false, dirPrefId);
					}
				} else {
					await cardbookBGRepo.removeCategoryFromRepository(category, true, dirPrefId);
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "categoryDeleted", [name, category.name], dirPrefId);
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].doneCats++;
				}
			}
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.deleteCategories error : " + e, "Error");
		}
	},

	saveEditionWindow: async function(aOrigCard, aOutCard, aMode, aAction = "CREATE") {
		try {
			switch (aMode) {
				// case "EditList":
				// case "EditContact":
				// case "CreateContact":
				// case "CreateList":
				// case "AddEmail":
				case "ViewList":
				case "ViewContact":
					return;
					break;
				case "ViewResultHideCreate":
				case "ViewResult":
					// remove the temporary card
					cardbookBGRepo.removeCardFromTemp(aOrigCard);
					break;
			}
			if (aAction == "CANCEL") {
				return
			}
			let topic = (cardbookBGRepo.cardbookCards[aOutCard.dirPrefId+"::"+aOutCard.uid]) ? "cardModified" : "cardCreated";
			let actionId = cardbookBGActions.startAction(cardbookBGRepo, topic, [aOutCard.fn], aOutCard.dirPrefId);
			await cardbookBGRepo.saveCardFromUpdate(aOrigCard, aOutCard, actionId, true);
			return actionId;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.saveEditionWindow error : " + e, "Error");
		}
	},
	
	saveCardFromUpdate: async function(aOldCard, aNewCard, aActionId, aCheckCategory) {
		cardbookBGUtils.setCalculatedFields(aNewCard);
		await cardbookBGRepo.saveCard(aOldCard, aNewCard, aActionId, aCheckCategory);
	},

	saveCardFromMove: async function(aOldCard, aNewCard, aActionId, aCheckCategory) {
		if (aNewCard.rev) {
			cardbookBGUtils.setCalculatedFieldsWithoutRev(aNewCard);
		} else {
			cardbookBGUtils.setCalculatedFields(aNewCard);
		}
		await cardbookBGRepo.saveCard(aOldCard, aNewCard, aActionId, aCheckCategory);
	},

	saveCard: async function(aOldCard, aNewCard, aActionId, aCheckCategory) {
		try {
			let writeCustom = false;
			let newFields = cardbookHTMLUtils.newFields.map(x => "X-" + x.toUpperCase());
			for (let code in cardbookBGRepo.possibleCustomFields) {
				if (!cardbookBGRepo.possibleCustomFields[code].added &&
					(aNewCard.version == "4.0" && !newFields.includes(code))) {
					let customValue = cardbookHTMLUtils.getCardValueByField(aNewCard, code, false)[0];
					if (customValue) {
						writeCustom = true;
						cardbookBGRepo.possibleCustomFields[code].add = true;
					}
				}
			}
			if (writeCustom) {
				cardbookBGRepo.writePossibleCustomFields();
			}
	
			if (cardbookBGPreferences.getReadOnly(aNewCard.dirPrefId) || !cardbookBGPreferences.getEnabled(aNewCard.dirPrefId)) {
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].doneCards++;
				}
				return;
			}

			let type = cardbookBGPreferences.getType(aNewCard.dirPrefId);
			let name = cardbookBGPreferences.getName(aNewCard.dirPrefId);

			if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
				cardbookBGRepo.currentAction[aActionId].addedCards++;
				cardbookBGRepo.currentAction[aActionId].totalCards++;
			}

			// category creation
			for (let category of aNewCard.categories) {
				if (!cardbookBGRepo.cardbookCategories[aNewCard.dirPrefId+"::"+category]) {
					let newCategory = new cardbookCategoryParser(category, aNewCard.dirPrefId);
					cardbookBGUtils.addTagCreated(newCategory);
					cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "categoryCreated", [name, category]);
					await cardbookBGRepo.addCategoryToRepository(newCategory, true, aNewCard.dirPrefId);
					if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
						cardbookBGRepo.currentAction[aActionId].newCats.push(newCategory);
					}
					if (aCheckCategory) {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.addActivity", code: "categoryCreated", array: [name, category], icon: "addItem"});
					}
				}
			}

			// Existing card
			if (aOldCard.dirPrefId && cardbookBGRepo.cardbookCards[aOldCard.dirPrefId+"::"+aNewCard.uid] && aOldCard.dirPrefId == aNewCard.dirPrefId) {
				var myCard = cardbookBGRepo.cardbookCards[aOldCard.dirPrefId+"::"+aNewCard.uid];
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					await cardbookBGRepo.addCardToAction(aActionId, "oldCards", myCard);
				}
				if (type === "FILE" || type === "DIRECTORY" || type === "LOCALDB") {
					await cardbookBGRepo.removeCardFromRepository(myCard, true);
					cardbookBGUtils.nullifyTagModification(aNewCard);
					cardbookBGUtils.nullifyEtag(aNewCard);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				} else {
					if (!aNewCard.created) {
						cardbookBGUtils.addTagUpdated(aNewCard);
						aNewCard.etag = myCard.etag;
					}
					await cardbookBGRepo.removeCardFromRepository(myCard, true);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardUpdated", [name, aNewCard.fn]);
			// Moved card
			} else if (aOldCard.dirPrefId && aOldCard.dirPrefId != "" && cardbookBGRepo.cardbookCards[aOldCard.dirPrefId+"::"+aNewCard.uid] && aOldCard.dirPrefId != aNewCard.dirPrefId) {
				var myCard = cardbookBGRepo.cardbookCards[aOldCard.dirPrefId+"::"+aNewCard.uid];
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					await cardbookBGRepo.addCardToAction(aActionId, "oldCards", myCard);
				}
				let oldName = cardbookBGPreferences.getName(myCard.dirPrefId);
				let oldType = cardbookBGPreferences.getType(myCard.dirPrefId);
				if (oldType === "FILE" || oldType === "DIRECTORY" || oldType === "LOCALDB") {
					await cardbookBGRepo.removeCardFromRepository(myCard, true);
				} else {
					if (myCard.created) {
						await cardbookBGRepo.removeCardFromRepository(myCard, true);
					} else {
						cardbookBGUtils.addTagDeleted(myCard);
						await cardbookBGRepo.addCardToCache(myCard, true);
						await cardbookBGRepo.removeCardFromRepository(myCard, false);
					}
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardDeleted", [oldName, myCard.fn]);
				
				let newName = cardbookBGPreferences.getName(aNewCard.dirPrefId);
				let newType = cardbookBGPreferences.getType(aNewCard.dirPrefId);
				aNewCard.cardurl = "";
				cardbookBGUtils.nullifyEtag(aNewCard);
				if (newType === "DIRECTORY" || newType === "LOCALDB" || newType === "FILE") {
					cardbookBGUtils.nullifyTagModification(aNewCard);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				} else {
					cardbookBGUtils.addTagCreated(aNewCard);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardCreated", [newName, aNewCard.fn]);
			// New card
			} else {
				cardbookBGUtils.nullifyEtag(aNewCard);
				if (type === "DIRECTORY" || type === "LOCALDB" || type === "FILE") {
					cardbookBGUtils.nullifyTagModification(aNewCard);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				} else {
					cardbookBGUtils.addTagCreated(aNewCard);
					await cardbookBGRepo.addCardToRepository(aNewCard, true);
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardCreated", [name, aNewCard.fn]);
			}
			
			if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
				await cardbookBGRepo.addCardToAction(aActionId, "newCards", aNewCard);
			}
			aOldCard = null;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.saveCard error : " + e, "Error");
		}
	},

	addAccountToRepository: async function(aAccountId, aAccountName, aAccountType, aAccountUrl, aAccountUser, aColor, aEnabled, aVCard, aDateFormat, aReadOnly, aUrnuuid,
										aSourceId, aDBcached, aAutoSyncEnabled, aAutoSyncInterval, aPrefInsertion) {
		if (cardbookBGRepo.cardbookAccounts.some(account => account[1] == aAccountId)) {
			return;
		}
		if (aPrefInsertion) {
			await cardbookBGPreferences.setId(aAccountId, aAccountId);
			await cardbookBGPreferences.setName(aAccountId, aAccountName);
			await cardbookBGPreferences.setType(aAccountId, aAccountType);
			await cardbookBGPreferences.setUrl(aAccountId, aAccountUrl);
			await cardbookBGPreferences.setUser(aAccountId, aAccountUser);
			await cardbookBGPreferences.setColor(aAccountId, aColor);
			await cardbookBGPreferences.setEnabled(aAccountId, aEnabled);
			await cardbookBGPreferences.setVCardVersion(aAccountId, aVCard);
			await cardbookBGPreferences.setReadOnly(aAccountId, aReadOnly);
			await cardbookBGPreferences.setUrnuuid(aAccountId, aUrnuuid);
			await cardbookBGPreferences.setSourceId(aAccountId, aSourceId);
			await cardbookBGPreferences.setDBCached(aAccountId, aDBcached);
			await cardbookBGPreferences.setAutoSyncEnabled(aAccountId, aAutoSyncEnabled);
			await cardbookBGPreferences.setAutoSyncInterval(aAccountId, aAutoSyncInterval);
			await cardbookBGPreferences.setDateFormat(aAccountId, aDateFormat);
		}
		cardbookBGRepo.cardbookAccounts.push([aAccountName, aAccountId, aEnabled, aAccountType, aReadOnly, aAccountId]);
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.addCardToAccounts", account: [aAccountName, aAccountId, aEnabled, aAccountType, aReadOnly, aAccountId]});
		cardbookHTMLUtils.sortMultipleArrayByString(cardbookBGRepo.cardbookAccounts,0,1);
		cardbookBGRepo.cardbookDisplayCards[aAccountId] = {modified: 0, cards: []};
		cardbookBGRepo.cardbookAccountsCategories[aAccountId] = [];
	},

	// only used from the import of Thunderbird standard address books
	addAccountToCollected: async function (aDirPrefId) {
		let allEmailsCollections = cardbookBGPreferences.getAllEmailsCollections();
		let newId = allEmailsCollections.length + 1;
		await cardbookBGPreferences.setEmailsCollection(newId.toString(), "true::allMailAccounts::" + aDirPrefId + "::");
	},

	removeAccountFromCollected: async function (aDirPrefId) {
		let result = [];
		let allEmailsCollections = cardbookBGPreferences.getAllEmailsCollections();
		result = allEmailsCollections.filter(child => child[2] != aDirPrefId);
		await cardbookBGPreferences.delEmailsCollection();
		for (let i = 0; i < result.length; i++) {
			await cardbookBGPreferences.setEmailsCollection(i.toString(), result[i].join("::"));
		}
	},

	enableOrDisableAccountFromCollected: async function (aDirPrefId, aValue) {
		let result = [];
		let allEmailsCollections = cardbookBGPreferences.getAllEmailsCollections();
		function filterAccount(element) {
			if (element[2] == aDirPrefId) {
				element[0] = aValue;
			}
			return true;
		}
		result = allEmailsCollections.filter(filterAccount);
		await cardbookBGPreferences.delEmailsCollection();
		for (let i = 0; i < result.length; i++) {
			await cardbookBGPreferences.setEmailsCollection(i.toString(), result[i].join("::"));
		}
	},

	removeAccountFromRestrictions: async function (aDirPrefId) {
		let allRestrictions = cardbookBGPreferences.getAllRestrictions();
		allRestrictions = allRestrictions.filter(element => element[3] != aDirPrefId);
		await cardbookBGPreferences.delRestrictions();
		for (let i = 0; i < allRestrictions.length; i++) {
			await cardbookBGPreferences.setRestriction(i.toString(), allRestrictions[i].join("::"));
		}
	},

	enableOrDisableAccountFromRestrictions: async function (aDirPrefId, aValue) {
		let allRestrictions = cardbookBGPreferences.getAllRestrictions();
		function filterAccount(element) {
			if (element[3] == aDirPrefId) {
				element[0] = aValue;
			}
			return true;
		}
		allRestrictions = allRestrictions.filter(filterAccount);
		await cardbookBGPreferences.delRestrictions();
		for (let i = 0; i < allRestrictions.length; i++) {
			await cardbookBGPreferences.setRestriction(i.toString(), allRestrictions[i].join("::"));
		}
	},

	removeAccountFromVCards: async function (aDirPrefId) {
		let allVCards = [];
		allVCards = cardbookBGPreferences.getAllVCards();
		allVCards = allVCards.filter(element => element[2] != aDirPrefId);
		await cardbookBGPreferences.delVCards();
		for (let i = 0; i < allVCards.length; i++) {
			await cardbookBGPreferences.setVCard(i.toString(), allVCards[i].join("::"));
		}
	},

	enableOrDisableAccountFromVCards: async function (aDirPrefId, aValue) {
		let allVCards = cardbookBGPreferences.getAllVCards();
		function filterAccount(element) {
			if (element[2] == aDirPrefId) {
				element[0] = aValue;
			}
			return true;
		}
		allVCards = allVCards.filter(filterAccount);
		for (let i = 0; i < allVCards.length; i++) {
			await cardbookBGPreferences.setRestriction(i.toString(), allVCards[i].join("::"));
		}
	},

	removeAccountFromBirthday: async function (aDirPrefId) {
		await cardbookBGRepo.enableOrDisableAccountFromBirthday(aDirPrefId, false);
	},

	enableOrDisableAccountFromBirthday: async function (aDirPrefId, aValue) {
		let addressBooks = cardbookBGPreferences.getPref("addressBooksNameList");
		let addressBooksList = [];
		addressBooksList = addressBooks.split(",").filter(element => element != aDirPrefId);
		if (aValue && aValue === true && addressBooksList[0] != "allAddressBooks") {
			addressBooksList.push(aDirPrefId);
		}
		await cardbookBGPreferences.setPref("addressBooksNameList", addressBooksList.join(","));
	},

	emptyUnCachedAccountFromRepository: async function(aDirPrefId, aName) {
		await cardbookBGRepo.removeAccountFromComplexSearch(aDirPrefId, true);
		await cardbookBGRepo.emptyAccountFromRepository(aDirPrefId, true);
		await cardbookIDBCard.removeCardsFromAccount(cardbookBGRepo.statusInformation, aDirPrefId, aName);
		await cardbookIDBCat.removeCatsFromAccount(cardbookBGRepo.statusInformation, aDirPrefId);
		await cardbookIDBImage.removeImagesFromAccount(cardbookBGRepo.statusInformation, aDirPrefId, aName);
	},

	emptyAccountFromRepository: async function(aDirPrefId, aCacheDeletion) {
		if (aCacheDeletion) {
			await cardbookIDBCard.removeViewsFromAccount(aDirPrefId);
		}
		for (let account in cardbookBGRepo.cardbookDisplayCards) {
			if (account.startsWith(aDirPrefId) && account != aDirPrefId) {
				delete cardbookBGRepo.cardbookDisplayCards[account];
			}
		}
		if (cardbookBGRepo.cardbookAccountsCategories[aDirPrefId]) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = [];
		}
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId]) {
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId] = {modified: 0, cards: []};
		}

		delete cardbookBGRepo.cardbookCardLongSearch[aDirPrefId];
		delete cardbookBGRepo.cardbookCardShortSearch[aDirPrefId];
		if (cardbookBGRepo.cardbookFileCacheCards[aDirPrefId]) {
			delete cardbookBGRepo.cardbookFileCacheCards[aDirPrefId];
		}
		if (cardbookBGRepo.cardbookFileCacheCategories[aDirPrefId]) {
			delete cardbookBGRepo.cardbookFileCacheCategories[aDirPrefId];
		}
		for (let i in cardbookBGRepo.cardbookCards) {
			let card = cardbookBGRepo.cardbookCards[i];
			if (card.dirPrefId == aDirPrefId) {
				delete cardbookBGRepo.cardbookCards[i];
			}
		}
		for (let i in cardbookBGRepo.cardbookCategories) {
			let myCategory = cardbookBGRepo.cardbookCategories[i];
			if (myCategory.dirPrefId == aDirPrefId) {
				delete cardbookBGRepo.cardbookCategories[i];
			}
		}
	},

	emptyComplexSearchFromRepository: async function(aDirPrefId, aCacheDeletion) {
		if (aCacheDeletion) {
			for (let account in cardbookBGRepo.cardbookDisplayCards) {
				if (account.startsWith(aDirPrefId+"::categories") || account.startsWith(aDirPrefId+"::org")) {
					delete cardbookBGRepo.cardbookDisplayCards[account];
				}
				await cardbookIDBCard.removeDisplayFromViews(account);
			}
			await cardbookIDBCat.removeCatsFromAccount(cardbookBGRepo.statusInformation, aDirPrefId);
		}
		if (cardbookBGRepo.cardbookAccountsCategories[aDirPrefId]) {
			cardbookBGRepo.cardbookAccountsCategories[aDirPrefId] = [];
		}
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId]) {
			cardbookBGRepo.cardbookDisplayCards[aDirPrefId] = {modified: 0, cards: []};
		}
		for (let i in cardbookBGRepo.cardbookCategories) {
			let myCategory = cardbookBGRepo.cardbookCategories[i];
			if (myCategory.dirPrefId == aDirPrefId) {
				delete cardbookBGRepo.cardbookCategories[i];
			}
		}
	},

	removeAccountFromDiscovery: async function (aDirPrefId) {
		let allDiscoveryAccounts = cardbookBGSynchronizationUtils.getAllURLsToDiscover();
		cardbookHTMLUtils.sortMultipleArrayByString(allDiscoveryAccounts,0,1);
		let withoutDiscoveryAccounts = cardbookBGSynchronizationUtils.getAllURLsToDiscover(aDirPrefId);
		if (allDiscoveryAccounts.length != withoutDiscoveryAccounts.length) {
			let addressBooks = cardbookBGPreferences.getPref("discoveryAccountsNameList");
			let addressBooksList = addressBooks.split(",");
			let user = cardbookBGPreferences.getUser(aDirPrefId);
			let url = cardbookBGSynchronizationUtils.getShortUrl(cardbookBGPreferences.getUrl(aDirPrefId));
			function filterAccount(element) {
				return (element != user + "::" + url);
			}
			addressBooksList = addressBooksList.filter(filterAccount);
			await cardbookBGPreferences.setPref("discoveryAccountsNameList", addressBooksList.join(","));
		}
	},

	removeAccountFromRepository: async function(aAccountId) {
		await cardbookBGRepo.removeAccountFromCollected(aAccountId);
		await cardbookBGRepo.removeAccountFromRestrictions(aAccountId);
		await cardbookBGRepo.removeAccountFromVCards(aAccountId);
		await cardbookBGRepo.removeAccountFromBirthday(aAccountId);
		await cardbookBGRepo.removeAccountFromDiscovery(aAccountId);

		if (cardbookBGRepo.cardbookAccountsCategories[aAccountId]) {
			delete cardbookBGRepo.cardbookAccountsCategories[aAccountId];
		}

		for (let displayId in cardbookBGRepo.cardbookDisplayCards) {
			if (displayId.startsWith(`${aAccountId}`)) {
				await cardbookIDBCard.removeDisplayFromViews(displayId);
				delete cardbookBGRepo.cardbookDisplayCards[displayId];
			}
		}

		function searchCard2(element) {
			return (element[1] != aAccountId);
		}
		cardbookBGRepo.cardbookAccounts = cardbookBGRepo.cardbookAccounts.filter(searchCard2, aAccountId);

		delete cardbookBGRepo.cardbookCardLongSearch[aAccountId];
		delete cardbookBGRepo.cardbookCardShortSearch[aAccountId];
		if (cardbookBGRepo.cardbookFileCacheCards[aAccountId]) {
			delete cardbookBGRepo.cardbookFileCacheCards[aAccountId];
		}
		if (cardbookBGRepo.cardbookFileCacheCategories[aAccountId]) {
			delete cardbookBGRepo.cardbookFileCacheCategories[aAccountId];
		}
		for (let i in cardbookBGRepo.cardbookCards) {
			let card = cardbookBGRepo.cardbookCards[i];
			if (card.dirPrefId == aAccountId) {
				delete cardbookBGRepo.cardbookCards[i];
			}
		}
	},

	removeAccountFromComplexSearch: async function (aDirPrefId, aCacheDeletion) {
		if (cardbookBGRepo.cardbookDisplayCards[aDirPrefId]) {
			for (let dirPrefId in cardbookBGRepo.cardbookComplexSearch) {
				for (let displayId in cardbookBGRepo.cardbookDisplayCards) {
					if (displayId.startsWith(`${dirPrefId}`)) {
						if (aCacheDeletion) {
							let cards = cardbookBGRepo.cardbookDisplayCards[displayId].cards.filter(card => card.dirPrefId == aDirPrefId);
							for (let card of cards) {
								await cardbookIDBCard.removeCardFromViews(displayId, card);
							}
						}
						cardbookBGRepo.cardbookDisplayCards[displayId].cards = cardbookBGRepo.cardbookDisplayCards[displayId].cards.filter(card => card.dirPrefId != aDirPrefId);
						let modifiedCards = cardbookBGRepo.cardbookDisplayCards[displayId].cards.filter(card => card.updated || card.created);
						cardbookBGRepo.cardbookDisplayCards[displayId].modified = modifiedCards.length;

						if (cardbookBGPreferences.getEnabled(dirPrefId) && displayId.includes("::categories::") &&
							cardbookBGRepo.cardbookDisplayCards[displayId].cards.length == 0) {
							let category = displayId.split("::")[2];
							if (cardbookBGRepo.cardbookCategories[dirPrefId+"::"+category]) {
								let otherCat = cardbookBGRepo.cardbookCategories[dirPrefId+"::"+category];
								await cardbookBGRepo.removeCategoryFromDisplay(otherCat, dirPrefId, aCacheDeletion);
								cardbookBGRepo.removeCategoryFromCache(otherCat);
								cardbookBGRepo.removeCategoryFromList(otherCat);
							}
						}
					}
				}
			}
		}
	},

	removeComplexSearchFromRepository: async function(aAccountId) {
		if (cardbookBGRepo.cardbookAccountsCategories[aAccountId]) {
			for (let category of cardbookBGRepo.cardbookAccountsCategories[aAccountId]) {
				let accountId = `${aAccountId}::categories::${category}`;
				delete cardbookBGRepo.cardbookDisplayCards[accountId];
				await cardbookIDBCard.removeDisplayFromViews(accountId);
			}
			delete cardbookBGRepo.cardbookAccountsCategories[aAccountId];
			delete cardbookBGRepo.cardbookDisplayCards[aAccountId];
			await cardbookIDBCard.removeDisplayFromViews(aAccountId);
		}

		function searchCard2(element) {
			return (element[1] != aAccountId);
		}
		cardbookBGRepo.cardbookAccounts = cardbookBGRepo.cardbookAccounts.filter(searchCard2, aAccountId);

		delete cardbookBGRepo.cardbookComplexSearch[aAccountId];
	},
		
	deleteCards: async function (aListOfCards, aActionId) {
		try {
			for (let card of aListOfCards) {
				let dirPrefId = card.dirPrefId;
				if (cardbookBGPreferences.getReadOnly(dirPrefId) || !cardbookBGPreferences.getEnabled(dirPrefId)) {
					if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
						cardbookBGRepo.currentAction[aActionId].doneCards++;
					}
					continue;
				}
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					await cardbookBGRepo.addCardToAction(aActionId, "oldCards", card);
				}
				let name = cardbookBGPreferences.getName(dirPrefId);
				let type = cardbookBGPreferences.getType(dirPrefId);
				if (type === "FILE" || type === "DIRECTORY" || type === "LOCALDB") {
					await cardbookBGRepo.removeCardFromRepository(card, true);
				} else {
					if (card.created) {
						await cardbookBGRepo.removeCardFromRepository(card, true);
					} else {
						cardbookBGUtils.addTagDeleted(card);
						await cardbookBGRepo.addCardToCache(card, true);
						await cardbookBGRepo.removeCardFromRepository(card, false);
					}
				}
				cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "cardDeleted", [name, card.fn]);
				if (aActionId && cardbookBGRepo.currentAction[aActionId]) {
					cardbookBGRepo.currentAction[aActionId].doneCards++;
				}
			}
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.deleteCards error : " + e, "Error");
		}
	},

	asyncDeleteCards: async function (aListOfCards, aActionId) {
		try {
			let asyncDeleteCards = new Promise( async function(resolve, reject) {
				let taskHandle;
				let currentTaskNumber = 0;
				let taskList = [];

				async function runTaskQueue(deadline) {
					while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
						const task = taskList.shift();
						currentTaskNumber++;
						await task.handler(task.data, aActionId);
					}
					if (taskList.length) {
						taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
					} else {
						taskHandle = 0;
						resolve();
					}
				}
				for (let card of aListOfCards) {
					taskList.push({ handler: cardbookBGRepo.deleteCards, data: [ card ] });
				}
				if (!taskHandle) {
					taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
				}
			});
			await asyncDeleteCards;
		}
		catch (e) {
			cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookBGRepo.asyncDeleteCards error : " + e, "Error");
		}
	},

	reWriteFiles: async function (aListOfFiles) {
		let listOfFilesToRewrite = cardbookHTMLUtils.arrayUnique(aListOfFiles);
		for (let file of listOfFilesToRewrite) {
			if (cardbookBGPreferences.getType(file) === "FILE" && !cardbookBGPreferences.getReadOnly(file)) {
				let cards = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[file].cards));
				cardbookHTMLUtils.sortMultipleArrayByString(cards, "uid", 1);
				let url = cardbookBGPreferences.getUrl(file);
				let filename = cardbookBGUtils.getFileNameFromUrl(url);
				let data = await cardbookBGUtils.getDataForUpdatingFile(cardbookBGRepo.statusInformation, cards);
				let result = await messenger.NotifyTools.notifyExperiment({query: "cardbook.writeContentToFile", url: url, data: data, utf8: "UTF8"});
				if (result == "OK") {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(cardbookBGRepo.statusInformation, "debug mode : file rewritten : " + filename);
				} else {
					cardbookBGLog.updateStatusProgressInformation(cardbookBGRepo.statusInformation, "cardbookXULUtils.writeContentToFile error : filename : " + filename, "Error");
				}
			}
		}
	},

	addAddressbook: async function (aAction, aDirPrefId, aAccountsToAdd) {
		let sourceUrl = "chrome/content/HTML/addressbooksconfiguration/wdw_addressbooksAdd.html";
		let params = new URLSearchParams();
		if (aDirPrefId) {
			params.set("dirPrefId", aDirPrefId);
		}
		if (aAction) {
			params.set("action", aAction);
		}
		if (aAccountsToAdd) {
			params.set("accountsToAdd", aAccountsToAdd);
		}

		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
			return win.id;
		}
	},

	removeAddressbook: async function (aDirPrefId, aWindId) {
		let path = cardbookBGPreferences.getUrl(aDirPrefId);
		let name = cardbookBGPreferences.getName(aDirPrefId);
		let type = cardbookBGPreferences.getType(aDirPrefId);
	
		let confirmTitle = messenger.i18n.getMessage("confirmTitle");
		let confirmMsg = messenger.i18n.getMessage("accountDeletionConfirmMessage", [name]);
		
		let sourceUrl = "chrome/content/HTML/alertUser/wdw_cardbookAlertUser.html";
		let params = new URLSearchParams();
		if (type === "FILE" || type === "DIRECTORY") {
			params.set("type", "confirmEx");
		} else {
			params.set("type", "confirm");
		}
		params.set("action", "removeAddressbook");
		params.set("winId", aWindId);
		params.set("lineId", aDirPrefId);
		params.set("title", confirmTitle);
		params.set("message", confirmMsg);
		if (type === "FILE" || type === "DIRECTORY") {
			let filename = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getLeafName", url: path});
			let deleteContentMsg = type === "FILE" ? messenger.i18n.getMessage("accountDeletiondeleteContentFileMessage", [filename]) :
											messenger.i18n.getMessage("accountDeletiondeleteContentDirMessage", [filename])	;
			params.set("buttons", "validate|cancel");
			params.set("checkboxAction", "deleteFile");
			params.set("checkboxMsg", deleteContentMsg);
			params.set("checkboxValue", "false");
		}
		let url = `${sourceUrl}?${params.toString()}`;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
			return win.id;
		}
	},

	fromValidationToArray: function (aDirPrefId, aType) {
		var aTargetArray = [];
		for (var url in cardbookBGRepo.cardbookServerValidation[aDirPrefId]) {
			if (url == "length" || url == "user") {
				continue;
			}
			if (cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].forget) {
				continue;
			}
			let version = [];
			let connection = cardbookBGRepo.supportedConnections.filter(connection => connection.id == aType);
			if (connection[0].vCard) {
				version = JSON.parse(JSON.stringify(connection[0].vCard));
			} else if (cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].version && 
							cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].version.length > 0) {
				version = cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].version;
			} else {
				version = JSON.parse(JSON.stringify(cardbookBGPreferences.getPref("supportedVersion")));
			}
			let type = connection[0].type;
			let id = "";
			let serverUrl = url;
			if (cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].id) {
				id = cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].id;
				serverUrl = cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].url;
			}

			let displayname = cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].displayName || cardbookBGRepo.cardbookServerValidation[aDirPrefId].user;
			let readOnly = cardbookBGRepo.cardbookServerValidation[aDirPrefId][url].readOnly || false;

			aTargetArray.push([type, serverUrl, cardbookBGRepo.cardbookServerValidation[aDirPrefId].user, displayname,
								version, "", id, false, readOnly, true]);
		}
		return aTargetArray;
	},

	renameOrgFromCard: function(aCard, aNodeId, aNewNodeName) {
		let nodeIdArray = aNodeId.split("::");
		aCard.org[nodeIdArray.length - 3] = cardbookHTMLUtils.escapeStringSemiColon(aNewNodeName);
	},

	renameCategoryFromCard: function(aCard, aOldCategoryName, aNewCategoryName) {
		cardbookHTMLUtils.removeCategoryFromCard(aCard, aOldCategoryName);
		cardbookHTMLUtils.addCategoryToCard(aCard, aNewCategoryName);
	},

	renameUncategorized: async function(aOldCategoryName, aNewCategoryName) {
		await cardbookBGPreferences.setPref("uncategorizedCards", aNewCategoryName);
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.setUncat", uncat: aNewCategoryName});
		for (let account of cardbookBGRepo.cardbookAccounts) {
			if (account[2]) {
				let dirPrefId = account[1];
				for (let category of cardbookBGRepo.cardbookAccountsCategories[dirPrefId]) {
					if (category == aOldCategoryName) {
						category = aNewCategoryName;
					}
				}
				let oldCatDisplayId = `${dirPrefId}::categories::${aOldCategoryName}`;
				let newCatDisplayId = `${dirPrefId}::categories::${aNewCategoryName}`;
				if (cardbookBGRepo.cardbookDisplayCards[oldCatDisplayId]) {
					cardbookBGRepo.cardbookDisplayCards[newCatDisplayId] = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[oldCatDisplayId]));
					delete cardbookBGRepo.cardbookDisplayCards[oldCatDisplayId];
					for (let card of cardbookBGRepo.cardbookDisplayCards[newCatDisplayId].cards) {
						await cardbookIDBCard.addCardToViews(newCatDisplayId, card);
					}
					await cardbookIDBCard.removeDisplayFromViews(oldCatDisplayId);
					let oldCat = cardbookBGRepo.cardbookCategories[`${dirPrefId}::${aOldCategoryName}`];
					await cardbookBGRepo.deleteCategories([oldCat]);
					let newCat = new cardbookCategoryParser(aNewCategoryName, dirPrefId);
					await cardbookBGRepo.saveCategory({}, newCat);
				}
				let oldOrgDisplayId = `${dirPrefId}::org::${aOldCategoryName}`;
				let newOrgDisplayId = `${dirPrefId}::org::${aNewCategoryName}`;
				if (cardbookBGRepo.cardbookDisplayCards[oldOrgDisplayId]) {
					cardbookBGRepo.cardbookDisplayCards[newOrgDisplayId] = JSON.parse(JSON.stringify(cardbookBGRepo.cardbookDisplayCards[oldOrgDisplayId]));
					for (let card of cardbookBGRepo.cardbookDisplayCards[newOrgDisplayId].cards) {
						await cardbookIDBCard.addCardToViews(newOrgDisplayId, card);
					}
					delete cardbookBGRepo.cardbookDisplayCards[oldOrgDisplayId];
					await cardbookIDBCard.removeDisplayFromViews(oldOrgDisplayId);
				}
			}
		}
	},

	askUser: async function(aType, aActionId, aMessage, aButtonArray) {
		let askUserPromise = new Promise( async function(resolve, reject) {
			let choice = aButtonArray.join("::");
			if (cardbookBGRepo.importConflictChoice[aActionId] && 
				cardbookBGRepo.importConflictChoice[aActionId][choice] &&
				cardbookBGRepo.importConflictChoice[aActionId][choice].result) {
				resolve(cardbookBGRepo.importConflictChoice[aActionId][choice].result);
			} else if (cardbookBGRepo.importConflictChoice[aActionId] && 
					cardbookBGRepo.importConflictChoice[aActionId][choice] &&
					cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage] &&
					cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage].result) {
				resolve(cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage].result);
			} else {
				if (!cardbookBGRepo.importConflictChoice[aActionId]) {
					cardbookBGRepo.importConflictChoice[aActionId] = {};
				}
				if (!cardbookBGRepo.importConflictChoice[aActionId][choice]) {
					cardbookBGRepo.importConflictChoice[aActionId][choice] = {};
					cardbookBGRepo.importConflictChoice[aActionId][choice].result = "";
				}
				if (!cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage]) {
					cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage] = {};
					cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage].result = "";
				}
				let sourceUrl = "chrome/content/HTML/askUser/wdw_cardbookAskUser.html";
				let params = new URLSearchParams();
				params.set("dirPrefId", aActionId);
				params.set("type", aType);
				params.set("message", aMessage);
				params.set("buttons", choice);
				let url = `${sourceUrl}?${params.toString()}`;
				let finalUrl = browser.runtime.getURL(url) + "*";
				let tabs = await browser.tabs.query({url: finalUrl});
				if (tabs.length) {
					await browser.windows.update(tabs[0].windowId, {focused:true});
					return tabs[0].windowId;
				} else {
					let state = {};
					try {
						let winPath = url.split("?")[0];
						let winName = winPath.split("/").pop();
						let prefName = `window.${winName}.state`;
						state = cardbookBGPreferences.getPref(prefName);
					} catch(e) {}
					let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
					let win = await browser.windows.create(winParams);
				}
		
				let waitTimer = setInterval( async function() {
					if (cardbookBGRepo.importConflictChoice[aActionId][choice].result) {
						resolve(cardbookBGRepo.importConflictChoice[aActionId][choice].result);
						clearInterval(waitTimer);
					} else if (cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage].result) {
						resolve(cardbookBGRepo.importConflictChoice[aActionId][choice][aMessage].result);
						clearInterval(waitTimer);
					}
				}, 100);
			}
		});
		let result = await askUserPromise;
		return result;
	},

	askPassword: async function(aUser, aUrl) {
		let askPasswordPromise = new Promise( async function(resolve, reject) {
			cardbookBGRepo.askPasswordChoice[aUser] = {}
			cardbookBGRepo.askPasswordChoice[aUser][aUrl] = "?";
			let sourceUrl = "chrome/content/HTML/askPassword/wdw_cardbookAskPassword.html";
			let params = new URLSearchParams();
			params.set("username", aUser);
			params.set("url", aUrl);
			let url = `${sourceUrl}?${params.toString()}`;
			let finalUrl = browser.runtime.getURL(url) + "*";
			let tabs = await browser.tabs.query({url: finalUrl});
			if (tabs.length) {
				await browser.windows.update(tabs[0].windowId, {focused:true});
				return tabs[0].windowId;
			} else {
				let state = {};
				try {
					let winPath = url.split("?")[0];
					let winName = winPath.split("/").pop();
					let prefName = `window.${winName}.state`;
					state = cardbookBGPreferences.getPref(prefName);
				} catch(e) {}
				let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
				let win = await browser.windows.create(winParams);
			}
	
			let waitTimer = setInterval( async function() {
				if (cardbookBGRepo.askPasswordChoice[aUser][aUrl] != "?") {
					resolve(cardbookBGRepo.askPasswordChoice[aUser][aUrl]);
					clearInterval(waitTimer);
				}
			}, 100);
		});
		let result = await askPasswordPromise;
		return result;
	},

	notifyObservers: async function(aTopic, aParam) {
		if (aTopic) {
			let message = aTopic.startsWith("cardbook.") ? aTopic : `cardbook.${aTopic}`;
			let notification = messenger.runtime.sendMessage({query: message, aData: aParam});
			await notification.catch(() => {});
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.notifyObserver", value: message, params: aParam});
		}
	},

	mergeCardsFromSync: async function(aCacheCard, aTempCard, aConnection, aEtag, aSource, aActionId = null) {
		let mergeCardsPromise = new Promise( async function(resolve, reject) {
			try {
				aTempCard.uid = cardbookHTMLUtils.getUUID();
				aTempCard.cbid = `${aCacheCard.dirPrefId}::${aTempCard.uid}`;
				cardbookBGUtils.addTagDeleted(aTempCard);
				aTempCard.cardurl = "";
				aTempCard.cacheuri = "";
				// temporary card
				cardbookBGRepo.addCardToTemp(aTempCard);
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid] = {};
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid].etag = aEtag;
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid].connection = aConnection;
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid].prefIdType = cardbookBGPreferences.getType(aCacheCard.dirPrefId);
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid].cacheCard = aCacheCard;
				cardbookBGRepo.cardbookServerSyncMerge[aCacheCard.dirPrefId][aCacheCard.uid].tempCard = aTempCard;
			} catch (e) {
				cardbookBGRepo.cardbookServerCardSyncDone[aCacheCard.dirPrefId]++;
				cardbookBGRepo.cardbookServerGetCardForMergeResponse[aCacheCard.dirPrefId]++;
				cardbookBGRepo.cardbookServerGetCardForMergeError[aCacheCard.dirPrefId]++;
				let name = cardbookBGPreferences.getName(aCacheCard.dirPrefId);
				if (e.message == "") {
					cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "parsingCardError", [name, messenger.i18n.getMessage(e.code), aTempCard], "Error");
				} else {
					cardbookBGLog.formatStringForOutput(cardbookBGRepo.statusInformation, "parsingCardError", [name, e.message, aTempCard], "Error");
				}
				return;
			}
			let ids = [];
			ids.push(aCacheCard.cbid);
			ids.push(aTempCard.cbid);
			let mergeId = cardbookHTMLUtils.getUUID();
			if (aSource == "IMPORT") {
				cardbookBGRepo.currentAction[aActionId].params = {};
				cardbookBGRepo.currentAction[aActionId].params[mergeId] = {mergeId: mergeId, status: "STARTED"};
			}
			let mode = aCacheCard.isAList ? "LIST" : "CONTACT";
			let sourceUrl = "chrome/content/HTML/mergeCards/wdw_mergeCards.html";
			let params = new URLSearchParams();
			let hideCreate = (aSource == "SYNC") ? true : false;
			params.set("hideCreate", hideCreate);
			params.set("source", aSource);
			params.set("mode", mode);
			params.set("ids", ids.join(","));
			params.set("actionId", aActionId);
			params.set("mergeId", mergeId);

			let url = `${sourceUrl}?${params.toString()}`;
			let finalUrl = browser.runtime.getURL(url) + "*";
			let tabs = await browser.tabs.query({url: finalUrl});
			if (tabs.length) {
				await browser.windows.update(tabs[0].windowId, {focused:true});
				return tabs[0].windowId;
			} else {
				let state = {};
				try {
					let winPath = url.split("?")[0];
					let winName = winPath.split("/").pop();
					let prefName = `window.${winName}.state`;
					state = cardbookBGPreferences.getPref(prefName);
				} catch(e) {}
				let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
				let win = await browser.windows.create(winParams);
			}
			if (aSource != "IMPORT") {
				resolve();
			} else {
				let waitTimer = setInterval( async function() {
					if (cardbookBGRepo.currentAction[aActionId].params[mergeId].status == "FINISHED") {
						resolve();
						clearInterval(waitTimer);
					}
				}, 100);
			}
		});
		let result = await mergeCardsPromise;
		return result;
	},
};
