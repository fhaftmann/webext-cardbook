import { MimeAddressParser } from "../mimeParser/MimeJSComponents.sys.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";

export function cardbookListConversion(aRepo, aEmails, aIdentity, aOnlyEmail) {
    this.emailResult = [];
    this.recursiveList = [];
    this._convert(aRepo, aEmails, aIdentity, aOnlyEmail);
}

cardbookListConversion.prototype = {
    _verifyRecursivity: function (aRepo, aListName) {
        if (this.recursiveList.includes(aListName)) {
            cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "errorInfiniteLoopRecursion", [this.recursiveList], "Warning");
            return false;
        } else {
            this.recursiveList.push(aListName);
            return true;
        }
    },
    
    _getEmails: function (aRepo, aCard, aIdentity, aOnlyEmail) {
        if (aCard.isAList) {
            let myList = aCard.fn;
            if (this._verifyRecursivity(aRepo, myList)) {
                this._convert(MimeAddressParser.prototype.makeMimeAddress(myList, myList), aIdentity, aOnlyEmail);
            }
        } else {
            let listOfEmail = cardbookBGUtils.getMimeEmailsFromCards([aCard], aOnlyEmail);
            this.emailResult = this.emailResult.concat(listOfEmail);
        }
    },
    
    _convert: function (aRepo, aEmails, aIdentity, aOnlyEmail) {
        let useOnlyEmail = aOnlyEmail || cardbookBGPreferences.getPref("useOnlyEmail");
        let addresses = MimeAddressParser.prototype.parseEncodedHeaderW(aEmails);
        for (let address of addresses) {
            let fullAddress = MimeAddressParser.prototype.makeMimeAddress(address.name, address.email);
            if (address.email.includes("@")) {
                if (useOnlyEmail) {
                    this.emailResult.push(address.email);
                } else {
                    this.emailResult.push(MimeAddressParser.prototype.makeMimeAddress(address.name, address.email));
                }
            // for Mail Merge compatibility
            } else if (fullAddress.includes("{{") && fullAddress.includes("}}")) {
                this.emailResult.push(fullAddress);
            } else {
                let found = false;
                for (let j in aRepo.cardbookCards) {
                    let myCard = aRepo.cardbookCards[j];
                    if (myCard.isAList && myCard.fn == address.name) {
                        found = true;
                        if (!this.recursiveList.includes(address.name)) {
                            this.recursiveList.push(address.name);
                        }
                        let myMembers = cardbookBGUtils.getMembersFromCard(aRepo, myCard);
                        for (let email of myMembers.mails) {
                            this.emailResult.push(email.toLowerCase());
                        }
                        for (let card of myMembers.uids) {
                            this._getEmails(aRepo, card, aIdentity, useOnlyEmail);
                        }
                        break;
                    }
                }
                if (!found) {
                    this.emailResult.push(fullAddress);
                }
            }
        }
    }
};