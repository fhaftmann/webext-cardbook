import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookHTMLFormulas } from "../../HTML/utils/scripts/cardbookHTMLFormulas.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export function cardbookCardParser(vCardData, vSiteUrl, vEtag, vDirPrefId) {
	this._init(vDirPrefId);
	if (vCardData) {
		this._parseCard(vCardData, vSiteUrl, vEtag);
	}
	this._finish();
}

cardbookCardParser.prototype = {
	_init: function (vDirPrefId) {
		if (vDirPrefId) {
			this.dirPrefId = vDirPrefId;
		} else {
			this.dirPrefId = "";
		}
		this.cardurl = "";
		this.cacheuri = "";
		this.etag = "0";
		this.updated = false;
		this.deleted = false;
		this.created = false;
		this.isAList = false;

		this.lastname = "";
		this.firstname = "";
		this.othername = "";
		this.prefixname = "";
		this.suffixname = "";
		this.fn = "";
		this.nickname = "";
		this.bday = "";
		this.gender = "";
		this.birthplace = "";
		this.anniversary = "";
		this.deathdate = "";
		this.deathplace = "";
		this.adr = [];
		this.tel = [];
		this.email = [];
		this.emails = [];
		this.impp = [];
		this.url = [];
		this.mailer = "";
		this.tz = [];
		this.geo = [];
		this.title = "";
		this.role = "";
		this.agent = "";
		this.org = [];
		this.categories = [];
		this.note = "";
		this.prodid = "";
		this.sortstring = "";
		this.uid = "";
		this.rev = "";
		this.version = "";
		this.class1 = "";
		// 3.0 allows one key as 4.0 allows multiples keys : chosen multiple keys for all
		this.key = [];
		this.others = [];
		this.cbid = "";

		this.photo = {types: [], value: "", URI: "", extension: "", attachmentId: ""};
		this.logo = {types: [], value: "", URI: "", extension: ""};
		this.sound = {types: [], value: "", URI: "", extension: ""};

		this.kind = "";
		this.member = [];
	},

	_finish: function () {
		if (this.uid == "") {
			cardbookBGUtils.setCardUUID(this);
		}
	},

	mediaParser: function (aField, aString) {
		let record = {types: [], value: "", URI: "", extension: ""};
		try {
			aString = aString.replace(/\\\\n/g,"");
			let localDelim0 = aString.indexOf(",",0);
			if (localDelim0 >= 0) {
				// 4.0
				// FROM : PHOTO:data:image/jpeg;base64,R0lGODlhCw…
				// FROM : KEY:data:application/pgp-keys;base64,[base64-data]
				var headerTmp = aString.substr(0,localDelim0).replace(/^:/, "");
				var trailerTmp = aString.substr(localDelim0+1,aString.length);
				var headerTmpArray = [];
				headerTmpArray = headerTmp.toLowerCase().split(";");
				record.types = JSON.parse(JSON.stringify(headerTmpArray));
				record.value = trailerTmp;
				for (let i = 0; i < headerTmpArray.length; i++) {
					if (headerTmpArray[i].indexOf("data:image",0) >= 0) {
						record.extension = headerTmpArray[i].replace(/data:image\//g,"").replace(/\s/g,"");
					}
				}
				record.extension = cardbookHTMLUtils.formatExtension(record.extension, "4.0");
			} else {
				// 3.0
				// FROM : PHOTO;ENCODING=b;TYPE=image/jpeg:R0lGODlhCw…
				// FROM : KEY;TYPE=PGP;ENCODING=b:[base64-data]
				// FROM : KEY;TYPE=PGP:http://example.com/key.pgp
				// FROM : PHOTO;X-ABCROP-RECTANGLE=ABClipRect_1&0&0&583&583&AbGQEWkRV74gSDvD5j4+wg==;VALUE=uri:https://p44-contacts.icloud.com:443/10151953909/wbs/01a57d0472c3cf027a6d20bb7d690688f17da8fb13
				// FROM : PHOTO;ENCODING=B;TYPE=JPEG;VALUE=BINARY:/9j/4AAQSkZ
				// 3.0 and 4.0
				// FROM : PHOTO:http://www.example.com/pub/photos/jqpublic.gif
				// 4.0
				// FROM : KEY;MEDIATYPE=application/pgp-keys:http://example.com/key.pgp
				var localDelim1 = aString.indexOf(":",0);
				if (localDelim1 >= 0) {
					var headerTmp = aString.substr(0,localDelim1);
					var trailerTmp = aString.substr(localDelim1+1,aString.length);
					var headerTmpArray = [];
					headerTmpArray = headerTmp.toUpperCase().split(";");
					if ((trailerTmp.search(/^http/i) >= 0) || (trailerTmp.search(/^file/i) >= 0)) {
						record.URI = trailerTmp;
						record.extension = cardbookHTMLUtils.getFileExtension(trailerTmp);
						if (!record.extension) {
							for (let i = 0; i < headerTmpArray.length; i++) {
								if (headerTmpArray[i].indexOf("TYPE=",0) >= 0) {
									record.extension = headerTmpArray[i].replace("TYPE=","").replace("IMAGE/","").replace(/\s/g,"");
								}
							}
						}
					} else {
						record.value = trailerTmp;
						record.types = JSON.parse(JSON.stringify(headerTmpArray));
						for (let i = 0; i < headerTmpArray.length; i++) {
							if (headerTmpArray[i].indexOf("TYPE=",0) >= 0) {
								record.extension = headerTmpArray[i].replace("TYPE=","").replace("IMAGE/","").replace(/\s/g,"");
							}
						}
					}
					record.extension = cardbookHTMLUtils.formatExtension(record.extension, this.version);
				}
			}
		} catch(e) {}
		if (Array.isArray(this[aField])) {
			this[aField].push(record);
		} else {
			this[aField] = record;
		}
	},

	_parseCard: function (vCardData, vSiteUrl, vEtag) {
		var re = /[\n\u0085\u2028\u2029]|\r\n?/;
		var vCardDataArray = cardbookHTMLUtils.cleanArrayWithoutTrim(vCardData.split(re));
		try {
			// For multilines data
			var limit = vCardDataArray.length-1;
			for (var i = 0; i < limit; i++) {
				while (vCardDataArray[i+1].substr(0,1) == " ") {
					vCardDataArray[i] = vCardDataArray[i] + vCardDataArray[i+1].substr(1);
					vCardDataArray.splice(i+1, 1);
					limit--;
				}
			}
			
			var myPGName = "";
			var myPGField = "";
			var myPGArray = [];
			var myPGToBeParsed = {};
			
			for (var vCardDataArrayIndex = 0; vCardDataArrayIndex < vCardDataArray.length-1;) {
				var localDelim1 = -1;
				var localDelim2 = -1;
				var vCardDataArrayHeaderKey = "";
				var vCardDataArrayHeaderOption = "";
				var vCardDataArrayHeader = "";
				var vCardDataArrayTrailer = "";
				var vCardDataArrayTrailerArray = [];
				var vCardDataArrayHeaderOptionArray = [];
				localDelim1 = vCardDataArray[vCardDataArrayIndex].indexOf(":",0);
				vCardDataArrayHeaderKey = "";
				vCardDataArrayHeaderOption = "";
				vCardDataArrayTrailer = "";
				
				// Splitting data
				if (localDelim1 >= 0) {
					vCardDataArrayHeader = vCardDataArray[vCardDataArrayIndex].substr(0,localDelim1).trim();
					vCardDataArrayTrailer = vCardDataArray[vCardDataArrayIndex].substr(localDelim1+1,vCardDataArray[vCardDataArrayIndex].length).trim();
					// for Google
					vCardDataArrayTrailer = vCardDataArrayTrailer.replace(/\\:/g, ":");
					localDelim2 = vCardDataArrayHeader.indexOf(";",0);
					if (localDelim2 >= 0) {
						vCardDataArrayHeaderKey = vCardDataArrayHeader.substr(0,localDelim2).toUpperCase();
						vCardDataArrayHeaderOption = vCardDataArrayHeader.substr(localDelim2+1,vCardDataArrayHeader.length);
					} else {
						vCardDataArrayHeaderKey = vCardDataArrayHeader.toUpperCase();
						vCardDataArrayHeaderOption = "";
					}
				}

				if ( vCardDataArrayIndex < vCardDataArray.length-1) {
					vCardDataArrayIndex++;
				}

				// property groups, example that should be parsed
				// ITEM1.X-ABLABEL:eee
				// ITEM1.ADR;TYPE=PREF:;;rue des angeliques;BORDEAUX;;33000;France
				// item1.X-ABADR:fr
				if (vCardDataArrayHeaderKey.indexOf(".") >= 0) {
					myPGArray = vCardDataArrayHeaderKey.toUpperCase().split(".");
					myPGName = myPGArray[0];
					myPGField = myPGArray[1];
					// Apple ITEM1.X-ABLABEL:_$!<HomePage>!$_
					// Apple ITEM1.X-ABLABEL:_$!<Anniversary>!$_
					if (vCardDataArrayTrailer.startsWith("_$!<") && vCardDataArrayTrailer.endsWith(">!$_")) {
						let translatedValue = "";
						let value = vCardDataArrayTrailer.replace("_$!<", "").replace(">!$_", "");
						try {
							translatedValue = messenger.i18n.getMessage(`${value}Type`);
						} catch (e) {}
						if (!translatedValue) {
							try {
								translatedValue = messenger.i18n.getMessage(`${value}Label`);
							} catch (e) {}
						}
						if (translatedValue) {
							vCardDataArrayTrailer = translatedValue;
						}
					}
					if (cardbookHTMLUtils.multilineFields.indexOf(myPGField.toLowerCase()) >= 0) {
						vCardDataArrayHeaderKey = myPGField;
					} else {
						if (!(myPGToBeParsed[myPGName] != null && myPGToBeParsed[myPGName] !== undefined && myPGToBeParsed[myPGName] != "")) {
							myPGToBeParsed[myPGName] = [];
						}
						if (vCardDataArrayHeaderOption) {
							myPGToBeParsed[myPGName].push(myPGField + ";" + vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer);
						} else {
							myPGToBeParsed[myPGName].push(myPGField + ":" + vCardDataArrayTrailer);
						}
						continue;
					}
				} else {
					myPGName = "";
				}

				switch (vCardDataArrayHeaderKey) {
					case "BEGIN":
						break;
					case "END":
						break;
					case "UID":
						this.uid = vCardDataArrayTrailer.replace(/^urn:uuid:/i, "");
						if (cardbookBGPreferences.getUrnuuid(this.dirPrefId)) {
							this.uid = "urn:uuid:" + this.uid;
						}
						break;
					case "N":
						vCardDataArrayTrailerArray = [];
						vCardDataArrayTrailerArray = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer).replace(/,/g, ' ').split(";");
						if (vCardDataArrayTrailerArray[0]) {
							this.lastname = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailerArray[0]);
							this.lastname = cardbookHTMLUtils.unescapeStringBackslash(this.lastname);
						} else {
							this.lastname = "";
						}
						if (vCardDataArrayTrailerArray[1]) {
							this.firstname = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailerArray[1]);
							this.firstname = cardbookHTMLUtils.unescapeStringBackslash(this.firstname);
						} else {
							this.firstname = "";
						}
						if (vCardDataArrayTrailerArray[2]) {
							this.othername = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailerArray[2]);
							this.othername = cardbookHTMLUtils.unescapeStringBackslash(this.othername);
						} else {
							this.othername = "";
						}
						if (vCardDataArrayTrailerArray[3]) {
							this.prefixname = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailerArray[3]);
							this.prefixname = cardbookHTMLUtils.unescapeStringBackslash(this.prefixname);
						} else {
							this.prefixname = "";
						}
						if (vCardDataArrayTrailerArray[4]) {
							this.suffixname = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailerArray[4]);
							this.suffixname = cardbookHTMLUtils.unescapeStringBackslash(this.suffixname);
						} else {
							this.suffixname = "";
						}
						break;
					case "FN":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.fn = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "NICKNAME":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.nickname = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "BDAY":
						this.bday = vCardDataArrayTrailer;
						break;
					case "ADR":
						let vCardDataArrayTrailerTmp = vCardDataArrayTrailer.replace(/;/g,"");
						if (vCardDataArrayTrailerTmp) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
							vCardDataArrayTrailer = cardbookHTMLUtils.replaceComma(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeArrayNew(vCardDataArrayTrailer.split(";"));
							// seven values
							if (vCardDataArrayTrailerArray.length > 7) {
								vCardDataArrayTrailerArray = vCardDataArrayTrailerArray.slice(0, 7);
							} else if (vCardDataArrayTrailerArray.length < 7) {
								while (vCardDataArrayTrailerArray.length < 7) {
									vCardDataArrayTrailerArray.push("");
								}
							}
							this.adr.push([vCardDataArrayTrailerArray, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray), myPGName, []]);
						}
						break;
					case "TEL":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.tel.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray), myPGName, []]);
						}
						break;
					case "EMAIL":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.email.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray), myPGName, []]);
						}
						break;
					case "TITLE":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.title = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "ROLE":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.role = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "ORG":
						if (vCardDataArrayTrailer != ";") {
							vCardDataArrayTrailer = vCardDataArrayTrailer.replace(/;+$/, "");
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
							this.org = cardbookHTMLUtils.unescapeArrayNew(vCardDataArrayTrailer.split(";"));
						}
						break;
					case "CATEGORIES":
						if (vCardDataArrayTrailer != "") {
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
							this.categories = cardbookHTMLUtils.unescapeArrayNew(vCardDataArrayTrailer.split(","));
							this.categories = cardbookHTMLUtils.cleanCategories(this.categories);
						}
						break;
					case "NOTE":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.note = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "PRODID":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.prodid = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "SORT-STRING":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.sortstring = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "PHOTO":
						this.mediaParser("photo", vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer);
						break;
					case "LOGO":
						this.mediaParser("logo", vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer);
						break;
					case "SOUND":
						this.mediaParser("sound", vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer);
						break;
					case "URL":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.url.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray), myPGName, []]);
						}
						break;
					case "VERSION":
						this.version = (vCardDataArrayTrailer == "2.1") ? "3.0" : vCardDataArrayTrailer;
						break;
					case "CLASS":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.class1 = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "KEY":
						this.mediaParser("key", vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer, true);
						break;
					case "REV":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.rev = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "KIND":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.kind = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "MEMBER":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						this.member.push(vCardDataArrayTrailer);
						break;
					case "GENDER":
						this.gender = vCardDataArrayTrailer;
						break;
					case "BIRTHPLACE":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.birthplace = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "ANNIVERSARY":
						this.anniversary = vCardDataArrayTrailer;
						break;
					case "DEATHDATE":
						this.deathdate = vCardDataArrayTrailer;
						break;
					case "DEATHPLACE":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.deathplace = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "MAILER":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						this.mailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						break;
					case "TZ":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.tz.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray)]);
						}
						break;
					case "GEO":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailerArray = [];
							vCardDataArrayHeaderOptionArray = [];
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.geo.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray)]);
						}
						break;
					case "AGENT":
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
						vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
						if (vCardDataArrayHeaderOption) {
							this.agent = vCardDataArrayHeaderOption + ":" + vCardDataArrayTrailer;
						} else {
							this.agent = vCardDataArrayTrailer;
						}
						break;
					case "IMPP":
						if (vCardDataArrayTrailer) {
							vCardDataArrayTrailer = cardbookHTMLUtils.escapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailerArray = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer).split(";");
							vCardDataArrayHeaderOptionArray = cardbookHTMLUtils.formatTypes(cardbookHTMLUtils.escapeString(vCardDataArrayHeaderOption).split(";"));
							let values = [];
							values = cardbookHTMLUtils.unescapeArray(vCardDataArrayTrailerArray);
							this.impp.push([values, cardbookHTMLUtils.unescapeArray(vCardDataArrayHeaderOptionArray), myPGName, []]);
						}
						break;
					default:
						if (vCardDataArrayHeaderKey) {
							vCardDataArrayTrailer = cardbookHTMLUtils.unescapeString(vCardDataArrayTrailer);
							vCardDataArrayTrailer = cardbookHTMLUtils.unescapeStringBackslash(vCardDataArrayTrailer);
							this.others.push(vCardDataArrayHeader.replace(/ /g,"") + ":" + vCardDataArrayTrailer);
						}
				}
			}

			// have to finish the PG parsing
			for (var i in myPGToBeParsed) {
				let myPGLocalName = i;
				let found = false;
				for (let field of cardbookHTMLUtils.multilineFields) {
					if (found) {
						break;
					}
					for (var k = 0; k < this[field].length; k++) {
						if (this[field][k][2] == myPGLocalName) {
							this[field][k][3] = JSON.parse(JSON.stringify(myPGToBeParsed[myPGLocalName]));
							found = true;
							break;
						}
					}
				}
				if (!found) {
					for (var j = 0; j < myPGToBeParsed[myPGLocalName].length; j++) {
						this.others.push(myPGLocalName + "." + myPGToBeParsed[myPGLocalName][j]);
					}
				}
			}
						
			cardbookBGUtils.setCalculatedFieldsWithoutRev(this);
			
			if (vSiteUrl) {
				this.cardurl = vSiteUrl;
			}
			
			if (this.fn == "") {
				cardbookHTMLFormulas.getDisplayedName(this, this.dirPrefId, [this.prefixname, this.firstname, this.othername, this.lastname, this.suffixname, this.nickname],
															[this.org, this.title, this.role]);
			}
			
			cardbookBGUtils.addEtag(this, vEtag);
		}
		catch (e) {
			throw({code : "UNKNOWN_ERROR", message: e});
		}
	}
};
