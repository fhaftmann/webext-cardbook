import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookHTMLDates } from "../../HTML/utils/scripts/cardbookHTMLDates.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export var cardbookBGBirthdays = {
	lBirthdayList : [],
	lBirthdayAccountList : {},
	popupShowed: 0,

	getEventName: function (aEventTitle, aDisplayName, aAge, aYear, aName, aEventType) {
		return aEventTitle.replace("%1$S", aDisplayName).replace("%2$S", aAge).replace("%3$S", aYear).replace("%4$S", aName).replace("%5$S", aEventType);
	},

	daysBetween: function (date1, date2) {
		// The number of milliseconds in one day
		let oneDay = 1000 * 60 * 60 * 24;
		let newDate1 = new Date(date1.getUTCFullYear(), date1.getUTCMonth(), date1.getUTCDate());
		let newDate2 = new Date(date2.getUTCFullYear(), date2.getUTCMonth(), date2.getUTCDate());
		return Math.round((newDate1.getTime() - newDate2.getTime())/(oneDay));
	},

	calcDateOfNextBirthday: function (lDateRef, lDateOfBirth) {
		let lDoB_Month= lDateOfBirth.getUTCMonth();
		let lDoB_Day = lDateOfBirth.getUTCDate();
		
		let lnextBirthday = new Date(lDateOfBirth);
		lnextBirthday.setUTCFullYear(lDateRef.getUTCFullYear());
		
		if (cardbookBGBirthdays.daysBetween(lnextBirthday, lDateRef)<0) {
			return new Date(Date.UTC(lDateRef.getUTCFullYear()+1, lDoB_Month, lDoB_Day));
		} else {
			return new Date(Date.UTC(lDateRef.getUTCFullYear(), lDoB_Month, lDoB_Day));
		}
	},

	getAllBirthdaysByName: function (aDateOfBirth, aCard, aNumberOfDays, aDateOfBirthFound, aEmail, aDirPrefId, aName, aType, aEventId) {
		let date_of_today = new Date();
		let endDate = new Date();
		let dateRef = new Date();
		let lnextBirthday;
		let lAge;
		let ldaysUntilNextBirthday;
		let lDateOfBirth = cardbookHTMLDates.convertDateStringToDateUTC(aDateOfBirth);

		let leventEntryTitle = cardbookBGPreferences.getPref("eventEntryTitle");
		endDate.setUTCDate(date_of_today.getDate()+parseInt(aNumberOfDays));
		while (dateRef < endDate) {
			lnextBirthday = cardbookBGBirthdays.calcDateOfNextBirthday(dateRef,lDateOfBirth);
			ldaysUntilNextBirthday = cardbookBGBirthdays.daysBetween(lnextBirthday, date_of_today);
			if (parseInt(ldaysUntilNextBirthday) <= parseInt(aNumberOfDays)) {
				if (lDateOfBirth.getUTCFullYear() == cardbookHTMLDates.defaultYear) {
					lAge = "?";
					var lBirthdayTitle =  cardbookBGBirthdays.getEventName(leventEntryTitle, aCard.fn, "0", "?", aName, aType);
				} else {
					lAge = lnextBirthday.getFullYear()-lDateOfBirth.getUTCFullYear();
					var lEventDate = cardbookHTMLDates.convertDateStringToDateUTC(aDateOfBirth);
					var lBirthdayTitle = cardbookBGBirthdays.getEventName(leventEntryTitle, aCard.fn, lAge, lEventDate.getUTCFullYear(), aName, aType);
				}

				if (ldaysUntilNextBirthday === parseInt(ldaysUntilNextBirthday)) {
					let dateDisplayedFormat = cardbookBGPreferences.getPref("dateDisplayedFormat");
					let dateOfBirthOld = cardbookHTMLDates.getFormattedDateForDateString(aDateOfBirth, dateDisplayedFormat)
					cardbookBGBirthdays.lBirthdayList.push([ldaysUntilNextBirthday, lBirthdayTitle, lAge, dateOfBirthOld, aDateOfBirthFound, aEmail, aDirPrefId, aName, aEventId]);
				} else {
					cardbookBGBirthdays.lBirthdayList.push(["0", lBirthdayTitle + " : Error", "0", "0", aDateOfBirthFound, aEmail, aDirPrefId, aName, aEventId]);
				}
				if (!(cardbookBGBirthdays.lBirthdayAccountList[aDirPrefId])) {
					cardbookBGBirthdays.lBirthdayAccountList[aDirPrefId] = "";
				}
			}
			dateRef.setMonth(dateRef.getMonth() + 12);
			// for repeating events one event is enough
			if (cardbookBGPreferences.getPref("repeatingEvent")) {
				return;
			}
		}
	},

	getBirthdayId: function (aCardId, aLabel, aUTCDate) {
		let dateSplitted = cardbookHTMLDates.splitUTCDateIntoComponents(aUTCDate);
		return `${aCardId}--${dateSplitted.year}${dateSplitted.month}${dateSplitted.day}-${aLabel}`;
	},

	getBirthdays: function (aRepo, aNumberOfDays) {
		let numberOfDays = (aNumberOfDays > 365) ? 365 : aNumberOfDays;
		let ABs = cardbookBGPreferences.getPref("addressBooksNameList");
		let useOnlyEmail = cardbookBGPreferences.getPref("useOnlyEmail");
		let search = {};
		for (let field of cardbookHTMLDates.dateFields) {
			search[field] = cardbookBGPreferences.getPref(`birthday.${field}`);
		}
		search.events = cardbookBGPreferences.getPref("birthday.events");
		cardbookBGBirthdays.lBirthdayList = [];

		let fieldType = {};
		for (let field of cardbookHTMLDates.dateFields) {
			fieldType[field] = messenger.i18n.getMessage(`${field}Label`);
		}
		fieldType["event"] = messenger.i18n.getMessage("eventInNoteEventPrefix");

		for (let i in aRepo.cardbookCards) {
			let card = aRepo.cardbookCards[i];
			let dirPrefId = card.dirPrefId;
			if (ABs.includes(dirPrefId) || ABs === "allAddressBooks") {
				let dateFormat = cardbookHTMLUtils.getDateFormat(dirPrefId, card.version);
				let ABname = cardbookBGPreferences.getName(dirPrefId);
				let name = cardbookHTMLUtils.getName(card);
				for (let field of cardbookHTMLDates.dateFields) {
					if (card[field] && card[field] != "" && search[field]) {
						let value = card[field];
						let isDate = cardbookHTMLDates.convertDateStringToDateUTC(value);
						if (isDate != "WRONGDATE") {
							let listOfEmail = cardbookBGUtils.getMimeEmailsFromCards([card], useOnlyEmail);
							let eventId = cardbookBGBirthdays.getBirthdayId(card.uid, field, isDate);
							cardbookBGBirthdays.getAllBirthdaysByName(value, card, numberOfDays, value, listOfEmail, dirPrefId, name, fieldType[field], eventId);
						} else {
							cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "dateEntry1Wrong", [ABname, card.fn, value, dateFormat], "Warning");
						}
					}
				}
				if (search.events) {
					let events = cardbookHTMLUtils.getEventsFromCard(card.note.split("\n"), card.others);
					for (let j = 0; j < events.result.length; j++) {
						let isDate = cardbookHTMLDates.convertDateStringToDateUTC(events.result[j][0]);
						if (isDate != "WRONGDATE") {
							let eventId = cardbookBGBirthdays.getBirthdayId(card.uid, events.result[j][1], isDate);
							let listOfEmail = cardbookBGUtils.getMimeEmailsFromCards([card], useOnlyEmail);
							cardbookBGBirthdays.getAllBirthdaysByName(events.result[j][0], card, numberOfDays, events.result[j][0], listOfEmail, dirPrefId, name, events.result[j][1], eventId);
						} else {
							cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "dateEntry1Wrong", [ABname, card.fn, events.result[j][0], dateFormat], "Warning");
						}
					}
				}
			}
		}
		return [ cardbookBGBirthdays.lBirthdayList, cardbookBGBirthdays.lBirthdayAccountList] ;
	},

	showBirthdayPopup: async function () {
		let sourceUrl = "chrome/content/HTML/birthdays/wdw_birthdayList.html";
		let url = sourceUrl;
		let finalUrl = browser.runtime.getURL(url) + "*";
		let tabs = await browser.tabs.query({url: finalUrl});
		if (tabs.length) {
			await browser.windows.update(tabs[0].windowId, {focused:true});
			return tabs[0].windowId;
		} else {
			let state = {};
			try {
				let winPath = url.split("?")[0];
				let winName = winPath.split("/").pop();
				let prefName = `window.${winName}.state`;
				state = cardbookBGPreferences.getPref(prefName);
			} catch(e) {}
			let winParams = { ...state, type: "popup", url: url, allowScriptsToClose: true};
			let win = await browser.windows.create(winParams);
			return win.id;
		}
	},

	init: async function (aRepo) {
		if (cardbookBGPreferences.getPref("showPopupOnStartup")) {
			let maxDaysUntilNextBirthday = cardbookBGPreferences.getPref("numberOfDaysForSearching");
			let [ birthdayList, birthdayAccountList ] = cardbookBGBirthdays.getBirthdays(aRepo, maxDaysUntilNextBirthday);
			let showPopupEvenIfNoBirthday = cardbookBGPreferences.getPref("showPopupEvenIfNoBirthday");
			if ((birthdayList[0] && birthdayList[0].length > 0) || showPopupEvenIfNoBirthday === true) {
				cardbookBGBirthdays.popupShowed++;
				cardbookBGBirthdays.showBirthdayPopup();
			}
		}
		
		if (cardbookBGPreferences.getPref("syncWithLightningOnStartup")) {
			let maxDaysUntilNextBirthday = cardbookBGPreferences.getPref("numberOfDaysForWriting");
			let [ birthdayList, birthdayAccountList ] = cardbookBGBirthdays.getBirthdays(aRepo, maxDaysUntilNextBirthday);
			
			let calendarsNameList = cardbookBGPreferences.getPref("calendarsNameList");
			let calendarEntryCategories = cardbookBGPreferences.getPref("calendarEntryCategories");
			let repeatingEvent = cardbookBGPreferences.getPref("repeatingEvent");
			let eventEntryWholeDay = cardbookBGPreferences.getPref("eventEntryWholeDay");
			let eventEntryTime = cardbookBGPreferences.getPref("eventEntryTime");
			let calendarEntryAlarm = cardbookBGPreferences.getPref("calendarEntryAlarm");
			let prefs = {calendarsNameList, calendarEntryCategories, repeatingEvent, eventEntryWholeDay, eventEntryTime, calendarEntryAlarm};
			for (let birthday of birthdayList) {
				birthday.push(cardbookHTMLUtils.getUUID());
			}
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.addBirthdaysToCalendar", birthdayList: birthdayList, prefs: prefs, sendResults: false});
		}

		let waitTimer = setInterval( async function() {
			if (cardbookBGPreferences.getPref("showPeriodicPopup")) {
				var popupTime = cardbookBGPreferences.getPref("periodicPopupIime");
				var dateOfToday = new Date();
				var dateOfTodayHour = (dateOfToday.getHours()<10?'0':'') + dateOfToday.getHours();
				var dateOfTodayMin = (dateOfToday.getMinutes()<10?'0':'') + dateOfToday.getMinutes();
				var checkTime = dateOfTodayHour.toString() + dateOfTodayMin.toString();
		
				var EmptyParamRegExp1 = new RegExp("(.*)([^0-9])(.*)", "ig");
				if (popupTime.replace(EmptyParamRegExp1, "$1")!=popupTime) {
					var checkPopupHour = popupTime.replace(EmptyParamRegExp1, "$1");
					var checkPopupMin = popupTime.replace(EmptyParamRegExp1, "$3");
					if (checkPopupHour < 10 && checkPopupHour.length == 1) {
						checkPopupHour = "0" + checkPopupHour;
					}
					if (checkPopupMin < 10 && checkPopupMin.length == 1) {
						checkPopupMin = "0" + checkPopupMin;
					}
					var checkPopupTime = checkPopupHour.toString() + checkPopupMin.toString();
				}
				
				if ((checkTime == checkPopupTime) && (cardbookBGBirthdays.popupShowed == 0)) {
					let maxDaysUntilNextBirthday = cardbookBGPreferences.getPref("numberOfDaysForSearching");
					let [ birthdayList, birthdayAccountList ] = cardbookBGBirthdays.getBirthdays(aRepo, maxDaysUntilNextBirthday);
					let showPopupEvenIfNoBirthday = cardbookBGPreferences.getPref("showPopupEvenIfNoBirthday");
					if ((birthdayList[0] && birthdayList[0].length > 0) || showPopupEvenIfNoBirthday === true) {
						cardbookBGBirthdays.popupShowed++;
						cardbookBGBirthdays.showBirthdayPopup();
					}
				} else if ((cardbookBGBirthdays.popupShowed > 0) && (cardbookBGBirthdays.popupShowed < 8)) {
					cardbookBGBirthdays.popupShowed++;
				} else {
					cardbookBGBirthdays.popupShowed=0;
				}
			}
		}, 10000);
	},
};

