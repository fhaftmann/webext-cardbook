
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

import { cardbookIDBEncryptor } from "../indexedDB/cardbookIDBEncryptor.mjs";
import { cardbookIndexedDB } from "../indexedDB/cardbookIndexedDB.mjs";
import { cardbookIDBCard } from "../indexedDB/cardbookIDBCard.mjs";
import { cardbookIDBCat } from "../indexedDB/cardbookIDBCat.mjs";
import { cardbookIDBDuplicate } from "../indexedDB/cardbookIDBDuplicate.mjs";
import { cardbookIDBImage } from "../indexedDB/cardbookIDBImage.mjs";
import { cardbookIDBMailPop } from "../indexedDB/cardbookIDBMailPop.mjs";
import { cardbookIDBPrefDispName } from "../indexedDB/cardbookIDBPrefDispName.mjs";
import { cardbookIDBSearch } from "../indexedDB/cardbookIDBSearch.mjs";
import { cardbookIDBUndo } from "../indexedDB/cardbookIDBUndo.mjs";

import { cardbookBGSynchronization } from "./cardbookBGSynchronization.mjs";
import { cardbookBGBirthdays } from "./cardbookBGBirthdays.mjs";
import { cardbookBGColumns } from "./cardbookBGColumns.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";

export var cardbookBGInit = {

	migratePrefsFromXul: async function () {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.migratePrefsFromXul"});
	},

	setThunderbirdPrefs: async function () {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.setThunderbirdPrefs"});
	},

	migrateOrgStructurePref: async function () {
		if (cardbookBGPreferences.getPref("orgStructureMigrated") == false) {
			let orgStructure = cardbookBGPreferences.getPref("orgStructure");
			if (!(orgStructure instanceof Array)) {
				let newOrg = [];
				if (orgStructure) {
					newOrg = orgStructure.split(";");
				}
				await cardbookBGPreferences.setPref("orgStructure", newOrg);
			}
			await cardbookBGPreferences.setPref("orgStructureMigrated", true);
		}
	},

	initPrefs: async function () {
		await cardbookBGPreferences.initPrefs();
	},

	initRepo: async function (repo) {
		// setting userAgent and prodid
		repo.userAgent = "Thunderbird CardBook/" + cardbookBGPreferences.getPref("addonVersion");
		let version = cardbookBGPreferences.getPref("addonVersion");
		let lang = navigator.language;
		lang = lang.toUpperCase();
		let prodid = `-//Thunderbird.net/NONSGML Thunderbird CardBook V${version}//${lang}`;
		await cardbookBGPreferences.setPref("prodid", prodid);

		// setting cardbookGenderLookup for having lookups
		for (let type in repo.cardbookGenderLookup) {
			repo.cardbookGenderLookup[type] = messenger.i18n.getMessage("types.gender." + type.toLowerCase());
		}

		// setting the default region
		let defaultRegion = cardbookBGPreferences.getPref("defaultRegion");
		if (defaultRegion == "NOTSET") {
			let defaultRegion = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getDefaultRegion"});
			await cardbookBGPreferences.setPref("defaultRegion", defaultRegion);
		}

		// setting the default region
		let impps = [];
		impps = cardbookBGPreferences.getAllIMPPs();
		if (impps.length == 0) {
			cardbookBGPreferences.insertIMPPsSeed();
		}

		// migration functions (should be removed)
		let customFields = cardbookBGPreferences.getAllCustomFields();
		for (let code in repo.possibleCustomFields) {
			repo.possibleCustomFields[code].label = messenger.i18n.getMessage(code.replace(/^X-/, "").replace(/-/g, "").toLowerCase() + "Label");
			let found = false;
			for (let j = 0; j < customFields.personal.length; j++) {
				if (customFields.personal[j][0] == code) {
					found = true;
					break;
				}
			}
			repo.possibleCustomFields[code].added = found;
		}
	},
	
	migrateCardsData: async function (aRepo) {
		let migrationStarted = false; 
		if (cardbookBGPreferences.getPref("indexedDB.searchesMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBSearch 1/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getSearchesData"});
			console.log("searches data.length : " + data.length);
			await cardbookIDBSearch.clearData();
			let result = await cardbookIDBSearch.loadData(data);
			console.log("searches : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.searchesMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.cardsMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBCard 2/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getCardsData"});
			console.log("cards data.length : " + data.length);
			await cardbookIDBCard.clearData();
			let result = await cardbookIDBCard.loadData(data);
			console.log("cards : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.cardsMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.catsMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBCat 3/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getCatsData"});
			console.log("cats data.length : " + data.length);
			await cardbookIDBCat.clearData();
			let result = await cardbookIDBCat.loadData(data);
			console.log("cats : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.catsMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.duplicatesMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBDuplicate 4/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getDuplicatesData"});
			console.log("duplicates data.length : " + data.length);
			await cardbookIDBDuplicate.clearData();
			let result = await cardbookIDBDuplicate.loadData(data);
			console.log("duplicates : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.duplicatesMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.imagesMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBImage 5/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getImagesData"});
			for (let media of cardbookHTMLUtils.allColumns.media) {
				console.log("images data.length : " + media + " : " + data[media].length);
			}
			await cardbookIDBImage.clearData();
			let result = await cardbookIDBImage.loadData(data);
			console.log("images : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.imagesMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.mailpopsMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBMailPop 6/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getMailPopsData"});
			console.log("mailpops data.length : " + data.length);
			await cardbookIDBMailPop.clearData();
			let result = await cardbookIDBMailPop.loadData(data);
			console.log("mailpops : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.mailpopsMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.prefDispNamesMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBPrefDispName 7/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPrefDispNamesData"});
			console.log("prefdispnames data.length : " + data.length);
			await cardbookIDBPrefDispName.clearData();
			let result = await cardbookIDBPrefDispName.loadData(data);
			console.log("prefdispnames : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.prefDispNamesMigrated", true);
			}
		}
		if (cardbookBGPreferences.getPref("indexedDB.undosMigrated") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBUndo 8/8");
			let data = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getUndosData"});
			console.log("undos data.length : " + data.length);
			await cardbookIDBUndo.clearData();
			let result = await cardbookIDBUndo.loadData(data);
			console.log("undos : " + result)
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.undosMigrated", true);
			}
		}

		if (cardbookBGPreferences.getPref("indexedDB.cardsMigrated2") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBCard 0/1");
			let result = await cardbookIDBCard.cardsMigrated2();
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.cardsMigrated2", true);
			}
		}

		if (cardbookBGPreferences.getPref("indexedDB.cardsMigrated3") === false) {
			migrationStarted = true;
			await aRepo.notifyObservers("DBMigrate", "cardbookIDBCard 0/1");
			let result = await cardbookIDBCard.cardsMigrated3(aRepo);
			if (result == "OK") {
				await cardbookBGPreferences.setPref("indexedDB.cardsMigrated3", true);
			}
		}

		if (migrationStarted) {
			await aRepo.notifyObservers("DBMigrateFinished");
		}
	},

	startup: async function (repo) {
		await cardbookBGInit.migratePrefsFromXul();
		await cardbookBGInit.migrateOrgStructurePref();
		await cardbookBGInit.setThunderbirdPrefs();
		await cardbookBGInit.initPrefs();
		await cardbookBGInit.initRepo(repo);

		await cardbookIDBEncryptor.init();

		await cardbookIDBCard.openCardDB();
		await cardbookIDBCat.openCatDB();
		await cardbookIDBDuplicate.openDuplicateDB();
		await cardbookIDBImage.openImageDB();
		await cardbookIDBMailPop.openMailPopDB();
		await cardbookIDBPrefDispName.openPrefDispNameDB();
		await cardbookIDBSearch.openSearchDB();
		await cardbookIDBUndo.openUndoDB(repo);

		await cardbookBGInit.migrateCardsData(repo);

		await cardbookIndexedDB.upgradeDBs();

		await cardbookIDBMailPop.loadMailPop(repo);
		await cardbookIDBPrefDispName.loadPrefDispName(repo);

		await cardbookBGSynchronization.loadComplexSearchAccounts(repo);
		await cardbookBGSynchronization.loadAccounts(repo);

		if (cardbookBGPreferences.getPref("initialSync") === true) {
			let initialSyncDelay = cardbookBGPreferences.getPref("initialSyncDelay");
			let initialSyncDelayMs = initialSyncDelay * 1000 || 0;
			if (initialSyncDelayMs == 0) {
				cardbookBGSynchronization.syncAccounts(repo);
			} else {
				setTimeout(async function() {
					cardbookBGSynchronization.syncAccounts(repo);
					}, initialSyncDelayMs);               
			}
		} else {
			repo.initialSync = false;
		}

		await cardbookBGBirthdays.init(repo);
		await cardbookBGColumns.init(repo);
	},
};



