import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGFiles } from "./cardbookBGFiles.mjs";
import { cardbookBGSynchronizationGoogle2 } from "./cardbookBGSynchronizationGoogle2.mjs";
import { cardbookBGSynchronizationCARDDAV } from "./cardbookBGSynchronizationCARDDAV.mjs";
import { cardbookBGSynchronizationUtils } from "./cardbookBGSynchronizationUtils.mjs";

import { cardbookHTMLDates } from "../../HTML/utils/scripts/cardbookHTMLDates.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

import { cardbookIDBSearch } from "../indexedDB/cardbookIDBSearch.mjs";
import { cardbookIDBCat } from "../indexedDB/cardbookIDBCat.mjs";
import { cardbookIDBCard } from "../indexedDB/cardbookIDBCard.mjs";

export var cardbookBGSynchronization = {
	// used for discoveries
	gDiscoveryDescription: "Discovery module",

	initDiscoveryOperations: function(aRepo, aDirPrefId) {
		aRepo.cardbookServerValidation[aDirPrefId] = {};
	},
	
	stopDiscoveryOperations: function(aRepo, aDirPrefId) {
		delete aRepo.cardbookServerValidation[aDirPrefId];
	},
	
	initMultipleOperations: function(aRepo, aDirPrefId) {
		aRepo.cardbookSyncMode[aDirPrefId] = 1;
		aRepo.cardbookAccessTokenRequest[aDirPrefId] = 0;
		aRepo.cardbookAccessTokenResponse[aDirPrefId] = 0;
		aRepo.cardbookAccessTokenError[aDirPrefId] = 0;
		aRepo.cardbookRefreshTokenRequest[aDirPrefId] = 0;
		aRepo.cardbookRefreshTokenResponse[aDirPrefId] = 0;
		aRepo.cardbookRefreshTokenError[aDirPrefId] = 0;
		aRepo.cardbookCardsFromCache[aDirPrefId] = {};
		aRepo.cardbookServerDiscoveryRequest[aDirPrefId] = 0;
		aRepo.cardbookServerDiscoveryResponse[aDirPrefId] = 0;
		aRepo.cardbookServerDiscoveryError[aDirPrefId] = 0;
		aRepo.cardbookServerSyncMerge[aDirPrefId] = {};
		aRepo.cardbookServerSyncRequest[aDirPrefId] = 0;
		aRepo.cardbookServerSyncResponse[aDirPrefId] = 0;
		aRepo.cardbookServerCardSyncDone[aDirPrefId] = 0;
		aRepo.cardbookServerCardSyncTotal[aDirPrefId] = 0;
		aRepo.cardbookServerCardSyncError[aDirPrefId] = 0;
		aRepo.cardbookServerCatSyncDone[aDirPrefId] = 0;
		aRepo.cardbookServerCatSyncTotal[aDirPrefId] = 0;
		aRepo.cardbookServerCatSyncError[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNotUpdatedCard[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNotUpdatedCat[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNewCardOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNewCatOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNewCardOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncNewCatOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCardOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCatOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCardOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCatOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCardOnBoth[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCatOnBoth[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCardOnDiskDeletedCardOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncUpdatedCatOnDiskDeletedCatOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCardOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCatOnDisk[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCardOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCatOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCardOnDiskUpdatedCardOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncDeletedCatOnDiskUpdatedCatOnServer[aDirPrefId] = 0;
		aRepo.cardbookServerSyncAgain[aDirPrefId] = false;
		aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] = 0;
		aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId] = 0;
		aRepo.cardbookServerSyncCompareCatWithCacheDone[aDirPrefId] = 0;
		aRepo.cardbookServerSyncCompareCatWithCacheTotal[aDirPrefId] = 0;
		aRepo.cardbookServerSyncHandleRemainingCardDone[aDirPrefId] = 0;
		aRepo.cardbookServerSyncHandleRemainingCardTotal[aDirPrefId] = 0;
		aRepo.cardbookServerSyncHandleRemainingCatDone[aDirPrefId] = 0;
		aRepo.cardbookServerSyncHandleRemainingCatTotal[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardRequest[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardResponse[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardError[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardForMergeRequest[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardForMergeResponse[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardForMergeError[aDirPrefId] = 0;
		aRepo.cardbookServerMultiGetArray[aDirPrefId] = [];
		aRepo.cardbookServerMultiGetGoogleArray[aDirPrefId] = {};
		aRepo.cardbookServerMultiPutGoogleArray[aDirPrefId] = {};
		aRepo.cardbookServerSyncParams[aDirPrefId] = [];
		aRepo.cardbookServerMultiGetRequest[aDirPrefId] = 0;
		aRepo.cardbookServerMultiGetResponse[aDirPrefId] = 0;
		aRepo.cardbookServerMultiGetError[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardRequest[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardResponse[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardError[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardPhotoRequest[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardPhotoResponse[aDirPrefId] = 0;
		aRepo.cardbookServerGetCardPhotoError[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardPhotoRequest[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardPhotoResponse[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCardPhotoError[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCatRequest[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCatResponse[aDirPrefId] = 0;
		aRepo.cardbookServerUpdatedCatError[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCardRequest[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCardResponse[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCardError[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCatRequest[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCatResponse[aDirPrefId] = 0;
		aRepo.cardbookServerCreatedCatError[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCardRequest[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCardResponse[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCardError[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCatRequest[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCatResponse[aDirPrefId] = 0;
		aRepo.cardbookServerDeletedCatError[aDirPrefId] = 0;
		aRepo.cardbookImageGetRequest[aDirPrefId] = 0;
		aRepo.cardbookImageGetResponse[aDirPrefId] = 0;
		aRepo.cardbookImageGetError[aDirPrefId] = 0;
		aRepo.cardbookServerNotPushed[aDirPrefId] = 0;
		aRepo.importConflictChoice[aDirPrefId] = {};
	},

	finishMultipleOperations: function(aRepo, aDirPrefId) {
		cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
		aRepo.cardbookSyncMode[aDirPrefId] = 0;
	},

    loadComplexSearchAccounts: async function (aRepo) {
		let results = cardbookBGPreferences.getAllComplexSearchIds();
		for (let dirPrefId of results) {
			await cardbookBGSynchronization.loadComplexSearchAccount(aRepo, dirPrefId, true);
		}
	},

	loadComplexSearchAccount: async function (aRepo, aDirPrefId, aInitial, aSearch) {
		if (aSearch && aSearch.rules && aSearch.rules.length) {
			aRepo.cardbookComplexSearch[aDirPrefId] = {}
			aRepo.cardbookComplexSearch[aDirPrefId].searchAB = aSearch.searchAB;
			aRepo.cardbookComplexSearch[aDirPrefId].matchAll = aSearch.matchAll;
			aRepo.cardbookComplexSearch[aDirPrefId].rules = JSON.parse(JSON.stringify(aSearch.rules));
			await cardbookBGSynchronization.loadComplexSearchAccountFinished(aRepo, aDirPrefId, aInitial);
		} else {
			cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
			await cardbookBGSynchronization.loadAccount(aRepo, aDirPrefId, true);
			await cardbookIDBSearch.loadSearch(aRepo, aDirPrefId);
            await cardbookBGSynchronization.loadComplexSearchAccountFinished(aRepo, aDirPrefId, aInitial);
			cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
		}
	},

	loadComplexSearchAccountFinished: async function (aRepo, aDirPrefId, aInitial) {
		if (!aInitial) {
			await cardbookBGSynchronization.loadComplexSearchCards(aRepo, aDirPrefId);
		}
	},
	
	loadComplexSearchCards: async function (aRepo, aDirPrefId) {
		if (aRepo.cardbookComplexSearch[aDirPrefId]) {
			for (let j in aRepo.cardbookCards) {
				let card = aRepo.cardbookCards[j];
				if (aRepo.isMyCardFound(card, aDirPrefId)) {
					await aRepo.addCardToDisplayAndCat(card, aDirPrefId, true);
				}
			}
			await aRepo.notifyObservers("complexSearchLoaded", aDirPrefId);
		}
	},

	loadAccounts: async function (aRepo) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.setUncat", uncat: cardbookBGPreferences.getPref("uncategorizedCards")});
		let results = cardbookBGPreferences.getAllPrefIds();
		for (let dirPrefId of results) {
			if (cardbookBGPreferences.getType(dirPrefId) != "SEARCH") {
				await cardbookBGSynchronization.loadAccount(aRepo, dirPrefId, true);
			}
		}
		cardbookBGSynchronization.setPeriodicSyncs(aRepo);
		await aRepo.notifyObservers("DBOpen");
	},

	loadAccount: async function (aRepo, aDirPrefId, aAddAccount) {
		let name = cardbookBGPreferences.getName(aDirPrefId);
        let type = cardbookBGPreferences.getType(aDirPrefId);
        let url = cardbookBGPreferences.getUrl(aDirPrefId);
        let user = cardbookBGPreferences.getUser(aDirPrefId);
        let color = cardbookBGPreferences.getColor(aDirPrefId);
        let enabled = cardbookBGPreferences.getEnabled(aDirPrefId);
        let version = cardbookBGPreferences.getVCardVersion(aDirPrefId);
        let readonly = cardbookBGPreferences.getReadOnly(aDirPrefId) || false;
        let urnuuid = cardbookBGPreferences.getUrnuuid(aDirPrefId);
        let sourceId = cardbookBGPreferences.getSourceId(aDirPrefId);
        let DBCached = cardbookBGPreferences.getDBCached(aDirPrefId);
        let autoSyncEnabled = cardbookBGPreferences.getAutoSyncEnabled(aDirPrefId);
        let autoSyncInterval = cardbookBGPreferences.getAutoSyncInterval(aDirPrefId);
        if (aAddAccount) {
            await aRepo.addAccountToRepository(aDirPrefId, name, type, url, user, color, enabled,
                                                        version, "", readonly, urnuuid, sourceId, DBCached, autoSyncEnabled, autoSyncInterval, false);
            cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "addressbookOpened", [name]);
        }

		if (enabled) {
			cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
			if ((cardbookBGUtils.isMyAccountRemote(type) && DBCached) || (type == "LOCALDB")) {
				await cardbookIDBCat.loadCategories(aRepo, aDirPrefId, name);
				await cardbookIDBCard.loadCards(aRepo, aDirPrefId, name);
			} else if (type == "FILE") {
				await cardbookIDBCat.loadCategories(aRepo, aDirPrefId, name);
				let cards = await cardbookBGFiles.getCardsFromFile(aRepo, url, aDirPrefId);
				for (let card of cards) {
					card.cardurl = "";
					await aRepo.addCardToRepository(card, false);
				}
			} else if (type == "DIRECTORY") {
				await cardbookIDBCat.loadCategories(aRepo, aDirPrefId, name);
				let cards = await cardbookBGFiles.getCardsFromDir(aRepo, url, aDirPrefId);
				for (let card of cards) {
					await aRepo.addCardToRepository(card, false);
				}
			} else if (type == "SEARCH") {
				await cardbookIDBCat.loadCategories(aRepo, aDirPrefId, name);
			}
			cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
		}
		await aRepo.notifyObservers("accountLoaded", aDirPrefId);
	},

	setPeriodicSyncs: function (aRepo, aDirPrefId) {
		for (let account of aRepo.cardbookAccounts.filter(child => aDirPrefId == child[1] || true)) {
			let dirPrefName = account[0];
			let dirPrefId = account[1];
			let enabled = account[2];
			let dirPrefType = account[3];
			if (enabled && cardbookBGUtils.isMyAccountRemote(dirPrefType)
				&& cardbookBGPreferences.getDBCached(dirPrefId)) {
				let autoSync = cardbookBGPreferences.getAutoSyncEnabled(dirPrefId);
				let autoSyncInterval = cardbookBGPreferences.getAutoSyncInterval(dirPrefId);
				if ((!aRepo.autoSync[dirPrefId]) ||
					(autoSync && aRepo.autoSyncInterval[dirPrefId] != autoSyncInterval)) {
					cardbookBGSynchronization.removePeriodicSync(aRepo, dirPrefId, dirPrefName);
					if (autoSync) {
						cardbookBGSynchronization.addPeriodicSync(aRepo, dirPrefId, dirPrefName, autoSyncInterval);
					}
				}
			} else {
				cardbookBGSynchronization.removePeriodicSync(aRepo, dirPrefId, dirPrefName);
			}
		}
	},

	removePeriodicSync: function(aRepo, aDirPrefId, aDirPrefName) {
		if (aRepo.autoSyncId[aDirPrefId]) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "periodicSyncDeleting", [aDirPrefName, aDirPrefId]);
			clearInterval(aRepo.autoSyncId[aDirPrefId]);
			delete aRepo.autoSyncId[aDirPrefId];
			delete aRepo.autoSync[aDirPrefId];
			delete aRepo.autoSyncInterval[aDirPrefId];
		}
	},
	
	addPeriodicSync: function(aRepo, aDirPrefId, aDirPrefName, aAutoSyncInterval) {
		if (!aRepo.autoSyncId[aDirPrefId]) {
			let autoSyncIntervalMs = aAutoSyncInterval * 60 * 1000;

			let waitTimer = setInterval( async function() {
				cardbookBGSynchronization.runPeriodicSync(aRepo, aDirPrefId, aDirPrefName);
			}, autoSyncIntervalMs);

			aRepo.autoSyncId[aDirPrefId] = waitTimer;
			aRepo.autoSync[aDirPrefId] = true;
			aRepo.autoSyncInterval[aDirPrefId] = aAutoSyncInterval;
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "periodicSyncSetting", [aDirPrefName, autoSyncIntervalMs, aDirPrefId]);
		}
	},
	
	runPeriodicSync: function (aRepo, aDirPrefId, aDirPrefName) {
		cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "periodicSyncSyncing", [aDirPrefName]);
		cardbookBGSynchronization.syncAccount(aRepo, aDirPrefId);
	},

	syncAccounts: function (aRepo, aAccountList) {
		let results = aAccountList || cardbookBGPreferences.getAllPrefIds();
		for (let result of results) {
			cardbookBGSynchronization.syncAccount(aRepo, result);
		}
	},

	syncAccount: async function (aRepo, aDirPrefId, aForceSync = false) {
		try {
			if (aRepo.cardbookSyncMode[aDirPrefId] == 1 && !aForceSync) {
				return;
			}
			
			let type = cardbookBGPreferences.getType(aDirPrefId);
			let url = cardbookBGPreferences.getUrl(aDirPrefId);
			let name = cardbookBGPreferences.getName(aDirPrefId);
			let user = cardbookBGPreferences.getUser(aDirPrefId);
			let enabled = cardbookBGPreferences.getEnabled(aDirPrefId);
			let DBCached = cardbookBGPreferences.getDBCached(aDirPrefId);
			if (enabled) {
				if (cardbookBGUtils.isMyAccountRemote(type)) {
					let params = {aDirPrefIdType: type};
					if (!DBCached) {
						params.aValue = cardbookBGPreferences.getLastSearch(aDirPrefId);
					}
					if (type == "GOOGLE2" || type == "GOOGLE3") {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.initSyncActivity", dirPrefId: aDirPrefId, name: name});
						cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
						aRepo.cardbookServerSyncRequest[aDirPrefId]++;
						var connection = {connUser: user, connPrefId: aDirPrefId, connType: type, connDescription: name};
						aRepo.cardbookServerSyncParams[aDirPrefId] = [ connection, type ]; 
						cardbookBGSynchronizationGoogle2.getNewAccessTokenForGooglePeople(aRepo, connection, cardbookBGSynchronizationGoogle2.googleSyncLabelsInit);
					} else if (type == "APPLE" || type == "YAHOO") {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.initSyncActivity", dirPrefId: aDirPrefId, name: name});
						cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
						aRepo.cardbookServerSyncRequest[aDirPrefId]++;
						var connection = {connUser: user, connPrefId: aDirPrefId, connUrl: url, connDescription: name};
						connection.connUrl = cardbookBGSynchronizationUtils.getSlashedUrl(connection.connUrl);
						aRepo.cardbookServerSyncParams[aDirPrefId] = [ connection, type ];
						await cardbookBGSynchronizationCARDDAV.discoverPhase1(aRepo, connection, "SYNCSERVER", params);
					} else if (type == "OFFICE365") {
						// not possible anymore
					} else if (type == "CARDDAV" && !DBCached) {
						if (params.aValue) {
							cardbookBGSynchronization.searchRemote(aRepo, aDirPrefId, params.aValue);
						}
					} else {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.initSyncActivity", dirPrefId: aDirPrefId, name: name});
						cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
						aRepo.cardbookServerSyncRequest[aDirPrefId]++;
						var connection = {connUser: user, connPrefId: aDirPrefId, connUrl: url, connDescription: name};
						aRepo.cardbookServerSyncParams[aDirPrefId] = [ connection, type ];
						// bug for supporting old format URL that might be short (for example carddav.gmx)
						if (cardbookBGSynchronizationUtils.getSlashedUrl(connection.connUrl) == cardbookBGSynchronizationUtils.getSlashedUrl(cardbookBGSynchronizationUtils.getRootUrl(connection.connUrl))) {
							connection.connUrl = cardbookBGSynchronizationUtils.getWellKnownUrl(connection.connUrl);
							await cardbookBGSynchronizationCARDDAV.discoverPhase1(aRepo, connection, "SYNCSERVER", params);
						} else {
							await cardbookBGSynchronizationCARDDAV.serverSyncCards(aRepo, connection);
						}
					}
					if (type == "GOOGLE2" || type == "GOOGLE3") {
						cardbookBGSynchronization.waitForGoogleSyncFinished(aRepo, aDirPrefId, name);
					} else if (type == "OFFICE365") {
						// not possible anymore
					} else if (type != "CARDDAV" || DBCached) {
						cardbookBGSynchronization.waitForSyncFinished(aRepo, aDirPrefId, name);
					}
				}
			}
		}
		catch (e) {
			console.log(e)
			cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "cardbookBGSynchronization.syncAccount error : " + e.message, "Error");
		}
	},

	searchRemote: async function (aRepo, aDirPrefId, aValue) {
		try {
			let URL = cardbookBGPreferences.getUrl(aDirPrefId);
			let name = cardbookBGPreferences.getName(aDirPrefId);
			let user = cardbookBGPreferences.getUser(aDirPrefId);
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.initSyncActivity", dirPrefId: aDirPrefId, name: name});
			cardbookBGSynchronization.initMultipleOperations(aRepo, aDirPrefId);
			cardbookBGSynchronization.waitForSyncFinished(aRepo, aDirPrefId, name);
			await aRepo.emptyUnCachedAccountFromRepository(aDirPrefId, name);
			aRepo.cardbookServerSyncRequest[aDirPrefId]++;
			let connection = { connUser: user, connPrefId: aDirPrefId, connUrl: URL, connDescription: name };
			await cardbookBGSynchronizationCARDDAV.serverSearchRemote(aRepo, connection, aValue);
		}
		catch (e) {
			console.log(e)
			cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "cardbookBGSynchronization.searchRemote error : " + e, "Error");
		}
	},

	finishSync: async function(aRepo, aDirPrefId, aPrefName, aPrefType) {
		await aRepo.notifyObservers("cardbook.syncFinished", aDirPrefId);
		if (cardbookBGUtils.isMyAccountRemote(aPrefType)) {
			if (aRepo.cardbookServerSyncRequest[aDirPrefId] == 0) {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroNotTried", [aPrefName]);
				await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishSyncActivity", dirPrefId: aDirPrefId});
			} else {
				var errorNum = aRepo.cardbookAccessTokenError[aDirPrefId] + aRepo.cardbookRefreshTokenError[aDirPrefId] + aRepo.cardbookServerDiscoveryError[aDirPrefId] + aRepo.cardbookServerCardSyncError[aDirPrefId];
				if (errorNum === 0) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishSyncActivityOK", dirPrefId: aDirPrefId, name: aPrefName});
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroFinishedResult", [aPrefName]);
					if (aPrefType == "GOOGLE2" || aPrefType == "GOOGLE3") {
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesUpToDate", [aPrefName, aRepo.cardbookServerSyncNotUpdatedCat[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesNewOnServer", [aPrefName, aRepo.cardbookServerSyncNewCatOnServer[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesUpdatedOnServer", [aPrefName, aRepo.cardbookServerSyncUpdatedCatOnServer[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesDeletedOnServer", [aPrefName, aRepo.cardbookServerSyncDeletedCatOnServer[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesDeletedOnDisk", [aPrefName, aRepo.cardbookServerSyncDeletedCatOnDisk[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesDeletedOnDiskUpdatedOnServer", [aPrefName, aRepo.cardbookServerSyncDeletedCatOnDiskUpdatedCatOnServer[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesNewOnDisk", [aPrefName, aRepo.cardbookServerSyncNewCatOnDisk[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesUpdatedOnDisk", [aPrefName, aRepo.cardbookServerSyncUpdatedCatOnDisk[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesUpdatedOnBoth", [aPrefName, aRepo.cardbookServerSyncUpdatedCatOnBoth[aDirPrefId]]);
						cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCategoriesUpdatedOnDiskDeletedOnServer", [aPrefName, aRepo.cardbookServerSyncUpdatedCatOnDiskDeletedCatOnServer[aDirPrefId]]);
					}
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsUpToDate", [aPrefName, aRepo.cardbookServerSyncNotUpdatedCard[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsNewOnServer", [aPrefName, aRepo.cardbookServerSyncNewCardOnServer[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsUpdatedOnServer", [aPrefName, aRepo.cardbookServerSyncUpdatedCardOnServer[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsDeletedOnServer", [aPrefName, aRepo.cardbookServerSyncDeletedCardOnServer[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsDeletedOnDisk", [aPrefName, aRepo.cardbookServerSyncDeletedCardOnDisk[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsDeletedOnDiskUpdatedOnServer", [aPrefName, aRepo.cardbookServerSyncDeletedCardOnDiskUpdatedCardOnServer[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsNewOnDisk", [aPrefName, aRepo.cardbookServerSyncNewCardOnDisk[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsUpdatedOnDisk", [aPrefName, aRepo.cardbookServerSyncUpdatedCardOnDisk[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsUpdatedOnBoth", [aPrefName, aRepo.cardbookServerSyncUpdatedCardOnBoth[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroCardsUpdatedOnDiskDeletedOnServer", [aPrefName, aRepo.cardbookServerSyncUpdatedCardOnDiskDeletedCardOnServer[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroModifGetOKFromServer", [aPrefName, aRepo.cardbookServerGetCardResponse[aDirPrefId] - aRepo.cardbookServerGetCardError[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroModifGetKOFromServer", [aPrefName, aRepo.cardbookServerGetCardError[aDirPrefId]]);
					var error = aRepo.cardbookServerCreatedCatError[aDirPrefId] + aRepo.cardbookServerUpdatedCatError[aDirPrefId] + aRepo.cardbookServerDeletedCatError[aDirPrefId] +
						aRepo.cardbookServerCreatedCardError[aDirPrefId] + aRepo.cardbookServerUpdatedCardError[aDirPrefId] + aRepo.cardbookServerDeletedCardError[aDirPrefId];
					var success = aRepo.cardbookServerCreatedCatResponse[aDirPrefId] + aRepo.cardbookServerUpdatedCatResponse[aDirPrefId] + aRepo.cardbookServerDeletedCatResponse[aDirPrefId] +
						aRepo.cardbookServerCreatedCardResponse[aDirPrefId] + aRepo.cardbookServerUpdatedCardResponse[aDirPrefId] + aRepo.cardbookServerDeletedCardResponse[aDirPrefId] - error - aRepo.cardbookServerNotPushed[aDirPrefId];
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroModifPutOKToServer", [aPrefName, success]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroModifPutKOToServer", [aPrefName, error]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "imageGetResponse", [aPrefName, aRepo.cardbookImageGetResponse[aDirPrefId]]);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "imageGetError", [aPrefName, aRepo.cardbookImageGetError[aDirPrefId]]);
				} else {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishSyncActivity", dirPrefId: aDirPrefId});
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroImpossible", [aPrefName]);
				}
			}
		} else if (aPrefType === "FILE") {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroFileFinishedResult", [aPrefName]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroFileCardsOK", [aPrefName, aRepo.cardbookServerCardSyncDone[aDirPrefId]]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroFileCardsKO", [aPrefName, aRepo.cardbookServerCardSyncError[aDirPrefId]]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "imageGetResponse", [aPrefName, aRepo.cardbookImageGetResponse[aDirPrefId]]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "imageGetError", [aPrefName, aRepo.cardbookImageGetError[aDirPrefId]]);
		} else if (aPrefType === "DIRECTORY") {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDirFinishedResult", [aPrefName]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDirCardsOK", [aPrefName, aRepo.cardbookServerCardSyncDone[aDirPrefId]]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDirCardsKO", [aPrefName, aRepo.cardbookServerCardSyncError[aDirPrefId]]);
		} else if (aPrefType === "LOCALDB") {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDBFinishedResult", [aPrefName]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDBCardsOK", [aPrefName, aRepo.cardbookServerCardSyncDone[aDirPrefId]]);
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroDBCardsKO", [aPrefName, aRepo.cardbookServerCardSyncError[aDirPrefId]]);
		}
	},

	startDiscovery: function (aRepo) {
		let allURLs = [];
		allURLs = cardbookBGPreferences.getDiscoveryAccounts();

		let first = true;
		for (let URL of allURLs) {
			let dirPrefId = cardbookHTMLUtils.getUUID();
			if (first) {
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "discoveryRunning", [cardbookBGSynchronization.gDiscoveryDescription]);
				first = false;
			}
			cardbookBGSynchronization.initDiscoveryOperations(aRepo, dirPrefId);
			cardbookBGSynchronization.initMultipleOperations(aRepo, dirPrefId);
			aRepo.cardbookServerValidation[dirPrefId] = {length: 0, user: URL[1]};
			aRepo.cardbookServerSyncRequest[dirPrefId]++;
			let connection = { connUser: URL[1], connPrefId: dirPrefId, connUrl: URL[0], connDescription: cardbookBGSynchronization.gDiscoveryDescription };
			let params = { aDirPrefIdType: "CARDDAV" };
			cardbookBGSynchronizationCARDDAV.discoverPhase1(aRepo, connection, "GETDISPLAYNAME", params);
			cardbookBGSynchronization.waitForDiscoveryFinished(aRepo, dirPrefId);
		}
	},

	stopDiscovery: async function (aRepo, aDirPrefId, aState) {
		cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
		if (aState) {
			let total = cardbookBGSynchronizationUtils.getRequest(aRepo) + cardbookBGSynchronizationUtils.getTotal(aRepo) + cardbookBGSynchronizationUtils.getResponse(aRepo) + cardbookBGSynchronizationUtils.getDone(aRepo);
			if (total === 0) {
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : aRepo.cardbookServerValidation : ", JSON.stringify(aRepo.cardbookServerValidation));
				let accountsToAdd = [];
				let accountsToRemove = [];
				// find all current CARDDAV accounts
				let currentAccounts = [];
				currentAccounts = JSON.parse(JSON.stringify(aRepo.cardbookAccounts));
				currentAccounts = currentAccounts.filter(x => x[3] == "CARDDAV");
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : currentAccounts : ", currentAccounts);
				
				// find all accounts that should be added and removed
				for (let dirPrefId in aRepo.cardbookServerValidation) {
					if (aRepo.cardbookServerValidation[dirPrefId].length != 0) {
						for (let url in aRepo.cardbookServerValidation[dirPrefId]) {
							if (url == "length" || url == "user") {
								continue;
							}
							for (let account of currentAccounts) {
								let currentUrl = cardbookBGPreferences.getUrl(account[1]);
								let currentUser = cardbookBGPreferences.getUser(account[1]);
								if ((currentUser == aRepo.cardbookServerValidation[dirPrefId].user) && (currentUrl == cardbookBGSynchronizationUtils.decodeURL(url))) {
									aRepo.cardbookServerValidation[dirPrefId][url].forget = true;
									account[3] = "CARDDAVFOUND";
								}
							}
						}
						// add accounts
						accountsToAdd.push(aRepo.fromValidationToArray(dirPrefId, "CARDDAV"));
					}
				}
				// remove accounts
				let currentAccountsNotFound = [];
				currentAccountsNotFound = currentAccounts.filter(x => x[3] == "CARDDAV");
				for (let currentAccountNotFound of currentAccountsNotFound) {
					let currentUrl = cardbookBGPreferences.getUrl(currentAccountNotFound[1]);
					let currentUser = cardbookBGPreferences.getUser(currentAccountNotFound[1]);
					let currentShortUrl = cardbookBGSynchronizationUtils.getShortUrl(currentUrl);
					for (let dirPrefId in aRepo.cardbookServerValidation) {
						for (let url in aRepo.cardbookServerValidation[dirPrefId]) {
							if (url == "length" || url == "user") {
								continue;
							}
							if ((currentUser == aRepo.cardbookServerValidation[dirPrefId].user) && (currentShortUrl == cardbookBGSynchronizationUtils.getShortUrl(cardbookBGSynchronizationUtils.decodeURL(url)))) {
								accountsToRemove.push(currentAccountNotFound[1]);
								break;
							}
						}
					}
				}

				for (let addAB of accountsToAdd) {
					if (addAB.length) {
						let accountToAdd = JSON.stringify(addAB);
						await aRepo.addAddressbook("discovery", null, accountToAdd);
					}
				}
				for (let removeAB of accountsToRemove) {
					await aRepo.removeAddressbook(removeAB, cardbookBGSynchronization.gDiscoveryDescription);
				}
				for (let dirPrefId in aRepo.cardbookServerValidation) {
					cardbookBGSynchronization.stopDiscoveryOperations(aRepo, dirPrefId);
				}
			}
		} else {
			cardbookBGSynchronization.stopDiscoveryOperations(aRepo, aDirPrefId);
		}
	},

	waitForDiscoveryFinished: function (aRepo, aDirPrefId) {
		aRepo.lTimerSyncAll[aDirPrefId] = setInterval(async function() {
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : aRepo.cardbookServerDiscoveryRequest : ", aRepo.cardbookServerDiscoveryRequest[aDirPrefId]);
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : aRepo.cardbookServerDiscoveryResponse : ", aRepo.cardbookServerDiscoveryResponse[aDirPrefId]);
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : aRepo.cardbookServerDiscoveryError : ", aRepo.cardbookServerDiscoveryError[aDirPrefId]);
					cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, cardbookBGSynchronization.gDiscoveryDescription + " : debug mode : aRepo.cardbookServerValidation : ", JSON.stringify(aRepo.cardbookServerValidation[aDirPrefId]));
					if (aRepo.cardbookServerDiscoveryError[aDirPrefId] >= 1) {
						await cardbookBGSynchronization.stopDiscovery(aRepo, aDirPrefId, false);
						clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
					} else if (aRepo.cardbookServerDiscoveryRequest[aDirPrefId] == aRepo.cardbookServerDiscoveryResponse[aDirPrefId] && aRepo.cardbookServerDiscoveryResponse[aDirPrefId] != 0) {
						await cardbookBGSynchronization.stopDiscovery(aRepo, aDirPrefId, true);
						clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
					}
			}, 1000);
		},

	waitForGoogleSyncFinished: function (aRepo, aDirPrefId, aPrefName) {
		if (aRepo.lTimerSyncAll[aDirPrefId]) {
			clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
		}
		let waitTime = 10000;
		aRepo.lTimerSyncAll[aDirPrefId] = setInterval(async function() {
				await aRepo.notifyObservers("cardbook.syncRunning", aDirPrefId);
				let type = cardbookBGPreferences.getType(aDirPrefId);
				if (aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] != 0) {
					if (aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] == aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId]) {
						aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] = 0;
						aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId] = 0;
						if (aRepo.cardbookServerMultiGetArray[aDirPrefId] && aRepo.cardbookServerMultiGetArray[aDirPrefId].length != 0) {
							cardbookBGSynchronizationGoogle2.serverMultiGet(aRepo, aRepo.cardbookServerSyncParams[aDirPrefId][0], type);
						}
					}
				}
				if (aRepo.cardbookServerSyncHandleRemainingCardDone[aDirPrefId] == aRepo.cardbookServerSyncHandleRemainingCardTotal[aDirPrefId]) {
					let request = cardbookBGSynchronizationUtils.getRequest(aRepo, aDirPrefId, aPrefName) + cardbookBGSynchronizationUtils.getTotal(aRepo, aDirPrefId, aPrefName);
					let response = cardbookBGSynchronizationUtils.getResponse(aRepo, aDirPrefId, aPrefName) + cardbookBGSynchronizationUtils.getDone(aRepo, aDirPrefId, aPrefName);
					let error = cardbookBGSynchronizationUtils.getError(aRepo, aDirPrefId);
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchSyncActivity", dirPrefId: aDirPrefId, done: aRepo.cardbookServerCardSyncDone[aDirPrefId], total: aRepo.cardbookServerCardSyncTotal[aDirPrefId]});
					if (request == response) {
						let currentConnection = aRepo.cardbookServerSyncParams[aDirPrefId][0];
						if (currentConnection && aRepo.cardbookServerSyncParams[aDirPrefId].length && aRepo.cardbookAccessTokenRequest[aDirPrefId] == 1 && aRepo.cardbookAccessTokenError[aDirPrefId] != 1) {
							let connection = {connUser: currentConnection.connUser, connPrefId: currentConnection.connPrefId, connType: type, connDescription: currentConnection.connDescription};
							aRepo.cardbookServerSyncParams[aDirPrefId] = [ connection ];
							aRepo.cardbookServerSyncRequest[aDirPrefId]++;
							if (type == "GOOGLE3") {
								cardbookBGSynchronizationGoogle2.getNewAccessTokenForGooglePeople(aRepo, connection, cardbookBGSynchronizationGoogle2.googleSyncWorkspaceInit);
							} else {
								cardbookBGSynchronizationGoogle2.getNewAccessTokenForGooglePeople(aRepo, connection, cardbookBGSynchronizationGoogle2.googleSyncContactsInit);
							}
						} else {
							if (aRepo.cardbookServerSyncAgain[aDirPrefId] && error == 0) {
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroForcedToResync", [aPrefName]);
								await cardbookBGSynchronization.finishSync(aRepo, aDirPrefId, aPrefName, type);
								cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
								// to avoid other sync during the wait time
								aRepo.cardbookSyncMode[aDirPrefId] = 1;
								setTimeout(async function() {
									await cardbookBGSynchronization.syncAccount(aRepo, aDirPrefId, true);
									}, waitTime);               
							} else {
								if (error == 0) {
									let sysdate = cardbookHTMLDates.getDateUTC();
									let syncdate = sysdate.year + sysdate.month + sysdate.day + "T" + sysdate.hour + sysdate.min + sysdate.sec + "Z";
									await cardbookBGPreferences.setLastSync(aDirPrefId, syncdate);
									await cardbookBGPreferences.setSyncFailed(aDirPrefId, false);
								} else {
									await cardbookBGPreferences.setSyncFailed(aDirPrefId, true);
								}
								await cardbookBGSynchronization.finishSync(aRepo, aDirPrefId, aPrefName, type);
								cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
								let total = cardbookBGSynchronizationUtils.getRequest(aRepo) + cardbookBGSynchronizationUtils.getTotal(aRepo) + cardbookBGSynchronizationUtils.getResponse(aRepo) + cardbookBGSynchronizationUtils.getDone(aRepo);
								// all sync are finished
								if (total === 0) {
									// should check if some should be restarted because of a changed password
									let syncAgain = [];
									let syncFailed = [];
									for (let i in aRepo.cardbookServerChangedPwd) {
										if (aRepo.cardbookServerChangedPwd[i].pwdChanged) {
											syncAgain = syncAgain.concat(aRepo.cardbookServerChangedPwd[i].dirPrefIdList);
										} else {
											syncFailed = syncFailed.concat(aRepo.cardbookServerChangedPwd[i].dirPrefIdList);
										}
									}
									aRepo.cardbookServerChangedPwd = {};
									for (var j = 0; j < syncAgain.length; j++) {
										let myPrefId = syncAgain[j];
										let myPrefName = cardbookBGPreferences.getName(myPrefId);
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroForcedToResync", [myPrefName]);
										cardbookBGSynchronization.syncAccount(aRepo, myPrefId, false);
									}
									for (let j = 0; j < syncFailed.length; j++) {
										let myPrefId = syncFailed[j];
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [cardbookBGPreferences.getName(myPrefId), "passwordNotChanged", cardbookBGPreferences.getUrl(myPrefId), 401], "Error");
									}
									if (syncAgain.length == 0) {
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroAllFinished");
										// final step for synchronizations
										cardbookBGSynchronization.startDiscovery(aRepo);
									}
								}
							}
							clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
						}
					}
				}
			}, 1000);
	},

	waitForSyncFinished: function (aRepo, aDirPrefId, aPrefName) {
		if (aRepo.lTimerSyncAll[aDirPrefId]) {
			clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
		}
		let waitTime = 10000;
		aRepo.lTimerSyncAll[aDirPrefId] = setInterval(async function() {
					await aRepo.notifyObservers("cardbook.syncRunning", aDirPrefId);
					let type = cardbookBGPreferences.getType(aDirPrefId);
					if (aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] != 0) {
						if (aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] == aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId]) {
							aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId] = 0;
							aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId] = 0;
							if (aRepo.cardbookServerMultiGetArray[aDirPrefId] && aRepo.cardbookServerMultiGetArray[aDirPrefId].length != 0) {
								await cardbookBGSynchronizationCARDDAV.serverMultiGet(aRepo, aRepo.cardbookServerSyncParams[aDirPrefId][0]);
							}
						}
					}
					if (aRepo.cardbookServerSyncHandleRemainingCardDone[aDirPrefId] == aRepo.cardbookServerSyncHandleRemainingCardTotal[aDirPrefId]) {
						let request = cardbookBGSynchronizationUtils.getRequest(aRepo, aDirPrefId, aPrefName) + cardbookBGSynchronizationUtils.getTotal(aRepo, aDirPrefId, aPrefName);
						let response = cardbookBGSynchronizationUtils.getResponse(aRepo, aDirPrefId, aPrefName) + cardbookBGSynchronizationUtils.getDone(aRepo, aDirPrefId, aPrefName);
						let error = cardbookBGSynchronizationUtils.getError(aRepo, aDirPrefId);
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchSyncActivity", dirPrefId: aDirPrefId, done: aRepo.cardbookServerCardSyncDone[aDirPrefId], total: aRepo.cardbookServerCardSyncTotal[aDirPrefId]});
						if (request == response) {
							if (aRepo.cardbookServerSyncAgain[aDirPrefId] === true) {
								await cardbookBGSynchronization.finishSync(aRepo, aDirPrefId, aPrefName, type);
								cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroForcedToResync", [aPrefName]);
								// to avoid other sync during the wait time
								aRepo.cardbookSyncMode[aDirPrefId] = 1;
								setTimeout(async function() {
									await cardbookBGSynchronization.syncAccount(aRepo, aDirPrefId, true);
									}, waitTime);               
							} else {
								if (error == 0) {
									let sysdate = cardbookHTMLDates.getDateUTC();
									let syncdate = sysdate.year + sysdate.month + sysdate.day + "T" + sysdate.hour + sysdate.min + sysdate.sec + "Z";
									await cardbookBGPreferences.setLastSync(aDirPrefId, syncdate);
									await cardbookBGPreferences.setSyncFailed(aDirPrefId, false);
								} else {
									await cardbookBGPreferences.setSyncFailed(aDirPrefId, true);
								}
								await cardbookBGSynchronization.finishSync(aRepo, aDirPrefId, aPrefName, type);
								cardbookBGSynchronization.finishMultipleOperations(aRepo, aDirPrefId);
								let total = cardbookBGSynchronizationUtils.getRequest(aRepo) + cardbookBGSynchronizationUtils.getTotal(aRepo) + cardbookBGSynchronizationUtils.getResponse(aRepo) + cardbookBGSynchronizationUtils.getDone(aRepo);
								// all sync are finished
								if (total === 0) {
									// should check if some should be restarted because of a changed password
									let syncAgain = [];
									let syncFailed = [];
									for (let i in aRepo.cardbookServerChangedPwd) {
										if (aRepo.cardbookServerChangedPwd[i].pwdChanged) {
											syncAgain = syncAgain.concat(aRepo.cardbookServerChangedPwd[i].dirPrefIdList);
										} else {
											syncFailed = syncFailed.concat(aRepo.cardbookServerChangedPwd[i].dirPrefIdList);
										}
									}
									aRepo.cardbookServerChangedPwd = {};
									for (let j = 0; j < syncAgain.length; j++) {
										let myPrefId = syncAgain[j];
										let myPrefName = cardbookBGPreferences.getName(myPrefId);
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroForcedToResync", [myPrefName]);
										cardbookBGSynchronization.syncAccount(aRepo, myPrefId, false);
									}
									for (let j = 0; j < syncFailed.length; j++) {
										let myPrefId = syncFailed[j];
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchronizationFailed", [cardbookBGPreferences.getName(myPrefId), "passwordNotChanged", cardbookBGPreferences.getUrl(myPrefId), 401], "Error");
									}
									if (syncAgain.length == 0) {
										cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "synchroAllFinished");
										// final step for synchronizations
										cardbookBGSynchronization.startDiscovery(aRepo);
									}
								}
							}
							clearInterval(aRepo.lTimerSyncAll[aDirPrefId]);
						}
					}
		}, 1000);
	},
};
