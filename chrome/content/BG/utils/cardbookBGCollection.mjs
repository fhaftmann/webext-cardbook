import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGActions } from "./cardbookBGActions.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { MimeAddressParser } from "../mimeParser/MimeJSComponents.sys.mjs";
import { cardbookIDBMailPop } from "../indexedDB/cardbookIDBMailPop.mjs";

export var cardbookBGCollection = {
    
    addCollectedContact: async function (aRepo, aIdentity, aAddresses) {
        cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : start of emails identity : " + aIdentity);
        let emailsCollections = cardbookBGPreferences.getAllEmailsCollections();
        cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : start of emails collection : " + emailsCollections);
        if (emailsCollections.length == 0) {
            return;
        }
        let addresses = MimeAddressParser.prototype.parseEncodedHeaderW(aAddresses);
        let actionStarted = false;
        let actionId = null;
        for (let address of addresses) {
            if (!address.email) {
                return;
            } else if (address.email.includes("{{") && address.email.includes("}}")) {
                return;
            }
            if (address.name) {
                if (address.name.includes("{{") && address.name.includes("}}")) {
                    return;
                }
            }
            if (!aRepo.isEmailRegistered(address.email)) {
                for (let emailsCollection of emailsCollections) {
                    let dirPrefId = emailsCollection[2];
                    // check for the address book
                    let account = aRepo.cardbookAccounts.filter(child => dirPrefId == child[1]);
                    if (account.length == 0) {
                        cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "Email collection : wrong dirPrefId : " + dirPrefId, "Error");
                        continue;
                    }
                    if (!cardbookBGPreferences.getReadOnly(dirPrefId)) {
                        if (emailsCollection[0] == "true") {
                            if ((aIdentity == emailsCollection[1]) || ("allMailAccounts" == emailsCollection[1])) {
                                let name = cardbookBGPreferences.getName(dirPrefId);
                                cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, name + " : debug mode : trying to collect contact " + address.name + " (" + address.email + ")");
                                if (actionStarted == false) {
                                    actionId = cardbookBGActions.startAction(aRepo, "outgoingEmailCollected");
                                    actionStarted = true;
                                }
                                await aRepo.addCardFromDisplayAndEmail(dirPrefId, address.name, address.email, emailsCollection[3], actionId);
                            }
                        }
                    }
                }
            } else {
                // if the contact is already registered, let's have a look if it's not possible to collect some informations
                if (aRepo.isEmailRegistered(address.email) && address.name) {
                    let card = aRepo.getCardFromEmail(address.email);
                    if (card.fn.toLowerCase() == address.email.toLowerCase() && address.name.toLowerCase() != address.email.toLowerCase()) {
                        if (actionStarted == false) {
                            actionId = cardbookBGActions.startAction(aRepo, "outgoingEmailCollected");
                            actionStarted = true;
                        }
                        let newCard = new cardbookCardParser();
                        await cardbookBGUtils.cloneCard(card, newCard);
                        newCard.fn = address.name;
                        await aRepo.saveCardFromUpdate(card, newCard, actionId, true);
                    }
                }
            }
        }
        return actionId;
    },

    addCollectedPopularity: async function (aRepo, aAddresses) {
		await cardbookIDBMailPop.openMailPopDB();
		let addresses = MimeAddressParser.prototype.parseEncodedHeaderW(aAddresses);
		for (let address of addresses) {
			await cardbookIDBMailPop.updateMailPop(aRepo, address.email);
		}
	},
};
