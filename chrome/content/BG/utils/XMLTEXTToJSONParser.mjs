export function XMLTEXTToJSONParser(text) {
    let parser = new DOMParser();
    let doc = parser.parseFromString(text, "application/xml");
    this._buildTree(doc);
}

XMLTEXTToJSONParser.prototype = {
    _buildTree: function XMLTEXTToJSONParser_buildTree(doc) {
        let nodeName = doc.documentElement.localName;
        this[nodeName] = [this._translateNode(doc.documentElement)];
    },

    _translateNode: function XMLTEXTToJSONParser_translateNode(node) {
        let value = null;
        if (node.childNodes.length) {
            let textValue = "";
            let dictValue = {};
            let hasElements = false;
            for (let i = 0; i < node.childNodes.length; i++) {
                let currentNode = node.childNodes[i];
                let nodeName = currentNode.localName;
                if (currentNode.nodeType == currentNode.TEXT_NODE) {
                    textValue += currentNode.nodeValue;
                } else if (currentNode.nodeType == currentNode.CDATA_SECTION_NODE) {
                    textValue += currentNode.nodeValue;
                } else if (currentNode.nodeType == currentNode.ELEMENT_NODE) {
                    hasElements = true;
                    let nodeValue = this._translateNode(currentNode);
                    if (!dictValue[nodeName]) {
                        dictValue[nodeName] = [];
                    }
                    dictValue[nodeName].push(nodeValue);
                }
            }
            if (hasElements) {
                value = dictValue;
            } else {
                value = textValue;
            }
        }
        return value;
    }
};
