import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";

export var cardbookBGAttachVCards = {
    
    getVCards: async function (aRepo, aIdentity) {
        let finalResult = [];
        let results = cardbookBGPreferences.getAllVCards();
        for (let result of results) {
            if (result[0] == "true") {
                if (result[1] == aIdentity || result[1] == "allMailAccounts") {
                    let filename = result[4];
                    if (aRepo.cardbookCards[result[2]+"::"+result[3]]) {
                        let card = aRepo.cardbookCards[result[2]+"::"+result[3]];
                        let vCard = await cardbookBGUtils.getvCardForEmail(aRepo.statusInformation, card);
                        finalResult.push({filename: filename, vCard: vCard});
                    }
                }
            }
        }
        return finalResult;
    }
};
