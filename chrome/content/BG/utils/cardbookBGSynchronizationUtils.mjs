import { cardbookIDBImage } from "../indexedDB/cardbookIDBImage.mjs";

import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookFetchParser } from "./cardbookFetchParser.mjs";

import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export var cardbookBGSynchronizationUtils = {
    decodeURL: function (aURL) {
		var relative = aURL.match("(https?)(://[^/]*)/([^#?]*)");
		if (relative && relative[3]) {
			var relativeHrefArray = [];
			relativeHrefArray = relative[3].split("/");
			for (var i = 0; i < relativeHrefArray.length; i++) {
				relativeHrefArray[i] = decodeURIComponent(relativeHrefArray[i]);
			}
			return relative[1] + relative[2] + "/" + relativeHrefArray.join("/");
		} else {
			return aURL;
		}
	},

	getRootUrl: function (aUrl) {
		try {
			aUrl = aUrl.trim();
			let urlArray1 = aUrl.split("://");
			let urlArray2 = urlArray1[1].split("/");
			if (urlArray1[0] != "http" && urlArray1[0] != "https") {
				return "";
			}
			return urlArray1[0] + "://" + urlArray2[0];
		}
		catch (e) {
			return "";
		}
	},

	getSlashedUrl: function (aUrl) {
		aUrl = aUrl.trim();
		if (aUrl[aUrl.length - 1] != "/") {
			aUrl += "/";
		}
		return aUrl;
	},

	getWellKnownUrl: function (aUrl) {
		aUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aUrl);
		aUrl += ".well-known/carddav";
		return aUrl;
	},
	
	getShortUrl: function (aUrl) {
		try {
			aUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aUrl);
			var urlArray1 = aUrl.split("://");
			var urlArray2 = urlArray1[1].split("/");
			if (urlArray1[0] != "http" && urlArray1[0] != "https") {
				return "";
			}
			urlArray2.pop();
			urlArray2.pop();
			return urlArray1[0] + "://" + urlArray2.join("/");
		}
		catch (e) {
			return "";
		}
	},

	getAllURLsToDiscover: function (aDirPrefIdToExclude) {
		let sortedDiscoveryAccounts = [];
		let results = cardbookBGPreferences.getAllPrefIds();
		for (let dirPrefId of results) {
			if (aDirPrefIdToExclude != null && aDirPrefIdToExclude !== undefined && aDirPrefIdToExclude != "") {
				if (dirPrefId == aDirPrefIdToExclude) {
					continue;
				}
			}
			let enabled = cardbookBGPreferences.getEnabled(dirPrefId);
			let type = cardbookBGPreferences.getType(dirPrefId)
			if (enabled === true && type == "CARDDAV") {
				let url = cardbookBGPreferences.getUrl(dirPrefId);
				let shortUrl = cardbookBGSynchronizationUtils.getShortUrl(url);
				let user = cardbookBGPreferences.getUser(dirPrefId);
				let isAccountThere = sortedDiscoveryAccounts.filter(element => element[1] == user + "::" + shortUrl);
				if (isAccountThere.length == 0) {
					sortedDiscoveryAccounts.push([user + " - " + shortUrl, user + "::" + shortUrl]);
				}
			}
		}
		return sortedDiscoveryAccounts;
	},

	cleanWebObject: function (aObject) {
		let cleanObjectArray = [];
		for (let key in aObject) {
			if (key == "access_token" || key == "refresh_token") {
				cleanObjectArray.push(key + ": *****");
			} else if (key == "Authorization") {
				cleanObjectArray.push(key + ': "' + aObject[key].replace(/^Basic (.*)/, "Basic ").replace(/^Digest (.*)/, "Digest ") + "*****");
			} else {
				cleanObjectArray.push(key + ': "' + aObject[key] + '"');
			}
		}
		return cleanObjectArray.join(", ");
	},

	encodeParams: function(params) {
		let paramsArray = [];
		if (params) {
			for (let param in params) {
				if (params.hasOwnProperty(param)) {
					paramsArray.push(encodeURIComponent(param) + "=" + encodeURIComponent(params[param]));
				}
			}
		}
		return paramsArray.join("&");
	},

	isStatusCorrect: function(aStatusText) {
		if (aStatusText.startsWith("HTTP/1.1 200") || aStatusText.startsWith("HTTP/1.0 200")) {
			return true;
		}
		return false;
	},

	isSupportedvCardType: function(aContentType, aFileName) {
		if (aContentType.indexOf("text/x-vcard") == 0 || aContentType.indexOf("text/vcard") == 0) {
			return true;
		} else {
			let extension = cardbookHTMLUtils.getFileNameExtension(aFileName);
			if (extension.toLowerCase() == "vcf") {
				return true;
			}
			return false;
		}
	},

	isSupportedvCardListType: function(aContentType, aFileName) {
		if (aContentType.indexOf("text/x-vlist") == 0) {
			return true;
		} else {
			let extension =  cardbookHTMLUtils.getFileNameExtension(aFileName);
			if (extension.toLowerCase() == "vcf") {
				return true;
			}
			return false;
		}
	},

	isSupportedContentType: function(aContentType, aFileName) {
		if (cardbookBGSynchronizationUtils.isSupportedvCardType(aContentType, aFileName) || cardbookBGSynchronizationUtils.isSupportedvCardListType(aContentType, aFileName) ) {
			return true;
		} else {
			return false;
		}
	},

	buildPropfindRequest: function(aProperties) {
		let query = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		query += "<D:propfind xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">";
		query += "<D:prop>";
		for (let i = 0; i < aProperties.length; i++) {
			query += "<" + aProperties[i] + "/>";
		}
		query += "</D:prop></D:propfind>";
		return query;
	},

	buildQueryRequest: function(aProperties, aValue) {
		let query = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		query += "<C:addressbook-query xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">";
		query += "<D:prop>";
		for (let i = 0; i < aProperties.length; i++) {
			query += "<" + aProperties[i] + "/>";
		}
		query += "</D:prop>";
		query += "<C:filter test=\"anyof\">";
		query += "<C:prop-filter name=\"FN\"><C:text-match collation=\"i;unicode-casemap\" match-type=\"contains\">" + aValue + "</C:text-match></C:prop-filter>";
		query += "<C:prop-filter name=\"EMAIL\"><C:text-match collation=\"i;unicode-casemap\" match-type=\"contains\">" + aValue + "</C:text-match></C:prop-filter>";
		query += "</C:filter>";
		query += "</C:addressbook-query>";
		return query;
	},

	formatRelativeHref: function(aString) {
		let decodeReport = true;
		try {
			decodeReport = cardbookBGPreferences.getPref("decodeReport");
		} catch (e) {
			decodeReport = true;
		}
		let relative = aString.match("(https?)(://[^/]*)/([^#?]*)");
		if (relative && relative[3]) {
			let relativeHrefArray = [];
			relativeHrefArray = relative[3].split("/");
			for (let i = 0; i < relativeHrefArray.length; i++) {
				if (decodeReport) {
					relativeHrefArray[i] = decodeURIComponent(relativeHrefArray[i]);
				} else {
					relativeHrefArray[i] = encodeURIComponent(relativeHrefArray[i]);
				}
			}
			return "/" + relativeHrefArray.join("/");
		}
		return "";
	},

	buildMultigetRequest: function(aProperties, aVersion) {
		let query = "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
		query += "<C:addressbook-multiget xmlns:D=\"DAV:\" xmlns:C=\"urn:ietf:params:xml:ns:carddav\">";
		query += "<D:prop><D:getetag/><C:address-data Content-Type='text/vcard' version='" + aVersion + "'/></D:prop>";
		for (let i = 0; i < aProperties.length; i++) {
			query += "<D:href>" + cardbookBGSynchronizationUtils.formatRelativeHref(aProperties[i]) + "</D:href>";
		}
		query += "</C:addressbook-multiget>";
		return query;
	},

	getDisplayname: function (aProp) {
		let displayName = "";
		if (aProp["displayname"]) {
			displayName = aProp["displayname"][0];
		}
		return displayName;
	},

	getReadOnly: function (aProp) {
		let readOnly = true;
		if (aProp["current-user-privilege-set"] && aProp["current-user-privilege-set"][0]) {
			let privs = aProp["current-user-privilege-set"][0];
			if (privs["privilege"]) {
				let writePrivs = [ "write", "write-content", "write-properties", "all" ];
				for (let priv of privs["privilege"]) {
					let privname = Object.keys(priv)[0].toLowerCase();
					if (writePrivs.includes(privname)) {
						readOnly = false;
						break;
					}
				}
			}
		}
		return readOnly;
	},

	prepareCardForCreation: function(aCard, aUrl) {
		aUrl = cardbookBGSynchronizationUtils.getSlashedUrl(aUrl);
		aCard.cardurl = aUrl + aCard.uid + ".vcf";
	},

	getRequest: function(aRepo, aDirPrefId, aPrefName) {
		if (aDirPrefId) {
			if (aPrefName) {
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookAccessTokenRequest : ", aRepo.cardbookAccessTokenRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookRefreshTokenRequest : ", aRepo.cardbookRefreshTokenRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDiscoveryRequest : ", aRepo.cardbookServerDiscoveryRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardRequest : ", aRepo.cardbookServerGetCardRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardPhotoRequest : ", aRepo.cardbookServerGetCardPhotoRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardForMergeRequest : ", aRepo.cardbookServerGetCardForMergeRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerMultiGetRequest : ", aRepo.cardbookServerMultiGetRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCatRequest : ", aRepo.cardbookServerUpdatedCatRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCardRequest : ", aRepo.cardbookServerUpdatedCardRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCardPhotoRequest : ", aRepo.cardbookServerUpdatedCardPhotoRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCreatedCatRequest : ", aRepo.cardbookServerCreatedCatRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCreatedCardRequest : ", aRepo.cardbookServerCreatedCardRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDeletedCatRequest : ", aRepo.cardbookServerDeletedCatRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDeletedCardRequest : ", aRepo.cardbookServerDeletedCardRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookImageGetRequest : ", aRepo.cardbookImageGetRequest[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncRequest : ", aRepo.cardbookServerSyncRequest[aDirPrefId]);
			}
			return aRepo.cardbookAccessTokenRequest[aDirPrefId] +
					aRepo.cardbookRefreshTokenRequest[aDirPrefId] +
					aRepo.cardbookServerDiscoveryRequest[aDirPrefId] +
					aRepo.cardbookServerGetCardRequest[aDirPrefId] +
					aRepo.cardbookServerGetCardPhotoRequest[aDirPrefId] +
					aRepo.cardbookServerGetCardForMergeRequest[aDirPrefId] +
					aRepo.cardbookServerMultiGetRequest[aDirPrefId] +
					aRepo.cardbookServerUpdatedCatRequest[aDirPrefId] +
					aRepo.cardbookServerUpdatedCardRequest[aDirPrefId] +
					aRepo.cardbookServerUpdatedCardPhotoRequest[aDirPrefId] +
					aRepo.cardbookServerCreatedCatRequest[aDirPrefId] +
					aRepo.cardbookServerCreatedCardRequest[aDirPrefId] +
					aRepo.cardbookServerDeletedCardRequest[aDirPrefId] +
					aRepo.cardbookServerDeletedCatRequest[aDirPrefId] +
					aRepo.cardbookImageGetRequest[aDirPrefId] +
					aRepo.cardbookServerSyncRequest[aDirPrefId];
		} else {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookAccessTokenRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookAccessTokenRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookRefreshTokenRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookRefreshTokenRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDiscoveryRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDiscoveryRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardPhotoRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardPhotoRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardForMergeRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardForMergeRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerMultiGetRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerMultiGetRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCatRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCatRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCardRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCardPhotoRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardPhotoRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCreatedCatRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCatRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCreatedCardRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCardRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDeletedCatRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCatRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDeletedCardRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCardRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookImageGetRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookImageGetRequest));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncRequest : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncRequest));
			return cardbookBGUtils.sumElements(aRepo.cardbookAccessTokenRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookRefreshTokenRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDiscoveryRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardPhotoRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardForMergeRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerMultiGetRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCatRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardPhotoRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCatRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCardRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCatRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCardRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookImageGetRequest) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerSyncRequest);
		}
	},
	
	getResponse: function(aRepo, aDirPrefId, aPrefName) {
		if (aDirPrefId) {
			if (aPrefName) {
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookAccessTokenResponse : ", aRepo.cardbookAccessTokenResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookRefreshTokenResponse : ", aRepo.cardbookRefreshTokenResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDiscoveryResponse : ", aRepo.cardbookServerDiscoveryResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardResponse : ", aRepo.cardbookServerGetCardResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardPhotoResponse : ", aRepo.cardbookServerGetCardPhotoResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerGetCardForMergeResponse : ", aRepo.cardbookServerGetCardForMergeResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerMultiGetResponse : ", aRepo.cardbookServerMultiGetResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCatResponse : ", aRepo.cardbookServerUpdatedCatResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCardResponse : ", aRepo.cardbookServerUpdatedCardResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerUpdatedCardPhotoResponse : ", aRepo.cardbookServerUpdatedCardPhotoResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCreatedCatResponse : ", aRepo.cardbookServerCreatedCatResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCreatedCardResponse : ", aRepo.cardbookServerCreatedCardResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDeletedCatResponse : ", aRepo.cardbookServerDeletedCatResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerDeletedCardResponse : ", aRepo.cardbookServerDeletedCardResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookImageGetResponse : ", aRepo.cardbookImageGetResponse[aDirPrefId]);
				cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncResponse : ", aRepo.cardbookServerSyncResponse[aDirPrefId]);
			}
			return aRepo.cardbookAccessTokenResponse[aDirPrefId] +
					aRepo.cardbookRefreshTokenResponse[aDirPrefId] +
					aRepo.cardbookServerDiscoveryResponse[aDirPrefId] +
					aRepo.cardbookServerGetCardResponse[aDirPrefId] +
					aRepo.cardbookServerGetCardPhotoResponse[aDirPrefId] +
					aRepo.cardbookServerGetCardForMergeResponse[aDirPrefId] +
					aRepo.cardbookServerMultiGetResponse[aDirPrefId] +
					aRepo.cardbookServerUpdatedCatResponse[aDirPrefId] +
					aRepo.cardbookServerUpdatedCardResponse[aDirPrefId] +
					aRepo.cardbookServerUpdatedCardPhotoResponse[aDirPrefId] +
					aRepo.cardbookServerCreatedCatResponse[aDirPrefId] +
					aRepo.cardbookServerCreatedCardResponse[aDirPrefId] +
					aRepo.cardbookServerDeletedCatResponse[aDirPrefId] +
					aRepo.cardbookServerDeletedCardResponse[aDirPrefId] +
					aRepo.cardbookImageGetResponse[aDirPrefId] +
					aRepo.cardbookServerSyncResponse[aDirPrefId];
		} else {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookAccessTokenResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookAccessTokenResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookRefreshTokenResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookRefreshTokenResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDiscoveryResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDiscoveryResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardPhotoResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardPhotoResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerGetCardForMergeResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardForMergeResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerMultiGetResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerMultiGetResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCatResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCatResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCardResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerUpdatedCardPhotoResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardPhotoResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCreatedCatResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCatResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCreatedCardResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCardResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDeletedCatResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCatResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerDeletedCardResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCardResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookImageGetResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookImageGetResponse));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncResponse : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncResponse));
			return cardbookBGUtils.sumElements(aRepo.cardbookAccessTokenResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookRefreshTokenResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDiscoveryResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardPhotoResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardForMergeResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerMultiGetResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCatResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardPhotoResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCatResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCardResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCatResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCardResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookImageGetResponse) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerSyncResponse);
		}
	},
	
	getDone: function(aRepo, aDirPrefId, aPrefName) {
		if (aDirPrefId) {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCatSyncDone : ", aRepo.cardbookServerCatSyncDone[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncCompareCatWithCacheDone : ", aRepo.cardbookServerSyncCompareCatWithCacheDone[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncHandleRemainingCatDone : ", aRepo.cardbookServerSyncHandleRemainingCatDone[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCardSyncDone : ", aRepo.cardbookServerCardSyncDone[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncCompareCardWithCacheDone : ", aRepo.cardbookServerSyncCompareCardWithCacheDone[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncHandleRemainingCardDone : ", aRepo.cardbookServerSyncHandleRemainingCardDone[aDirPrefId]);
			return aRepo.cardbookServerCatSyncDone[aDirPrefId] + aRepo.cardbookServerCardSyncDone[aDirPrefId];
		} else {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCatSyncDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCatSyncDone));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncCompareCatWithCacheDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncCompareCatWithCacheDone));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncHandleRemainingCatDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncHandleRemainingCatDone));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCardSyncDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCardSyncDone));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncCompareCardWithCacheDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncCompareCardWithCacheDone));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncHandleRemainingCardDone : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncHandleRemainingCardDone));
			return cardbookBGUtils.sumElements(aRepo.cardbookServerCatSyncDone) + cardbookBGUtils.sumElements(aRepo.cardbookServerCardSyncDone);
		}
	},
	
	getTotal: function(aRepo, aDirPrefId, aPrefName) {
		if (aDirPrefId) {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCatSyncTotal : ", aRepo.cardbookServerCatSyncTotal[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncCompareCatWithCacheTotal : ", aRepo.cardbookServerSyncCompareCatWithCacheTotal[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncHandleRemainingCatTotal : ", aRepo.cardbookServerSyncHandleRemainingCatTotal[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerCardSyncTotal : ", aRepo.cardbookServerCardSyncTotal[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncCompareCardWithCacheTotal : ", aRepo.cardbookServerSyncCompareCardWithCacheTotal[aDirPrefId]);
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, aPrefName + " : debug mode : aRepo.cardbookServerSyncHandleRemainingCardTotal : ", aRepo.cardbookServerSyncHandleRemainingCardTotal[aDirPrefId]);
			return aRepo.cardbookServerCatSyncTotal[aDirPrefId] + aRepo.cardbookServerCardSyncTotal[aDirPrefId];
		} else {
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCatSyncTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCatSyncTotal));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncCompareCatWithCacheTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncCompareCatWithCacheTotal));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncHandleRemainingCatTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncHandleRemainingCatTotal));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerCardSyncTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerCardSyncTotal));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncCompareCardWithCacheTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncCompareCardWithCacheTotal));
			cardbookBGLog.updateStatusProgressInformationWithDebug1(aRepo.statusInformation, "Total : debug mode : aRepo.cardbookServerSyncHandleRemainingCardTotal : ", cardbookBGUtils.sumElements(aRepo.cardbookServerSyncHandleRemainingCardTotal));
			return cardbookBGUtils.sumElements(aRepo.cardbookServerCatSyncTotal) + cardbookBGUtils.sumElements(aRepo.cardbookServerCardSyncTotal);
		}
	},

	getError: function(aRepo, aDirPrefId) {
		if (aDirPrefId) {
			return aRepo.cardbookAccessTokenError[aDirPrefId] +
				aRepo.cardbookRefreshTokenError[aDirPrefId] +
				aRepo.cardbookServerCardSyncError[aDirPrefId] +
				aRepo.cardbookServerCatSyncError[aDirPrefId] +
				aRepo.cardbookServerCreatedCardError[aDirPrefId] +
				aRepo.cardbookServerCreatedCatError[aDirPrefId] +
				aRepo.cardbookServerDeletedCardError[aDirPrefId] +
				aRepo.cardbookServerDeletedCatError[aDirPrefId] +
				aRepo.cardbookServerGetCardError[aDirPrefId] +
				aRepo.cardbookServerGetCardPhotoError[aDirPrefId] +
				aRepo.cardbookServerGetCardForMergeError[aDirPrefId] +
				aRepo.cardbookServerMultiGetError[aDirPrefId] +
				aRepo.cardbookServerUpdatedCardError[aDirPrefId] +
				aRepo.cardbookServerUpdatedCardPhotoError[aDirPrefId] +
				aRepo.cardbookServerUpdatedCatError[aDirPrefId];
		} else {
			return cardbookBGUtils.sumElements(aRepo.cardbookAccessTokenError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookRefreshTokenError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCardSyncError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCatSyncError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCardError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerCreatedCatError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCardError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerDeletedCatError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardPhotoError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerGetCardForMergeError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerMultiGetError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCardPhotoError[aDirPrefId]) +
					cardbookBGUtils.sumElements(aRepo.cardbookServerUpdatedCatError[aDirPrefId]);
		}
	},

	getModifsPushed: function(aRepo, aDirPrefId) {
		return aRepo.cardbookServerUpdatedCardRequest[aDirPrefId] +
				aRepo.cardbookServerCreatedCardRequest[aDirPrefId] +
				aRepo.cardbookServerDeletedCardRequest[aDirPrefId];
	},

	getImageFromURI: function (aLog, aCard, aField, aUser, aDirPrefId) {
		return new Promise(async (resolve, reject) => {
			let password = "";
			if (aUser && aDirPrefId) {
				let url = cardbookBGPreferences.getUrl(aDirPrefId);
				password = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getPassword", user: aUser, url : url});
			}
			let req = new cardbookFetchParser(aUser, password);
			let request = req.fetch(aCard[aField].URI, {
				signal: AbortSignal.timeout(cardbookBGPreferences.getPref("requestsTimeout") * 1000)
			});
			request.then(async (response) => {
				if (response.ok) {
					let blob = await response.blob();
					const reader = new FileReader() ;
					try {
						reader.onload = async function() {
							let contentType = response.headers.get("Content-Type");
							let base64 = this.result.replace(`data:${contentType};base64,`, "");
							let extension = contentType.toLowerCase().replace("image/", "");
							let name = cardbookBGPreferences.getName(aCard.dirPrefId)
							await cardbookIDBImage.openImageDB();
							await cardbookIDBImage.addImage(aLog, "photo", name,
								{cbid: aCard.dirPrefId+"::"+aCard.uid, dirPrefId: aCard.dirPrefId, extension: extension, content: base64},
								aCard.fn);
							resolve([base64, extension]);
							};
						reader.readAsDataURL(blob) ;
					} catch(e) {
						console.log(response);
						resolve(["", ""]);
					}
				} else {
					resolve(["", ""]);
				}
			}).catch(async (response) => {
				console.log(response);
				resolve(["", ""]);
			});
		});
	},
};
