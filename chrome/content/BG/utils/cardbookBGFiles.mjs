import { cardbookBGPreferences } from "./cardbookBGPreferences.mjs";
import { cardbookCardParser } from "./cardbookCardParser.mjs";
import { cardbookBGLog } from "./cardbookBGLog.mjs";
import { cardbookBGUtils } from "./cardbookBGUtils.mjs";
import { cardbookHTMLUtils } from "../../HTML/utils/scripts/cardbookHTMLUtils.mjs";

export var cardbookBGFiles = {
	getCardsFromDir: async function (aRepo, aDirUrl, aDirPrefId) {
        let files = await messenger.NotifyTools.notifyExperiment({query: "cardbook.getFilesFromDir", url: aDirUrl});
		let cards = [];
		for (let file of files) {
			let newCards = await cardbookBGFiles.getCardsFromFile(aRepo, file, aDirPrefId);
			cards = cards.concat(newCards);
		}
		return cards;
	},

	getCardsFromFile: async function (aRepo, aFileUrl, aDirPrefId) {
		let content = await messenger.NotifyTools.notifyExperiment({query: "cardbook.readContentFromFile", url: aFileUrl});
		let cardResults = cardbookBGFiles.getCardsFromFileContent(aRepo, content, aFileUrl, aDirPrefId);
		return cardResults;
	},

	getCardsFromFileContent: function (aRepo, aContent, aFileUrl, aDirPrefId) {
		try {
			let cardResults = [];
            let name = cardbookBGPreferences.getName(aDirPrefId);
			if (aContent) {
				aContent = aContent.replace(/^$/, "");
				let re = /[\n\u0085\u2028\u2029]|\r\n?/;
				let fileContentArray = cardbookHTMLUtils.cleanArrayWithoutTrim(aContent.split(re));
				let cardContent = "";
				let endVcardFound = false;
				for (let line of fileContentArray) {
					if (line.toUpperCase().startsWith("BEGIN:VCARD")) {
						cardContent = line;
					} else if (line.toUpperCase().startsWith("END:VCARD")) {
						endVcardFound = true
						cardContent = cardContent + "\r\n" + line;
						try {
							let card = new cardbookCardParser(cardContent, "", "", aDirPrefId);
							if (card.version != "") {
								if (aFileUrl) {
									cardbookBGUtils.setCacheURIFromValue(card, aFileUrl);
								}
								cardResults.push(card);
							}
						}
						catch (e) {
							if (e.message == "") {
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [name, messenger.i18n.getMessage(e.code), cardContent], "Error");
							} else {
								cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [name, e.message, cardContent], "Error");
							}
						}
						cardContent = "";
					} else if (line == "") {
						continue;
					} else {
						cardContent = cardContent + "\r\n" + line;
					}
				}
				if (!endVcardFound) {
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "parsingCardError", [name, "END:VCARD not found", cardContent], "Error");
				}
			} else {
				// test to do if aFileUrl && aContent is null
				cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "fileEmpty", [aFileUrl]);
			}
			return cardResults
		}
		catch (e) {
			cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "cardbookBGFiles.getCardsFromFileContent error : " + e, "Error");
			return [];
		}
	},

};
