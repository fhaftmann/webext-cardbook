import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookBGUtils } from "../utils/cardbookBGUtils.mjs";
import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";
import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";

export var cardbookIDBCat = {
	cardbookCatDatabaseVersion: "3",
	cardbookCatDatabaseName: "CardBookCat",
	db: {},

	// generic output when errors on DB
	onerror: function(e) {
		console.log(e);
	},

	// first step for getting the categories
	openCatDB: function() {
        let openCatDB = new Promise( async function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBCat.cardbookCatDatabaseName, cardbookIDBCat.cardbookCatDatabaseVersion);
            // when version changes
            // for the moment delete all and recreate one new empty
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
				e.target.transaction.onerror = cardbookIDBCat.onerror;

                if (db.objectStoreNames.contains("categories")) {
                    db.deleteObjectStore("categories");
                }
                var store = db.createObjectStore("categories", {keyPath: "cbid", autoIncrement: false});
                store.createIndex("dirPrefIdIndex", "dirPrefId", { unique: false });
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = function(e) {
				cardbookIDBCat.db = e.target.result;
				resolve();
            };
            // when error, call the observer for starting the load cache and maybe the sync
			request.onerror = function(e) {
				cardbookIDBCat.onerror(e);
				reject();
			};
        });
		return openCatDB;
	},

	// check if the category is in a wrong encryption state
	// then decrypt the category if possible
	checkCategory: async function(aLog, aDirPrefName, aCategory) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != 'encrypted' in aCategory;
			var versionMismatched = aCategory.encryptionVersion && aCategory.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ('encrypted' in aCategory) {
					aCategory = await cardbookIDBEncryptor.decryptCategory(aCategory);
				}
				await cardbookIDBCat.addCategory(aLog, aDirPrefName, aCategory);
			} else {
				if ('encrypted' in aCategory) {
					aCategory = await cardbookIDBEncryptor.decryptCategory(aCategory);
				}
			}
			return aCategory;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aLog, "debug mode : Decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the category : " + e);
		}
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readwrite");
			var store = transaction.objectStore("categories");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptCategory(data)) : data;
				var transaction = cardbookIDBCat.db.transaction(["categories"], "readwrite");
				var store = transaction.objectStore("categories");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

	// add or override the category to the cache
	addCategory: async function(aLog, aDirPrefName, aCategory, aMode) {
        let addCategory = new Promise( async function(resolve, reject) {
			var storedCategory = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptCategory(aCategory)) : aCategory;
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readwrite");
			var store = transaction.objectStore("categories");
			var cursorRequest = store.put(storedCategory);
			cursorRequest.onsuccess = async function(e) {
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Category " + aCategory.name + " written to encrypted CatDB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Category " + aCategory.name + " written to CatDB");
				}
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				if (aMode) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
				}
				cardbookIDBCat.onerror(e);
				reject();
			};
		});
        await addCategory;
	},

	removeCatsFromAccount: async function(aLog, aDirPrefId) {
		let removeCatsFromAccount = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readwrite");
			var store = transaction.objectStore("categories");
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var cursorRequest = store.delete(keyRange);
			cursorRequest.onsuccess = async function(e) {
				let name = cardbookBGPreferences.getName(aDirPrefId)
				if (cardbookIDBEncryptor.encryptionEnabled) {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, name + " : deleted from encrypted CatDB");
				} else {
					cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, name + " : deleted from CatDB");
				}
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				cardbookIDBCat.onerror(e);
				resolve();
			};
		});
		await removeCatsFromAccount;
    },

	// delete the category
	removeCategory: function(aLog, aDirPrefName, aCategory) {
		var transaction = cardbookIDBCat.db.transaction(["categories"], "readwrite");
		var store = transaction.objectStore("categories");
		var cursorRequest = store.delete(aCategory.cbid);
		cursorRequest.onsuccess = function(e) {
			if (cardbookIDBEncryptor.encryptionEnabled) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Category " + aCategory.name + " deleted from encrypted CatDB");
			} else {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, aDirPrefName + " : debug mode : Category " + aCategory.name + " deleted from CatDB");
			}
		};
		cursorRequest.onerror = cardbookIDBCat.onerror;
	},

	// Check if a category is present in the database
	checkCatForUndoAction: function(aRepo, aMessage, aCategory, aActionId) {
		var transaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
		var store = transaction.objectStore("categories");
		var cursorRequest = store.get(aCategory.cbid);
	
		cursorRequest.onsuccess = async function(e) {
            cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, aMessage);
            cardbookBGUtils.addTagCreated(aCategory);
			var category = e.target.result;
			if (category) {
				aCategory.etag = category.etag;
			}
			await aRepo.saveCategory({}, aCategory, aActionId);
            let action = aRepo.currentAction[aActionId].actionCode;
            await messenger.runtime.sendMessage({query: "cardbook.notifyObserver", value: `cardbook.${action}`});
		};
		cursorRequest.onerror = cardbookIDBCat.onerror;
	},

	getCategories: async function(aDirPrefId) {
		let results = {};
        let getCategories = new Promise( async function(resolve, reject) {
			var keyRange = aDirPrefId ? IDBKeyRange.bound(aDirPrefId, aDirPrefId + "\uffff") : IDBKeyRange.lowerBound("0");
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
			var store = transaction.objectStore("categories");
			var cursorRequest = store.getAll(keyRange);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (let category of result) {
						category = await cardbookIDBCat.checkCategory({}, "", category);
						let dirPrefId = category.dirPrefId;
						if (cardbookBGPreferences.getEnabled(dirPrefId) && !category.deleted) {
							let name = category.name;
							let id = `${dirPrefId}::categories::${name}`;
							if (!results[dirPrefId]) {
								results[dirPrefId] = [];
							}
							results[dirPrefId].push({name, id});
						}
					}
				}
				resolve(results);
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCat.onerror(e);
				resolve(results);
			}
		});
		await getCategories;
		return results;
	},


	// once the DB is open, this is the second step for the AB
	// which use the DB caching
	loadCategories: function(aRepo, aDirPrefId, aDirPrefName) {
        let loadCategories = new Promise( async function(resolve, reject) {
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var transaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
			var store = transaction.objectStore("categories");
			var cursorRequest = store.getAll(keyRange);
			const handleCategory = async category => {
				try {
					category = await cardbookIDBCat.checkCategory(aRepo.statusInformation, aDirPrefName, category);
				}
				catch(e) {
					return;
				}
				if (!category.deleted && category.name != cardbookBGPreferences.getPref("uncategorizedCards")) {
					await aRepo.addCategoryToRepository(category, false, aDirPrefId);
					cardbookBGLog.formatStringForOutput(aRepo.statusInformation, "categoryLoadedFromCacheDB", [aDirPrefName, category.name]);
				} else {
					if (aRepo.cardbookFileCacheCategories[aDirPrefId]) {
						aRepo.cardbookFileCacheCategories[aDirPrefId][category.href] = category;
					} else {
						aRepo.cardbookFileCacheCategories[aDirPrefId] = {};
						aRepo.cardbookFileCacheCategories[aDirPrefId][category.href] = category;
					}
				}
			};
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var category of result) {
						await handleCategory(category);
					}
				}
				resolve();
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBCat.onerror(e);
				reject();
			}
		});
		return loadCategories;
	},

	encryptCategories: async function(aCallBack, aRepo) {
		var categoriesTransaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
		return aCallBack(
			cardbookIDBCat.db,
			categoriesTransaction.objectStore("categories"),
			async category => {
				try {
					await cardbookIDBCat.addCategory(aRepo.statusInformation, cardbookBGPreferences.getName(category.dirPrefId), category, "encryption");
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
				}
			},
			category => !("encrypted" in category),
			"encryption"
		);
	},

	decryptCategories: async function(aCallBack, aRepo) {
		var categoriesTransaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
		return aCallBack(
			cardbookIDBCat.db,
			categoriesTransaction.objectStore("categories"),
			async category => {
				try {
					category = await cardbookIDBEncryptor.decryptCategory(category);
					await cardbookIDBCat.addCategory(aRepo.statusInformation, cardbookBGPreferences.getName(category.dirPrefId), category, "decryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Decryption failed e : " + e, "Error");
				}
			},
			category => ("encrypted" in category),
			"decryption"
		);
	},

	upgradeCategories: async function(aCallBack, aRepo) {
		var categoriesTransaction = cardbookIDBCat.db.transaction(["categories"], "readonly");
		return aCallBack(
			cardbookIDBCat.db,
			categoriesTransaction.objectStore("categories"),
			async category => {
				try {
					category = await cardbookIDBEncryptor.decryptCard(category);
					await cardbookIDBCat.addCategory(aRepo.statusInformation, cardbookBGPreferences.getName(category.dirPrefId), category, "encryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
				}
			},
			category => ("encrypted" in category && category.encryptionVersion != cardbookIDBEncryptor.VERSION),
			"encryption"
		);
	}
};
