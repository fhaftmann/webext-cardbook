import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookIDBCard } from "./cardbookIDBCard.mjs";
import { cardbookIDBCat } from "./cardbookIDBCat.mjs";

import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";
import { cardbookBGUtils } from "../utils/cardbookBGUtils.mjs";
import { cardbookBGActions } from "../utils/cardbookBGActions.mjs";
import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";

export var cardbookIDBUndo = {
	cardbookUndoDatabaseVersion: "6",
	cardbookUndoDatabaseName: "CardBookUndo",
    db: {},

    // generic output when errors on DB
    onerror: function(e) {
		console.log(e);
    },

	// first step for getting the undos
	openUndoDB: function(aRepo) {
        let openUndoDB = new Promise( function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBUndo.cardbookUndoDatabaseName, cardbookIDBUndo.cardbookUndoDatabaseVersion);
            // when version changes
            // for the moment delete all and recreate one new empty
            request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBUndo.onerror;

                if (db.objectStoreNames.contains("cardUndos")) {
                    db.deleteObjectStore("cardUndos");
                }
                var store = db.createObjectStore("cardUndos", {keyPath: "undoId", autoIncrement: false});
            };
            request.onsuccess = function(e) {
				cardbookIDBUndo.db = e.target.result;
                aRepo.currentUndoId = Number(cardbookBGPreferences.getPref("currentUndoId"));
                resolve();
            };
            request.onerror = function(e) {
				cardbookIDBUndo.onerror(e);
                reject();
            };
        });
        return openUndoDB;
	},

	// check if the card is in a wrong encryption state
	// then decrypt the card if possible
	checkUndoItem: async function(aRepo, aItem) {
		try {
			var stateMismatched = cardbookIDBEncryptor.encryptionEnabled != "encrypted" in aItem;
			var versionMismatched = aItem.encryptionVersion && aItem.encryptionVersion != cardbookIDBEncryptor.VERSION;
			if (stateMismatched || versionMismatched) {
				if ("encrypted" in aItem) {
					aItem = await cardbookIDBEncryptor.decryptUndoItem(aItem);
				}
				await cardbookIDBUndo.addUndoItem(aRepo, aItem.undoId, aItem.undoCode, aItem.undoMessage, aItem.oldCards, aItem.newCards, aItem.oldCats, aItem.newCats, true);
			} else {
				if ("encrypted" in aItem) {
					aItem = await cardbookIDBEncryptor.decryptUndoItem(aItem);
				}
			}
			return aItem;
		}
		catch(e) {
			cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Undo decryption failed e : " + e, "Error");
			throw new Error("failed to decrypt the undo : " + e);
		}
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readwrite");
			var store = transaction.objectStore("cardUndos");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptUndoItem(data)) : data;
				var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readwrite");
				var store = transaction.objectStore("cardUndos");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

	// remove an undo action
	removeUndoItem: function(aLog, aUndoId) {
		var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readwrite");
		var store = transaction.objectStore("cardUndos");
		var keyRange = IDBKeyRange.upperBound(aUndoId);
		var cursorRequest = store.delete(keyRange);
		cursorRequest.onsuccess = function(e) {
			if (cardbookIDBEncryptor.encryptionEnabled) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : undo(s) less than " + aUndoId + " deleted from encrypted undoDB");
			} else {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : undo(s) less than " + aUndoId + " deleted from undoDB");
			}
		};
		
		cursorRequest.onerror = cardbookIDBUndo.onerror;
	},

	// add an undo action
	addUndoItem: async function(aRepo, aUndoId, aUndoCode, aUndoMessage, aOldCards, aNewCards, aOldCats, aNewCats, aExactId, aMode) {
		var undoItem = {undoId : aUndoId, undoCode : aUndoCode, undoMessage : aUndoMessage, oldCards: aOldCards, newCards: aNewCards, oldCats: aOldCats, newCats: aNewCats};
		var storedItem = cardbookIDBEncryptor.encryptionEnabled ? (await cardbookIDBEncryptor.encryptUndoItem(undoItem)) : undoItem;
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readwrite");
			var store = transaction.objectStore("cardUndos");
			if (aExactId) {
				var keyRange = IDBKeyRange.only(aUndoId);
			} else {
				var keyRange = IDBKeyRange.lowerBound(aUndoId);
			}
			try {
				var cursorDeleteRequest = store.delete(keyRange);
				cursorDeleteRequest.onsuccess = function(e) {
					if (cardbookIDBEncryptor.encryptionEnabled) {
						if (aExactId) {
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undo " + aUndoId + " deleted from encrypted undoDB");
						} else {
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undos more than " + aUndoId + " deleted from encrypted undoDB");
						}
					} else {
						if (aExactId) {
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undo " + aUndoId + " deleted from undoDB");
						} else {
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undos more than " + aUndoId + " deleted from undoDB");
						}
					}
					try {
						var cursorAddRequest = store.put(storedItem);
						cursorAddRequest.onsuccess = async function(e) {
							aRepo.currentUndoId = aUndoId;
							await cardbookBGPreferences.setPref("currentUndoId", aUndoId);
							if (cardbookIDBEncryptor.encryptionEnabled) {
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undo " + aUndoId + " written to encrypted undoDB");
							} else {
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : undo " + aUndoId + " written to undoDB");
							}
							var maxUndoChanges = cardbookBGPreferences.getPref("maxUndoChanges");
							var undoIdToDelete = aUndoId - maxUndoChanges;
							if (undoIdToDelete > 0) {
								cardbookIDBUndo.removeUndoItem(aRepo.statusInformation, undoIdToDelete);
							}
							if (aMode) {
								await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
							}
							resolve();
						};
						
						cursorAddRequest.onerror = async function(e) {
							if (aMode) {
								await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
							}
							cardbookIDBUndo.onerror(e);
						};
					} catch(e) {
						cardbookIDBUndo.onerror(e);
					}
				};

				cursorDeleteRequest.onerror = async function(e) {
					if (aMode) {
						await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: aMode});
					}
					cardbookIDBUndo.onerror(e);
				};
			} catch(e) {
				cardbookIDBUndo.onerror(e);
			}
		});
	},

	// set the menu label for the undo and redo menu entries
	getChangeLabel: async function(aRepo, aCode, aUndoId) {
        let getChangeLabel = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.bound(aUndoId, aUndoId);
			var cursorRequest = store.getAll(keyRange);

			const handleItem = async item => {
				try {
					item = await cardbookIDBUndo.checkUndoItem(aRepo, item);
				}
				catch(e) {
					return;
				}
				let label = messenger.i18n.getMessage(`${aCode}LongLabel`, [item.undoMessage]);
				resolve(label);
			};

			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result && result.length != 0) {
					for (let item of result) {
						handleItem(item);
					}
				} else {
					let label = messenger.i18n.getMessage(`${aCode}ShortLabel`);
					resolve(label);
				}
			};
			
			cursorRequest.onerror = async function(e) {
				let label = messenger.i18n.getMessage(`${aCode}ShortLabel`);
				resolve(label);
			};
        });
        return getChangeLabel;
	},

	// do the undo action
	executeUndoItem: async function(aRepo) {
        let executeUndoItem = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var keyRange = IDBKeyRange.bound(aRepo.currentUndoId, aRepo.currentUndoId);
			var cursorRequest = store.getAll(keyRange);
		
			const handleItem = async item => {
				try {
					item = await cardbookIDBUndo.checkUndoItem(aRepo, item);
				}
				catch(e) {
					return;
				}
				let topic = "undoActionDone";
				let actionId = cardbookBGActions.startAction(aRepo, topic, [item.undoMessage]);
				for (let myCatToCreate of item.oldCats) {
					let myCatToDelete = item.newCats.find(child => child.dirPrefId+"::"+child.uid == myCatToCreate.dirPrefId+"::"+myCatToCreate.uid);
					if (!myCatToDelete) {
						let message = "debug mode : executing undo " + aRepo.currentUndoId + " adding myCatToCreate.cbid : " + myCatToCreate.cbid;
						cardbookIDBCat.checkCatForUndoAction(aRepo, message, myCatToCreate, actionId);
					} else {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing undo " + aRepo.currentUndoId + " updating myCatToCreate.cbid : " + myCatToCreate.cbid);
						cardbookBGUtils.addTagUpdated(myCatToCreate);
						myCatToCreate.etag = myCatToDelete.etag;
						await aRepo.saveCategory(myCatToDelete, myCatToCreate, actionId);
					}
				}
				for (let myCardToDelete of item.newCards) {
					let myCardToCreate1 = item.oldCards.find(child => child.cbid == myCardToDelete.cbid);
					if (!myCardToCreate1) {
						let myCardToCreate2 = aRepo.cardbookDisplayCards[myCardToDelete.dirPrefId].cards.find(child => child.cbid == myCardToDelete.cbid);
						// may not exist anymore with the same id, for example for Google contacts where ids are changed
						// in this tricky case, pass
						if (myCardToCreate2) {
							if (myCardToCreate2.created === true) {
								cardbookBGUtils.addTagCreated(myCardToDelete);
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing undo " + aRepo.currentUndoId + " deleting myCardToDelete.cbid : " + myCardToDelete.cbid);
								aRepo.currentAction[actionId].totalCards++;
								await aRepo.deleteCards([myCardToDelete], actionId);
							} else {
								cardbookBGUtils.addTagDeleted(myCardToDelete);
								myCardToDelete.etag = myCardToCreate2.etag;
								cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing undo " + aRepo.currentUndoId + " deleting myCardToDelete.cbid : " + myCardToDelete.cbid);
								aRepo.currentAction[actionId].totalCards++;
								await aRepo.deleteCards([myCardToDelete], actionId);
							}
						}
					}
				}
				for (let myCardToCreate of item.oldCards) {
					let myCardToDelete = aRepo.cardbookDisplayCards[myCardToCreate.dirPrefId].cards.find(child => child.cbid == myCardToCreate.cbid);
					if (!myCardToDelete) {
						let message = "debug mode : executing undo " + aRepo.currentUndoId + " adding myCardToCreate.cbid : " + myCardToCreate.cbid;
						await cardbookIDBCard.checkCardForUndoAction(aRepo, message, myCardToCreate, actionId);
					} else {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing undo " + aRepo.currentUndoId + " updating myCardToCreate.cbid : " + myCardToCreate.cbid);
						cardbookBGUtils.addTagUpdated(myCardToCreate);
						myCardToCreate.etag = myCardToDelete.etag;
						await aRepo.saveCardFromMove(myCardToDelete, myCardToCreate, actionId, false);
					}
				}
				for (let myCatToDelete of item.newCats) {
					let myCatToCreate = item.oldCats.find(child => child.dirPrefId+"::"+child.uid == myCatToDelete.dirPrefId+"::"+myCatToDelete.uid);
					if (!myCatToCreate) {
						cardbookBGUtils.addTagCreated(myCatToDelete);
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing undo " + aRepo.currentUndoId + " deleting myCatToDelete.cbid : " + myCatToDelete.cbid);
						await aRepo.deleteCategories([myCatToDelete], actionId);
					}
				}
				aRepo.currentUndoId--;
				await cardbookBGPreferences.setPref("currentUndoId", aRepo.currentUndoId);
				resolve(actionId);
			};

			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var item of result) {
						await handleItem(item);
					}
				}
			};

			cursorRequest.onerror = async function(e) {
				console.log(e);
				reject("");
			};
        });
        return executeUndoItem;
	},

	// do the redo action
	executeRedoItem: async function(aRepo) {
        let executeRedoItem = new Promise( function(resolve, reject) {
			var transaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
			var store = transaction.objectStore("cardUndos");
			var nextUndoId = aRepo.currentUndoId;
			nextUndoId++;
			var keyRange = IDBKeyRange.bound(nextUndoId, nextUndoId);
			var cursorRequest = store.getAll(keyRange);
		
			const handleItem = async item => {
				try {
					item = await cardbookIDBUndo.checkUndoItem(aRepo, item);
				}
				catch(e) {
					return;
				}
				let topic = "redoActionDone";
				let actionId = cardbookBGActions.startAction(aRepo, topic, [item.undoMessage]);
				for (let myCatToCreate of item.newCats) {
					let myCatToDelete = item.oldCats.find(child => child.dirPrefId+"::"+child.uid == myCatToCreate.dirPrefId+"::"+myCatToCreate.uid);
					if (!myCatToDelete) {
						let message = "debug mode : executing redo " + aRepo.currentUndoId + " adding myCatToCreate.cbid : " + myCatToCreate.cbid;
						cardbookIDBCat.checkCatForUndoAction(aRepo, message, myCatToCreate, actionId);
					} else {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing redo " + aRepo.currentUndoId + " updating myCatToCreate.cbid : " + myCatToCreate.cbid);
						cardbookBGUtils.addTagUpdated(myCatToCreate);
						myCatToCreate.etag = myCatToDelete.etag;
						await aRepo.saveCategory(myCatToDelete, myCatToCreate, actionId);
					}
				}
				for (let myCardToDelete of item.oldCards) {
					let myCardToCreate1 = item.newCards.find(child => child.cbid == myCardToDelete.cbid);
					if (!myCardToCreate1) {
						let myCardToCreate2 = aRepo.cardbookDisplayCards[myCardToDelete.dirPrefId].cards.find(child => child.cbid == myCardToDelete.cbid);
						if (myCardToCreate2.created === true) {
							cardbookBGUtils.addTagCreated(myCardToDelete);
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing redo " + aRepo.currentUndoId + " deleting myCardToDelete.cbid : " + myCardToDelete.cbid);
							aRepo.currentAction[actionId].totalCards++;
							await aRepo.deleteCards([myCardToDelete], actionId);
						} else {
							cardbookBGUtils.addTagDeleted(myCardToDelete);
							myCardToDelete.etag = myCardToCreate2.etag;
							cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing redo " + aRepo.currentUndoId + " deleting myCardToDelete.cbid : " + myCardToDelete.cbid);
							aRepo.currentAction[actionId].totalCards++;
							await aRepo.deleteCards([myCardToDelete], actionId);
						}
					}
				}
				for (let myCardToCreate of item.newCards) {
					let myCardToDelete = aRepo.cardbookDisplayCards[myCardToCreate.dirPrefId].cards.find(child => child.cbid == myCardToCreate.cbid);
					if (!myCardToDelete) {
						let message = "debug mode : executing undo " + aRepo.currentUndoId + " adding myCardToCreate.cbid : " + myCardToCreate.cbid;
						await cardbookIDBCard.checkCardForUndoAction(aRepo, message, myCardToCreate, actionId);
					} else {
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing redo " + aRepo.currentUndoId + " updating myCardToCreate.cbid : " + myCardToCreate.cbid);
						cardbookBGUtils.addTagUpdated(myCardToCreate);
						myCardToCreate.etag = myCardToDelete.etag;
						await aRepo.saveCardFromMove(myCardToDelete, myCardToCreate, actionId, false);
					}
				}
				for (let myCatToDelete of item.oldCats) {
					let myCatToCreate = item.newCats.find(child => child.dirPrefId+"::"+child.uid == myCatToDelete.dirPrefId+"::"+myCatToDelete.uid);
					if (!myCatToCreate) {
						cardbookBGUtils.addTagCreated(myCatToDelete);
						cardbookBGLog.updateStatusProgressInformationWithDebug2(aRepo.statusInformation, "debug mode : executing redo " + aRepo.currentUndoId + " deleting myCatToDelete.cbid : " + myCatToDelete.cbid);
						await aRepo.deleteCategories([myCatToDelete], actionId);
					}
				}
				aRepo.currentUndoId++;
				await cardbookBGPreferences.setPref("currentUndoId", aRepo.currentUndoId);
				resolve(actionId);
			};

			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var item of result) {
						handleItem(item);
					}
				}
			};

			cursorRequest.onerror = async function(e) {
				console.log(e);
				reject("");
			};
		});
		return executeRedoItem;
	},

	encryptUndos: async function(aCallBack, aRepo) {
		var undoTransaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
		return aCallBack(
			cardbookIDBUndo.db,
			undoTransaction.objectStore("cardUndos"),
			async item => {
				try {
					await cardbookIDBUndo.addUndoItem(aRepo, item.undoId, item.undoCode, item.undoMessage, item.oldCards, item.newCards, item.oldCats, item.newCats, true, "encryption");
				}
				catch(e) {
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Encryption failed e : " + e, "Error");
				}
			},
			item => !("encrypted" in item),
			"encryption"
		);
	},

	decryptUndos: async function(aCallBack, aRepo) {
		var undoTransaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
		return aCallBack(
			cardbookIDBUndo.db,
			undoTransaction.objectStore("cardUndos"),
			async item => {
				try {
					item = await cardbookIDBEncryptor.decryptUndoItem(item);
					await cardbookIDBUndo.addUndoItem(aRepo, item.undoId, item.undoCode, item.undoMessage, item.oldCards, item.newCards, item.oldCats, item.newCats, true, "decryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "decryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Upgrade failed e : " + e, "Error");
				}
			},
			item => ("encrypted" in item),
			"decryption"
		);
	},

	upgradeUndos: async function(aCallBack, aRepo) {
		var undoTransaction = cardbookIDBUndo.db.transaction(["cardUndos"], "readonly");
		return aCallBack(
			cardbookIDBUndo.db,
			undoTransaction.objectStore("cardUndos"),
			async item => {
				try {
					item = await cardbookIDBEncryptor.decryptUndoItem(item);
					await cardbookIDBUndo.addUndoItem(aRepo, item.undoId, item.undoCode, item.undoMessage, item.oldCards, item.newCards, item.oldCats, item.newCats, true, "encryption");
				}
				catch(e) {
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoActivity", mode: "encryption"});
					cardbookBGLog.updateStatusProgressInformation(aRepo.statusInformation, "debug mode : Undo encryption failed e : " + e, "Error");
				}
			},
			item => ("encrypted" in item && item.encryptionVersion != cardbookIDBEncryptor.VERSION),
			"encryption"
		);
	}
};
