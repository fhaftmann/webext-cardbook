import { cardbookBGLog } from "../utils/cardbookBGLog.mjs";

export var cardbookIDBSearch = {
	cardbookSearchDatabaseVersion: "8",
	cardbookSearchDatabaseName: "cardbookSearch",
	doUpgrade: false,
    db: {},

    // generic output when errors on DB
    onerror: function(e) {
		console.log(e);
    },

	// first step for getting the mail popularities
	openSearchDB: function() {
        let openSearchDB = new Promise( function(resolve, reject) {
            var request = indexedDB.open(cardbookIDBSearch.cardbookSearchDatabaseName, cardbookIDBSearch.cardbookSearchDatabaseVersion);
			request.onupgradeneeded = function(e) {
                var db = e.target.result;
                e.target.transaction.onerror = cardbookIDBSearch.onerror;
                if (e.oldVersion < 1) {
                    if (db.objectStoreNames.contains("searches")) {
                        db.deleteObjectStore("searches");
                    }
                    let store = db.createObjectStore("searches", {keyPath: "dirPrefId", autoIncrement: false});
                }
            };
            // when success, call the observer for starting the load cache and maybe the sync
            request.onsuccess = async function(e) {
				cardbookIDBSearch.db = e.target.result;
                resolve();
            };
            
            // when error, call the observer for starting the load cache and maybe the sync
            request.onerror = function(e) {
				cardbookIDBSearch.onerror(e);
                reject();
            };
        });
        return openSearchDB;
	},

	clearData: async function(aData) {
		let removeData = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["searches"], "readwrite");
			var store = transaction.objectStore("searches");
			var cursorRequest = store.clear();
		
			cursorRequest.onsuccess = async function(e) {
				resolve();
			};
			cursorRequest.onerror = async function(e) {
				console.log(e)
				reject();
			};
		});
		await removeData;
	},

	loadData: async function(aData) {
		let loadData = new Promise( async function(resolve, reject) {
			let total = aData.length;
            if (total == 0) {
                resolve("OK");
                return
            }
			let current = 0;
			for (let data of aData) {
				var storedData = data;
				var transaction = cardbookIDBSearch.db.transaction(["searches"], "readwrite");
				var store = transaction.objectStore("searches");
				var cursorRequest = store.put(storedData);
				cursorRequest.onsuccess = async function(e) {
					current++;
					if (current == total) {
						resolve("OK");
					}
				};
				cursorRequest.onerror = async function(e) {
					console.log(e);
					reject("KO");
				};
			}
        });
        return loadData;
	},

	// add or override the search to the cache
	addSearch: function(aLog, aSearch) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["searches"], "readwrite");
			var store = transaction.objectStore("searches");
			var cursorRequest = store.put(aSearch);

			cursorRequest.onsuccess = function(e) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : Search " + aSearch.dirPrefId + " written to searchDB");
				resolve();
			};

			cursorRequest.onerror = function(e) {
				cardbookIDBSearch.onerror(e);
				resolve();
			};
		});
	},

	// delete the search
	removeSearch: function(aLog, aDirPrefId) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["searches"], "readwrite");
			var store = transaction.objectStore("searches");
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var cursorDelete = store.delete(keyRange);
			
			cursorDelete.onsuccess = async function(e) {
				cardbookBGLog.updateStatusProgressInformationWithDebug2(aLog, "debug mode : Search " + aDirPrefId + " deleted from searchDB");
				resolve();
			};
			cursorDelete.onerror = function(e) {
				cardbookIDBSearch.onerror(e);
				resolve();
			};
		});
	},

	// get the search
	getSearch: function(aDirPrefId) {
		return new Promise( function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["searches"], "readwrite");
			var store = transaction.objectStore("searches");
			var keyRange = IDBKeyRange.only(aDirPrefId);
			var cursorDelete = store.getAll(keyRange);
			
			cursorDelete.onsuccess = function(e) {
				resolve(e.target.result[0]);
			};
			cursorDelete.onerror = function(e) {
				cardbookIDBSearch.onerror(e);
				resolve();
			};
		});
	},

	// once the DB is open, this is the second step 
	loadSearch: function(aRepo, aDirPrefId) {
		let loadSearch = new Promise( async function(resolve, reject) {
			var transaction = cardbookIDBSearch.db.transaction(["searches"], "readonly");
			var store = transaction.objectStore("searches");
			var keyRange = IDBKeyRange.bound(aDirPrefId, aDirPrefId + '\uffff');
			var cursorRequest = store.getAll(keyRange);
			cursorRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var search of result) {
						aRepo.cardbookComplexSearch[aDirPrefId] = {}
						aRepo.cardbookComplexSearch[aDirPrefId].searchAB = search.searchAB;
						aRepo.cardbookComplexSearch[aDirPrefId].matchAll = search.matchAll;
						aRepo.cardbookComplexSearch[aDirPrefId].rules = [];
						for (let rule of search.rules) {
							if (rule.value) {
								aRepo.cardbookComplexSearch[aDirPrefId].rules.push({case: rule.case, field: rule.field, term: rule.term, value: rule.value});
							} else {
								aRepo.cardbookComplexSearch[aDirPrefId].rules.push({case: rule.case, field: rule.field, term: rule.term, value: ""});
							}
						}
					}
				}
				resolve();
			};
			cursorRequest.onerror = (e) => {
				cardbookIDBSearch.onerror(e);
				reject();
			}
		});
		return loadSearch;
	}
};
