import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";
import { cardbookIDBEncryptor } from "./cardbookIDBEncryptor.mjs";
import { cardbookIDBCard } from "./cardbookIDBCard.mjs";
import { cardbookIDBCat } from "./cardbookIDBCat.mjs";
import { cardbookIDBUndo } from "./cardbookIDBUndo.mjs";
import { cardbookIDBImage } from "./cardbookIDBImage.mjs";
import { cardbookIDBMailPop } from "./cardbookIDBMailPop.mjs";
import { cardbookIDBPrefDispName } from "./cardbookIDBPrefDispName.mjs";

export var cardbookIndexedDB = {

	// remove an account
	removeAccount: async function(aLog, aDirPrefId, aDirPrefName) {
        await cardbookIDBCard.removeCardsFromAccount(aLog, aDirPrefId, aDirPrefName);
		await cardbookIDBCat.removeCatsFromAccount(aLog, aDirPrefId);
		await cardbookIDBImage.removeImagesFromAccount(aLog, aDirPrefId, aDirPrefName);
	},

	migrateItems: async function(aDatabase, aStore, aMigrateItem, aShouldMigrateItem, aMode) {
		return new Promise( function(resolve, reject) {
			var getAllRequest = aStore.getAll();
			var toBeMigrated = [];

			getAllRequest.onsuccess = async function(e) {
				var result = e.target.result;
				if (result) {
					for (var item of result) {
						if (aShouldMigrateItem(item)) {
							toBeMigrated.push(item);
						}
					}
					await messenger.NotifyTools.notifyExperiment({query: "cardbook.fetchCryptoCount", count: toBeMigrated.length});
					let taskHandle;
					let currentTaskNumber = 0;
					let taskList = [];
					async function runTaskQueue(deadline) {
						while ((deadline.timeRemaining() > 0 || deadline.didTimeout) && taskList.length) {
							const task = taskList.shift();
							currentTaskNumber++;
							await task.handler(task.data);
						}
						if (taskList.length) {
							taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
						} else {
							taskHandle = 0;
							resolve();
						}
					}
					for (var item of toBeMigrated) {
						taskList.push({ handler: aMigrateItem, data: item });
					}
					if (!taskHandle) {
						taskHandle = requestIdleCallback(runTaskQueue, { timeout: 1000 });
					}
				}
			};
			
			getAllRequest.onerror = async (e) => {
				aDatabase.onerror(e);
				reject();
			};
		});
	},

	encryptDBs: async function(aRepo) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.initCryptoActivity", mode: "encryption"});
		await Promise.all([
			cardbookIDBCard.encryptCards(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBCat.encryptCategories(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBUndo.encryptUndos(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBImage.encryptImages(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBMailPop.encryptMailPops(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBPrefDispName.encryptPrefDispNames(cardbookIndexedDB.migrateItems, aRepo)
		]).then(async () => {
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishCryptoActivityOK", mode: aMode});
		}).catch(async () => {
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishCryptoActivity"});
		});
	},

	decryptDBs: async function(aRepo) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.initCryptoActivity", mode: "decryption"});
		await Promise.all([
			cardbookIDBCard.decryptCards(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBCat.decryptCategories(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBUndo.decryptUndos(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBImage.decryptImages(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBMailPop.decryptMailPops(cardbookIndexedDB.migrateItems, aRepo),
			cardbookIDBPrefDispName.decryptPrefDispNames(cardbookIndexedDB.migrateItems, aRepo)
		]).then(async () => {
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishCryptoActivityOK", mode: aMode});
		}).catch(async () => {
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.finishCryptoActivity"});
		});
	},

	upgradeDBs: async function(aRepo) {
		var lastValidatedVersion = cardbookBGPreferences.getPref("localDataEncryption.validatedVersion")
		if (lastValidatedVersion != cardbookIDBEncryptor.VERSION) {
			await messenger.NotifyTools.notifyExperiment({query: "cardbook.initCryptoActivity", mode: "encryption"});
			Promise.all([
				cardbookIDBCat.upgradeCategories(cardbookIndexedDB.migrateItems, aRepo),
				cardbookIDBCard.upgradeCards(cardbookIndexedDB.migrateItems, aRepo),
				cardbookIDBUndo.upgradeUndos(cardbookIndexedDB.migrateItems, aRepo),
				cardbookIDBImage.upgradeImages(cardbookIndexedDB.migrateItems, aRepo),
				cardbookIDBMailPop.upgradeMailPops(cardbookIndexedDB.migrateItems, aRepo),
				cardbookIDBPrefDispName.upgradePrefDispNames(cardbookIndexedDB.migrateItems, aRepo)
			]).then(async () => {
				 await cardbookBGPreferences.setPref("localDataEncryption.validatedVersion", String(cardbookIDBEncryptor.VERSION));
			});
		}
	}
};
