import { cardbookBGPreferences } from "../utils/cardbookBGPreferences.mjs";

export var cardbookIDBEncryptor = {
	VERSION: 2,
	encryptionPrefix: "chrome://cardbook/encryption",
	counter: "",

	IV_PREFIX_SIZE: 8,
	IV_COUNTER_SIZE: 8,

	get encryptionEnabled() {
		return cardbookBGPreferences.getPref("localDataEncryption");
	},

	encryptImage: async function (aImage) {
		var [encrypted, iv] = await cardbookIDBEncryptor.encryptString(JSON.stringify(aImage));
		return {
			cbid:      aImage.cbid,
			dirPrefId: aImage.dirPrefId,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptImage: async function (aImage) {
		var decryptedImage = JSON.parse(await cardbookIDBEncryptor.decryptString(aImage.encrypted, aImage.iv));
		return decryptedImage;
	},

	encryptMailPop: async function (aMailPop) {
		var [encrypted, iv] = await cardbookIDBEncryptor.encryptString(JSON.stringify(aMailPop));
		return {
			mailPopId:      aMailPop.mailPopId,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptPrefDispName: async function (aPrefDispName) {
		var decryptedDispName = JSON.parse(await cardbookIDBEncryptor.decryptString(aPrefDispName.encrypted, aPrefDispName.iv));
		return decryptedDispName;
	},

	encryptPrefDispName: async function (aPrefDispName) {
		var [encrypted, iv] = await cardbookIDBEncryptor.encryptString(JSON.stringify(aPrefDispName));
		return {
			prefDispNameId:      aPrefDispName.prefDispNameId,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptMailPop: async function (aMailPop) {
		var decryptedMailPop = JSON.parse(await cardbookIDBEncryptor.decryptString(aMailPop.encrypted, aMailPop.iv));
		return decryptedMailPop;
	},

	encryptCategory: async function (aCategory) {
		var [encrypted, iv] = await this.encryptString(JSON.stringify(aCategory));
		return {
			cbid:      aCategory.cbid,
			cacheuri:  aCategory.cacheuri,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptCategory: async function (aCategory) {
		var decryptedCategory = JSON.parse(await this.decryptString(aCategory.encrypted, aCategory.iv));
		return decryptedCategory;
	},

	encryptCard: async function (aCard) {
		var [encrypted, iv] = await this.encryptString(JSON.stringify(aCard));
		return {
			cbid:      aCard.cbid,
			cacheuri:  aCard.cacheuri,
			dirPrefId: aCard.dirPrefId,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptCard: async function (aCard) {
		var decryptedCard = JSON.parse(await this.decryptString(aCard.encrypted, aCard.iv));
		return decryptedCard;
	},

	encryptUndoItem: async function (aItem) {
		var [encrypted, iv] = await this.encryptString(JSON.stringify(aItem));
		return {
			undoId:    aItem.undoId,
			encrypted,
			iv,
			encryptionVersion: this.VERSION
		};
	},

	decryptUndoItem: async function (aItem) {
		return JSON.parse(await this.decryptString(aItem.encrypted, aItem.iv));
	},


	encryptString: async function (aInput) {
		var data = (new TextEncoder()).encode(aInput);
		var [encryptedData, iv] = await this.encrypt(data);
		return [
			Array.from(new Uint8Array(encryptedData), function (aChar) {
				return String.fromCharCode(aChar);
			}).join(""),
			iv
		];
	},

	decryptString: async function (aEncrypted, aIv) {
		var data = Uint8Array.from(aEncrypted.split(""), function (aChar) {
			return aChar.charCodeAt(0);
		});
		var decryptedData = await this.decrypt(data, aIv);
		return (new TextDecoder()).decode(new Uint8Array(decryptedData));
	},

	get crypto() {
		if ("undefined" !== typeof(crypto)) {
			return crypto;
		}
		return window.crypto;
	},

	init: async function () {
		var key = await this.getStoredKey(this.VERSION);
		if (!key) {
			this.resetKey(this.VERSION);
		} else {
			this.loadCount();
			let loginName = this.getLoginName(this.VERSION);
			await this.localizeLogin(loginName);
		}
	},

	resetKey: async function (version) {
		var key = await this.generateKey(version);
		var exportedKey = await this.crypto.subtle.exportKey("jwk", key);
		var iv = this.crypto.getRandomValues(new Uint8Array(this.IV_PREFIX_SIZE));
		this.counter = new Uint8Array(this.IV_COUNTER_SIZE);
		await this.saveCount();
		var loginName = this.getLoginName(this.VERSION);
		await this.storeKey(loginName, JSON.stringify({ key: exportedKey, iv: Array.from(iv) }));
	},

	getLoginName: function (version) {
		return (version ? this.encryptionPrefix + "/" + version : "cardbook");
	},

	getStoredPwd: async function (version) {
		let loginName = this.getLoginName(version);
		return await messenger.NotifyTools.notifyExperiment({query: "cardbook.getStoredPwd", loginName: loginName});
	},

	getStoredKey: async function (version) {
		try {
			var password = await this.getStoredPwd(version);
			if (password) {
				var key = JSON.parse(password);
				var iv = key.iv ? Uint8Array.from(key.iv) : null;
				key = iv ? key.key : key;
				key = await this.crypto.subtle.importKey(
					"jwk",
					key,
					{ name: version > 1 ? "AES-GCM" : "AES-CTR" },
					false,
					["encrypt", "decrypt"]
				);
				return { key, iv };
			}
		}
		catch(e) {}
		return null;
	},

	storeKey: async function (loginName, key) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.storeKey", loginName: loginName, key: key});
	},

	localizeLogin: async function (loginName) {
		await messenger.NotifyTools.notifyExperiment({query: "cardbook.localizeLogin", loginName: loginName});
	},

	generateKey: async function (version) {
		var algorithm = {
				name: version > 1 ? "AES-GCM" : "AES-CTR",
				length: 256
		};
		return this.crypto.subtle.generateKey(
			algorithm,
			true,
			["encrypt", "decrypt"]
		);
	},

	generateIV: async function (aIvA, aIvB) {
		if (!aIvB) {
			aIvB = this.incrementCount();
			await this.saveCount();
		}
		return Uint8Array.from([...aIvA, ...aIvB]);
	},

	incrementCount: function () {
		let increment = slot => {
			if (slot >= this.counter.length) {
				return true;
			}
			let result = ++this.counter[slot];
			if (result <= 255) {
				return false;
			}
			this.counter[slot] = +0;
			return increment(slot + 1);
		}
		let overflow = increment(0);
		if (overflow) {
			this.counter.fill(0);
		}
		return this.counter;
	},

	saveCount: async function() {
		await cardbookBGPreferences.setPref("localDataEncryption.counter", JSON.stringify(Array.from(this.counter)));
	},

	loadCount: function() {
		var counter = cardbookBGPreferences.getPref("localDataEncryption.counter");
		if (counter) {
			try {
				counter = JSON.parse(counter);
				if (Array.isArray(counter)) {
					this.counter = Uint8Array.from(counter);
					return;
				}
			}
			catch(e) {
			}
		}
		this.counter = new Uint8Array(this.IV_COUNTER_SIZE);
	},

	encrypt: async function (aInput) {
		var key = await this.getStoredKey(this.VERSION);
		if (!key) throw new Error("failed to get secret key for encryption");
		var algorithm, iv;
		var iv = await this.generateIV(key.iv);
		var encrypted = await this.crypto.subtle.encrypt(
			{
				name: "AES-GCM",
				iv
			},
			key.key,
			aInput
		);
		return [encrypted, Array.from(iv.slice(8, 16))];
	},

	decrypt: async function (aEncrypted, aIv) {
		var key = await this.getStoredKey(this.VERSION);
		if (!key) throw new Error("failed to get secret key for decryption");
		var algorithm;
		if (!aIv) { // backward compatibility: encrypted with an old version
			key = await this.getStoredKey(null);
			algorithm = {
				name:    "AES-CTR",
				counter: new Uint8Array(16),
				length:  128
			};
		} else {
			algorithm = {
				name: "AES-GCM",
				iv:   await this.generateIV(key.iv, aIv)
			};
		}
		try {
			var result = this.crypto.subtle.decrypt(
				algorithm,
				key.key,
				aEncrypted
			);
			return result;
		}
		catch(e) {
			throw new Error("mismatched secret key: you need to clear all encrypted local data.");
		}
	}
};
